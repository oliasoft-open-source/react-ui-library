declare module '*.less';

declare module '*.svg' {
  const content: never;
  export default content;
}
