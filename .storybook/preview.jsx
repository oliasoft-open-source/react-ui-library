import React, { useEffect, useState } from 'react';
import { Toaster } from '../src/components/toaster/toaster';
import { useDarkMode, DARK_MODE_EVENT_NAME } from 'storybook-dark-mode';
import { themes } from '@storybook/theming';
import { DocsContainer } from '@storybook/addon-docs';
import '../src/style/global.less';
import './storybook.less';

export const decorators = [
  (Story) => (
    <>
      <Story />
      <Toaster />
    </>
  ),
  (Story) => {
    const isDarkMode = useDarkMode();
    useEffect(() => {
      document.documentElement.dataset.theme = isDarkMode ? 'dark' : 'default';

      return () => {
        delete document.documentElement.dataset.theme;
      };
    }, [isDarkMode]);

    return <Story />;
  },
];

/**
 * Fixed Storybook 8 compatibility for docs container.
 * - Hooks inside `container` were causing issues.
 * - Moved state logic to a separate wrapper component.
 */
const DocsContainerWrapper = (props) => {
  const [isDark, setDark] = useState(false);

  useEffect(() => {
    const handleDarkModeChange = (state) => setDark(state);
    props.context.channel.on(DARK_MODE_EVENT_NAME, handleDarkModeChange);

    return () => {
      props.context.channel.off(DARK_MODE_EVENT_NAME, handleDarkModeChange);
    };
  }, [props.context.channel]);

  return (
    <DocsContainer {...props} theme={isDark ? themes.dark : themes.light} />
  );
};

export const parameters = {
  docs: {
    container: DocsContainerWrapper,
  },
  darkMode: {
    current: 'light',
    stylePreview: true,
  },
};

export const tags = ['autodocs'];
