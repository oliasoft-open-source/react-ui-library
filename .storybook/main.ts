module.exports = {
  stories: [
    '../src/components/**/*.mdx',
    '../src/**/*.stories.@(js|jsx|ts|tsx)',
  ],

  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-interactions',
    {
      name: 'storybook-dark-mode',
    },
    '@storybook/addon-mdx-gfm',
    '@chromatic-com/storybook',
    {
      name: '@storybook/addon-docs',
      options: {
        sourceLoaderOptions: {
          injectStoryParameters: false,
        },
      },
    },
  ],

  framework: {
    name: '@storybook/react-vite',
    options: {},
  },

  async viteFinal(
    config: Record<string, any>,
    { configType }: { configType: string },
  ) {
    if (configType === 'PRODUCTION') {
      config.base = '';
      config.build = {
        ...config.build,
        sourcemap: false,
        chunkSizeWarningLimit: 3500,
        plugins: config.build.plugins || [],
      };
      config.publicDir = false;
      config.optimizeDeps = {
        include: ['storybook-dark-mode'],
      };
    }
    return config;
  },

  docs: {
    autodocs: true,
  },

  typescript: {
    reactDocgen: 'react-docgen',
  },
};
