# 5.0.0

 - Upgrade dependencies([OW-20197](https://oliasoft.atlassian.net/browse/OW-20197))

# 4.20.12

- Bump version of @oliasoft-open-source/units ([OW-20060](https://oliasoft.atlassian.net/browse/OW-20060))

# 4.20.11

- added  `onListReorder` and `canListReorder`props to the Table component in UnitTable ([OW-20210](https://oliasoft.atlassian.net/browse/OW-20210))

# 4.20.10

- Add `active` prop to `Menu` and its trigger

# 4.20.9

- Replaced `react-keyboard-event-handler` with standard `onKeyDown` event handler ([OW-17967](https://oliasoft.atlassian.net/browse/OW-17967))

# 4.20.8

- Fix `SettingField` select shrink ([OW-20227](https://oliasoft.atlassian.net/browse/OW-20227))

# 4.20.7

- Fix `Select` options with boolean values ([OW-20208](https://oliasoft.atlassian.net/browse/OW-20208))

# 4.20.6

- Fix `groupOrder` inheritance for nested `UnitInput` ([OW-19686)](https://oliasoft.atlassian.net/browse/OW-19686))
- Standardise `groupOrder` typings

# 4.20.5

- Move `vite-tsconfig-paths` to `deveDependencies` to avoid polluting upstream images ([OW-20129)](https://oliasoft.atlassian.net/browse/OW-20129))

# 4.20.4

- Added `stickyHeader` prop to `Tree` ([OW-20019](https://oliasoft.atlassian.net/browse/OW-20019))

# 4.20.3

- Improve setting-field component to support boolean selectOption `SettingField`

# 4.20.2

- Fixed focusing in `Custom Select` ([OW-9165](https://oliasoft.atlassian.net/browse/OW-9165))

# 4.20.1

- Fixed styling of disabled `Menu` parent items ([OW-19803](https://oliasoft.atlassian.net/browse/OW-19803))

# 4.20.0

- Added `tabIndex` prop to `Button` and `Menu` and set `tabIndex='-1'` for `Menu` trigger ([OW-19585](https://oliasoft.atlassian.net/browse/OW-19585))
- Added timeout to `selectOnFocus` in `Input`
- Enable `selectOnFocus` in inputs buy default

# 4.19.7

- Added `testId` to `OptionDropdown` ([OW-19730](https://oliasoft.atlassian.net/browse/OW-19730))

# 4.19.6

- Bump version of @oliasoft-open-source/units ([OW-19695](https://oliasoft.atlassian.net/browse/OW-19695))

# 4.19.5

- Prevent tooltip sticking on predefined options menu in `UnitInput`

# 4.19.4

- Remove transparency from disabled `Menu` trigger

# 4.19.3

- Add support for conditional drag and drop in `Table` ([OW-19131](https://oliasoft.atlassian.net/browse/OW-19131))

# 4.19.2

- Update `List` drag wrapper to avoid potential hooks errors ([OW-19528](https://oliasoft.atlassian.net/browse/OW-19528))

# 4.19.1

- Add `disabled` property to Select Cell in Unit Table

# 4.19.0

- Added virtual scrolling to `Table` and `List` ([OW-17973](https://oliasoft.atlassian.net/browse/OW-17973))

# 4.18.6

- Updated table drag wrapper to avoid hooks error ([OW-19485](https://oliasoft.atlassian.net/browse/OW-19485))

# 4.18.5

- Form controls use default tooltip placement ([OW-19433](https://oliasoft.atlassian.net/browse/OW-19433))

# 4.18.3

- Bump version of `oliasoft-open-source/units` to `4.2.2` ([OW-19280](https://oliasoft.atlassian.net/browse/OW-19280))

# 4.18.2

- Added padding and borders to `SettingField`

# 4.18.1

- No wrap for settingsfield

# 4.18.0

- Added SettingField component([OW-18494](https://oliasoft.atlassian.net/browse/OW-18494))

# 4.17.1

- Added unit to `formatDisplayValue` inside cells in `UnitTable`

# 4.17.0

- Added conversions to predefined options in `UnitInput`([OW-18279](https://oliasoft.atlassian.net/browse/OW-18279))

# 4.16.1

- Add `ignoreDisabledContext` prop to `Button`
- `Dialog` close button ignores disabled context ([OW-19160](https://oliasoft.atlassian.net/browse/OW-19160))

# 4.16.0

- Replaced `react-beautiful-dnd` with `dnd-kit` in `List` and `Table` ([OW-18592](https://oliasoft.atlassian.net/browse/OW-18592))
- Refactored `Table` to avoid repeating code between draggable/non-draggable variants ([OW-9203](https://oliasoft.atlassian.net/browse/OW-9203))
- Updated `list-row.tsx` to return a single row rather than set of rows

# 4.15.10

- `UnitInput` - updated rounding in units menu([OW-17142](https://oliasoft.atlassian.net/browse/OW-17142))

# 4.15.9

- Upgrade to latest version of @oliasoft-open-source/units package

# 4.15.8

- Add `testId` prop to pagination component ([OW-18948](https://oliasoft.atlassian.net/browse/OW-18948)

# 4.15.7

- Add `selectOnFocus` prop to select `Input`, `UnitInput`, and `NumberInput` contents on focus ([OW-18715](https://oliasoft.atlassian.net/browse/OW-18715)
- Make `selectOnFocus` the default behaviour for `UnitTable` cells

# 4.15.6

- Fix E2E test failures by wrapping onClick handlers in requestAnimationFrame

# 4.15.5

- added guards (WELLDESIGN-4NN)

# 4.15.4

- Fixed a bug in `OptionDropdown` component([OW-18796](https://oliasoft.atlassian.net/browse/OW-18796))

# 4.15.3

- restore `ev.stopPropagation` in `Select`

# 4.15.2

- Fixed bugs in `Select` component([OW-17634](https://oliasoft.atlassian.net/browse/OW-17634), [OW-17359](https://oliasoft.atlassian.net/browse/OW-17359))

# 4.15.1

- Added the property 'small' to `OptionDropdown` (OW-18786)(https://oliasoft.atlassian.net/browse/OW-18786)

# 4.15.0

- Refactored `UnitInput` (OW-17917](https://oliasoft.atlassian.net/browse/OW-17917))

# 4.14.1

- Export `IMenuSection` so `Menu` sections can be properly typed externally ([OW-18599](https://oliasoft.atlassian.net/browse/OW-18599))

# 4.14.0

- Upgrade base image to Node.js v22 ([OW-18526](https://oliasoft.atlassian.net/browse/OW-18526))

# 4.13.2

- Updated type for `CheckBox` onChange([OW-18497](https://oliasoft.atlassian.net/browse/OW-18497))

# 4.13.1

- Selected style for `Menu` submenu parent items ([OW-18476](https://oliasoft.atlassian.net/browse/OW-18476))

# 4.13.0

- Upgraded `@oliasoft-open-source/units` to version `4.1.0` ([OW-17705](https://oliasoft.atlassian.net/browse/OW-17705))

# 4.12.9

- `Menu` disabled and selected item styles match `Select` ([OW-18325](https://oliasoft.atlassian.net/browse/OW-18325))

# 4.12.8

- `UnitInput` & `NumberINput` allowed infinity in placeholder

# 4.12.7

- `UnitTable`: remove unnecessary conditional logic on when to apply cosmetic rounding ([OW-18325](https://oliasoft.atlassian.net/browse/OW-18325))

# 4.12.6

- Apply column alignment to Number Input cells in `Table`

# 4.12.5

- `UnitInput`: expose the `roundDisplayValue` callback prop for overriding the default display rounding function
- `NumberInput`: change the default display rounding function to `roundByMagnitude`, except in tables (`roundToFixed`)¨
- See [OW-18318](https://oliasoft.atlassian.net/browse/OW-18318)

# 4.12.4

- Dont include public folder into package build (dist) ([OW-18269](https://oliasoft.atlassian.net/browse/OW-18269))

# 4.12.3

- Restrict font imports to latin variants only

# 4.12.2

- Show `Slider` marks as tooltips on dots instead of labels ([OW-18060](https://oliasoft.atlassian.net/browse/OW-18060))

# 4.12.1

- Exported `onChange` type for `UnitInput`

# 4.12.0

- Fixed value for unknown unit in `UnitInput`

# 4.11.2

- Allowed infinity for `NumberInput`([OW-18234](https://oliasoft.atlassian.net/browse/OW-18234))

# 4.11.1

- Added data attributes for errors and warnings in form elements
- Updated tests for `NumberInput`, `UnitInput` and `UnitTable` to check error state ([OW-17920](https://oliasoft.atlassian.net/browse/OW-17920))

# 4.11.0

- Integrate context `UnitTable`([OW-17919](https://oliasoft.atlassian.net/browse/OW-17919))

# 4.10.10

- Fixed `allowEmpty` prop in `UnitInput`

# 4.10.9

- Fix unintended invocation of `onChange` handler upon blur in `<NumberInput>` ([OW-18150](https://oliasoft.atlassian.net/browse/OW-18150))

# 4.10.8

- Moved interaction tests and other test stories into separate folders ([OW-17964](https://oliasoft.atlassian.net/browse/OW-17964))

# 4.10.7

- Improved placeholder for `UnitInput` and `NumberInput`

# 4.10.6

- Reduce logs noise in `UnitInput` ([OW-17913](https://oliasoft.atlassian.net/browse/OW-17913))

# 4.10.5

- Fix returning multiple files in `FileInput` ([OW-18086](https://oliasoft.atlassian.net/browse/OW-18086))

# 4.10.4

- remove `dependency` on Immer package because it was only used in stories

# 4.10.3

- remove `peerDependency` on Immer package because it was only used in stories

# 4.10.2

- Fix `NumberInput` "backspace trailing zero" UX bug ([OW-17109](https://oliasoft.atlassian.net/browse/OW-17109))

# 4.10.1

- Fix `NumberInput` heights in tables with wrapping cells ([OW-17899](https://oliasoft.atlassian.net/browse/OW-17899))

# 4.10.0

- Improvements to `<NumberInput>`, `<UnitInput>`, and `<UnitTable>`
  - Turn on Excel-style cosmetic rounding by default
  - Fixes some "libraries rounding noise" bugs ([OW-10614](https://oliasoft.atlassian.net/browse/OW-10614))

# 4.9.2

- fixed unit string and Infinity bugs caused by divergence from old `<PureUnitInput>` ([OW-17784](https://oliasoft.atlassian.net/browse/OW-17784))

# 4.9.1

- Fix input sizing in small variant of `UnitInput` ([OW-17800](https://oliasoft.atlassian.net/browse/OW-17800))

# 4.9.0

- `UnitInput` and `NumberInput` improvements:
  - optional Excel-style cosmetic rounding (off by default) to resolve rounding noise bugs when storage and display units are different
- Groundwork for fixing some "libraries rounding noise" bugs ([OW-10614](https://oliasoft.atlassian.net/browse/OW-10614))

# 4.8.2

- Patch `<NumberInput>` (minor regression in `<NumberInput>`)

# 4.8.1

- Refactor and simplify validation logic inside `<NumberInput>` ([OW-17709](https://oliasoft.atlassian.net/browse/OW-17709))

# 4.8.0

- Reuse `<NumberInput>` inside `<UnitInput>` ([OW-17549](https://oliasoft.atlassian.net/browse/OW-17549))

# 4.7.9

- Exported onChange type, replaced all usage

# 4.7.8

- Resolve issue where error tooltips didn't show if placeholder prop is set ([OW-17693](https://oliasoft.atlassian.net/browse/OW-17693))

# 4.7.7

- Add border to error/warning cells in table so that status is visible even if they are empty ([OW-17678](https://oliasoft.atlassian.net/browse/OW-17678))

# 4.7.6

- Integrate context `UnitInput`

# 4.7.5

- Added initial interaction tests to form components, Modal & Toast ([OW-17567](https://oliasoft.atlassian.net/browse/OW-17567))

# 4.7.4

- Upgraded `@oliasoft-open-source/units` to version `3.12.4`

# 4.7.3

- Fixed console warnings for onPointerEnterCapture, onPointerLeaveCapture ([OW-17585](https://oliasoft.atlassian.net/browse/OW-17585))

# 4.7.2

- Fixed ajv packages as peerDependencies

# 4.7.1

- Updated types for `UnitInput`

# 4.7.0

- Updated Storybook to v8 ([OW-17566](https://oliasoft.atlassian.net/browse/OW-17566))

# 4.6.19

- Updated types for `UnitInput`

# 4.6.18

- Updated `FileButton` & `FileInput` components to allow select the same file

# 4.6.17

- Added `FileButton` component

# 4.6.16

- Added `onChange` property of `unitConfig` in `UnitTable` that is called whenever the display unit changes ([OW-17475](https://oliasoft.atlassian.net/browse/OW-17475))

# 4.6.15

- Added `Popover` controlled version

# 4.6.14

- Added default maximum width for `Tooltip`

# 4.6.13

- Added story for single sort `Table`

# 4.6.12

- Added modal for smart upload ([OW-16760](https://oliasoft.atlassian.net/browse/OW-16760))
- Added story for smart upload modal

# 4.6.11

- Insert modal container at top of document instead of bottom

# 4.6.10

- Disabled `searchable` for `UnitTable` unit selects to fix double click bug ([OW-17342](https://oliasoft.atlassian.net/browse/OW-17342))

# 4.6.9

- Added `error` and `warning` props to `Table` rows ([OW-17038](https://oliasoft.atlassian.net/browse/OW-17038))

# 4.6.8

- Restore `Slider` tooltip ([OW-17285](https://oliasoft.atlassian.net/browse/OW-17285))

# 4.6.7

- Fixed maximum height for `Menu` dropdown layer to avoid overflowing window ([OW-17282](https://oliasoft.atlassian.net/browse/OW-17282))

# 4.6.6

- Added multiselect support to `Table` select cells ([OW-16709](https://oliasoft.atlassian.net/browse/OW-16709))

# 4.6.5

- `Modal`s portal into single modal container at bottom of document ([OW-17080](https://oliasoft.atlassian.net/browse/OW-17080))

# 4.6.4

- `AutoUnit` options of `UnitTable` now display correct unit label, instead of internally represented unit ([OW-17215](https://oliasoft.atlassian.net/browse/OW-17215))
- `native` property of `AutoUnit` header cell is no longer hard-coded to `true`
- `onChange` value of `UnitTable` is now converted to the expected unit when `convertBackToStorageUnit` is `true`

# 4.6.3

- Added `testId` support for AutoUnit select cells in `UnitTable` ([OW-17119](https://oliasoft.atlassian.net/browse/OW-17119))

# 4.6.2

- Removed `Link` support from `SideBar` links ([OW-17101](https://oliasoft.atlassian.net/browse/OW-17101))

# 4.6.1

- `UnitTable`: move `roundDisplayValue` callback prop from root to cell to allow different rules in different columns

# 4.6.0

- Support (optional) automatic display rounding in `NumberInput` and `UnitTable` ([OW-17060](https://oliasoft.atlassian.net/browse/OW-17060))

# 4.5.4

- Allow React Router `Link` to be used in UI Library components ([OW-9300](https://oliasoft.atlassian.net/browse/OW-9300))

# 4.5.3

- Moved TopBar item `user-select:none` to avoid disabling child text selection in modals, etc ([OW-16950](https://oliasoft.atlassian.net/browse/OW-16950))

# 4.5.2

- Updated styles for `Tree` drag and drop ([OW-16557](https://oliasoft.atlassian.net/browse/OW-16557))

# 4.5.1

- `UnitTable` bugfix: handle cosmetic rounding when switching back to storage unit ([OW-17070](https://oliasoft.atlassian.net/browse/OW-17070))

# 4.5.0

- New `UnitTable` component ([OW-15745](https://oliasoft.atlassian.net/browse/OW-15745))

# 4.4.9

- Add `allowParentReassignment` property to `Tree` component

# 4.4.8

- Add `DateInput` cell type to `Table` component

# 4.4.7

- Prevent `Table` header shrinking when content overflows container

# 4.4.6

- `PopConfirm` - added prop to disable confirm button

# 4.4.5

- Restored `onCreate` story for `Select` component ([OW-10406](https://oliasoft.atlassian.net/browse/OW-10406))

# 4.4.4

- `Message` variant with details open by default ([OW-9431](https://oliasoft.atlassian.net/browse/OW-9431))

# 4.4.3

- Removed `key` prop from `CheckBox` to fix console warnings ([OW-9425](https://oliasoft.atlassian.net/browse/OW-9425))

# 4.4.2

- Added tooltip type to `List` action

# 4.4.1

- Added disabled type to `List` action

# 4.4.0

- Updated readme file

# 4.3.2

- Enable submenus in `Action` subActions

# 4.3.1

- Removed redundant scollbar in infiniteScroll `List` ([OW-15914](https://oliasoft.atlassian.net/browse/OW-15914))

# 4.3.0

- Upgraded vite to v5 and vitest v1 ([OW-14846](https://oliasoft.atlassian.net/browse/OW-14846))

# 4.2.8

- Fixed onClick type in `TopBar` component ([OW-16030](https://oliasoft.atlassian.net/browse/OW-16030))

# 4.2.7

- Fixed name placeholder in `File input` component

# 4.2.6

- Prevent drag indicators shrinking when list titles wrap ([OW-15913](https://oliasoft.atlassian.net/browse/OW-15913))

# 4.2.5

- Added `display` prop to `Toggle` to allow full width toggles

# 4.2.4

- Implemented multi-file support in `FileInput` component([OW-15908](https://oliasoft.atlassian.net/browse/OW-15908))

# 4.2.3

- Reverted 4.2.2 changes to `Input`

# 4.2.2

- Add synchronous value update to `Input` to prevent caret jumping ([OW-15817](https://oliasoft.atlassian.net/browse/OW-15817))

# 4.2.1

- Added support not known units to `UnitInput`([OW-15658](https://oliasoft.atlassian.net/browse/OW-15658))

# 4.2.0

- Added `UnitInput`([OW-15390](https://oliasoft.atlassian.net/browse/OW-15390))

# 4.1.11

- Don't show "Value no longer available for re-selection" warning on disabled `Select` ([OW-14816](https://oliasoft.atlassian.net/browse/OW-14816))

# 4.1.10

- Allow `clearable` Multiselect and updated `selectedOptions` type to include arrays

# 4.1.9

- Improved `Modal` accessibility with focus management and keyboard event handling([OW-14458](https://oliasoft.atlassian.net/browse/OW-14458))

# 4.1.8

- Add story for table with inline units

# 4.1.7

- Show `Sidebar` tooltip when hovering anywhere inside clickable area [OW-14879](https://oliasoft.atlassian.net/browse/OW-14879)

# 4.1.6

- Placed lodash to peer dependency and used peerDeepExternal plugin [OW-14871](https://oliasoft.atlassian.net/browse/OW-14871)

# 4.1.5

- Convert `Tab` labels to title case [OW-15016](https://oliasoft.atlassian.net/browse/OW-15016)

# 4.1.4

- Allow `Toast` autoclose to be disabled
- Exported `IMessageType` and `dismissToast` from `Toast` component

# 4.1.3

- Add story for table with info icons

# 4.1.2

- Allow `Drawer` tabs to be `disabled` [OW-14878](https://oliasoft.atlassian.net/browse/OW-14878)

# 4.1.1

- Fix custom native `Select` options background in browsers that support this [OW-14931](https://oliasoft.atlassian.net/browse/OW-14931)

# 4.1.0

- Parent tree items now toggle open/close state when clicked if `onClick` is not provided [OW-14795](https://oliasoft.atlassian.net/browse/OW-14795)

# 4.0.2

- Convert `Row` spacing to string so it can be used in CSS `calc` [OW-14833](https://oliasoft.atlassian.net/browse/OW-14833)

# 4.0.1

- Fix mattermost url and changed channel name to Release bots [OW-14756](https://oliasoft.atlassian.net/browse/OW-14756)

# 4.0.0

- Rebranded `react-ui-library` with fonts and styles for Oliasoft's new brand identity [OW-13041](https://oliasoft.atlassian.net/browse/OW-13041)

# 3.15.2

- Added optional `testIds` parameter to `Pagination` component ([OW-14647](https://oliasoft.atlassian.net/browse/OW-14647))

# 3.15.1

- Allowed jsx for label in `Menu` component

# 3.15.0

- Added `icons` prop to `Tree` component for custom expand and collapse icons ([OW-14537](https://oliasoft.atlassian.net/browse/OW-14537))

# 3.14.3

- Support small and/or empty badges in list items ([OW-14438](https://oliasoft.atlassian.net/browse/OW-14438))

# 3.14.2

- `NumberInput` minor fix

# 3.14.1

- Make indented `Tree` style the default ([OW-14434](https://oliasoft.atlassian.net/browse/OW-14434))

# 3.14.0

- Added `indented` prop to `Tree` component ([OW-14392](https://oliasoft.atlassian.net/browse/OW-14392))
- Changed color of expanded tree item icon to primary color ([OW-14392](https://oliasoft.atlassian.net/browse/OW-14392))

# 3.13.16

- Use transient props in styled component `Grid` to avoid invalid attributes being passed to DOM

# 3.13.15

- Allow to show help text when disabled components(`CheckBox`, `Toggle`, `RadioButton`)([OW-14204](https://oliasoft.atlassian.net/browse/OW-14204))

# 3.13.14

- Removed default Table column header alignment

# 3.13.13

- Added vertical padding to small `Button` variant ([OW-14152](https://oliasoft.atlassian.net/browse/OW-14152))

# 3.13.12

- Use transient props in styled component `Column` to avoid invalid attributes being passed to DOM ([OW-14110](https://oliasoft.atlassian.net/browse/OW-14110))

# 3.13.11

- Fix file upload variant of `Menu` option

# 3.13.10

- Fix Tree's props types

# 3.13.9

- Migrated styled-components from v5 to v6 ([OW-13887](https://oliasoft.atlassian.net/browse/OW-13887))

# 3.13.8

- Update definition of Tree's isInitialOpen prop

# 3.13.7

- Fixed output file name ([OW-14033](https://oliasoft.atlassian.net/browse/OW-14033))

# 3.13.6

- Fix bug with empty title `Badge` displaying as dot
- Fixed bug with draggable `Table` headers missing first cell

# 3.13.5

- Rename prop name

# 3.13.4

- Fixed Slider ButtonMax onChange

# 3.13.3

- Updated storybook & addons versions

# 3.13.2

- Set default `Table` cell alignment to left

# 3.13.1

- Allow boolean as evt.target.value in checkbox

# 3.13.0

- Migrate to TypeScript ([OW-12843](https://oliasoft.atlassian.net/browse/OW-12843))

# 3.12.1

- Added `itemHasChild` and `onItemToggle` propfunctions. See storybook for a usage example ([OW-13792](https://oliasoft.atlassian.net/browse/OW-13792))

# 3.12.0

- Added `onPaste` propfunction for cell type `Input` in Table cell ([OW-13483](https://oliasoft.atlassian.net/browse/OW-13483))

# 3.11.14

- Updated storybook & addons versions([OW-13818](https://oliasoft.atlassian.net/browse/OW-13818))

# 3.11.13

- Added optional `testId` parameter to `Empty` component

# 3.11.12

- Added testId for BTNs in PopConfirm

# 3.11.11

- Added missing `Popover` props to `PopConfirm` and updated stories ([OW-13822](https://oliasoft.atlassian.net/browse/OW-13822))

# 3.11.10

- Fixed coverage report to cover all relevant files only, including untested ones

# 3.11.9

- Updated `Tree` item actions markup & style to match `List` item actions ([OW-13565](https://oliasoft.atlassian.net/browse/OW-13565))

# 3.11.8

- Allow JSX in `List` labels ([OW-13576](https://oliasoft.atlassian.net/browse/OW-13576))

# 3.11.7

- Fix missing React key bug in `Dialog` component

# 3.11.6

- Updated `text` propType on `HelpIcon` to accept JSX

# 3.11.5

- Added tooltip prop to buttons, actions & icon cells ([OW-13491](https://oliasoft.atlassian.net/browse/OW-13491))

# 3.11.4

- Allowed continue pipelines when betta fails([OW-13420](https://oliasoft.atlassian.net/browse/OW-13420))

# 3.11.3

- Extend search filter in Select-component to also search the 'details'-text ([OW-11508](https://oliasoft.atlassian.net/browse/OW-11508))

# 3.11.2

- Fixed numberInput sanitisation for bad values ([OW-13272](https://oliasoft.atlassian.net/browse/OW-13272))

# 3.11.1

- Switched icons to named variants ([OW-13110](https://oliasoft.atlassian.net/browse/OW-13110))

# 3.11.0

- `Table` handles its own scrolling and actions column is sticky ([OW-12914](https://oliasoft.atlassian.net/browse/OW-12914))
- Reduced default row height & input height ([OW-12911](https://oliasoft.atlassian.net/browse/OW-12911))
- Muted color for readonly cells ([OW-12912](https://oliasoft.atlassian.net/browse/OW-12912))
- Header cells inherit `columnAlignment` if `headerAlignments` is not set
- Width can be set directly on all cells

# 3.10.4

- Added `onClick` prop to `TextLink`

# 3.10.3

- fixed default prop and guard for missing `onClose` in `Drawer`

# 3.10.2

- Fixed `actions` in `Tree`

# 3.10.1

- Added `onOpen` prop to `Drawer`

# 3.10.0

- Added Beta release label and added rules to pipelines to run beta release when added

# 3.9.23

- Added `onClose` prop to `Drawer`

# 3.9.22

- Fixed `onListReorder` behaviour in `Tree`

# 3.9.21

- Remove background from detailed Select variant ([OW-12927](https://oliasoft.atlassian.net/browse/OW-12927))
- Fix placeholder styling for error/warning variants in dark mode

# 3.9.20

- Removed padding of list item content when content is 'null' (React components that return null instead of JSX element)

# 3.9.19

- Added support for tab badge/count in `Drawer` ([OW-12830](https://oliasoft.atlassian.net/browse/OW-12830))

# 3.9.18

- Added `toolbarComponent` prop to `RichInputText` to be able to add components in the toolbar

# 3.9.17

- Added `JSX` support for `labels` in `Menu` ([OW-12750](https://oliasoft.atlassian.net/browse/OW-12750))

# 3.9.16

- Fix dnd conflict ([OW-12724](https://oliasoft.atlassian.net/browse/OW-12724))

# 3.9.15

- Fix dnd conflict ([OW-12724](https://oliasoft.atlassian.net/browse/OW-12724))

# 3.9.14

- Added `error` and `warning` states for `Menu` ([OW-12615](https://oliasoft.atlassian.net/browse/OW-12615))

# 3.9.13

- Remove plugin for peerDependency and remove prop-types from external list

# 3.9.12

- Fixed auto add new row feature not showing when `Show all` was selected in paginations

# 3.9.11

- Fix for custom `Select` overflowing in `InputGroup`

# 3.9.10

- `toast` will now display progress bar with time before toast is deleted
- `toast` with same ID as existing toast, will result in that toast being updated and its progress reset
- Updated `toast` story "Prevent duplicates" to display the possibility of updating a toast

# 3.9.9

- Fixed bug in `RichTextInput` that rendered one step behind input

# 3.9.8

- Added support for `onChangeOpen` in `Tree`

# 3.9.7

- `groupOrder` prop for `NumberInput` in `InputGroup` ([OW-12266](https://oliasoft.atlassian.net/browse/OW-12266))

# 3.9.6

- Forward ref to `RichTextInput` to allow clear buttons, etc

# 3.9.5

- Table row hover color matches list hover color ([OW-12238](https://oliasoft.atlassian.net/browse/OW-12238))

# 3.9.4

- Added basic, transparent variants of error & success buttons

# 3.9.3

- Handle arrows btn, when range option defined([OW-11965](https://oliasoft.atlassian.net/browse/OW-11965))

# 3.9.2

- Fix multi select issue([OW-12204](https://oliasoft.atlassian.net/browse/OW-12204))

# 3.9.1

- Updated Table `columnWidths` and `fixedWidth` props to set width (not minWidth) for more predictable sizing ([OW-9298](https://oliasoft.atlassian.net/browse/OW-9298))

# 3.9.0

- Added sourcemap for building the package ([OW-12178](https://oliasoft.atlassian.net/browse/OW-12178))

# 3.8.1

- Fixed iframe stories ([OW-12156](https://oliasoft.atlassian.net/browse/OW-12156))

# 3.8.0

- Updated rc-slider to version 10.2.1 ([OW-12160](https://oliasoft.atlassian.net/browse/OW-12160))

## 3.7.0

- Upgrade Storybook to version 7 [OW-11963](https://oliasoft.atlassian.net/browse/OW-11963)

## 3.6.5

- Added support for base 64 encoded images in `Icon`

## 3.6.4

- Fixed checked disabled style for `Toggle` ([OW-12054](https://oliasoft.atlassian.net/browse/OW-12054))

## 3.6.3

- Allow boolean values in `Select` ([OW-11999](https://oliasoft.atlassian.net/browse/OW-11999))

## 3.6.2

- `NumberInput`: fix inputted values to correct format - dots/commas/spaces are formatted

## 3.6.1

- `NumberInput`: fix bug where display state was not updating when the parent value prop changes ([OW-12040](https://oliasoft.atlassian.net/browse/OW-12040))

## 3.6.0

- Switch unit test framework from Jest to Vitest ([OW-11966](https://oliasoft.atlassian.net/browse/OW-11966))

## 3.5.6

- Fix `NumberInput` bug that prevented users typing decimal values with trailing zeros. Component now returns `string`
  types instead of `number` types, better following the
  [HTML spec](<https://html.spec.whatwg.org/multipage/input.html#number-state-(type%3Dnumber)>)
  ([OW-11920](https://oliasoft.atlassian.net/browse/OW-11920))

## 3.5.5

- Update React DnD TreeView to the latest version

# 3.5.4

- NumberInput: convert comma decimal to do immediately ([OW-11887](https://oliasoft.atlassian.net/browse/OW-11887))

## 3.5.3

- Added `rowSpan` support to table cells ([OW-8325](https://oliasoft.atlassian.net/browse/OW-8325))

## 3.5.2

- Fix trimming input value in NumberInput ([OW-11814](https://oliasoft.atlassian.net/browse/OW-11814))

## 3.5.1

- Republish previous release (NPM outage problems)

## 3.5.0

- Support new `<NumberInput>` component in `<Table>` component ([OW-11791](https://oliasoft.atlassian.net/browse/OW-11791))

## 3.4.2

- Fix NumberInput validation import

## 3.4.1

- Upgrade version of units package

## 3.4.0

- Added `NumberInput` component ([OW-11667](https://oliasoft.atlassian.net/jira/software/c/projects/OW/issues/OW-11667))

## 3.3.23

- Fixed `RichTextInput` component export

## 3.3.22

- Added `RichTextInput` component ([OW-11565](https://oliasoft.atlassian.net/browse/OW-11565))

## 3.3.21

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 3.3.20

- Updated `CheckBox` layout to fix alignment in tables ([OW-11652](https://oliasoft.atlassian.net/browse/OW-11652))

## 3.3.19

- Added `helpText` prop to `Heading`

## 3.3.18

- Added `helpText` and `onClickHelp` props to `CheckBox`, `Toggle`, and `RadioButton` ([OW-11568](https://oliasoft.atlassian.net/browse/OW-11568))
- Standard `HelpIcon` component for labels, headings and form components

## 3.3.17

- Higher contrast borders for `CheckBox` and `RadioButton` ([OW-11541](https://oliasoft.atlassian.net/browse/OW-11541))

## 3.3.16

- Set default `Drawer` background to `var(--color-background-raised)` ([OW-11465](https://oliasoft.atlassian.net/browse/OW-11465))

## 3.3.15

- Fixed standalone width bug with custom `Select`

## 3.3.14

- Custom `Select` wrapper to allow width control in `InputGroup`

## 3.3.13

- Remove `delayLeave` from Tooltip to avoid it getting stuck

## 3.3.12

- Added `type` prop for `Button` to allow submit type

## 3.3.11

- reinstate `lodash` and `react-icons` as `dependencies` (due to downstream build issues in `charts-library` when
  they are specified as `peerDependencies`)

## 3.3.10

- remove dead `less-vars-to-js` package ([OW-11017](https://oliasoft.atlassian.net/browse/OW-11017))

## 3.3.9

- Don't store copy of list items in infinite scroll list state ([OW-10946](https://oliasoft.atlassian.net/browse/OW-10946))

## 3.3.8

- Cleaned up `dependencies`, `devDependencies` and `peerDependencies` to reduce unnecessary and duplicate version
  installs in parent apps ([OW-10979](https://oliasoft.atlassian.net/browse/OW-10979))

## 3.3.7

- `Drawer` can be dragged entirely closed, or dragged open from closed state ([OW-10747](https://oliasoft.atlassian.net/browse/OW-10747))

## 3.3.6

- Fixed active style for colored `Button` ([OW-10956](https://oliasoft.atlassian.net/browse/OW-10956))

## 3.3.5

- Hide `Select` value when search input is filled

## 3.3.4

- Improved OptionDropdown height styled by the prop `maxHeight` and added scroll overflow to ([OW-10674](https://oliasoft.atlassian.net/browse/OW-10674))

## 3.3.3

- Improved TopBar link readability and tightened spacing ([OW-1165](https://oliasoft.atlassian.net/browse/OW-1165))

## 3.3.2

- Move borderless `Accordion` margin from heading to content

## 3.3.1

- Added footer prop to `Message` for standard buttons layout

## 3.3.0

- `Table`: support auto-insert of new rows ([OW-10618](https://oliasoft.atlassian.net/browse/OW-10618))

## 3.2.6

- Fixed vertical centering of static `Table` cells ([OW-10219](https://oliasoft.atlassian.net/browse/OW-10219))

## 3.2.5

- Allow title override in `Menu` submenus ([OW-10637](https://oliasoft.atlassian.net/browse/OW-10637))

## 3.2.4

- Fixed border overlap in `InputGroup` ([OW-10297](https://oliasoft.atlassian.net/browse/OW-10297))

## 3.2.3

- Added `activeTab` and `setActiveTab` prop to `Drawer` to allow controlled version ([OW-10495](https://oliasoft.atlassian.net/browse/OW-10495))

## 3.2.2

- Added `setOpen` prop to `Drawer` to allow controlled version ([OW-10495](https://oliasoft.atlassian.net/browse/OW-10495))

## 3.2.1

- Allow JSX in Menu item labels and added story ([OW-10637](https://oliasoft.atlassian.net/browse/OW-10637))

## 3.2.0

- Add optional CI pipeline that automates publishing of beta releases ([OW-10003](https://oliasoft.atlassian.net/browse/OW-10003))

## 3.1.29

- Fixed bug with infinite scrolling ([OW-10531](https://oliasoft.atlassian.net/browse/OW-10531))

## 3.1.28

- Fixed bug with autoLayerWidth Select

## 3.1.27

- Roll back Jest test ([MR-660](https://gitlab.com/oliasoft-open-source/react-ui-library/-/merge_requests/660))

## 3.1.26

- Consistent styling for disabled static cells ([OW-10514](https://oliasoft.atlassian.net/browse/OW-10514))

## 3.1.25

- Updated single-line multiselect component with overflow behaviour

## 3.1.24

- Restored pointer-event for input in `Toggle`

## 3.1.23

- Restored story for automatically adding table rows

## 3.1.22

- Allow numbers for width, height & gap props
- Allow JSX in name props for tables and lists

## 3.1.21

- Default maximum height for `Menu` dropdown

## 3.1.20

- Default empty placeholder for custom `Select`

## 3.1.19

- Added guard to `document.fonts` to fix failing tests

## 3.1.18

- Updated `Select` auto width detection & updated dropdown styles for small variant

## 3.1.17

- Added `small` variant of `Slider`

## 3.1.16

- Improved ProgressBar handling of low values; simplified styles

## 3.1.15

- Fixed Toggle onChange bug when clicking on toggle itself

## 3.1.14

- Removed `Table`s `testId` for every cell.

## 3.1.13

- Pointer cursor for Table LinkCell

## 3.1.12

- Added `testId` to `Layer` component inside `Menu` and `CustomSelect` components
- Cells of `Table`s with `testId` will now have their own `testId` with structure `tableTestId-rowIndex-columnIndex`

## 3.1.11

- Resizable drawer now follows cursor position correctly

## 3.1.10

- Fixed disabled slider styles

## 3.1.9

- Added `testId` to `Dialog` and `ListSubHeading` components

## 3.1.8

Fixes to `Table` header:

- New property `actionsRight: boolean` that can be used to position actions of `Table` header to right
- Action button now have width “auto” s.t. button is rendered correctly and does not overlap other actions
- `Table` header is now displayed when one or more actions are defined, but there is no header name/label.
- Title of `Table`s with `testId` will now have its own generated `testId` with postfix `-title`
- Removed property of `Actions`: `isBlockElement` (No known usage or implementation)

Also:

- Fixed issue where `dataId` defaulted to empty `string` instead of `null`. This caused components to get `data-testid` property without any value.
  Components affected:
  - `Accordion`
  - `Icon`
  - `Column`
  - `Row`
  - `Table` cells

## 3.1.7

- Fixed duplicate data-testid for list row (Both button `a` and text container `span` had same testid, regression from [3.1.1](#3.1.1))

## 3.1.6

- Added `noStyle` property to `onRowClick` and `onRowMouseEnter` of `Table` component

## 3.1.5

- Clearer button borders in dark mode

## 3.1.4

- Switch _all_ package commands in `gitlab-ci.yml` to yarn

## 3.1.3

- Fixed Drawer overlap

## 3.1.2

- Fix table header wrapping in Firefox

## 3.1.1

- Added `testId` support for accessing content, name and details of `List` component
- Added `testId` support to `Loader` component
- Added `testId` to `List > Details` story
- Added `testId` to `Loader > Default` story

## 3.1.0

- First publish of release notes
