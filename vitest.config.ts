import { defineConfig } from 'vitest/config';
import { resolve } from 'path';

export default defineConfig({
  test: {
    globals: true,
    environment: 'jsdom',
    include: ['src/**/*.{test,spec}.{js,ts,jsx,tsx}'],
    exclude: ['node_modules', 'dist'],
    coverage: {
      all: true,
      include: ['**/src/**/*'],
      provider: 'istanbul',
      reporter: ['text', 'json', 'html'],
      reportsDirectory: './coverage',
    },
  },
  resolve: {
    alias: {
      components: resolve(__dirname, './src/components'),
      helpers: resolve(__dirname, './src/helpers'),
      hooks: resolve(__dirname, './src/hooks'),
      style: resolve(__dirname, './src/style'),
      typings: resolve(__dirname, './src/typings'),
      docs: resolve(__dirname, './src/docs'),
    },
    conditions: ['browser', 'module', 'import', 'default'],
  },
});
