import { FlatCompat } from '@eslint/eslintrc';
import js from "@eslint/js";
import ts from "@typescript-eslint/eslint-plugin";
import tsParser from "@typescript-eslint/parser";
import react from "eslint-plugin-react";
import reactHooks from "eslint-plugin-react-hooks";
import jsxA11y from "eslint-plugin-jsx-a11y";
import importPlugin from "eslint-plugin-import";
import storybook from "eslint-plugin-storybook";
import vitest from "eslint-plugin-vitest";
import vitestGlobals from "eslint-plugin-vitest-globals";
import oliasoft from "@oliasoft-open-source/eslint-config-oliasoft";
import prettier from "eslint-config-prettier";

const envConfig = {
  languageOptions: {
    ecmaVersion: 2022,
    sourceType: "module",
    parser: tsParser,
    parserOptions: {
      ecmaFeatures: {
        jsx: true,
      },
    },
    globals: {
      window: "readonly",
      document: "readonly",
      process: "readonly",
      "vitest-globals/env": "readonly",
    },
  },
  linterOptions: {
    reportUnusedDisableDirectives: true,
  },
};

const compat = new FlatCompat({
  recommendedConfig: js.configs.recommended,
});

const legacyOliasoftConfig = compat.config(oliasoft).map((cfg) => {
  const { env, extends: legacyExtends, ...rest } = cfg;
  return rest;
});

export default [
  js.configs.recommended,
  ...legacyOliasoftConfig,
  envConfig,
  {
    ignores: ["node_modules/", "public/", "dist/", "**/tree.stories.jsx"],
  },
  {
    plugins: {
      "@typescript-eslint": ts,
      react,
      "react-hooks": reactHooks,
      "jsx-a11y": jsxA11y,
      import: importPlugin,
      storybook,
      vitest,
      "vitest-globals": vitestGlobals,
    },
    settings: {
      "import/resolver": {
        typescript: {
          alwaysTryTypes: true,
          project: "./tsconfig.json",
        },
      },
      react: {
        version: "detect",
      },
    },
    rules: {
      "react/button-has-type": "off",
      "react/react-in-jsx-scope": "off",
      "react/jsx-no-useless-fragment": "warn",
      "react/jsx-filename-extension": ["warn", { extensions: [".jsx", ".tsx"] }],
      "react/jsx-props-no-spreading": "off",
      "react/self-closing-comp": ["error", {
        component: true,
        html: false
      }],
      "react/require-default-props": "off",
      "react/no-unused-prop-types": "warn",
      "react-hooks/rules-of-hooks": "error",
      "react-hooks/exhaustive-deps": "warn",
      "@typescript-eslint/no-unused-vars": "off",
      "@typescript-eslint/no-explicit-any": "off",
      "@typescript-eslint/ban-ts-comment": ["warn", { "ts-ignore": "allow-with-description" }],
      "jsx-a11y/no-static-element-interactions": "off",
      "jsx-a11y/click-events-have-key-events": "off",
      "jsx-a11y/tabindex-no-positive": "off",
      "jsx-a11y/label-has-associated-control": "off",
      "jsx-a11y/anchor-is-valid": "off",
      "import/no-extraneous-dependencies": [
        "error",
        {
          devDependencies: [
            "src/components/**/*.stories.{js,jsx,ts,tsx}",
            "*.config.{mjs,ts,js}",
            "*.docs.{mjs,ts,js}",
          ],
        },
      ],
    },
  },
  prettier,
];
