import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { createHtmlPlugin } from 'vite-plugin-html';
import { resolve } from 'path';

// https://vitejs.dev/config/

export default defineConfig({
  base: '',
  publicDir: false,
  plugins: [
    react(),
    createHtmlPlugin({
      entry: '/src/docs/start.jsx',
      template: 'src/docs/html/index.html',
    }),
  ],
  server: {
    port: 9001,
    host: true,
  },
  build: {
    minify: false,
    outDir: 'public',
    emptyOutDir: true,
    assetsDir: '',
  },
  resolve: {
    alias: {
      components: resolve(__dirname, './src/components'),
      helpers: resolve(__dirname, './src/helpers'),
      hooks: resolve(__dirname, './src/hooks'),
      style: resolve(__dirname, './src/style'),
      typings: resolve(__dirname, './src/typings'),
      docs: resolve(__dirname, './src/docs'),
    },
  },
});
