import { debounce } from 'lodash';
import { useState, useEffect } from 'react';

const getWidth = (): number =>
  window.innerWidth ||
  document.documentElement.clientWidth ||
  document.body.clientWidth;

export const useWindowWidth = (): number => {
  const [width, setWidth] = useState<number>(getWidth());

  const resizeListener = debounce(() => setWidth(getWidth()), 150);

  useEffect(() => {
    window.addEventListener('resize', resizeListener);
    return () => {
      window.removeEventListener('resize', resizeListener);
    };
  }, []);

  return width;
};
