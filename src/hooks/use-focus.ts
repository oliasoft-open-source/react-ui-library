import { useRef, MutableRefObject } from 'react';
import { TEmpty } from 'typings/common-type-definitions';

type UseFocusReturnType = [MutableRefObject<HTMLInputElement | null>, TEmpty];

export const useFocus = (): UseFocusReturnType => {
  const ref = useRef<HTMLInputElement | null>(null);
  const setFocus = () => {
    if (ref.current) {
      ref.current.focus();
    }
  };
  return [ref, setFocus];
};
