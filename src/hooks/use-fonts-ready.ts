import { useState, useEffect, useRef } from 'react';

export const useFontsReady = (): boolean => {
  const [isFontLoaded, setFontLoaded] = useState<boolean>(false);
  const isMounted = useRef(true);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    const fontIsReady = async (): Promise<void> => {
      if (!document.fonts) return;

      await document.fonts.ready.then(() => {
        if (isMounted.current) {
          setFontLoaded(true);
        }
      });
    };

    fontIsReady();
  }, []);

  return isFontLoaded;
};
