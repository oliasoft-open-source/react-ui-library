import { useEffect, DependencyList } from 'react';
import { TEmpty } from 'typings/common-type-definitions';

export const useKeyboardEvent = (
  key: string,
  fn: TEmpty,
  dependencyList: DependencyList = [],
): void => {
  useEffect(() => {
    const handler = (evt: KeyboardEvent) => {
      if (evt.key === key) {
        fn();
      }
    };

    window.addEventListener('keydown', handler);
    return () => {
      window.removeEventListener('keydown', handler);
    };
  }, dependencyList);
};
