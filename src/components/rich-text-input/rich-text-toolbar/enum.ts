export enum RichTextIcon {
  BOLD = 'bold',
  ITALIC = 'italic',
  CODE = 'code',
  UL = 'ul',
  OL = 'ol',
}
