import React, { ReactNode } from 'react';
import { useActive, useCommands } from '@remirror/react';
import 'remirror/styles/all.css';
import { Color } from 'typings/common-types';
import { Button } from '../../button/button';
import styles from './rich-text-toolbar.module.less';
import { RichTextIcon } from './enum';

export interface IRichTextToolbarProps {
  disabled?: boolean;
  toolbarComponent?: ReactNode;
}

export const RichTextToolbar = ({
  disabled = false,
  toolbarComponent = null,
}: IRichTextToolbarProps) => {
  const {
    focus,
    toggleBold,
    toggleBulletList,
    toggleCode,
    toggleItalic,
    toggleOrderedList,
  } = useCommands();

  const active = useActive() as any;

  const items = [
    {
      icon: RichTextIcon.BOLD,
      onClick: toggleBold,
      active: active.bold(),
    },
    {
      icon: RichTextIcon.ITALIC,
      onClick: toggleItalic,
      active: active.italic(),
    },
    {
      icon: RichTextIcon.CODE,
      onClick: toggleCode,
      active: active.code(),
    },
    {
      icon: RichTextIcon.UL,
      onClick: toggleBulletList,
      active: active.bulletList(),
    },
    {
      icon: RichTextIcon.OL,
      onClick: toggleOrderedList,
      active: active.orderedList(),
    },
  ];

  return (
    <div className={styles.richTextToolbarContainer}>
      <div className={styles.richTextToolbar}>
        {items.map((item, index) => (
          <Button
            key={index}
            small
            round
            basic
            icon={item.icon}
            colored={Color.MUTED}
            onClick={() => {
              item.onClick();
              focus();
            }}
            active={item.active}
            disabled={disabled}
          />
        ))}
      </div>
      {toolbarComponent}
    </div>
  );
};
