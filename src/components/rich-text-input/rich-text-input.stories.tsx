import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IRichTextInputProps, RichTextInput } from './rich-text-input';
import { Button } from '../button/button';
import { Select } from '../select/select';

export default {
  title: 'Forms/RichTextInput',
  component: RichTextInput,
  args: {},
} as Meta;

const Template: StoryFn<IRichTextInputProps> = (args) => {
  const [value, setValue] = React.useState(args.value);
  return (
    <>
      <RichTextInput
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...args}
        value={value}
        onChange={setValue}
      />
      <pre>
        <strong>Markdown output:</strong>
      </pre>
      <pre>{value}</pre>
    </>
  );
};
export const Default = Template.bind({});

export const Filled = Template.bind({});
Filled.args = {
  value:
    'Paragraph goes here lorem ipsum dolor.\n1. Item (*italic*)\n2. Item (**bold**)\n3. Item (`code`)\n* Item\n* Item',
};

export const Placeholder = Template.bind({});
Placeholder.args = {
  placeholder: 'Enter message...',
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const HandlingStateExternallyWithRef = () => {
  const [value, setValue] = React.useState('starting value');
  const editorRef = React.useRef(null);

  const handleClick = (ref: any) => {
    setValue('');
    ref.current.clearContent();
  };

  return (
    <>
      <Button
        label="Clear input!"
        colored="red"
        onClick={() => handleClick(editorRef)}
      />
      <RichTextInput value={value} onChange={setValue} ref={editorRef} />
      <pre>
        <strong>Markdown output:</strong>
      </pre>
      <pre>{value}</pre>
    </>
  );
};

const SelectorComponent = (
  <Select
    options={[
      {
        label: 'Aardvarks',
        value: 'termites',
      },
      {
        label: 'Kangaroos',
        value: 'grass',
      },
      {
        label: 'Koalas',
        value: 'leaves',
      },
      {
        label: 'Wombats',
        value: 'bark',
      },
    ]}
    width="auto"
    value="termites"
    small
  />
);

export const WithToolbarComponent = Template.bind({});
WithToolbarComponent.args = {
  toolbarComponent: SelectorComponent,
};
