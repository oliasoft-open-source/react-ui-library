import React, {
  forwardRef,
  ReactNode,
  useContext,
  useImperativeHandle,
} from 'react';
import {
  BoldExtension,
  BulletListExtension,
  CodeExtension,
  ItalicExtension,
  MarkdownExtension,
  OrderedListExtension,
  PlaceholderExtension,
} from 'remirror/extensions';
import { EditorComponent, Remirror, useRemirror } from '@remirror/react';
import 'remirror/styles/all.css';

import { DisabledContext } from '../../helpers/disabled-context';
import { RichTextToolbar } from './rich-text-toolbar/rich-text-toolbar';
import styles from './rich-text-input.module.less';

export interface IRichTextInputProps {
  disabled?: boolean;
  onChange?: (markdown: string) => void;
  placeholder?: string;
  value?: string;
  toolbarComponent?: ReactNode;
}

export const RichTextInput = forwardRef<any, IRichTextInputProps>(
  (
    { placeholder, onChange, value, disabled: disabledProp, toolbarComponent },
    ref,
  ) => {
    const disabledContext = useContext(DisabledContext);
    const disabled = disabledProp || disabledContext;

    const { manager, state, setState, getContext } = useRemirror<any>({
      extensions: () => [
        new BoldExtension({}),
        new BulletListExtension({}),
        new CodeExtension(),
        new ItalicExtension(),
        new MarkdownExtension({}),
        new OrderedListExtension(),
        new PlaceholderExtension({ placeholder }),
      ],
      content: value,
      selection: 'start',
      stringHandler: 'markdown',
    });

    useImperativeHandle(ref, () => getContext(), [getContext]);

    const handleChange = ({ helpers, state }: { helpers: any; state: any }) => {
      const markdown = helpers.getMarkdown(state);
      if (onChange) {
        onChange(markdown);
      }
      setState(state);
    };

    return (
      <div className={styles.richTextInput}>
        <Remirror
          manager={manager}
          state={state}
          placeholder={placeholder}
          onChange={handleChange}
          editable={!disabled}
        >
          <RichTextToolbar
            disabled={disabled}
            toolbarComponent={toolbarComponent}
          />
          <EditorComponent />
        </Remirror>
      </div>
    );
  },
);
