import React, { ReactNode } from 'react';
import styles from './top-bar.module.less';

export interface ITopBarWarningProps {
  warning: ReactNode;
}

export const Warning = ({ warning }: ITopBarWarningProps) => {
  return <div className={styles.alert}>{warning}</div>;
};
