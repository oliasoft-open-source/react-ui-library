import React from 'react';
import cx from 'classnames';
import { TriggerType } from 'typings/common-types';
import {
  TButtonElement,
  TComponentElement,
  ElementType,
  TLinkElement,
  TMenuElement,
} from 'components/top-bar/types';
import { Button } from '../../button/button';
import { Menu } from '../../menu/menu';
import { Link } from './link';
import styles from '../top-bar.module.less';

export type TTopBarElementProps = {
  element: TLinkElement | TButtonElement | TMenuElement | TComponentElement;
};

export const Element = ({ element }: TTopBarElementProps) => {
  switch (element.type) {
    case ElementType.LINK:
      return (
        <div className={styles.item}>
          <Link
            label={element.label}
            url={element.url}
            icon={element.icon}
            onClick={element.onClick}
            active={element.active}
            disabled={element.disabled}
            testId={element.testId}
            component={element.component}
          />
        </div>
      );
    case ElementType.BUTTON:
      return (
        <div className={cx(styles.item, styles.button)}>
          <Button
            label={element.label}
            colored={element.colored}
            pill
            disabled={element.disabled}
            onClick={element.onClick}
            icon={element.icon}
            testId={element.testId}
          />
        </div>
      );
    case ElementType.MENU:
      return (
        <div className={cx(styles.item, styles.menu)}>
          <Menu
            maxHeight="100%"
            menu={{
              trigger: TriggerType.COMPONENT,
              fullHeightTrigger: true,
              placement: 'bottom-start',
              component: (
                <Link
                  label={element.label}
                  icon={element.icon}
                  onClick={element.onClick}
                />
              ),
              sections: element.sections,
            }}
          />
        </div>
      );
    case ElementType.COMPONENT:
      return <div className={styles.item}>{element.component}</div>;
    default:
      return null;
  }
};
