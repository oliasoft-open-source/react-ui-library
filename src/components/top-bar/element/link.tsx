import React, { useContext, MouseEventHandler } from 'react';
import cx from 'classnames';
import { DisabledContext } from 'helpers/disabled-context';
import styles from '../top-bar.module.less';

export type TTopBarLinkProps = {
  label?: string | null;
  url?: string;
  icon?: React.ReactNode;
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  active?: boolean;
  disabled?: boolean;
  testId?: string;
  component?: React.ElementType;
};

export const Link = ({
  label,
  url,
  icon,
  onClick,
  active,
  disabled,
  testId,
  component: Component = 'a',
}: TTopBarLinkProps) => {
  const disabledContext = useContext(DisabledContext);

  const handleClick: MouseEventHandler<HTMLAnchorElement> = (event) => {
    if (disabled || disabledContext) {
      event.preventDefault();
    }
    onClick?.(event);
  };

  return (
    <Component
      className={cx(styles.link, active ? styles.active : '')}
      href={url || '#'}
      to={url}
      onClick={handleClick}
      // Note: anchor tags don't have a disabled attribute. Handle this in the click handler
      aria-disabled={disabled || disabledContext}
      data-testid={testId}
      role={onClick && !url ? 'button' : undefined}
    >
      {icon}
      <span className={styles.label}>{label}</span>
    </Component>
  );
};
