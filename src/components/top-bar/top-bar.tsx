import React from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { TTopBarContent } from 'components/top-bar/types';
import { Warning } from './warning';
import { ITopBarAppSwitcherProps, AppSwitcher } from './app-switcher';
import { ITopBarTitleProps, Title } from './title';
import { Element } from './element/element';
import styles from './top-bar.module.less';

export interface ITopBarProps {
  title?: ITopBarTitleProps;
  appSwitcher?: ITopBarAppSwitcherProps;
  content?: TTopBarContent[];
  contentRight?: TTopBarContent[];
  warning?: string;
  height?: TStringOrNumber;
  fixedPosition?: boolean;
}

export const TopBar = ({
  title,
  appSwitcher,
  content = [],
  contentRight = [],
  warning = '',
  height = 'var(--size-topbar)',
  fixedPosition = true,
}: ITopBarProps) => {
  return (
    <div>
      {warning && <Warning warning={warning} />}
      <div
        className={cx(styles.topbar, fixedPosition ? styles.fixed : '')}
        style={{ height }}
      >
        {!!appSwitcher && (
          <AppSwitcher
            icon={appSwitcher.icon}
            url={appSwitcher.url}
            onClick={appSwitcher.onClick}
          />
        )}

        <div className={styles.left}>
          {title && (
            <Title
              label={title.label}
              logo={title.logo}
              url={title.url}
              version={title.version}
              onClick={title.onClick}
            />
          )}
          {content.map((element: TTopBarContent, i: number) => {
            if (element) {
              return <Element key={i} element={element} />;
            }
            return null;
          })}
        </div>
        <div className={styles.right}>
          {contentRight.map((element: TTopBarContent, i: number) => {
            if (element) {
              return <Element key={i} element={element} />;
            }
            return null;
          })}
        </div>
      </div>
    </div>
  );
};
