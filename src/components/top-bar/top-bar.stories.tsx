import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter, Link, Route, Routes } from 'react-router-dom';
import { IconType, TriggerType } from 'typings/common-types';
import { ITopBarProps, TopBar } from './top-bar';
import { Page } from '../layout/page/page';
import { Menu } from '../menu/menu';
import { Button } from '../button/button';
import { Badge } from '../badge/badge';
import LogoSVG from '../../images/logo.svg';
import svgIcon from '../../images/icons/icon-custom.svg';
import { MenuType } from '../menu/types';

const content = [
  {
    type: 'Link',
    label: 'Link',
    onClick: () => {},
  },
  {
    type: 'Link',
    label: 'Active',
    active: true,
    onClick: () => {},
  },
  {
    type: 'Link',
    label: 'Disabled',
    disabled: true,
    onClick: () => {},
  },
  {
    type: 'Menu',
    label: 'Dropdown',
    onClick: () => {},
    sections: [
      {
        type: 'Option',
        label: 'Alpacas',
        onClick: () => {},
      },
      {
        type: 'Option',
        label: 'Llamas',
        onClick: () => {},
      },
    ],
  },
  {
    type: 'Button',
    label: 'Save',
    colored: true,
    onClick: () => {},
    testId: 'top-bar-button-save',
  },
];

const contentRight = [
  {
    type: 'Component',
    component: (
      <Badge title="3" small margin="5px">
        <Button icon={IconType.NOTIFICATION} round onClick={() => {}} />
      </Badge>
    ),
  },
  {
    type: 'Component',
    component: (
      <Menu
        menu={{
          trigger: TriggerType.COMPONENT,
          fullHeightTrigger: true,
          placement: 'bottom-end',
          component: <Button label="TU" round onClick={() => {}} />,
          sections: [
            {
              type: MenuType.HEADING,
              label: 'Test User',
            },
            {
              type: MenuType.OPTION,
              label: 'Profile',
              icon: 'user',
              onClick: () => {},
            },
            { type: MenuType.DIVIDER },
            {
              type: MenuType.OPTION,
              label: 'Sign out',
              icon: 'lock',
              onClick: () => {},
            },
          ],
        }}
      />
    ),
  },
];

export default {
  title: 'Navigation/TopBar',
  component: TopBar,
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  argTypes: {
    height: { control: 'text' },
  },
  args: {
    title: {
      logo: <img src={LogoSVG} alt="logo" />,
      onClick: () => {},
    },
    content,
    contentRight,
  },
} as Meta;

const Template: StoryFn<ITopBarProps> = (args) => <TopBar {...args} />;

export const Default = Template.bind({});

export const WithWarning = Template.bind({});
WithWarning.args = {
  warning: 'Application update available. Please refresh your browser.',
};

export const TextBranding = Template.bind({});
TextBranding.args = {
  title: {
    label: 'Text Branding',
    onClick: () => {},
  },
};

export const AppSwitcher = Template.bind({});
AppSwitcher.args = {
  appSwitcher: {
    icon: svgIcon,
    onClick: () => {},
  },
};

export const AsRouterLink = () => {
  return (
    <MemoryRouter>
      <Page left={0}>
        <TopBar
          content={[
            {
              type: 'Link',
              label: 'Dashboard',
              component: Link,
              url: '/dashboard',
            },
            {
              type: 'Link',
              label: 'Settings',
              component: Link,
              url: '/settings',
            },
          ]}
        />
        <Routes>
          <Route path="/dashboard" element={<div>Dashboard</div>} />
          <Route path="/settings" element={<div>Settings</div>} />
        </Routes>
      </Page>
    </MemoryRouter>
  );
};
