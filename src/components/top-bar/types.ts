import { ReactNode } from 'react';
import { TMouseEventHandler } from 'typings/common-type-definitions';

export enum ElementType {
  LINK = 'Link',
  BUTTON = 'Button',
  MENU = 'Menu',
  COMPONENT = 'Component',
}

export type TBaseElementProps = {
  label?: string | null;
  icon?: ReactNode;
  onClick?: TMouseEventHandler;
  testId?: string;
};

export type TLinkElement = TBaseElementProps & {
  type: 'Link';
  url?: string;
  active?: boolean;
  disabled?: boolean;
  component?: React.ElementType;
};

export type TButtonElement = TBaseElementProps & {
  type: 'Button';
  colored?: boolean;
  disabled?: boolean;
};

export type TMenuElement = TBaseElementProps & {
  type: 'Menu';
  sections?: any[] | ReactNode;
};

export type TComponentElement = TBaseElementProps & {
  type: 'Component';
  component?: ReactNode;
};

export type TTopBarContent =
  | TLinkElement
  | TButtonElement
  | TMenuElement
  | TComponentElement;
