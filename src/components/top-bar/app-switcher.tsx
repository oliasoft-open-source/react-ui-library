import React, { MouseEventHandler } from 'react';
import styles from './top-bar.module.less';
import { Icon } from '../icon/icon';

export interface ITopBarAppSwitcherProps {
  icon?: React.ReactNode;
  url?: string;
  onClick?: MouseEventHandler<HTMLDivElement | HTMLAnchorElement>;
}

export const AppSwitcher = ({
  icon,
  url,
  onClick,
}: ITopBarAppSwitcherProps) => {
  const content = <Icon icon={icon} />;
  return url ? (
    <a href={url} onClick={onClick} className={styles.appSwitcher}>
      {content}
    </a>
  ) : (
    <div onClick={onClick} className={styles.appSwitcher}>
      {content}
    </div>
  );
};
