import React, { MouseEventHandler } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './top-bar.module.less';

export interface ITopBarTitleProps {
  label?: string | null;
  logo?: React.ReactNode;
  url?: string;
  version?: TStringOrNumber;
  onClick?: MouseEventHandler<HTMLDivElement | HTMLAnchorElement>;
}

export const Title = ({
  label,
  logo,
  url,
  version,
  onClick,
}: ITopBarTitleProps) => {
  const content = (
    <div className={styles.title}>
      {logo && <div className={styles.logo}>{logo}</div>}
      {label && <span className={styles.label}>{label}</span>}
      {version && <div className={styles.version}>{version}</div>}
    </div>
  );

  return (
    <div className={cx(styles.item, styles.brand)}>
      {url ? (
        <a href={url} onClick={onClick}>
          {content}
        </a>
      ) : (
        <div onClick={onClick}>{content}</div>
      )}
    </div>
  );
};
