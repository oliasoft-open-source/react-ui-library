import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IconType } from 'typings/common-types';
import { ITopBarProps, TopBar } from './top-bar';
import { Button } from '../button/button';
import { Badge } from '../badge/badge';
import LogoSVG from '../../images/logo.svg';

export default {
  title: 'Navigation/TopBar/Test Cases',
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
} as Meta;

export const OverflowingLinks: StoryFn<ITopBarProps> = () => (
  <TopBar
    title={{
      logo: <img src={LogoSVG} alt="logo" />,
      version: 'V1.2.3',
      onClick: () => {},
    }}
    content={[
      {
        type: 'Link',
        label: 'Link',
        onClick: () => {},
      },
      {
        type: 'Link',
        label:
          'Lorem ipsum dolor est compendum lorem ipsum dolor est compendum lorem ipsum dolor est (cloned 2022-06-17 01:33:42)',
        active: true,
        onClick: () => {},
      },
      {
        type: 'Link',
        label: 'Disabled',
        disabled: true,
        onClick: () => {},
      },
      {
        type: 'Button',
        label: 'Save',
        colored: true,
        onClick: () => {},
        testId: 'top-bar-button-save',
      },
    ]}
    contentRight={[
      {
        type: 'Component',
        component: (
          <Badge title="3" small margin="5px">
            <Button icon={IconType.NOTIFICATION} round onClick={() => {}} />
          </Badge>
        ),
      },
    ]}
  />
);
