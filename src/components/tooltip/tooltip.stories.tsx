import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ITooltipProps, Tooltip } from './tooltip';

export default {
  title: 'Basic/Tooltip',
  component: Tooltip,
  args: {
    text: 'Tooltip text',
    error: false,
    warning: false,
    enabled: true,
    placement: undefined,
    triggerOffset: undefined,
    fontSize: undefined,
    padding: undefined,
    maxWidth: undefined,
    display: undefined,
    children: <span>Hover over me</span>,
  },
} as Meta;

const Template: StoryFn<ITooltipProps> = (args) => (
  <Tooltip
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);
export const Default = Template.bind({});

export const Error = Template.bind({});
Error.args = { error: true };

export const Warning = Template.bind({});
Warning.args = { warning: true };

export const JSX = Template.bind({});
JSX.args = {
  text: (
    <>
      Select from options:
      <ul>
        <li>One</li>
        <li>Two</li>
      </ul>
    </>
  ),
};

export const Placement = Template.bind({});
Placement.args = { placement: 'right-end' };

export const Offset = Template.bind({});
Offset.args = { triggerOffset: 30 };

export const ForceOpen = Template.bind({});
ForceOpen.args = { forceOpen: true };

export const Display = Template.bind({});
Display.args = {
  display: 'block',
  children: <div style={{ background: 'lightgray' }}>Block element</div>,
};
Display.parameters = {
  docs: {
    source: { type: 'code' },
    description: {
      story:
        'The `Tooltip` trigger wrapper defaults to `display: inline`. If you encounter layout issues, try changing this to match its content (i.e. `display: block` for `block` elements)',
    },
  },
};
