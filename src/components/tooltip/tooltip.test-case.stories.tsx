import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ITooltipProps, Tooltip } from './tooltip';
import { Menu } from '../menu/menu';
import { menu } from '../menu/menu.stories-data';

export default {
  title: 'Basic/Tooltip/Test Cases',
  component: Tooltip,
  args: {
    text: 'Tooltip text',
  },
  tags: ['!autodocs'],
} as Meta;

const Template: StoryFn<ITooltipProps> = (args) => (
  <Tooltip
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);

export const CloseWhenMenuOpen = Template.bind({});
CloseWhenMenuOpen.args = {
  children: <Menu menu={menu} />,
  placement: 'bottom-end',
  display: 'inline-block',
};

export const TestLongTooltip = Template.bind({});
TestLongTooltip.args = {
  children: <span>Hover over me</span>,
  text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pharetra tellus eu cursus suscipit. Maecenas leo sapien, convallis ac urna ullamcorper, tempus porttitor eros. Suspendisse pellentesque odio ac metus semper, nec sagittis nisi cursus. Aenean felis libero, bibendum id accumsan id, commodo suscipit ex. Pellentesque gravida rhoncus neque, ac tincidunt libero maximus vel. In lacus nisi, malesuada vitae velit vel, laoreet porta nisl.',
};
