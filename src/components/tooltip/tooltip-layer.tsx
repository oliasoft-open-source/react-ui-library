import React, { ReactNode, CSSProperties } from 'react';
import { Arrow, ArrowProps } from 'react-laag';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './tooltip.module.less';

export interface ITooltipLayerProps {
  text?: TStringOrNumber | ReactNode;
  error?: boolean;
  warning?: boolean;
  fontSize?: TStringOrNumber;
  padding?: TStringOrNumber;
  maxWidth?: TStringOrNumber;
  layerProps?: {
    style?: CSSProperties;
    [key: string]: any;
  };
  arrowProps?: Partial<ArrowProps>;
}

export const TooltipLayer = ({
  text = '',
  error = false,
  warning = false,
  maxWidth = 'none',
  fontSize = 'inherit',
  padding = 'var(--padding-xxs) var(--padding-xs)',
  layerProps = {
    style: {
      position: 'fixed',
    },
  },
  arrowProps = {
    style: {
      top: '100%',
      bottom: undefined,
      left: '50%',
      right: undefined,
      position: 'absolute',
    },
  },
}: ITooltipLayerProps) => {
  return (
    <div
      className={cx(
        styles.tooltip,
        error ? styles.error : warning ? styles.warning : '',
      )}
      {...layerProps}
    >
      <div style={{ maxWidth, fontSize, padding }}>{text}</div>
      {/* @ts-expect-error: onPointerEnterCapture, onPointerLeaveCapture warnings otherwise */}
      <Arrow {...arrowProps} size={6} />
    </div>
  );
};
