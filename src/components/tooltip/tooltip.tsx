import React, { isValidElement, ReactNode } from 'react';
import ResizeObserver from 'resize-observer-polyfill';
import { useHover, useLayer, Placement } from 'react-laag';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { TooltipLayer } from './tooltip-layer';

export interface ITooltipProps {
  text?: TStringOrNumber | ReactNode;
  error?: boolean;
  warning?: boolean;
  placement?: Placement;
  possiblePlacements?: Placement[];
  enabled?: boolean;
  maxWidth?: TStringOrNumber;
  triggerOffset?: number;
  fontSize?: TStringOrNumber;
  padding?: TStringOrNumber;
  display?: string;
  flex?: string;
  forceOpen?: boolean;
  children?: ReactNode;
}

export const Tooltip = ({
  children,
  text = '',
  error = false,
  placement = 'top-center',
  possiblePlacements = [
    'top-center',
    'left-center',
    'bottom-center',
    'right-center',
  ],
  enabled = true,
  warning = false,
  maxWidth = '40em',
  triggerOffset = 12,
  fontSize = 'inherit',
  padding = 'var(--padding-xxs) var(--padding-xs)',
  display = 'inline',
  flex = 'none',
  forceOpen = false,
}: ITooltipProps) => {
  const visible =
    enabled &&
    (isValidElement(text) ||
      (typeof text === 'string' && text.length > 0) ||
      typeof text === 'number');

  const [isOver, hoverProps, forceClose] = useHover({
    delayEnter: 100,
    delayLeave: 0,
  });

  const isOpen = !visible ? false : forceOpen ? true : isOver;

  const { renderLayer, triggerProps, layerProps, arrowProps } = useLayer({
    isOpen,
    overflowContainer: true,
    ResizeObserver,
    auto: true,
    triggerOffset,
    placement,
    possiblePlacements,
    onParentClose: forceClose,
  });

  return (
    <>
      {isOpen &&
        renderLayer(
          <TooltipLayer
            text={text}
            error={error}
            warning={warning}
            padding={padding}
            maxWidth={maxWidth}
            fontSize={fontSize}
            layerProps={layerProps}
            arrowProps={arrowProps}
          />,
        )}
      <span {...triggerProps} {...hoverProps} style={{ display, flex }}>
        {children}
      </span>
    </>
  );
};
