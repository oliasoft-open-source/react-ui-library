import React from 'react';
import { Text } from '../text/text';

export interface ITextLinkProps {
  href?: string;
  target?: string;
  testId?: string;
  onClick?: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  children?: React.ReactNode;
  component?: React.ElementType;
}

export const TextLink = ({
  children,
  href = undefined,
  target = undefined,
  testId,
  onClick,
  component: Component = 'a',
}: ITextLinkProps) => {
  return (
    <Component
      href={href}
      to={href}
      target={target}
      onClick={onClick}
      data-testid={testId}
    >
      <Text link>{children}</Text>
    </Component>
  );
};
