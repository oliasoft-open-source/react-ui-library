import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Flex } from 'components/layout/flex/flex';
import { ITextLinkProps, TextLink } from './text-link';
import { Link, MemoryRouter, Route, Routes } from 'react-router-dom';

export default {
  title: 'Basic/TextLink',
  component: TextLink,
  args: {
    children: 'Link text goes here',
    href: 'http://oliasoft.com',
  },
} as Meta;

const Template: StoryFn<ITextLinkProps> = (args) => <TextLink {...args} />;

export const Default = Template.bind({});

export const OnClick = Template.bind({});
OnClick.args = {
  href: undefined,
  onClick: () => console.log('Clicked!'),
};

export const AsRouterLink = () => (
  <MemoryRouter>
    <Flex gap>
      <TextLink component={Link} href="/dashboard">
        Dashboard
      </TextLink>
      <TextLink component={Link} href="/settings">
        Settings
      </TextLink>
    </Flex>
    <Routes>
      <Route path="/dashboard" element={<div>Dashboard</div>} />
      <Route path="/settings" element={<div>Settings</div>} />
    </Routes>
  </MemoryRouter>
);
