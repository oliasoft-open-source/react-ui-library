import React, { Fragment, ReactElement } from 'react';
import { IMessageType, toast } from '../toaster/toaster';

export const colorsText = [
  '--color-text',
  '--color-text-muted',
  '--color-text-faint',
  '--color-text-error',
  '--color-text-warning',
  '--color-text-success',
  '--color-text-info',
  '--color-text-primary',
  '--color-text-primary-hover',
  '--color-text-primary-active',
];

export const colorsBackground = [
  '--color-background',
  '--color-background-raised',
  '--color-background-primary',
  '--color-background-info',
  '--color-background-warning',
  '--color-background-success',
  '--color-background-error',
];

export const colorsBorder = [
  '--color-border',
  '--color-border-error',
  '--color-border-warning',
  '--color-border-focus',
];

const colorVariants = [
  50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800,
  850, 900, 950,
];
export const colorsNeutral = colorVariants.map(
  (value) => `--color-neutral-${value}`,
);
export const colorsPrimary = colorVariants.map(
  (value) => `--color-primary-${value}`,
);
export const colorsError = colorVariants.map(
  (value) => `--color-error-${value}`,
);
export const colorsWarning = colorVariants.map(
  (value) => `--color-warning-${value}`,
);
export const colorsSuccess = colorVariants.map(
  (value) => `--color-success-${value}`,
);
export const colorsInfo = colorVariants.map((value) => `--color-info-${value}`);

interface IColorPaletteProps {
  colors: string[];
}

export const ColorPalette = ({ colors }: IColorPaletteProps): ReactElement => {
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: 'auto 1fr auto 1fr',
        gap: 20,
        alignItems: 'center',
      }}
    >
      {colors.map((color, index) => {
        const variable = `var(${color})`;
        const handleClick = () => {
          // TODO - consider this error (promise returned from writeText is ignored )
          navigator.clipboard.writeText(variable);
          toast({
            message: {
              type: 'Success',
              content: 'Copied to clipboard',
            } as IMessageType,
          });
        };
        return (
          <Fragment key={index}>
            <div
              style={{
                padding: 'var(--padding)',
                backgroundColor: variable,
                border: '1px solid rgba(0,0,0,0.1)',
              }}
              onClick={handleClick}
            ></div>
            {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions */}
            <code onClick={handleClick}>{variable}</code>
          </Fragment>
        );
      })}
    </div>
  );
};
