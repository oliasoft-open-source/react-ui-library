import React, { MouseEvent, ReactNode } from 'react';
import cx from 'classnames';
import {
  TEmpty,
  TStringNumberNull,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import styles from './label.module.less';
import { HelpIcon } from '../help-icon/help-icon';

export interface ILabelLockProps {
  visible?: boolean;
  active?: boolean;
  onClick?: TEmpty;
  tooltip?: string;
  testId?: string;
}

export interface ILabelIcon {
  onClick: TEmpty;
  tooltip?: string;
}

export interface ILabelProps {
  label?: TStringNumberNull | ReactNode;
  helpText?: TStringOrNumber | ReactNode;
  helpTextMaxWidth?: TStringOrNumber;
  width?: TStringOrNumber;
  info?: string;
  onClickHelp?: (event?: MouseEvent<HTMLDivElement>) => void;
  lock?: ILabelLockProps;
  libraryIcon?: ILabelIcon;
  labelLeft?: boolean;
}

export const Label = ({
  label = null,
  width = 'auto',
  helpText = '',
  helpTextMaxWidth = '300px',
  onClickHelp = undefined,
  lock = {
    visible: false,
    active: false,
    onClick: () => {},
    tooltip: '',
    testId: undefined,
  },
  info,
  libraryIcon,
  labelLeft = false,
}: ILabelProps) => {
  return (
    <div className={cx(styles.label, labelLeft ? styles.labelLeft : '')}>
      <label style={{ width: width || '' }}>
        {label}
        <div className={styles.icons}>
          {(helpText || onClickHelp) && (
            <HelpIcon
              text={helpText}
              onClick={onClickHelp}
              maxWidth={helpTextMaxWidth}
            />
          )}
          {info && <HelpIcon text={info} icon={IconType.INFO} />}
          {lock && lock.visible && lock.onClick && (
            <HelpIcon
              onClick={lock.onClick}
              icon={lock.active ? IconType.LOCK : IconType.UNLOCK}
              active={lock.active}
              testId={lock.testId}
            />
          )}
          {libraryIcon && (
            <HelpIcon
              text={libraryIcon?.tooltip ?? ''}
              onClick={libraryIcon.onClick}
              icon={IconType.LIBRARY}
            />
          )}
        </div>
      </label>
    </div>
  );
};
