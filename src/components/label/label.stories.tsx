import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ILabelProps, Label } from './label';

export default {
  title: 'Forms/Label',
  component: Label,
  args: {
    label: 'Label',
  },
} as Meta;

const Template: StoryFn<ILabelProps> = (args: ILabelProps) => (
  <Label {...args} />
);

export const Default = Template.bind({});

export const HelpIconTooltip = Template.bind({});
HelpIconTooltip.args = {
  helpText: 'Tooltip goes here',
};

export const HelpIconClickable = Template.bind({});
HelpIconClickable.args = {
  onClickHelp: () => {},
};

export const InfoIcon = Template.bind({});
InfoIcon.args = {
  info: 'Info goes here',
};

export const LockIcon: StoryFn<ILabelProps> = () => {
  const [locked, setLocked] = useState(false);
  const handleToggleLock = () => setLocked(!locked);
  return (
    <Label
      label="Label"
      lock={{
        visible: true,
        active: locked,
        onClick: handleToggleLock,
        tooltip: locked ? 'Unlock' : 'Lock',
        testId: 'testId',
      }}
    />
  );
};

export const LibraryIcon = Template.bind({});
LibraryIcon.args = {
  libraryIcon: {
    onClick: () => {},
    tooltip: 'View in library',
  },
};
