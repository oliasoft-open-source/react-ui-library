import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IconType } from 'typings/common-types';
import { HelpIcon, IHelpIconProps } from './help-icon';

export default {
  title: 'Basic/HelpIcon',
  component: HelpIcon,
} as Meta;

const Template: StoryFn<IHelpIconProps> = (args) => <HelpIcon {...args} />;

export const Default = Template.bind({});

export const Text = Template.bind({});
Text.args = {
  text: 'Tooltip goes here',
};

export const JSXText = Template.bind({});
JSXText.args = {
  text: (
    <div>
      Tooltip <em>goes</em> <strong>here</strong>
    </div>
  ),
};

export const OnClick = Template.bind({});
OnClick.args = {
  onClick: () => console.log('Help clicked'),
};

export const Library = Template.bind({});
Library.args = {
  text: 'Open in library',
  onClick: () => console.log('Library clicked'),
  icon: 'library',
  testId: 'library',
};

export const Lock = () => {
  const [locked, setLocked] = useState(false);
  return (
    <HelpIcon
      onClick={() => setLocked(!locked)}
      icon={locked ? IconType.LOCK : IconType.UNLOCK}
      active={locked}
      testId="lock"
    />
  );
};

export const TestId = Template.bind({});
TestId.args = {
  testId: 'help',
};
