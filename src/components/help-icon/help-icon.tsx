import React, { ReactNode, MouseEvent } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { Tooltip } from '../tooltip/tooltip';
import styles from './help-icon.module.less';
import { Icon } from '../icon/icon';

interface IHelpIconTooltipProps {
  children: ReactNode;
  text?: TStringOrNumber | ReactNode;
  maxWidth?: TStringOrNumber;
}

// Only show the tooltip if there is text to show
const HelpIconTooltip = ({
  children,
  text,
  maxWidth,
}: IHelpIconTooltipProps) => {
  const show = Boolean(text);
  return show ? (
    <Tooltip text={text} display="inline-flex" maxWidth={maxWidth}>
      {children}
    </Tooltip>
  ) : (
    <>{children}</>
  );
};

export interface IHelpIconProps {
  text?: TStringOrNumber | ReactNode;
  onClick?: (event?: MouseEvent<HTMLDivElement>) => void;
  icon?: ReactNode;
  active?: boolean;
  maxWidth?: TStringOrNumber;
  testId?: string;
}

export const HelpIcon = ({
  text,
  onClick,
  icon = 'help',
  active = false,
  maxWidth = '300px',
  testId,
}: IHelpIconProps) => {
  const onClickHelpIcon = (e: MouseEvent<HTMLDivElement>) => {
    if (onClick) {
      e.stopPropagation();
      onClick();
    }
  };

  return (
    <div
      className={cx(
        styles.helpIcon,
        onClick ? styles.clickable : '',
        active ? styles.active : '',
      )}
      onClick={onClickHelpIcon}
      data-testid={testId}
    >
      <HelpIconTooltip text={text} maxWidth={maxWidth}>
        <Icon icon={icon} />
      </HelpIconTooltip>
    </div>
  );
};
