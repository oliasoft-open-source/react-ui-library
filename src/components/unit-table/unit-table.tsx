import { isEqual } from 'lodash';
import React, { useEffect, useState } from 'react';
import { getUnitsForQuantity, label } from '@oliasoft-open-source/units';
import { TStringNumberNull } from 'typings/common-type-definitions';
import { usePrevious } from 'components/unit-input/hooks/use-previous';
import {
  IInputCell,
  INumberInputCell,
  IStaticCell,
  TCellShape,
} from 'components/table/cell/cell-types/cell-types.interface';
import { TRowType } from 'components/table/table.viewdata';
import { ITableProps, Table } from 'components/table/table';
import { CellType } from 'components/table/enum';
import { useUnitContext } from 'helpers/initialize-context';
import { getPreferredUnit } from 'helpers/get-preferred-unit';
import { convertUnit } from 'helpers/convert-unit/convert-unit';
import { safeToString } from 'helpers/cosmetic-rounding/cosmetic-rounding';

export interface IUnitTableOnChangeEvent
  extends React.ChangeEvent<HTMLInputElement> {
  target: EventTarget &
    HTMLInputElement & { rowIndex: number; cellIndex: number; unit: string };
}

interface IUnitTableCell {
  autoUnit: boolean;
  unitKey: string;
  formatDisplayValue?: (
    value: TStringNumberNull,
    selectedUnit: string,
  ) => TStringNumberNull;
}

interface IUnitTableNumberInputCell extends INumberInputCell, IUnitTableCell {
  onChange: (args: any) => void;
}
interface IUnitTableInputCell extends IInputCell, IUnitTableCell {
  onChange: (args: any) => void;
}

interface IUnitTableStaticCell extends IStaticCell, IUnitTableCell {}

type TUnitTableCellShape =
  | TCellShape
  | IUnitTableStaticCell
  | IUnitTableInputCell
  | IUnitTableNumberInputCell;

export interface IUnitTableRow extends Omit<TRowType, 'cells'> {
  cells: TUnitTableCellShape[];
}

interface IUnitTable extends Omit<ITableProps['table'], 'rows'> {
  rows?: IUnitTableRow[];
}

interface IUnitConfigItem {
  onChange?: (params: {
    oldUnit: string;
    newUnit: string;
    unitKey: string;
  }) => void;
  unitKey: string;
  storageUnit: string;
  preferredUnit?: string;
}

interface IUnitEntry {
  [key: string]: string;
}

export interface IUnitTableProps extends Omit<ITableProps, 'table'> {
  table: IUnitTable;
  unitConfig: IUnitConfigItem[];
  convertBackToStorageUnit?: boolean;
  enableCosmeticRounding?: boolean;
  enableDisplayRounding?: boolean;
  onListReorder?: (obj: { from: number; to: number }) => void;
  canListReorder?: (obj: { from: number; to: number }) => boolean;
}

/*
  converts the unitConfig prop into a more convenient structure for internal table use
 */
const normalizeUnits = (unitConfig: IUnitConfigItem[]) =>
  unitConfig.reduce(
    (
      { preferredUnits, storageUnits },
      { unitKey, preferredUnit, storageUnit },
    ) => {
      const { unitTemplate = null } = useUnitContext() ?? {};

      preferredUnits[unitKey] =
        preferredUnit ?? getPreferredUnit(unitKey, unitTemplate);
      storageUnits[unitKey] = storageUnit;

      return { preferredUnits, storageUnits };
    },
    { preferredUnits: {} as IUnitEntry, storageUnits: {} as IUnitEntry },
  );

/*
   convertVisibleRows overrides the view data that was set in the parent component:
    - unit header rows with type AutoUnit gets converted to display the currently active unit
    - cells with autoUnit: true get converted to currently active unit
    - all other cells and props are preserved and forward to underlying base Table component
*/

const convertVisibleRows = ({
  headers,
  rows,
  selectedUnits,
  storageUnits,
  onChangeUnit,
  convertBackToStorageUnit,
  enableCosmeticRounding,
  enableDisplayRounding,
}: {
  headers: IUnitTableProps['table']['headers'];
  rows: IUnitTableProps['table']['rows'];
  selectedUnits: IUnitEntry;
  storageUnits: IUnitEntry;
  onChangeUnit: ({
    unitKey,
    value,
  }: {
    unitKey: string;
    value: string;
  }) => void;
  convertBackToStorageUnit: boolean;
  enableCosmeticRounding: boolean;
  enableDisplayRounding: boolean;
}) => {
  const convertedHeaders = headers
    ? headers.map((headerRow) => ({
        ...headerRow,
        cells: headerRow.cells.map((headerCell: any) => {
          if (headerCell.type === CellType.AUTO_UNIT) {
            const { testId, unitKey, disabled = false } = headerCell;
            return {
              value: selectedUnits[unitKey],
              type: CellType.SELECT,
              disabled,
              searchable: false,
              options: getUnitsForQuantity(unitKey)?.map((unit) => ({
                label: label(unit),
                value: unit,
              })),
              native: headerCell.native,
              onChange: (evt: any) =>
                onChangeUnit({ unitKey, value: evt.target.value }),
              testId,
            };
          }
          return headerCell;
        }),
      }))
    : [];
  const convertedRows = rows
    ? rows.map((row, rowIndex) => ({
        ...row,
        cells: row.cells.map((cell, cellIndex) => {
          //Only override cells with autoUnit prop, and number or string values
          if (
            'autoUnit' in cell &&
            cell?.autoUnit &&
            (typeof cell?.value === 'string' || typeof cell?.value === 'number')
          ) {
            const { unitKey, value, formatDisplayValue } = cell;
            const roundDisplayValue =
              enableDisplayRounding && 'roundDisplayValue' in cell
                ? cell?.roundDisplayValue
                : null;
            const selectedUnit = selectedUnits[unitKey];
            const storageUnit = storageUnits[unitKey];
            const unitChanged = selectedUnit !== storageUnit;
            const { value: resultValue = value } = convertUnit({
              value: safeToString(value),
              unitkey: unitKey,
              toUnit: selectedUnit,
              fromUnit: storageUnit,
            });
            const convertedValue = unitChanged ? resultValue : cell.value;
            const formattedDisplayValue = formatDisplayValue
              ? formatDisplayValue(convertedValue, selectedUnit)
              : convertedValue;
            return {
              ...cell,
              value: formattedDisplayValue,
              enableCosmeticRounding,
              enableDisplayRounding,
              roundDisplayValue,
              selectOnFocus: true,
              onChange: (evt: React.ChangeEvent<HTMLInputElement>) => {
                const { value } = evt.target;
                const storageUnit = storageUnits[unitKey];
                const selectedUnit = selectedUnits[unitKey];
                const unitChanged = selectedUnit !== storageUnit;
                const nextUnit = convertBackToStorageUnit
                  ? storageUnit
                  : selectedUnit;

                const { value: resultValue = value } = convertUnit({
                  value: String(value),
                  unitkey: unitKey,
                  toUnit: nextUnit,
                  fromUnit: selectedUnit,
                });

                const nextValueConverted = unitChanged ? resultValue : value;
                if ('onChange' in cell) {
                  const { onChange } = cell;
                  const event: IUnitTableOnChangeEvent = {
                    ...evt,
                    target: {
                      ...evt.target,
                      value: String(nextValueConverted),
                      rowIndex,
                      cellIndex,
                      unit: nextUnit,
                    },
                  };
                  onChange(event);
                }
              },
            };
          }
          return cell;
        }),
      }))
    : [];
  return { convertedHeaders, convertedRows };
};

export const UnitTable = ({
  table,
  unitConfig,
  convertBackToStorageUnit = true,
  enableCosmeticRounding = true,
  enableDisplayRounding = true,
  onListReorder,
  canListReorder,
}: IUnitTableProps) => {
  const { rows, headers, ...otherProps } = table;
  const { storageUnits, preferredUnits } = normalizeUnits(unitConfig);
  const previousPreferredUnits = usePrevious(preferredUnits);

  // Local state for selected units (as set by user in table header)
  const [selectedUnits, setSelectedUnits] = useState(preferredUnits);
  const onChangeUnit = ({
    unitKey,
    value,
  }: {
    unitKey: string;
    value: string;
  }) => {
    // Update local display state
    setSelectedUnits({
      ...selectedUnits,
      [unitKey]: value,
    });

    // Call unit onChange callback
    const specificUnitConfiguration = unitConfig.find(
      (configuration) => configuration.unitKey === unitKey,
    );
    if (
      specificUnitConfiguration &&
      typeof specificUnitConfiguration.onChange === 'function'
    ) {
      specificUnitConfiguration.onChange({
        oldUnit: selectedUnits[unitKey],
        newUnit: value,
        unitKey,
      });
    }
  };

  // Override the view data (visible rows) to convert units
  const convertViewData = (selectedUnits: IUnitEntry) =>
    convertVisibleRows({
      headers,
      rows,
      selectedUnits,
      storageUnits,
      onChangeUnit,
      convertBackToStorageUnit,
      enableCosmeticRounding,
      enableDisplayRounding,
    });

  // Initialise local state copy of visible rows
  const convertedViewData = convertViewData(selectedUnits);
  const [viewData, setViewData] = useState(convertedViewData);

  // Reset the unit and cell state when the parent unit preferences changes (e.g. user switches unit template)
  useEffect(() => {
    const templateChanged = !isEqual(preferredUnits, previousPreferredUnits);
    if (templateChanged) {
      Object.keys(preferredUnits).forEach((unitKey) => {
        const oldUnit = previousPreferredUnits?.[unitKey];
        const newUnit = preferredUnits[unitKey];
        if (oldUnit !== newUnit) {
          onChangeUnit({ unitKey, value: newUnit });
        }
      });
      setViewData(convertViewData(preferredUnits));
    }
  }, [unitConfig]);

  // Reset the cell local state when:
  //  - the parent table changes (e.g. store changes, or another user updates values)
  //  - units change internally (table header unit cells updated)
  useEffect(() => {
    setViewData(convertViewData(selectedUnits));
  }, [table, selectedUnits]);

  return (
    <Table
      table={{
        ...otherProps,
        headers: viewData.convertedHeaders,
        rows: viewData.convertedRows,
      }}
      onListReorder={onListReorder}
      canListReorder={canListReorder}
    />
  );
};
