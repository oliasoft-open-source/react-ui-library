import React, { useState } from 'react';
import { produce } from 'immer';
import {
  convertAndGetValue,
  getUnitsForQuantity,
  label,
  roundByMagnitude,
} from '@oliasoft-open-source/units';
import { TStringNumberNull } from 'typings/common-type-definitions';
import { Meta, StoryFn } from '@storybook/react';
import { Grid } from 'components/layout/grid/grid';
import { Spacer } from 'components/layout/spacer/spacer';
import { Card } from '../card/card';
import { Field } from '../form/field';
import { Heading } from '../heading/heading';
import { Select } from '../select/select';
import { Toggle } from '../toggle/toggle';
import {
  UnitTable,
  IUnitTableOnChangeEvent,
  IUnitTableRow,
  IUnitTableProps,
} from './unit-table';

export default {
  title: 'Basic/UnitTable',
  args: {},
} as Meta;

/***********************************************************************************************************************
 Feature flags for testing variants of the prototype (we may not need all these in final release)
***********************************************************************************************************************/

/*
  Convert the onChange return values back to the original storage unit. Allows us to avoid converting the units of
  storage values, but you need to turn on enableCosmeticRounding for this to work well.
*/

const initialConvertBackToStorageUnit = true;

/*
  Enable Excel-style cosmetic rounding of display values (to 14 significant digits). This solves the "rounding precision
  noise" glitches we get if always store in on fixed storage unit.
*/

const initialEnableCosmeticRounding = true;

/*
  Enable automatic display rounding of NumberInput table cells (less precision is shown until the cell is clicked
  / focused
*/

const initialEnableDisplayRounding = true;

/*
  Whether to convert and update the entire table when values are edited in different units to the storage unit
  - if true: table story behaves like current tables
  - if false: only the edited cell gets updated in the data store (but you need convertBackToStorageUnit and
    enableCosmeticRounding to be enabled for this).
*/

const initialConvertWholeTable = false;

/**********************************************************************************************************************/

/***********************************************************************************************************************
 Initialize a storage table (represents Redux store)
***********************************************************************************************************************/

const initialLengthUnit = 'ft';
const initialTemperatureUnit = 'C';
const initialRowCount = 3;

type TTableUnitRow = [
  fromUnit: string,
  toUnit: string,
  temperatureUnit: string,
];
type TTableMainRow = [
  from: string | number,
  to: string | number,
  temperature: string | number,
];
type TTable = [TTableUnitRow, ...TTableMainRow[]];

const createTable = (
  rowCount: number,
  lengthUnit: string,
  temperatureUnit: string,
): TTable => {
  const header: TTableUnitRow = [lengthUnit, lengthUnit, temperatureUnit];
  const rows: TTableMainRow[] = [...Array(rowCount)].map((_row, i) => {
    const from = i;
    const to = i + 1.25;
    const temperature = 4 + i * 0.25;
    return [String(from), String(to), String(temperature)];
  });
  return [header, ...rows];
};

const initialTable = createTable(
  initialRowCount,
  initialLengthUnit,
  initialTemperatureUnit,
);

/***********************************************************************************************************************
 onChange handler to convert an entire table to new storage units (represents the "old" / existing way of doing things
***********************************************************************************************************************/

const convertStorageTable = ({
  reduxTable,
  value,
  unit,
  rowIndex,
  cellIndex,
}: {
  reduxTable: TTable;
  value: string | number;
  unit: string;
  rowIndex: number;
  cellIndex: number;
}): TTable => {
  return produce(reduxTable, (reduxTable) => {
    const currentStorageUnit = reduxTable[0][cellIndex];

    const isUnitRow = (
      _row: TTableMainRow | TTableUnitRow,
      rowIndex: number,
    ): _row is TTableUnitRow => rowIndex === 0;

    reduxTable.forEach((row, rI) => {
      if (isUnitRow(row, rI)) {
        row[cellIndex] = unit;
        return;
      }

      // Update row with currently updated cell
      if (rI === rowIndex + 1) {
        row[cellIndex] = String(value); // changed cell value is already in new display unit
        return;
      }

      // Convert rest of column to new unit
      const convertedValue = convertAndGetValue(
        row[cellIndex],
        unit,
        currentStorageUnit,
      );
      row[cellIndex] = String(convertedValue);
    });

    return reduxTable;
  });
};

/***********************************************************************************************************************
 onChange handler to update one cell only, in the original storage unit (represents new Excel-style cosmetic rounding)
***********************************************************************************************************************/

const updateCell = ({
  reduxTable,
  value,
  rowIndex,
  cellIndex,
}: {
  reduxTable: TTable;
  value: string;
  rowIndex: number;
  cellIndex: number;
}) => {
  return produce(reduxTable, (draft) => {
    draft[rowIndex + 1 /*OFFSET UNIT ROW*/][cellIndex] = value;
  });
};

/***********************************************************************************************************************
 Story
***********************************************************************************************************************/

export const UnitTableExample: StoryFn<IUnitTableProps> = () => {
  const [preferredLengthUnit, setPreferredLengthUnit] =
    useState(initialLengthUnit);
  const [preferredTemperatureUnit, setPreferredTemperatureUnit] = useState(
    initialTemperatureUnit,
  );
  const [reduxTable, setReduxTable] = useState<TTable>(initialTable);
  const [convertBackToStorageUnit, setConvertBackToStorageUnit] = useState(
    initialConvertBackToStorageUnit,
  );
  const [enableDisplayRounding, setEnableDisplayRounding] = useState(
    initialEnableDisplayRounding,
  );
  const [enableCosmeticRounding, setEnableCosmeticRounding] = useState(
    initialEnableCosmeticRounding,
  );
  const [convertWholeTable, setConvertWholeTable] = useState(
    initialConvertWholeTable,
  );
  const [displayedLengthUnit, setDisplayedLengthUnit] =
    useState(initialLengthUnit);

  /*********************************************************************************************************************
   Cell onChange handler
  *********************************************************************************************************************/
  const onUpdateCell = (evt: IUnitTableOnChangeEvent) => {
    const { rowIndex, cellIndex, value, unit } = evt.target;
    const nextTable = convertWholeTable
      ? convertStorageTable({
          reduxTable,
          value,
          unit,
          rowIndex,
          cellIndex,
        })
      : updateCell({ reduxTable, value, rowIndex, cellIndex });
    setReduxTable(nextTable);
  };

  /*********************************************************************************************************************
   Construct table view data (same as old Table component, with a new props)
  *********************************************************************************************************************/
  const tableViewData = reduxTable.reduce(
    (acc, row, rowIndex) => {
      if (rowIndex === 0) {
        acc.headers = [
          {
            cells: [
              { value: 'From' },
              { value: 'To' },
              { value: 'Temperature' },
            ],
          },
          {
            cells: [
              { type: 'AutoUnit', unitKey: 'length', disabled: true },
              { type: 'AutoUnit', unitKey: 'length', disabled: false },
              {
                type: 'AutoUnit',
                unitKey: 'temperature',
                testId: 'table-unit-selector-3',
              },
            ],
          },
        ];
      } else {
        acc.rows = acc.rows.concat({
          cells: row.map((cell, cellIndex) => {
            const specialCell = rowIndex === 1 && cellIndex === 1;
            const type = specialCell ? 'Text' : 'NumberInput';
            const formatDisplayValue = specialCell
              ? (value: number | string) => `T* ${value}`
              : null;
            const customDisplayRounding =
              rowIndex === 3 && cellIndex === 2
                ? (value: TStringNumberNull) => roundByMagnitude(value, 2)
                : null;
            const value = cell;
            return {
              value,
              type,
              unitKey: cellIndex === 2 ? 'temperature' : 'length',
              onChange: onUpdateCell,
              roundDisplayValue: customDisplayRounding, // optionally customize the display rounding of specific columns/cells
              formatDisplayValue, // optionally extend/customize the display formatting of specific columns/cells
              autoUnit: true,
              testId: `testId_${rowIndex}_${cellIndex}`,
            };
          }),
        });
      }
      return acc;
    },
    {
      testId: 'testId',
      headers: [] as any[],
      rows: [] as IUnitTableRow[],
    },
  );

  return (
    <Grid columns="1fr 1fr" gap>
      <Card heading={<Heading>Settings</Heading>}>
        <Field label="Convert back to storage unit:" labelLeft labelWidth={200}>
          <Toggle
            checked={convertBackToStorageUnit}
            onChange={(evt) => setConvertBackToStorageUnit(evt.target.checked)}
          />
        </Field>
        <Field label="Enable display rounding:" labelLeft labelWidth={200}>
          <Toggle
            checked={enableDisplayRounding}
            onChange={(evt) => setEnableDisplayRounding(evt.target.checked)}
          />
        </Field>
        <Field label="Enable cosmetic rounding:" labelLeft labelWidth={200}>
          <Toggle
            checked={enableCosmeticRounding}
            onChange={(evt) => setEnableCosmeticRounding(evt.target.checked)}
          />
        </Field>
        <Field label="Convert whole table:" labelLeft labelWidth={200}>
          <Toggle
            checked={convertWholeTable}
            onChange={(evt) => setConvertWholeTable(evt.target.checked)}
          />
        </Field>
      </Card>
      <Card heading={<Heading>Unit template</Heading>}>
        <Field label="Length unit:" labelLeft labelWidth={120}>
          <Select
            options={getUnitsForQuantity('length')?.map((unit) => ({
              value: unit,
              label: label(unit),
            }))}
            value={{ value: preferredLengthUnit }}
            onChange={(evt) => setPreferredLengthUnit(evt.target.value)}
            width="100px"
          />
        </Field>
        <Field label="Temperature unit:" labelLeft labelWidth={120}>
          <Select
            options={getUnitsForQuantity('temperature')?.map((unit) => ({
              value: unit,
              label: label(unit),
            }))}
            value={{ value: preferredTemperatureUnit }}
            onChange={(evt) => setPreferredTemperatureUnit(evt.target.value)}
            width="100px"
          />
        </Field>
      </Card>
      <Card heading={<Heading>Storage State (Redux)</Heading>}>
        <pre>
          <code>{JSON.stringify(reduxTable, null, 2)}</code>
        </pre>
      </Card>
      <Card heading={<Heading>UnitTable</Heading>}>
        <UnitTable
          table={tableViewData}
          //New props:
          unitConfig={[
            {
              unitKey: 'length',
              storageUnit: reduxTable[0][0],
              preferredUnit: preferredLengthUnit,
              onChange: ({ newUnit }) => setDisplayedLengthUnit(newUnit),
            },
            {
              unitKey: 'temperature',
              storageUnit: reduxTable[0][2],
              preferredUnit: preferredTemperatureUnit,
            },
          ]}
          //Most tables should just use the default values here (true):
          convertBackToStorageUnit={convertBackToStorageUnit}
          enableDisplayRounding={enableDisplayRounding}
          enableCosmeticRounding={enableCosmeticRounding}
        />
        <Spacer height={20} />
        <Field label="Displayed length unit:" labelLeft labelWidth={150}>
          {displayedLengthUnit}
        </Field>
      </Card>
    </Grid>
  );
};
