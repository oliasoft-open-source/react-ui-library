import React from 'react';
import { Meta } from '@storybook/react';
import { expect, fireEvent, userEvent, waitFor, within } from '@storybook/test';
import { UnitTableExample } from './unit-table.stories';

export default {
  title: 'Basic/UnitTable/Test Cases',
  args: {},
  tags: ['!autodocs'],
} as Meta;

export const Test = UnitTableExample.bind({});

Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); //for portal layer elements https://github.com/storybookjs/storybook/issues/16971
  await step('Can enter decimal values', async () => {
    const input = canvas.getByTestId('testId_2_1');
    await userEvent.clear(input);
    await userEvent.type(input, '123.123456789');
    await userEvent.click(canvasElement);
    await expect(canvas.getByDisplayValue('123.12')).toBeInTheDocument(); // display rounding
  });
  await step('Can enter comma decimals (converts to dot)', async () => {
    const input = canvas.getByTestId('testId_2_1');
    await userEvent.clear(input);
    await userEvent.type(input, '123,456');
    await userEvent.click(canvasElement);
    await expect(canvas.getByDisplayValue('123.46')).toBeInTheDocument(); // display rounding
  });
  await step('Can enter small numbers', async () => {
    const input = canvas.getByTestId('testId_2_1');
    await userEvent.clear(input);
    await userEvent.type(input, '0.0000000023');
    await userEvent.click(canvasElement);
    await expect(input).toHaveDisplayValue('0.00'); // display rounding
  });
  await step('Can enter large numbers', async () => {
    const input = canvas.getByTestId('testId_2_1');
    await userEvent.clear(input);
    await userEvent.type(input, '999999999999');
    await userEvent.click(canvasElement);
    await expect(
      canvas.getByDisplayValue('999999999999.00'),
    ).toBeInTheDocument();
  });
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId_2_1');
    await userEvent.clear(input);
    await userEvent.type(input, '123x45s6');
    await userEvent.click(canvasElement);
    await expect(canvas.getByDisplayValue('123x45s6')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toEqual(
      'Must be a numerical value',
    );
    await fireEvent.mouseOver(input);
    await waitFor(() => {
      expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
    });
  });
};
