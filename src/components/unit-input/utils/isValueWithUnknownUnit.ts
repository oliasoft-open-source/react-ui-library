import { getUnit, KNOWN_UNITS } from '@oliasoft-open-source/units';

export const isValueWithUnknownUnit = (value: string | undefined) => {
  if (!value) return false;
  const unit = getUnit(value);

  return !KNOWN_UNITS.includes(unit);
};
