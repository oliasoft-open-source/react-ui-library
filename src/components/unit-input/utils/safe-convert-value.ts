import { isWrongValue } from 'components/unit-input/utils/is-wrong-value';
import { getValue, isValueWithUnit } from '@oliasoft-open-source/units';
import { isValueWithUnknownUnit } from 'components/unit-input/utils/isValueWithUnknownUnit';
import {
  convertUnit,
  IConvertUnitResult,
} from 'helpers/convert-unit/convert-unit';
import { checkConversion } from 'helpers/check-conversion/check-conversion';

export interface ISafeConvertValue {
  value?: string;
  toUnit: string;
  unitkey: string;
  defaultFromUnit: string;
  doNotConvertValue: boolean;
}

/**
 * Safely converts a value from one unit to another, checking for invalid inputs and allowing for bypass.
 * @returns The converted value as a string, the original value if conversion is bypassed, or an empty string if conversion is not possible.
 */
export const safeConvertValue = ({
  value = '',
  toUnit,
  unitkey,
  defaultFromUnit,
  doNotConvertValue,
}: ISafeConvertValue): IConvertUnitResult => {
  const rawValue = getValue(value);
  const isInvalidInput = isWrongValue(rawValue);
  const shouldConvert = checkConversion({
    value,
    unitkey,
    toUnit,
  });

  if (!shouldConvert) {
    return { value: rawValue };
  }

  if (isValueWithUnknownUnit(value)) {
    return { value: rawValue };
  }

  if (isInvalidInput) {
    return { value: rawValue };
  }

  if (!isValueWithUnit(value)) {
    return { value };
  }

  return convertUnit({
    value,
    unitkey,
    toUnit,
    fromUnit: defaultFromUnit,
    doNotConvertValue,
    exactPrecision: true,
  });
};
