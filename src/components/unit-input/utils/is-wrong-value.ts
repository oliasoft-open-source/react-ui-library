/**
 * Checks if the given value is considered "wrong" based on specific criteria.
 * @param val - The value to check. Can be of various types.
 * @returns A boolean indicating if the value is considered wrong.
 */
export const isWrongValue = (val: any): boolean => {
  return (
    val === 'undefined' ||
    val === undefined ||
    val === '' ||
    val === 'null' ||
    val === null ||
    val === 'NaN' ||
    isNaN(val)
  );
};
