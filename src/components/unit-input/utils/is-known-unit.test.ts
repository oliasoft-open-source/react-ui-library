import { isKnownUnit } from 'components/unit-input/utils/is-known-unit';

describe('isKnownUnit', () => {
  test('should return true for known default and value units', () => {
    expect(isKnownUnit('100|m', 'ft')).toBe(true);
  });

  test('should return false for unknown unit from value', () => {
    expect(isKnownUnit('100|"', '"')).toBe(false);
  });

  test('should return false for unknown default unit', () => {
    expect(isKnownUnit('100|ft', 'inch')).toBe(false);
  });

  test('should return false for both unknown units', () => {
    expect(isKnownUnit('100|yd', 'inch')).toBe(false);
  });

  test('should return false when no unit in value and known default unit', () => {
    expect(isKnownUnit('100', 'm')).toBe(false);
  });

  test('should return false when no unit in value and unknown default unit', () => {
    expect(isKnownUnit('100', 'unknownUnit')).toBe(false);
  });

  test('should return false when value is undefined', () => {
    expect(isKnownUnit(undefined, 'm')).toBe(false);
  });

  test('should return false when default unit is empty string and test is considered as known', () => {
    expect(isKnownUnit('100|m', '')).toBe(false);
  });
  test('should return false when default unit and value is empty', () => {
    expect(isKnownUnit('', '')).toBe(false);
  });
});
