import {
  isScientificStringNum,
  roundByMagnitude,
} from '@oliasoft-open-source/units';

export const safeRoundNumbers = (value: string | number) => {
  const isScientific = isScientificStringNum(value);

  return !isScientific ? roundByMagnitude(value) : value;
};
