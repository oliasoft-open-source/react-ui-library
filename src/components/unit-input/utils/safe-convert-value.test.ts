import { safeConvertValue } from './safe-convert-value';

describe('safeConvertValue', () => {
  test('converts units', () => {
    const { value } = safeConvertValue({
      value: '123|ft',
      toUnit: 'm',
      unitkey: 'length',
      defaultFromUnit: 'ft',
      doNotConvertValue: false,
    });

    expect(value).toBe('37.5');
  });
  test('always handles unit strings (OW-17784)', () => {
    const { value } = safeConvertValue({
      value: '170|g/mol',
      toUnit: 'g/mol',
      unitkey: 'moleWeight',
      defaultFromUnit: 'g/mol',
      doNotConvertValue: true,
    });
    expect(value).toBe('170');
  });
});
