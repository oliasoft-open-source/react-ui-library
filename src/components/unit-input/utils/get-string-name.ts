import { get, isArray, isFunction, isObject } from 'lodash';

export const getStringName = (
  name?: string | { fieldName?: string },
): string => {
  if (isObject(name) && !isArray(name) && !isFunction(name)) {
    return get(name, 'fieldName', '');
  }

  return typeof name === 'string' ? name : '';
};
