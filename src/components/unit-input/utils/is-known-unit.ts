import { getUnit, KNOWN_UNITS } from '@oliasoft-open-source/units';

const isUnitKnown = (unit: string = '') => KNOWN_UNITS?.includes(unit);

export const isKnownUnit = (
  valueWithUnit: string | undefined,
  defaultUnit: string,
) => {
  const unitFromValue = getUnit(valueWithUnit || '');

  return isUnitKnown(defaultUnit) && isUnitKnown(unitFromValue);
};
