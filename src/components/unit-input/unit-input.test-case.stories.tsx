import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { convertAndGetValue, withUnit } from '@oliasoft-open-source/units';
import { expect, fireEvent, userEvent, waitFor, within } from '@storybook/test';
import { IUnitInputProps, UnitInput } from 'components/unit-input/unit-input';
import { Button } from 'components/button/button';
import { Spacer } from 'components/layout/spacer/spacer';
import { Divider } from 'components/divider/divider';
import { Row } from 'components/layout/row/row';
import { Column } from 'components/layout/column/column';
import { Field } from 'components/form/field';
import { Message } from 'components/message/message';
import { Grid } from 'components/layout/grid/grid';
import { Card } from 'components/card/card';
import { Heading } from 'components/heading/heading';
import { Toggle } from 'components/toggle/toggle';
import { Text } from 'components/text/text';
import { Select } from 'components/select/select';
import { Icon } from 'components/icon/icon';
import { useArgs } from '@storybook/preview-api';

export default {
  title: 'Forms/UnitInput/Test Cases',
  component: UnitInput,
  tags: ['!autodocs'],
  args: {
    name: 'example',
    unitkey: 'length',
    value: '123|m',
    testId: 'unit-input',
  },
} as Meta<IUnitInputProps>;

const Template: StoryFn<IUnitInputProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: any) => {
    updateArgs({ value: evt.target.value });
  };
  return <UnitInput {...args} onChange={handleChange} />;
};

export const Default = Template.bind({});

export const Placeholder: StoryFn<IUnitInputProps> = () => {
  const [value, setValue] = useState('123|m');
  return (
    <UnitInput
      name="example"
      onChange={(evt) => setValue(evt.target.value)}
      placeholder="Type a value..."
      value={value}
      initUnit="m"
      unitkey="depth"
      unitTemplate={{ depth: 'm' }}
      testId="testId"
    />
  );
};
Placeholder.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); //for tooltip portal elements https://github.com/storybookjs/storybook/issues/16971
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123x45s6');
    await expect(canvas.getByDisplayValue('123x45s6')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toEqual(
      'Must be a numerical value',
    );
    await fireEvent.mouseOver(input);
    // check that tooltip shows (OW-17693)
    await waitFor(() => {
      expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
    });
  });
};

export const PlaceholderWithUnit: StoryFn<IUnitInputProps> = () => {
  const [value, setValue] = useState('|m');
  return (
    <UnitInput
      name="example-ft"
      onChange={(evt) => setValue(evt.target.value)}
      placeholder="7|ft"
      value={value}
      initUnit="m"
      unitkey="depth"
      unitTemplate={{ depth: 'm' }}
      testId="testId-ft"
    />
  );
};
PlaceholderWithUnit.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); // for tooltip portal elements

  await step('Handle input with unit only', async () => {
    const input = canvas.getByTestId('testId-ft');

    await expect(canvas.getByDisplayValue('')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toBeNull();
    await fireEvent.mouseOver(input);
    await expect(input.getAttribute('data-error')).toBeNull();
  });

  await step('Clear input and check placeholder', async () => {
    const input = canvas.getByTestId('testId-ft');
    await userEvent.clear(input);
    // Ensure the placeholder is shown
    await expect(input).toHaveAttribute('placeholder', '2.13');
  });

  await step('Type a correct numeric value and ensure no error', async () => {
    const input = canvas.getByTestId('testId-ft');
    await userEvent.type(input, '123');
    // Ensure the value is correctly displayed
    await expect(canvas.getByDisplayValue('123')).toBeInTheDocument();
    // Ensure there is no error
    await expect(input.getAttribute('data-error')).toBeNull();
    await fireEvent.mouseOver(input);
    await expect(input.getAttribute('data-error')).toBeNull();
  });

  await step(
    'Remove value, type incorrect value and check for error',
    async () => {
      const input = canvas.getByTestId('testId-ft');
      await userEvent.clear(input);
      await userEvent.type(input, '123abc');
      await expect(canvas.getByDisplayValue('123abc')).toBeInTheDocument();
      await expect(input.getAttribute('data-error')).toEqual(
        'Must be a numerical value',
      );
      // Check that tooltip shows validation message
      await fireEvent.mouseOver(input);
      await waitFor(() => {
        expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
      });
    },
  );
};

// TODO: Show full source code
export const TestExternalStateChangeWithSynchronizedUnits = (
  args: IUnitInputProps,
) => {
  const [value, setValue] = useState('123|m');
  const [value2, setValue2] = useState('223|m');
  const [initUnit, setInitUnit] = useState('ft');

  return (
    <>
      <Button
        label="Change initUnit"
        onClick={() => setInitUnit(initUnit === 'm' ? 'ft' : 'm')}
      />
      <Spacer />
      <Button
        label="Change value"
        onClick={() => setValue(value === '123|m' ? '456|m' : '123|m')}
      />
      <Spacer />
      <UnitInput
        name="example"
        onChange={(evt) => {
          setValue(evt.target.value);
        }}
        unitkey="length"
        value={value}
        initUnit={initUnit}
        onSwitchUnit={setInitUnit}
      />
      <Spacer />
      <UnitInput
        name="example2"
        onChange={(evt) => {
          setValue2(evt.target.value);
        }}
        unitkey="length"
        value={value2}
        initUnit={initUnit}
        onSwitchUnit={setInitUnit}
      />
      <Divider />
      <h4>Without initUnit property (taken from the redux store)</h4>
      <UnitInput
        name="example"
        onChange={(evt) => {
          setValue(evt.target.value);
        }}
        unitkey="length"
        value={value}
        onSwitchUnit={setInitUnit}
      />
      <UnitInput
        name="example2"
        onChange={(evt) => {
          setValue2(evt.target.value);
        }}
        unitkey="length"
        value={value2}
        onSwitchUnit={setInitUnit}
      />
    </>
  );
};
TestExternalStateChangeWithSynchronizedUnits.parameters = {
  docs: {
    description: {
      story:
        '[OW-4380](https://oliasoft.atlassian.net/browse/OW-4380?atlOrigin=eyJpIjoiMmM1N2RkNWU3Mjk5NDYxY2E0YzhkMzk5ZTVmZTJhMjciLCJwIjoiaiJ9)',
    },
  },
};

export const TestEdgeCases = () => {
  const width = '500px';
  const [value, setValue] = useState<any>(undefined);
  return (
    <>
      <h4>Different edge cases such as +/- infinity, undefined, NaN, null:</h4>
      <Row>
        <Column width="25%">
          <Button
            label="Change value infinity"
            onClick={() => setValue(Infinity)}
          />
          <br />
          <Button
            label="Change value -infinity"
            onClick={() => setValue(-Infinity)}
          />
          <br />
          <Button label="Change value null" onClick={() => setValue(null)} />
          <br />
          <Button
            label="Change value - undefined"
            onClick={() => setValue(undefined)}
          />
          <br />
          <Button label="Change value - NaN" onClick={() => setValue(NaN)} />
          <br />
          <Button
            label="Change value - nothing (empty)"
            onClick={() => setValue(undefined)}
          />
          <br />
          <Button
            label="Change value - empty string"
            onClick={() => setValue('')}
          />
        </Column>
        <Column width="25%">
          <Button
            label="Change value infinity|m"
            onClick={() => setValue(Infinity + '|m')}
          />
          <br />
          <Button
            label="Change value -infinity|m"
            onClick={() => setValue(-Infinity + '|m')}
          />
          <br />
          <Button
            label="Change value null|m"
            onClick={() => setValue(null + '|m')}
          />
          <br />
          <Button
            label="Change value - undefined|m"
            onClick={() => setValue(undefined + '|m')}
          />
          <br />
          <Button
            label="Change value - NaN|m"
            onClick={() => setValue(NaN + '|m')}
          />
          <br />
          <Button
            label="Change value - nothing (empty)|m"
            onClick={() => setValue('|m')}
          />
        </Column>
      </Row>
      <br />
      <Field label="Normal PUI">
        <UnitInput
          name="example1"
          onChange={(evt) => setValue(evt.target.value)}
          value={value}
          unitkey="length"
          width={width}
          placeholder="value|unit"
        />
      </Field>
      <Field label="Disabled validation">
        <UnitInput
          name="example2"
          onChange={(evt) => setValue(evt.target.value)}
          value={value}
          unitkey="length"
          disabledValidation
          width={width}
        />
      </Field>
      <Field label="Disabled validation and manual error">
        <UnitInput
          name="example3"
          onChange={(evt) => setValue(evt.target.value)}
          value={value}
          unitkey="length"
          error="test"
          disabledValidation
          width={width}
        />
      </Field>
      <Message
        message={{
          heading: 'Current value inside PUI: ' + value,
          visible: true,
        }}
      />
    </>
  );
};
TestEdgeCases.parameters = {
  docs: {
    description: {
      story: '[OW-8884](https://oliasoft.atlassian.net/browse/OW-8884)',
    },
  },
};

export const ManagedTestCase = () => {
  const [visible, setVisible] = useState(true);
  const triggerRerender = () => {
    setVisible(false);
    setTimeout(() => setVisible(true), 0);
  };
  const [value, setValue] = useState('123|ft');
  return (
    <Grid columns="1fr 1fr" gap>
      <Card heading={<Heading>Storage State (Redux)</Heading>}>
        {value}
        <Spacer />
        <Button
          label="Change value"
          onClick={() =>
            setValue(withUnit(String(Math.floor(Math.random() * 100)), 'ft'))
          }
        />
      </Card>
      <Card
        heading={
          <Heading
            icon={<Icon icon="refresh" testId="triggerRerenderTestId" />}
            onIconClick={triggerRerender}
          >
            UnitInput
          </Heading>
        }
      >
        {visible && (
          <UnitInput
            name="example"
            onChange={(evt) => setValue(evt.target.value)}
            value={value}
            initUnit="m"
            unitkey="depth"
            unitTemplate={{ depth: 'm' }}
          />
        )}
      </Card>
    </Grid>
  );
};

export const OW_10614TestCase = () => {
  return (
    <>
      <Text faint>
        Reproduce OW-10614 rounding noise bug when storing values in a different
        unit to the display unit
      </Text>
      <Spacer />
      <UnitInput
        name="example"
        onChange={() => {}}
        value={withUnit(convertAndGetValue('9750|psi', 'bar'), 'bar')}
        initUnit="psi"
        unitkey="pressure"
        unitTemplate={{ pressure: 'psi' }}
      />
      <Spacer />
      <UnitInput
        name="example2"
        onChange={() => {}}
        value={withUnit(
          convertAndGetValue('8642|lbf', 'tonneForce'),
          'tonneForce',
        )}
        initUnit="lbf"
        unitkey="force"
        unitTemplate={{ force: 'tonneForce' }}
      />
    </>
  );
};

export const OW_17784TestCase: StoryFn<IUnitInputProps> = () => {
  return (
    <>
      <UnitInput
        name="example"
        unitkey="moleWeight"
        initUnit="g/mol"
        noConversion //reproduces OW_17784
        value="170|g/mol"
        unitTemplate={{ moleWeight: 'g/mol' }}
        testId="case1"
      />
      <Spacer />
      <UnitInput
        name="gor"
        unitkey="moleWeight"
        initUnit="gor"
        value="Infinity|SCF/STB"
        testId="case2"
      />
    </>
  );
};
OW_17784TestCase.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step('Can enter decimal values', async () => {
    await expect(canvas.getByDisplayValue('170')).toBeInTheDocument();
    await expect(canvas.getByDisplayValue('Infinity')).toBeInTheDocument();
  });
};

export const DemoCosmeticRounding = () => {
  const [visible, setVisible] = useState(true);
  const triggerRerender = () => {
    setVisible(false);
    setTimeout(() => setVisible(true), 0);
  };
  const [preferredUnit, setPreferredUnit] = useState('psi');
  const [enableDisplayRounding, setEnableDisplayRounding] = useState(false);
  const [enableCosmeticRounding, setEnableCosmeticRounding] = useState(true);
  const [convertBackToStorageUnit, setConvertBackToStorageUnit] =
    useState(true);
  const [value, setValue] = useState('672.2388357750001|bar');
  const [callBackError, setCallBackError] = useState('');

  return (
    <Grid columns="1fr 1fr" gap>
      <Card heading={<Heading>Settings</Heading>}>
        <Field label="Enable display rounding:" labelLeft labelWidth={200}>
          <Toggle
            checked={enableDisplayRounding}
            onChange={(evt) => setEnableDisplayRounding(evt.target.checked)}
          />
        </Field>
        <Field label="Enable cosmetic rounding:" labelLeft labelWidth={200}>
          <Toggle
            checked={enableCosmeticRounding}
            onChange={(evt) => setEnableCosmeticRounding(evt.target.checked)}
          />
        </Field>
        <Field
          label="Convert back to storage unit (onChange):"
          labelLeft
          labelWidth={200}
        >
          <Toggle
            checked={convertBackToStorageUnit}
            onChange={(evt) => setConvertBackToStorageUnit(evt.target.checked)}
          />
        </Field>
      </Card>
      <Card
        heading={
          <Heading
            icon={<Icon icon="refresh" testId="triggerRerenderTestId" />}
            onIconClick={triggerRerender}
          >
            UnitInput
          </Heading>
        }
      >
        <Text faint>
          Cosmetic Rounding Test Case: type 9750 then return (compare with
          enableCosmeticRounding on and off)
        </Text>
        <Spacer />
        <Text warning>
          Note: UnitInput uses convertSamePrecision internally (decision prior
          to cosmetic value implementation). This means not all rounding noise
          occurrences seen in NumberInput will be reproducible in UnitInput,
          because they get rounded off by other logic.
        </Text>
        <Spacer />
        {visible && (
          <UnitInput
            name="Example"
            unitkey="pressure"
            initUnit={preferredUnit}
            unitTemplate={{ pressure: 'psi' }}
            value={value}
            onChange={(evt) => {
              const { value } = evt.target;
              if (value) {
                setValue(value);
              }
            }}
            enableDisplayRounding={enableDisplayRounding}
            enableCosmeticRounding={enableCosmeticRounding}
            error={
              convertAndGetValue(value, 'bar') >= 0
                ? ''
                : 'Value must be positive'
            }
            validationCallback={(name, error) => {
              setCallBackError(error ?? '');
            }}
            convertBackToStorageUnit={convertBackToStorageUnit}
          />
        )}
      </Card>
      <Card heading={<Heading>Unit Template</Heading>}>
        <Select
          options={[
            { label: 'Bar', value: 'bar' },
            { label: 'psi', value: 'psi' },
          ]}
          value={preferredUnit}
          width={70}
          onChange={(evt) => setPreferredUnit(evt.target.value)}
        />
      </Card>
      <Card heading={<Heading>Test Validation Callback</Heading>}>
        {callBackError && (
          <Message
            message={{
              content: callBackError,
              icon: true,
              type: 'Error',
              visible: true,
            }}
          />
        )}
      </Card>
      <Card heading={<Heading>Storage State / Redux</Heading>}>
        {value}
        <Spacer />
        <Button
          label="Change value"
          onClick={() =>
            setValue(withUnit(String(Math.floor(Math.random() * 100)), 'bar'))
          }
        />
      </Card>
    </Grid>
  );
};

export const TestUserInput: StoryFn<IUnitInputProps> = () => {
  const [value, setValue] = useState('123|m');
  return (
    <UnitInput
      name="example"
      onChange={(evt) => setValue(evt.target.value)}
      value={value}
      initUnit="m"
      unitkey="depth"
      unitTemplate={{ depth: 'm' }}
      testId="testId"
    />
  );
};
TestUserInput.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); //for tooltip portal elements https://github.com/storybookjs/storybook/issues/16971
  await step('Can enter decimal values', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123.123456789');
    await expect(canvas.getByDisplayValue('123.123456789')).toBeInTheDocument();
  });
  await step('Can enter scientific notation', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '-1.12e3');
    await expect(canvas.getByDisplayValue('-1.12e3')).toBeInTheDocument();
  });
  await step('Can enter comma decimals (converts to dot)', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123,456');
    await expect(canvas.getByDisplayValue('123.456')).toBeInTheDocument();
  });
  await step('Can enter small numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '0.0000000023');
    await expect(canvas.getByDisplayValue('0.0000000023')).toBeInTheDocument();
  });
  await step('Can enter large numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '999999999999');
    await expect(canvas.getByDisplayValue('999999999999')).toBeInTheDocument();
  });
  await step('Can convert units', async () => {
    const input = canvas.getByTestId('testId');
    const menu = canvas.getByTestId('testId-menu');
    await userEvent.clear(input);
    await userEvent.type(input, '456.1');
    await userEvent.click(menu);
    await userEvent.click(body.getByTestId('testId-unit-ft'));
    await expect(canvas.getByDisplayValue('1496')).toBeInTheDocument();
  });
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123x45s6');
    await expect(canvas.getByDisplayValue('123x45s6')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toEqual(
      'Must be a numerical value',
    );
    await fireEvent.mouseOver(input);
    await waitFor(() => {
      expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
    });
  });
};

// Test the `disabled` prop
export const TestDisabledProp = Default.bind({});
TestDisabledProp.args = { disabled: true };
TestDisabledProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');

  expect(input).toBeDisabled();
};

// Test the `disabledUnit` prop
export const TestDisabledUnitProp = Default.bind({});
TestDisabledUnitProp.args = { disabledUnit: true };
TestDisabledUnitProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const unitDropdown = canvas.queryByTestId('unit-input-menu');

  expect(unitDropdown).toBeInTheDocument();

  const button = within(unitDropdown as HTMLElement).queryByRole('button');
  expect(button).toBeDisabled();
};

// Test the `error` prop
export const TestErrorProp = Default.bind({});
TestErrorProp.args = { error: 'Own Error message' };
TestErrorProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByTestId('unit-input');
  const body = within(canvasElement.ownerDocument.body);

  await fireEvent.mouseOver(input);
  await waitFor(() => {
    expect(body.getByText('Own Error message')).toBeInTheDocument();
  });
};

// Test the `left` prop
export const TestLeftProp = Default.bind({});
TestLeftProp.args = { left: true };
TestLeftProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByTestId('unit-input');

  expect(input).not.toHaveStyle('text-align: right');
};

// Test the `value` prop
export const TestValueProp = Default.bind({});
TestValueProp.args = { value: '456|ft', initUnit: 'ft' };
TestValueProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');

  expect(input).toHaveValue('456');
};

// Test the `unitkey` prop
export const TestUnitKeyProp = Default.bind({});
TestUnitKeyProp.args = { value: '10|kg', unitkey: 'weight' };
TestUnitKeyProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);

  const unitDropdown = canvas.getByRole('button');
  await userEvent.click(unitDropdown);

  const dropdownMenu = canvasElement.ownerDocument.body;

  expect(within(dropdownMenu).getByText('t')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('lbf')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('mt')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('kip')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('N')).toBeInTheDocument();
};

// Test the `initUnit` prop
export const TestInitUnitProp = Default.bind({});
TestInitUnitProp.args = { initUnit: 'ft' };
TestInitUnitProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');
  expect(input).toHaveValue('404');

  const unitDropdown = canvas.getByRole('button');
  expect(unitDropdown).toHaveTextContent('ft');
};

// Test the `noConversion` prop
export const TestNoConversionProp = Default.bind({});
TestNoConversionProp.args = { noConversion: true };
TestNoConversionProp.play = async ({ canvasElement }) => {
  const addonElement = canvasElement.querySelector(
    '._addon_b4w53_1._groupOrderLast_b4w53_21',
  );

  expect(addonElement).toBeInTheDocument();
  expect(addonElement).toHaveTextContent('m');
};

// Test the `warning` prop
export const TestWarningProp = Default.bind({});
TestWarningProp.args = { warning: 'Warning message' };
TestWarningProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByTestId('unit-input');
  const body = within(canvasElement.ownerDocument.body);

  await fireEvent.mouseOver(input);
  await waitFor(() => {
    expect(body.getByText('Warning message')).toBeInTheDocument();
  });
};

// Test the `predefinedOptions` prop
export const TestPredefinedOptionsProp = Default.bind({});
TestPredefinedOptionsProp.args = {
  predefinedOptions: [
    { label: 'Option 1', value: '10|m' },
    { label: 'Option 2', value: '20|m' },
  ],
};
TestPredefinedOptionsProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const predefinedDropdown = canvas.queryByTestId('unit-input-predefined-menu');

  expect(predefinedDropdown).toBeInTheDocument();

  const button = within(predefinedDropdown as HTMLElement).queryByRole(
    'button',
  );
  await userEvent.click(button as HTMLElement);

  const dropdownMenu = canvasElement.ownerDocument.body;

  expect(within(dropdownMenu).getByText('Custom')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('Option 1')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('Option 2')).toBeInTheDocument();
};

// Test the conversions for preferred options
export const TestConversionsInPredefinedOptions = Default.bind({});
TestConversionsInPredefinedOptions.args = {
  unitkey: 'length',
  unitTemplate: {
    length: 'ft',
  },
  predefinedOptions: [
    { label: 'Option 1', value: '10|m' },
    { label: 'Option 2', value: '20|m' },
  ],
};
TestConversionsInPredefinedOptions.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const predefinedDropdown = canvas.queryByTestId('unit-input-predefined-menu');

  expect(predefinedDropdown).toBeInTheDocument();

  const button = within(predefinedDropdown as HTMLElement).queryByRole(
    'button',
  );
  await userEvent.click(button as HTMLElement);

  const dropdownMenu = canvasElement.ownerDocument.body;
  expect(within(dropdownMenu).getByText('32.8 ft')).toBeInTheDocument();
  expect(within(dropdownMenu).getByText('65.6 ft')).toBeInTheDocument();
};

// Test the `validationCallback` prop
export const TestValidationCallbackProp = Default.bind({});
let validationArgs = {};
TestValidationCallbackProp.args = {
  validationCallback: (name, error) => {
    validationArgs = { name, error };
    return { name, error };
  },
};
TestValidationCallbackProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');
  const body = within(canvasElement.ownerDocument.body);

  await userEvent.clear(input);
  await userEvent.type(input, '123asd');

  const someOtherElement = canvasElement.querySelector('body');
  await userEvent.click(someOtherElement as HTMLBodyElement);

  await waitFor(() => {
    expect(validationArgs).toEqual({
      name: 'example',
      error: 'Must be a numerical value',
    });
  });

  await fireEvent.mouseOver(input);
  await waitFor(() => {
    expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
  });
};

// Test the `disabledValidation` prop
export const TestDisabledValidationProp = Default.bind({});
TestDisabledValidationProp.args = { disabledValidation: true };
TestDisabledValidationProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');
  const body = within(canvasElement.ownerDocument.body);

  await userEvent.clear(input);
  await userEvent.type(input, '123asd');
  await fireEvent.mouseOver(input);

  await expect(input.getAttribute('data-error')).toBeNull();
};

// Test the `allowEmpty` prop
export const TestAllowEmptyProp = Default.bind({});
TestAllowEmptyProp.args = { allowEmpty: true, value: '|m' };
TestAllowEmptyProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');

  expect(input).toHaveValue('');
};

// Test the `onSwitchUnit` prop
export const TestOnSwitchUnitProp = Default.bind({});
let selectedUnit = '';
TestOnSwitchUnitProp.args = {
  onSwitchUnit: (unit) => {
    selectedUnit = unit;
  },
};

TestOnSwitchUnitProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');
  const unitDropdown = canvas.getByRole('button');
  await userEvent.click(unitDropdown);

  const dropdownMenu = canvasElement.ownerDocument.body;
  const el = within(dropdownMenu).getByText('km');
  await userEvent.click(el);

  expect(input).toHaveValue('0.123');
  expect(selectedUnit).toEqual('km');
};

// Test the `unitTemplate` prop
export const TestUnitTemplateProp = Default.bind({});
TestUnitTemplateProp.args = { unitTemplate: { length: 'ft' } };
TestUnitTemplateProp.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole('textbox');
  const unitDropdown = canvas.getByRole('button');
  expect(unitDropdown).toHaveTextContent('ft');
  expect(input).toHaveValue('404');
};

export const TestNotKnownUnit = Default.bind({});
TestNotKnownUnit.args = {
  unitkey: undefined,
  value: '77|"',
};
TestNotKnownUnit.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const input = canvas.getByTestId('unit-input');
  await userEvent.clear(input);
  await userEvent.type(input, '22');
  await expect(canvas.getByDisplayValue('22')).toBeInTheDocument();
  await expect(input.getAttribute('data-error')).toBeNull();
};

export const TestRoundNumberInUnitsMenu = Default.bind({});
TestRoundNumberInUnitsMenu.args = {
  unitkey: 'length',
  value: '0.127836182735|m',
  unitTemplate: { length: 'm' },
};
TestRoundNumberInUnitsMenu.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);

  const unitDropdown = canvas.getByRole('button');
  await userEvent.click(unitDropdown);

  const dropdownMenu = canvasElement.ownerDocument.body;

  const testCases = [
    { rounded: '0.1278', fullValue: '0.127836182735' },
    { rounded: '12.78', fullValue: '12.7836182735' },
    { rounded: '0.4194', fullValue: '0.419410048343' },
    { rounded: '0.0001278', fullValue: '0.000127836182735' },
    { rounded: '5.033', fullValue: '5.03292058012' },
    { rounded: '127.8', fullValue: '127.836182735' },
  ];

  const checkUnitSelectionDisplaysFullValue = async (
    rounded: string,
    fullValue: string,
  ) => {
    await userEvent.click(within(dropdownMenu).getByText(rounded));
    await waitFor(() => {
      const input = canvas.getByTestId('unit-input');
      expect(input).toHaveValue(fullValue);
    });
    await userEvent.click(unitDropdown);
  };

  await step('Check if value rounded in menu', async () => {
    await Promise.all(
      testCases.map(({ rounded }) =>
        waitFor(() => {
          expect(within(dropdownMenu).getByText(rounded)).toHaveTextContent(
            rounded,
          );
        }),
      ),
    );
  });

  await step(
    'Check each unit selection displays full value without rounding',
    async () => {
      await checkUnitSelectionDisplaysFullValue('0.1278', '0.127836182735');
      await checkUnitSelectionDisplaysFullValue('12.78', '12.7836182735');
      await checkUnitSelectionDisplaysFullValue('0.4194', '0.419410048343');
      await checkUnitSelectionDisplaysFullValue(
        '0.0001278',
        '0.000127836182735',
      );
      await checkUnitSelectionDisplaysFullValue('5.033', '5.03292058012');
      await checkUnitSelectionDisplaysFullValue('127.8', '127.836182735');
    },
  );
};
