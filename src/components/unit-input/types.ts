export enum PredefinedOptionsMenuState {
  CUSTOM = 'custom',
  PREDEFINED = 'predefined',
}
