import React, {
  ChangeEventHandler,
  FocusEventHandler,
  MouseEventHandler,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {
  altUnitsList,
  convertAndGetValue,
  getUnit,
  getValue,
  isValueWithUnit,
  label,
  split,
  validateNumber,
  withUnit,
  isValidNum,
  getAltUnitsListByQuantity,
  withPrettyUnitLabel,
} from '@oliasoft-open-source/units';
import cx from 'classnames';
import { useRunAfterUpdate } from 'components/unit-input/hooks/use-run-after-update';
import { usePrevious } from 'components/unit-input/hooks/use-previous';
import { getPreferredUnit } from 'helpers/get-preferred-unit';
import { PredefinedOptionsMenuState } from 'components/unit-input/types';
import { safeConvertValue } from 'components/unit-input/utils/safe-convert-value';
import { Menu } from 'components/menu/menu';
import { InputGroupAddon } from 'components/input-group/input-group-addon/input-group-addon';
import { Button } from 'components/button/button';
import { Tooltip } from 'components/tooltip/tooltip';
import { InputGroup } from 'components/input-group/input-group';
import { Spacer } from 'components/layout/spacer/spacer';
import { Text } from 'components/text/text';
import { MenuType } from 'components/menu/types';
import { getStringName } from 'components/unit-input/utils/get-string-name';
import { isKnownUnit } from 'components/unit-input/utils/is-known-unit';
import { useUnitContext } from 'helpers/initialize-context';
import { noop } from 'lodash';
import { checkConversion } from 'helpers/check-conversion/check-conversion';
import { convertUnit } from 'helpers/convert-unit/convert-unit';
import type { TStringOrNumber } from 'typings/common-type-definitions';
import { safeRoundNumbers } from 'components/unit-input/utils/safe-round-numbers';
import { GroupOrder, TGroupOrder } from 'typings/common-types';
import { NumberInput } from '../number-input/number-input';
import styles from './unit-input.module.less';

export interface IPredefinedOption {
  value?: string;
  label: string;
  valueKey?: string;
}

export type TOnChangeEventBaseTarget = { name: string; value: string };
export type TOnChangeEventTarget =
  | TOnChangeEventBaseTarget
  | (TOnChangeEventBaseTarget & {
      predefinedSelected: boolean;
      predefinedOption: IPredefinedOption;
    });

export type TOnChangeEvent = { target: TOnChangeEventTarget };
export type TOnChangeEventHandler = (evt: TOnChangeEvent) => void;

export interface IUnitInputProps {
  name?: string | { fieldName?: string };
  placeholder?: string;
  disabled?: boolean;
  disabledUnit?: boolean;
  error?: string | null | boolean;
  left?: boolean;
  small?: boolean;
  width?: string | number;
  value?: string;
  unitkey?: string;
  initUnit?: string;
  noConversion?: boolean;
  testId?: string;
  warning?: string | boolean | null;
  predefinedOptions?: IPredefinedOption[] | null;
  initialPredefinedOption?: boolean;
  shouldLinkAutomaticly?: boolean;
  selectedPredefinedOptionKey?: string;
  validationCallback?: (name: string, error: string | null) => any; //some WellDesign usages do return, so can't use void (fix later)
  disabledValidation?: boolean;
  allowEmpty?: boolean;
  onChange?: TOnChangeEventHandler;
  onSwitchUnit?: (unit: string) => void;
  onClick?: MouseEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  unitTemplate?: Record<string, any>;
  convertBackToStorageUnit?: boolean;
  enableCosmeticRounding?: boolean;
  enableDisplayRounding?: boolean;
  roundDisplayValue?: (args: TStringOrNumber) => TStringOrNumber;
  selectOnFocus?: boolean;
  groupOrder?: TGroupOrder; // Position in `InputGroup`
}

export const UnitInput = ({
  name,
  placeholder = '',
  disabled = false,
  disabledUnit = false,
  error = null,
  left = false,
  small = false,
  width = '100%',
  value,
  unitkey = '',
  initUnit,
  noConversion = false,
  onChange = noop,
  onClick = noop,
  onFocus = noop,
  onSwitchUnit = noop,
  unitTemplate,
  testId,
  warning = null,
  predefinedOptions,
  initialPredefinedOption = false,
  shouldLinkAutomaticly = true,
  selectedPredefinedOptionKey,
  validationCallback = () => ({ name: '', error: null }),
  disabledValidation = false,
  allowEmpty = false,
  convertBackToStorageUnit = false,
  enableCosmeticRounding = true,
  enableDisplayRounding = false,
  roundDisplayValue,
  selectOnFocus,
  groupOrder,
}: IUnitInputProps) => {
  const context = useUnitContext();
  const runAfterUpdate = useRunAfterUpdate();

  if (typeof value === 'number') {
    value = `${value}`;
  }

  const [propValue = '', propUnit = ''] =
    value !== undefined ? split(value) : [];

  const preferredUnit = useMemo(
    () => getPreferredUnit(unitkey, unitTemplate || context?.unitTemplate),
    [unitkey, unitTemplate, context?.unitTemplate],
  );

  const knownUnit = isKnownUnit(value, preferredUnit);
  const initDisplayUnit = initUnit || preferredUnit || propUnit;
  const previousInitUnit = usePrevious(initDisplayUnit);
  const { value: convertedValue } = safeConvertValue({
    value,
    toUnit: initDisplayUnit,
    unitkey,
    defaultFromUnit: propUnit,
    doNotConvertValue: noConversion,
  });
  const initDisplayLayer =
    convertedValue !== ''
      ? { value: convertedValue, unit: initDisplayUnit }
      : { value: propValue, unit: propUnit };

  const [displayLayer, setDisplayLayer] = useState(initDisplayLayer);
  const derivedAllowEmpty = allowEmpty || displayLayer?.value === undefined;
  const [predefinedOptionsMenuState, setPredefinedOptionsMenuState] = useState(
    initialPredefinedOption
      ? PredefinedOptionsMenuState.PREDEFINED
      : PredefinedOptionsMenuState.CUSTOM,
  );

  const disableInternalErrorValidationMessages = !!(
    disabledValidation || disabled
  );

  const foundPredefinedMenuOption =
    predefinedOptions &&
    predefinedOptions.find((el) => {
      if (!el?.value) return;
      if (selectedPredefinedOptionKey) {
        return selectedPredefinedOptionKey === el.valueKey;
      }

      const predefinedMenuItemUnit = isValueWithUnit(el.value)
        ? getUnit(el.value)
        : '';

      const { value: resultValue = value } = convertUnit({
        value: String(value),
        unitkey,
        toUnit: predefinedMenuItemUnit,
      });

      return withUnit(resultValue, predefinedMenuItemUnit) === el.value;
    });

  const getAlternativeUnits = () => {
    const valueToList = value ?? '';
    const initToUnit = displayLayer.unit;
    const shouldConvert = checkConversion({
      value: valueToList,
      unitkey,
      toUnit: initToUnit,
    });

    if (!shouldConvert) {
      let alternativeUnits = getAltUnitsListByQuantity(unitkey);
      return alternativeUnits?.map((unitRow) => ['', unitRow?.unit]);
    } else if (unitkey) {
      try {
        let alternativeUnits = altUnitsList(valueToList, unitkey);
        // Change all values in list to empty string if there is empty state in PUI OW-13969
        if (getValue(valueToList) === '') {
          return alternativeUnits.map((unitRow) => ['', unitRow[1]]);
        }
        return alternativeUnits;
      } catch (e) {
        /* do nothing, return null*/
        return null;
      }
    }
  };

  const onSetValue: ChangeEventHandler<HTMLInputElement> = (evt) => {
    const stringName = getStringName(name);
    const input = evt.target;
    const { value: inputValue, selectionStart: currentCursorPosition } = input;
    const newValue = withUnit(inputValue, displayLayer?.unit || '');
    const returnValue = convertBackToStorageUnit
      ? withUnit(convertAndGetValue(newValue, propUnit), propUnit)
      : newValue;

    onChange({
      target: {
        value: returnValue,
        name: stringName,
      },
    });

    const [val, u] = split(newValue);
    setDisplayLayer({ value: val, unit: u });

    // Preserve cursor position
    runAfterUpdate(() => {
      input.selectionStart = currentCursorPosition;
      input.selectionEnd = currentCursorPosition;
    });
  };

  const onClickUnit = (nextValue: string, nextUnit: string) => {
    if (nextUnit === displayLayer.unit || isNaN(Number(nextValue))) {
      return;
    }
    setDisplayLayer({ value: nextValue, unit: nextUnit });
    if (onSwitchUnit) {
      onSwitchUnit(nextUnit);
    }
  };

  useEffect(() => {
    if (initDisplayUnit) {
      let newValue,
        newUnit = '';
      if (previousInitUnit !== initDisplayUnit) {
        // initDisplayUnit monitoring for the feature of synchronizing two different unitInputs with the same unit
        const { value: resultValue } = safeConvertValue({
          value,
          toUnit: initDisplayUnit,
          unitkey,
          defaultFromUnit: propUnit,
          doNotConvertValue: noConversion,
        });
        newValue = resultValue;
        newUnit = initDisplayUnit;
      } else if (withUnit(displayLayer.value, displayLayer.unit) !== value) {
        // stringValue monitoring for value changes in redux for example webSocket changes between 2 browsers
        const { value: resultValue } = safeConvertValue({
          value,
          toUnit: displayLayer.unit,
          unitkey,
          defaultFromUnit: propUnit,
          doNotConvertValue: noConversion,
        });
        newValue = resultValue;
        newUnit = displayLayer.unit;
      }

      if (newValue !== undefined) {
        setDisplayLayer({ value: newValue, unit: newUnit });
      }

      if (predefinedOptions) {
        if (foundPredefinedMenuOption && shouldLinkAutomaticly) {
          setPredefinedOptionsMenuState(PredefinedOptionsMenuState.PREDEFINED);
        } else {
          setPredefinedOptionsMenuState(PredefinedOptionsMenuState.CUSTOM);
        }
      }
    }
  }, [initDisplayUnit, value, error, shouldLinkAutomaticly]);

  const alternativeUnits = getAlternativeUnits();
  const displayUnitLabel = label(displayLayer.unit) || displayLayer.unit || '';
  const noConvert =
    noConversion ||
    !alternativeUnits ||
    (alternativeUnits && alternativeUnits.length === 1);

  const stringName = getStringName(name);

  let sectionsPredefinedMenu;

  const createPredefinedOption = (el: IPredefinedOption) => {
    const elementValue = el?.value ? el.value : '';
    const [value = '', unit = ''] = isValueWithUnit(elementValue)
      ? split(elementValue)
      : [elementValue];

    let description = withPrettyUnitLabel(elementValue);

    const shouldConvert = checkConversion({
      value: elementValue,
      unitkey,
      toUnit: unit,
    });

    if (shouldConvert) {
      const { value = '' } = safeConvertValue({
        value: elementValue,
        toUnit: preferredUnit,
        unitkey,
        defaultFromUnit: unit,
        doNotConvertValue: noConversion,
      });

      description = withPrettyUnitLabel(withUnit(value, preferredUnit));
    }

    return {
      type: MenuType.OPTION,
      inline: true,
      onClick: () => {
        const validation = validateNumber(value);

        if (validation.valid && !disabled) {
          setPredefinedOptionsMenuState(PredefinedOptionsMenuState.PREDEFINED);
          onChange({
            target: {
              value: elementValue,
              name: typeof name === 'string' ? name : name?.fieldName || '',
              predefinedSelected: true,
              predefinedOption: el,
            },
          });
        }
      },
      label: (
        <>
          <Text>{el.label}</Text>
          <Spacer width="20px" height="0" />
        </>
      ),
      description,
      selected:
        foundPredefinedMenuOption === el &&
        predefinedOptionsMenuState === PredefinedOptionsMenuState.PREDEFINED,
    };
  };

  sectionsPredefinedMenu = [
    {
      type: MenuType.OPTION,
      inline: true,
      onClick: () => {
        if (
          predefinedOptionsMenuState !== PredefinedOptionsMenuState.CUSTOM &&
          !disabled
        ) {
          onChange({
            target: {
              value: withUnit(displayLayer.value, displayLayer.unit),
              name: typeof name === 'string' ? name : name?.fieldName || '',
            },
          });
        }
      },
      label: 'Custom',
      selected:
        predefinedOptionsMenuState === PredefinedOptionsMenuState.CUSTOM,
    },
  ];

  if (predefinedOptions?.length) {
    const dynamicOptions = predefinedOptions.map(createPredefinedOption);
    sectionsPredefinedMenu = [...sectionsPredefinedMenu, ...dynamicOptions];
  }

  const getPlaceholder = (placeholder: string) => {
    if (isValueWithUnit(placeholder)) {
      const placeholderUnit = getUnit(placeholder);
      const { value: resultValue } = safeConvertValue({
        value: placeholder,
        toUnit: displayLayer.unit,
        unitkey,
        defaultFromUnit: placeholderUnit,
        doNotConvertValue: noConversion,
      });

      return resultValue;
    }

    return placeholder;
  };

  const inputGroupOrder =
    !predefinedOptions && (!groupOrder || groupOrder === GroupOrder.FIRST)
      ? GroupOrder.FIRST
      : GroupOrder.MIDDLE;

  const unitGroupOrder =
    !groupOrder || groupOrder === GroupOrder.LAST
      ? GroupOrder.LAST
      : GroupOrder.MIDDLE;

  return (
    <div
      className={
        predefinedOptionsMenuState === PredefinedOptionsMenuState.PREDEFINED
          ? cx(styles.predefinedMenuActive)
          : ''
      }
    >
      <InputGroup small={small} width={width}>
        {predefinedOptions && (
          <Menu
            maxHeight={380}
            groupOrder="first"
            testId={testId && `${testId}-predefined-menu`}
            disabled={disabled}
            menu={{
              colored: true,
              trigger: 'Component',
              component: (
                <Button
                  groupOrder="first"
                  active={
                    predefinedOptionsMenuState ===
                    PredefinedOptionsMenuState.PREDEFINED
                  }
                  icon={
                    predefinedOptionsMenuState ===
                    PredefinedOptionsMenuState.PREDEFINED
                      ? 'link'
                      : 'unlink'
                  }
                />
              ),
              small,
              sections: sectionsPredefinedMenu,
            }}
            tooltip={
              predefinedOptionsMenuState ===
              PredefinedOptionsMenuState.PREDEFINED
                ? foundPredefinedMenuOption?.label
                : ''
            }
          />
        )}
        <div className={styles.inputWrapper}>
          <NumberInput
            name={stringName}
            key={stringName}
            testId={testId}
            disabled={disabled}
            placeholder={getPlaceholder(placeholder)}
            value={displayLayer.value}
            onChange={onSetValue}
            onFocus={onFocus}
            //UnitInput and NumberInput seem to have different type interfaces for error/warning prop
            error={error === null ? undefined : error}
            warning={warning === null ? undefined : warning}
            left={left}
            allowEmpty={derivedAllowEmpty}
            validationCallback={(_name, error) =>
              validationCallback(stringName, error)
            }
            enableCosmeticRounding={enableCosmeticRounding}
            enableDisplayRounding={enableDisplayRounding}
            roundDisplayValue={roundDisplayValue}
            groupOrder={inputGroupOrder}
            disableInternalErrorValidationMessages={
              disableInternalErrorValidationMessages
            }
            small={small}
            selectOnFocus={selectOnFocus}
          />
        </div>
        {displayUnitLabel &&
          (noConvert || !knownUnit ? (
            <InputGroupAddon groupOrder={unitGroupOrder}>
              {displayUnitLabel}
            </InputGroupAddon>
          ) : (
            <Menu
              groupOrder={unitGroupOrder}
              maxHeight={380}
              disabled={disabledUnit}
              testId={testId && `${testId}-menu`}
              tabIndex={-1}
              menu={{
                label: displayUnitLabel,
                trigger: 'DropDownButton',
                small,
                sections: alternativeUnits.map(
                  ([value = '', altUnit = '', label]) => {
                    const displayUnit = label || altUnit || '';
                    const safeValue = isValidNum(value) ? value : '';
                    const displayValue = enableCosmeticRounding
                      ? safeRoundNumbers(safeValue)
                      : safeValue;
                    return {
                      type: 'Option',
                      label: displayValue,
                      inline: true,
                      onClick: (evt) => {
                        evt.stopPropagation();
                        onClickUnit(value, altUnit);
                      },
                      description: displayUnit,
                      selected: displayUnit === displayUnitLabel,
                      testId: `${testId}-unit-${displayUnit}`,
                    };
                  },
                ),
              }}
            />
          ))}
      </InputGroup>
    </div>
  );
};
