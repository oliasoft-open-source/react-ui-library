import { useLayoutEffect, useRef } from 'react';

export const useRunAfterUpdate = (): ((fn: () => void) => void) => {
  const afterPaintRef = useRef<(() => void) | null>(null);

  useLayoutEffect(() => {
    if (afterPaintRef.current) {
      afterPaintRef.current();
      afterPaintRef.current = null;
    }
  });

  const runAfterUpdate = (fn: () => void) => {
    afterPaintRef.current = fn;
  };

  return runAfterUpdate;
};
