import React, { useState } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { IUnitInputProps, UnitInput } from 'components/unit-input/unit-input';
import { Button } from 'components/button/button';
import { Spacer } from 'components/layout/spacer/spacer';
import { Message } from 'components/message/message';
import { InputGroup } from 'components/input-group/input-group';
import { Select } from 'components/select/select';
import { Grid } from 'components/layout/grid/grid';

export default {
  title: 'Forms/UnitInput',
  component: UnitInput,
  args: {
    name: 'example',
    unitkey: 'length',
    value: '123|m',
  },
} as Meta<IUnitInputProps>;

const Template: StoryFn<IUnitInputProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: any) => {
    updateArgs({ value: evt.target.value });
  };
  return <UnitInput {...args} onChange={handleChange} />;
};

export const Default = Template.bind({});

export const InitUnit = Template.bind({});
InitUnit.args = {
  initUnit: 'ft',
  unitTemplate: { length: 'ft' },
};

export const NotKnownUnit = Template.bind({});
NotKnownUnit.args = {
  value: '123|"',
  unitkey: '"',
};

export const Width = Template.bind({});
Width.args = {
  width: '180px',
};

export const NoConversion = Template.bind({});
NoConversion.args = {
  noConversion: true,
};

export const Small = Template.bind({});
Small.args = {
  small: true,
};

export const Error = Template.bind({});
Error.args = {
  error: 'Error message goes here',
};

export const InternalValidationError = Template.bind({});
InternalValidationError.args = {
  value: 'asas123asd|%',
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  disabledUnit: true,
};
Disabled.parameters = {
  docs: {
    description: {
      story:
        'Each part of UnitInput can be disabled separately. `disabled` disables only the main text input, `disabledUnit` disables only the unit menu. **Needs review (comes from Reservoir input requirements)**',
    },
  },
};

export const DisabledNoConversion = Template.bind({});
DisabledNoConversion.args = {
  disabled: true,
  disabledUnit: true,
  noConversion: true,
};

export const DisabledUnitWithoutUnitKey = Template.bind({});
DisabledUnitWithoutUnitKey.args = {
  disabledUnit: true,
  unitkey: undefined,
};

export const DisabledUnitWithUnitKey = Template.bind({});
DisabledUnitWithUnitKey.args = {
  disabledUnit: true,
  unitkey: 'length',
};

export const DisabledWithEnabledUnitMenu = Template.bind({});
DisabledWithEnabledUnitMenu.args = {
  disabledUnit: false,
  disabled: true,
};

export const PredefinedOptionsMenu = Template.bind({});
PredefinedOptionsMenu.args = {
  predefinedOptions: [
    {
      label: 'bottom of casing',
      value: '12|km',
    },
    {
      label: 'mud weight at shoe depth',
      value: '8|mm',
    },
  ],
};

export const PredefinedOptionsConverted = Template.bind({});
PredefinedOptionsConverted.args = {
  name: 'unitInput',
  placeholder: 'Enter value',
  unitkey: 'length',
  unitTemplate: {
    length: 'ft',
  },
  predefinedOptions: [
    {
      label: 'Bottom of casing in ft',
      value: '7|ft',
    },
    {
      label: 'Bottom of casing',
      value: '12|m',
    },
    {
      label: 'Mud weight at shoe depth',
      value: '8|m',
    },
    {
      label: 'Percents',
      value: '777|%',
    },
  ],
};

export const ValidationCallback = () => {
  const [value, setValue] = useState('123|%');
  const [validation, setValidation] = useState<string | null>(null);
  const predictedMessage = 'This should be visible when an error appears';

  const handleValidation = (name: string, error: string | null) => {
    setValidation(error);
    return { name, error };
  };

  return (
    <>
      <h4>3rd party validation message when inputting invalid value:</h4>
      <UnitInput
        name="example"
        onChange={(evt) => setValue(evt.target.value)}
        error={!validation || predictedMessage}
        validationCallback={handleValidation}
        value={value}
      />
      <br />
      <Spacer />
      <h4>Original validation message: </h4>
      <Message message={{ heading: validation, visible: true }} />
    </>
  );
};

export const AllowEmptyPUI = () => {
  const [value, setValue] = useState('123|m');
  const [placeholder, setPlaceholder] = useState('');
  const [initUnit, setInitUnit] = useState('km');

  const setPlaceholderValue = () => {
    setInitUnit('ft');
    setPlaceholder('placeholder');
    setValue('|m');
  };

  return (
    <>
      <h4>Empty value case:</h4>
      <UnitInput
        name="example"
        onChange={(evt) => setValue(evt.target.value)}
        value={value}
        allowEmpty
        placeholder={placeholder}
        initUnit={initUnit}
        unitkey="length"
      />
      <br />
      <Spacer />
      <Button
        label="Change value |m and unit ft"
        onClick={() => setPlaceholderValue()}
      />
    </>
  );
};

export const OnSwitchUnit = () => {
  const [value, setValue] = useState('123|m');
  const [unit, setUnit] = useState('m');

  return (
    <>
      <UnitInput
        name="example"
        onChange={(evt) => setValue(evt.target.value)}
        value={value}
        initUnit={unit}
        unitkey="length"
        onSwitchUnit={setUnit}
      />
      <Spacer />
      <UnitInput
        name="example"
        onChange={(evt) => setValue(evt.target.value)}
        value={value}
        initUnit={unit}
        unitkey="length"
        onSwitchUnit={setUnit}
      />
    </>
  );
};

export const DisabledValidationWhenDisabled = Template.bind({});
DisabledValidationWhenDisabled.args = {
  value: '|m',
  disabled: true,
};

export const DisableSelectOnFocus = Template.bind({});
DisableSelectOnFocus.args = {
  selectOnFocus: false,
};

export const InInputGroup = () => {
  return (
    <Grid gap>
      <InputGroup>
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value="1"
          onChange={() => {}}
          width="auto"
        />
        <UnitInput
          name="example"
          onChange={() => {}}
          value="123"
          initUnit="m"
          unitkey="length"
          onSwitchUnit={() => {}}
        />
      </InputGroup>
      <InputGroup>
        <UnitInput
          name="example"
          onChange={() => {}}
          value="123"
          initUnit="m"
          unitkey="length"
          onSwitchUnit={() => {}}
        />
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value="1"
          onChange={() => {}}
          width="auto"
        />
      </InputGroup>
    </Grid>
  );
};
