import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { TEmpty } from 'typings/common-type-definitions';
import { Spacer } from 'components/layout/spacer/spacer';
import { IPopoverProps, Popover } from './popover';
import { Input } from '../input/input';
import { Button } from '../button/button';
import { Flex } from '../layout/flex/flex';
import { Select } from '../select/select';

interface OptionType {
  label: string;
  value: string;
}

const options: OptionType[] = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
];

interface SelectFormProps {
  close?: TEmpty;
}

const SelectForm = ({ close }: SelectFormProps) => (
  <Flex gap="var(--padding-xs)">
    <Input value="Value" width="150px" small />
    <Select options={options} value={options[0]} width="auto" small />
    <Button colored label="Save" onClick={() => {}} small />
    <Button label="Cancel" onClick={close} small />
  </Flex>
);

export default {
  title: 'Basic/Popover',
  component: Popover,
  args: {
    overflowContainer: true,
    content: <SelectForm />,
    children: <Button label="Toggle me" />,
  },
  decorators: [(story) => <div style={{ display: 'flex' }}>{story()}</div>],
  parameters: {
    docs: {
      source: {
        excludeDecorators: true,
      },
    },
  },
} as Meta;

const Template: StoryFn<IPopoverProps> = (args) => <Popover {...args} />;

export const Default = Template.bind({});

export const InputTrigger = Template.bind({});
InputTrigger.args = {
  children: <Input value="Value" width="100%" />,
};

export const CloseButton = Template.bind({});
CloseButton.args = {
  closeOnOutsideClick: false,
  showCloseButton: true,
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const ExternalControl: StoryFn<IPopoverProps> = (args) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <Button
        onClick={() => setIsOpen(!isOpen)}
        label="Toggle Popover Externally"
      />
      <Spacer />
      <Popover {...args} isOpen={isOpen}>
        <Input />
      </Popover>
    </div>
  );
};
