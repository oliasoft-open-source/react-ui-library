import React, {
  ReactElement,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';
import ResizeObserver from 'resize-observer-polyfill';
import cx from 'classnames';
import { Arrow, Placement, useLayer } from 'react-laag';
import { TEmpty } from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import { Button } from '../button/button';
import styles from './popover.module.less';

export interface IPopoverProps {
  children?: ReactNode;
  content?: ReactNode;
  placement?: Placement;
  closeOnOutsideClick?: boolean;
  fullWidth?: boolean;
  showCloseButton?: boolean;
  testId?: string;
  disabled?: boolean;
  overflowContainer?: boolean;
  isOpen?: boolean;
  onToggle?: (isOpen: boolean) => void;
}

export const Popover = ({
  children,
  content,
  placement = 'top-center',
  closeOnOutsideClick = true,
  fullWidth = false,
  showCloseButton = false,
  testId,
  disabled = false,
  overflowContainer = false,
  isOpen: externalIsOpen,
  onToggle: externalOnToggle,
}: IPopoverProps) => {
  const disabledContext = useContext(DisabledContext);
  const [isOpen, setOpen] =
    externalIsOpen !== undefined
      ? [externalIsOpen, externalOnToggle]
      : useState(false);

  const close = () => {
    if (!disabled && !disabledContext) {
      if (setOpen) {
        setOpen(false);
      }
    }
  };

  const toggle = () => {
    if (!(disabled || disabledContext)) {
      if (setOpen) {
        setOpen(!isOpen);
      }
    }
  };

  useEffect(() => {
    if (disabled || disabledContext) {
      close();
    }
  }, [disabled, disabledContext]);

  const { renderLayer, arrowProps, layerProps, triggerProps } = useLayer({
    isOpen,
    placement,
    auto: true,
    triggerOffset: 6,
    onOutsideClick: closeOnOutsideClick ? close : undefined,
    ResizeObserver,
    overflowContainer,
  });

  return (
    <>
      <div ref={triggerProps.ref} onClick={toggle} data-testid={testId}>
        {children}
      </div>
      {isOpen &&
        renderLayer(
          <div
            className={
              fullWidth
                ? cx(styles.toggleBox, styles.fullWidthStyleFix)
                : cx(styles.toggleBox)
            }
            {...layerProps}
          >
            <div>
              {React.isValidElement(content)
                ? React.cloneElement(
                    content as ReactElement<{ close: TEmpty }>,
                    { close },
                  )
                : content}
              {showCloseButton && (
                <div className={styles.dismiss}>
                  <Button small round onClick={close} icon={IconType.CLOSE} />
                </div>
              )}
            </div>
            {/* @ts-expect-error: onPointerEnterCapture, onPointerLeaveCapture warnings otherwise */}
            <Arrow
              {...arrowProps}
              backgroundColor="var(--color-background-layer)"
              borderColor="var(--color-border)"
              borderWidth={1}
              size={6}
            />
          </div>,
        )}
    </>
  );
};
