import React from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './progress-bar.module.less';

const getColor = (perct: number): string => {
  if (perct < 20) return styles.grey;
  if (perct < 40) return styles.red;
  if (perct < 60) return styles.orange;
  if (perct < 80) return styles.yellow;
  return styles.green;
};

export interface IProgressBarProps {
  width?: TStringOrNumber;
  inverted?: boolean;
  colored?: boolean;
  showProgressColors?: boolean;
  percentage?: number;
  noLabel?: boolean;
}

export const ProgressBar = ({
  width = 'auto',
  inverted = false,
  colored = true,
  showProgressColors = false,
  percentage = 0,
  noLabel = false,
}: IProgressBarProps) => {
  return (
    <div style={{ width }} data-percent={percentage}>
      <div
        className={cx(styles.progress, inverted ? styles.inverted : '')}
        style={{ width }}
      >
        <div
          className={cx(
            styles.bar,
            showProgressColors
              ? getColor(percentage)
              : colored
              ? styles.colored
              : '',
          )}
          style={{ width: `${percentage}%` }}
        >
          <div className={styles.label}>{!noLabel && `${percentage}%`}</div>
        </div>
        <div className={styles.label}>{!noLabel && `${percentage}%`}</div>
      </div>
    </div>
  );
};
