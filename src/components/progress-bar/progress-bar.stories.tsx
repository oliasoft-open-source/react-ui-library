import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IProgressBarProps, ProgressBar } from './progress-bar';

export default {
  title: 'Progress/ProgressBar',
  component: ProgressBar,
  args: {
    colored: true,
    inverted: false,
    noLabel: false,
    percentage: 30,
    showProgressColors: false,
    width: 'auto',
  },
} as Meta;

const Template: StoryFn<IProgressBarProps> = (args) => (
  <ProgressBar {...args} />
);
export const Default = Template.bind({});
