import React from 'react';
import cx from 'classnames';
import { IconType } from 'typings/common-types';
import styles from './chevron.module.less';
import { Icon } from '../../icon/icon';

interface IChevronProps {
  expanded: boolean;
}

export const Chevron = ({ expanded }: IChevronProps) => {
  return (
    <span className={cx(styles.chevron, expanded ? styles.expanded : '')}>
      <Icon icon={IconType.CHEVRON_RIGHT} />
    </span>
  );
};
