import { ReactNode } from 'react';
import { TEmpty } from 'typings/common-type-definitions';

export interface IAccordionBaseProps {
  heading: ReactNode;
  expanded?: boolean;
  bordered?: boolean;
  padding?: boolean;
  children?: ReactNode;
  squareBottom?: boolean;
  onClick?: TEmpty;
  testId?: string;
}

export interface IAccordionManagedProps extends IAccordionBaseProps {
  expanded?: boolean;
}
