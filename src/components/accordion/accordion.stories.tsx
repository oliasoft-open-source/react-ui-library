import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { useArgs } from '@storybook/preview-api';
import { IconType } from 'typings/common-types';
import { AccordionWithDefaultToggle as AccordionWithDefaultToggleComponent } from './helpers/accordion-with-default-toggle';
import { Accordion, IAccordionProps } from './accordion';
import { Heading } from '../heading/heading';
import { Divider } from '../divider/divider';
import { Table } from '../table/table';

export default {
  title: 'Layout/Accordion',
  component: Accordion,
  args: {
    bordered: false,
    expanded: false,
    managed: false,
    children: 'Example content',
  },
} as Meta<IAccordionProps>;

const Template: StoryFn<IAccordionProps> = (args: IAccordionProps) => {
  const [_, updateArgs] = useArgs();
  const toggleExpanded = () => {
    updateArgs({ expanded: !args.expanded });
  };
  return (
    <Accordion
      // @ts-ignore: write explicit reason: write explicit reason
      heading={<Heading onClick={toggleExpanded}>Heading</Heading>}
      {...args}
    />
  );
};

export const Default = Template.bind({});

export const Bordered = Template.bind({});
Bordered.args = {
  bordered: true,
};

export const NoContentPadding = Template.bind({});
NoContentPadding.args = {
  bordered: true,
  expanded: true,
  padding: false,
};

export const HelpIcon = Template.bind({});
HelpIcon.args = {
  bordered: true,
  expanded: true,
  heading: <Heading onClickHelp={() => console.log('help!')}>Heading</Heading>,
};

export const LibraryIcon = Template.bind({});
LibraryIcon.args = {
  bordered: true,
  expanded: true,
  heading: (
    <Heading
      libraryIcon={{
        onClick: () => {},
        tooltip: 'Show in library',
      }}
    >
      Heading
    </Heading>
  ),
};

export const CustomIcon = Template.bind({});
CustomIcon.args = {
  bordered: true,
  expanded: true,
  heading: (
    <Heading onIconClick={() => {}} icon={IconType.ADD}>
      Heading
    </Heading>
  ),
};

export const AccordionWithDefaultToggle: StoryFn = () => {
  const [enabled, setEnabled] = useState(true);
  const tableWithFluidWidth = {
    bordered: false,
    headers: [
      {
        cells: [{ value: 'Name' }, { value: 'Origin' }],
      },
    ],
    rows: [
      {
        cells: [{ value: 'Brown rice' }, { value: 'Vietnam' }],
      },
      {
        cells: [{ value: 'Buckwheat' }, { value: 'Poland' }],
      },
      {
        cells: [{ value: 'Couscous' }, { value: 'France' }],
      },
    ],
  };
  const tableWithFluidWidthEditable = {
    bordered: false,
    headers: [
      {
        cells: [{ value: 'Name' }, { value: 'Origin' }],
      },
    ],
    rows: [
      {
        cells: [
          { value: 'Brown rice', type: 'Input', onChange: () => {} },
          {
            type: 'Select',
            options: [
              { label: 'Vietnam', value: 'Vietnam' },
              { label: 'Other', value: 'Other' },
            ],
            value: { label: 'Vietnam', value: 'Vietnam' },
            native: true,
            onChange: () => {},
          },
        ],
      },
      {
        cells: [
          { value: 'Buckwheat', type: 'Input', onChange: () => {} },
          {
            type: 'Select',
            options: [
              { label: 'Poland', value: 'Poland' },
              { label: 'Other', value: 'Other' },
            ],
            value: { label: 'Poland', value: 'Poland' },
            native: true,
            onChange: () => {},
          },
        ],
      },
      {
        cells: [
          { value: 'Couscous', type: 'Input', onChange: () => {} },
          {
            type: 'Select',
            options: [
              { label: 'France', value: 'France' },
              { label: 'Other', value: 'Other' },
            ],
            value: { label: 'France', value: 'France' },
            native: true,
            onChange: () => {},
          },
        ],
      },
    ],
  };
  return (
    <AccordionWithDefaultToggleComponent
      heading={
        <Heading onClickHelp={() => console.log('help!')}>Heading</Heading>
      }
      toggleLabel="Default Settings"
      onClickDefaultToggle={() => setEnabled(!enabled)}
      padding={false}
    >
      <Divider margin={0} />
      <Table
        table={enabled ? tableWithFluidWidth : tableWithFluidWidthEditable}
      />
    </AccordionWithDefaultToggleComponent>
  );
};
