import React from 'react';
import cx from 'classnames';
import { Chevron } from './chevron/chevron';
import styles from './accordion.module.less';
import {
  IAccordionBaseProps,
  IAccordionManagedProps,
} from './accordion.interface';

export const AccordionBase = ({
  heading,
  expanded = false,
  bordered = false,
  padding = true,
  children,
  squareBottom = false,
  onClick,
  testId,
}: IAccordionBaseProps) => {
  return (
    <div
      className={cx(
        styles.accordion,
        bordered ? styles.bordered : '',
        expanded ? styles.expanded : '',
        squareBottom ? styles.squareBottom : '',
      )}
    >
      <div
        className={cx(styles.accordionHeader, onClick ? styles.clickable : '')}
        onClick={onClick}
        data-testid={testId}
      >
        <Chevron expanded={expanded} />
        <div className={styles.heading}>{heading}</div>
      </div>
      {expanded && children && (
        <div
          className={cx(
            styles.accordionContent,
            bordered && padding ? styles.padding : '',
          )}
        >
          {children}
        </div>
      )}
    </div>
  );
};

export const AccordionManaged = ({
  heading,
  bordered,
  padding,
  expanded: initialExpanded = false,
  children,
  testId,
}: IAccordionManagedProps) => {
  const [expanded, toggle] = React.useState(initialExpanded);

  return (
    <AccordionBase
      heading={heading}
      bordered={bordered}
      padding={padding}
      expanded={expanded}
      onClick={() => {
        toggle(!expanded);
      }}
      testId={testId}
    >
      {children}
    </AccordionBase>
  );
};

export interface IAccordionProps extends IAccordionBaseProps {
  managed?: boolean;
}

export const Accordion = ({
  heading,
  expanded = false,
  managed = false,
  bordered = false,
  padding = true,
  children,
  onClick,
  squareBottom = false,
  testId,
}: IAccordionProps) => {
  return managed ? (
    <AccordionManaged
      heading={heading}
      expanded={expanded}
      bordered={bordered}
      padding={padding}
      squareBottom={squareBottom}
      onClick={onClick}
      testId={testId}
    >
      {children}
    </AccordionManaged>
  ) : (
    <AccordionBase
      heading={heading}
      expanded={expanded}
      bordered={bordered}
      padding={padding}
      squareBottom={squareBottom}
      onClick={onClick}
      testId={testId}
    >
      {children}
    </AccordionBase>
  );
};
