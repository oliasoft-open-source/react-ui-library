import React, { ChangeEvent, ReactNode, useReducer } from 'react';
import cx from 'classnames';
import { Accordion } from '../accordion';
import { CheckBox, TCheckBoxClickHandler } from '../../check-box/check-box';
import styles from './accordion-with-default-toggle.module.less';
import { AccordionActionType } from '../enum';

export interface IAccordionWithDefaultToggleProps {
  heading: ReactNode;
  toggleLabel: string;
  expanded?: boolean;
  defaultEnabled?: boolean;
  onClickDefaultToggle?: TCheckBoxClickHandler;
  padding?: boolean;
  children?: ReactNode;
}

type IAction =
  | { type: AccordionActionType.TOGGLE_ACCORDION }
  | { type: AccordionActionType.TOGGLE_ENABLE };

type IAccordionState = {
  accordionExpanded: boolean;
  defaultEnabled: boolean;
};

const reducer = (state: IAccordionState, action: IAction): IAccordionState => {
  switch (action.type) {
    case AccordionActionType.TOGGLE_ACCORDION:
      return {
        ...state,
        accordionExpanded: !state.accordionExpanded,
      };
    case AccordionActionType.TOGGLE_ENABLE:
      return {
        ...state,
        defaultEnabled: !state.defaultEnabled,
        accordionExpanded: state.defaultEnabled,
      };
    default:
      return state;
  }
};

export const AccordionWithDefaultToggle = ({
  heading,
  toggleLabel,
  onClickDefaultToggle,
  expanded = false,
  defaultEnabled = true,
  padding = true,
  children,
}: IAccordionWithDefaultToggleProps) => {
  const [state, dispatch] = useReducer(reducer, {
    accordionExpanded: expanded,
    defaultEnabled,
  });

  const content = (
    <>
      <div className={styles.checkboxWrapper}>
        <CheckBox
          label={toggleLabel}
          onChange={(evt) => {
            dispatch({ type: AccordionActionType.TOGGLE_ENABLE });
            onClickDefaultToggle?.(evt);
          }}
          checked={state.defaultEnabled}
        />
      </div>
      {state.accordionExpanded && (
        <div className={styles.contentWrapper}>{children}</div>
      )}
    </>
  );

  return (
    <>
      <Accordion
        heading={heading}
        bordered
        expanded={state.accordionExpanded}
        squareBottom
        onClick={() => dispatch({ type: AccordionActionType.TOGGLE_ACCORDION })}
      />
      <div className={cx(styles.bordered, padding ? styles.padding : '')}>
        {content}
      </div>
    </>
  );
};
