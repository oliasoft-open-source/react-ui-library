import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IPopConfirmProps, PopConfirm } from './pop-confirm';
import { Button } from '../button/button';
import { IMessageType, toast } from '../toaster/toaster';

const onOk = (close: any) => {
  toast({
    message: {
      type: 'Success',
      content: 'success!',
      details: 'You successfully booked your cruise to Bahamas.',
    } as IMessageType,
  });
  close();
};

export default {
  title: 'Basic/PopConfirm',
  component: PopConfirm,
  args: {
    title: 'Do you want a free cruise to Bahamas?',
    onClickCancel: (close: any) => close(),
    onClickOk: onOk,
    children: <Button label="Click me!" />,
    overflowContainer: true,
    placement: 'top-center',
  },
  decorators: [
    (story) => (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        {story()}
      </div>
    ),
  ],
  parameters: {
    docs: {
      source: {
        excludeDecorators: true,
      },
    },
  },
} as Meta;

const Template: StoryFn<IPopConfirmProps> = (args) => (
  <PopConfirm
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);
export const Default = Template.bind({});

export const Action = Template.bind({});
Action.args = {
  children: <Button small round basic colored="muted" icon="delete" />,
};

export const Placement = Template.bind({});
Placement.args = {
  placement: 'bottom-end',
};

export const DisableConfirmButton = Template.bind({});
DisableConfirmButton.args = {
  disableConfirmButton: true,
};
