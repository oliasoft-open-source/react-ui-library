import React, { ReactNode } from 'react';
import { Placement } from 'react-laag';
import { TEmpty } from 'typings/common-type-definitions';
import { Popover } from '../popover/popover';
import { Content } from './content';

export interface IPopConfirmProps {
  children?: ReactNode;
  placement?: Placement;
  closeOnOutsideClick?: boolean;
  fullWidth?: boolean;
  title?: string;
  cancelText?: string;
  onClickCancel?: (close?: TEmpty) => void;
  disableConfirmButton?: boolean;
  okText?: string;
  onClickOk?: (close?: TEmpty) => void;
  overflowContainer?: boolean;
  testId?: string;
}

export const PopConfirm = ({
  children,
  placement = 'top-center',
  closeOnOutsideClick = true,
  fullWidth = false,
  title = '',
  cancelText = 'Cancel',
  onClickCancel = (close) => close && close(),
  okText = 'Ok',
  onClickOk = () => {},
  disableConfirmButton = false,
  overflowContainer = false,
  testId = undefined,
}: IPopConfirmProps) => {
  const content = (
    <Content
      title={title}
      okText={okText}
      onClickOk={onClickOk}
      disableConfirmButton={disableConfirmButton}
      cancelText={cancelText}
      onClickCancel={onClickCancel}
      testId={testId}
    />
  );

  return (
    <Popover
      content={content}
      placement={placement}
      closeOnOutsideClick={closeOnOutsideClick}
      fullWidth={fullWidth}
      overflowContainer={overflowContainer}
      testId={testId}
    >
      {children}
    </Popover>
  );
};
