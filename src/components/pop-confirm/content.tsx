import React from 'react';
import { TEmpty } from 'typings/common-type-definitions';
import { Button } from '../button/button';
import { Text } from '../text/text';
import styles from './pop-confirm.module.less';

export interface IPopConfirmContentProps {
  title?: string;
  okText: string;
  onClickOk: (close?: TEmpty) => void;
  disableConfirmButton?: boolean;
  cancelText: string;
  onClickCancel: (close?: TEmpty) => void;
  close?: TEmpty;
  testId?: string;
}

export const Content = ({
  title,
  okText,
  onClickOk,
  disableConfirmButton,
  cancelText,
  onClickCancel,
  close,
  testId,
}: IPopConfirmContentProps) => (
  <div className={styles.popConfirm}>
    {title && (
      <div className={styles.content}>
        <Text small>{title}</Text>
      </div>
    )}
    <div className={styles.buttons}>
      <Button
        small
        colored
        label={okText}
        onClick={() => onClickOk(close)}
        disabled={disableConfirmButton}
        testId={`${testId}-ok-button`}
      />
      <Button
        small
        label={cancelText}
        onClick={() => onClickCancel(close)}
        testId={`${testId}-cancel-button`}
      />
    </div>
  </div>
);
