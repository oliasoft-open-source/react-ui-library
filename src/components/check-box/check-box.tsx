import React, { useContext } from 'react';
import cx from 'classnames';
import { TEmpty } from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import styles from './check-box.module.less';
import { HelpIcon } from '../help-icon/help-icon';
import { Icon } from '../icon/icon';

export interface ICheckBoxClickEvent {
  target: {
    name: string;
    checked: boolean;
    value: boolean;
  };
}

export type TCheckBoxClickHandler = (
  evt: React.MouseEvent<HTMLDivElement, MouseEvent> & {
    target: ICheckBoxClickEvent['target'];
  },
) => void;

export interface ICheckBoxProps {
  checked?: boolean;
  isInTable?: boolean;
  label?: string | null;
  name?: string;
  noMargin?: boolean;
  onChange: TCheckBoxClickHandler;
  tabIndex?: number;
  disabled?: boolean;
  small?: boolean;
  testId?: string;
  dataix?: number;
  value?: string;
  helpText?: string;
  onClickHelp?: TEmpty;
}

export const CheckBox = ({
  noMargin = false,
  dataix = 0,
  isInTable = false,
  tabIndex = 0,
  checked = false,
  name,
  label = '',
  disabled = false,
  small = false,
  onChange,
  testId,
  value,
  helpText,
  onClickHelp,
}: ICheckBoxProps) => {
  const disabledContext = useContext(DisabledContext);
  const showHelp = helpText || onClickHelp;
  const isDisabled = disabled || disabledContext;

  return (
    <div
      className={cx(
        styles.checkbox,
        noMargin && styles.noMargin,
        isInTable && styles.isInTable,
        isDisabled && styles.disabled,
        small && styles.small,
        !label && styles.noLabel,
      )}
      data-ix={dataix}
      onClick={(evt) => {
        if (!isDisabled) {
          const target = evt.target as HTMLInputElement;
          target.name = name!;
          target.checked = !checked;

          onChange({
            ...evt,
            target: {
              ...target,
              value: target.checked,
            },
          });
        }
      }}
      data-testid={testId}
    >
      <input
        type="checkbox"
        tabIndex={tabIndex}
        checked={checked}
        name={name}
        onChange={() => {}}
        disabled={isDisabled}
      />
      <label htmlFor={name}>
        <span className={styles.checkmark}>
          <Icon icon={IconType.CHECK} />
        </span>
        {label}
      </label>
      {showHelp && (
        <div className={styles.helpIconEnabled} onClick={onClickHelp}>
          <HelpIcon text={helpText} />
        </div>
      )}
    </div>
  );
};
