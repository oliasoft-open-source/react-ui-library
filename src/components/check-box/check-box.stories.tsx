import React from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { CheckBox, ICheckBoxProps } from './check-box';

export default {
  title: 'Forms/CheckBox',
  component: CheckBox,
  args: {
    disabled: false,
    label: 'Label',
  },
} as Meta<ICheckBoxProps>;

const Template: StoryFn<ICheckBoxProps> = (args) => {
  const [{ checked }, updateArgs] = useArgs();
  const handleChange = () => {
    updateArgs({ checked: !checked });
  };
  return (
    <CheckBox
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
      onChange={handleChange}
    />
  );
};

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const DisabledChecked = Template.bind({});
DisabledChecked.args = { checked: true, disabled: true };

export const Standalone = Template.bind({});
Standalone.args = { label: null };

export const HelpText = Template.bind({});
HelpText.args = {
  helpText: 'Help text',
};

export const OnClickHelp = Template.bind({});
OnClickHelp.args = {
  onClickHelp: () => console.log('Help clicked'),
};

export const DisabledWithHelpText = Template.bind({});
DisabledWithHelpText.args = {
  helpText: 'Help text',
  disabled: true,
};

export const test = Template.bind({});
test.args = {
  onChange: (e) => console.log(e?.target?.value),
};
