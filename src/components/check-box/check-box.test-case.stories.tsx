import React from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { expect, userEvent, waitFor, within } from '@storybook/test';
import { CheckBox, ICheckBoxProps } from './check-box';

export default {
  title: 'Forms/CheckBox/Test Cases',
  component: CheckBox,
  args: {
    disabled: false,
    label: 'Label',
  },
  tags: ['!autodocs'],
} as Meta<ICheckBoxProps>;

const Template: StoryFn<ICheckBoxProps> = (args) => {
  const [{ checked }, updateArgs] = useArgs();
  const handleChange = () => {
    updateArgs({ checked: !checked });
  };
  return (
    <CheckBox
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
      onChange={handleChange}
    />
  );
};

export const TestKeys = () => {
  const [selected, setSelected] = React.useState<string[]>([]);
  return ['1', '2', '3', '4'].map((i) => (
    <CheckBox
      key={i}
      label={`Label ${i}`}
      checked={selected.includes(i)}
      onChange={() => {
        setSelected((prev) =>
          prev.includes(i) ? prev.filter((x) => x !== i) : [...prev, i],
        );
      }}
    />
  ));
};

export const Test = Template.bind({});
Test.args = { testId: 'testId' };
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step('Clicking checkbox should toggle it', async () => {
    const component = canvas.getByTestId('testId');
    const input = component.querySelector('input');

    await userEvent.click(component);
    await waitFor(() => expect(input).toBeChecked());

    await userEvent.click(component);
    await waitFor(() => expect(input).not.toBeChecked());
  });
};

export const TestDisabled = Template.bind({});
TestDisabled.args = { testId: 'testId', disabled: true };
TestDisabled.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step("Clicking disabled checkbox shouldn't toggle it", async () => {
    const component = canvas.getByTestId('testId');
    await component.click();
    await expect(component.querySelector('input')).not.toBeChecked();
  });
};
