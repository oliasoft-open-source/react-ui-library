import React, {
  CSSProperties,
  Fragment,
  isValidElement,
  ReactElement,
  ReactNode,
} from 'react';
import cx from 'classnames';
import { GroupOrder } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './input-group.module.less';

export interface IInputGroupProps {
  children: ReactNode;
  small?: boolean;
  width?: TStringOrNumber;
}

const isDOMTypeElement = (element: ReactNode): boolean => {
  return isValidElement(element) && typeof element.type === 'string';
};

export const InputGroup = ({
  children,
  small = false,
  width = '100%',
}: IInputGroupProps): ReactElement => {
  const childElements: React.ReactElement[] = Array.isArray(children)
    ? children.filter((c): c is React.ReactElement => isValidElement(c))
    : [children as React.ReactElement];
  const elements = childElements.map((item, index) => {
    const isDOMType = isDOMTypeElement(item);
    const { groupOrder: groupOrderProp } = !isDOMType
      ? item.props
      : { groupOrder: null };
    const count = childElements.length;
    const isFirst = index === 0;
    const isLast = index === count - 1;
    const isOnly = !count || count === 1;
    const groupOrder: string | null =
      groupOrderProp ||
      (isOnly
        ? null
        : isFirst
        ? GroupOrder.FIRST
        : isLast
        ? GroupOrder.LAST
        : GroupOrder.MIDDLE);
    return isDOMType ? (
      item
    ) : (
      <Fragment key={index}>
        {React.cloneElement(item, { groupOrder, small })}
      </Fragment>
    );
  });
  return (
    <div
      className={cx(styles.inputGroup)}
      style={{ width: width as CSSProperties['width'] }}
    >
      {elements}
    </div>
  );
};
