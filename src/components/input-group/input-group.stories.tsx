import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { TriggerType } from 'typings/common-types';
import { IInputGroupProps, InputGroup } from './input-group';
import { InputGroupAddon } from './input-group-addon/input-group-addon';
import { Input } from '../input/input';
import { Button } from '../button/button';
import { Menu } from '../menu/menu';
import { Select } from '../select/select';
import { Tooltip } from '../tooltip/tooltip';
import { MenuType } from '../menu/types';

export default {
  title: 'Forms/InputGroup',
  component: InputGroup,
  args: {},
} as Meta;

const Template: StoryFn<IInputGroupProps> = (args) => {
  return (
    <InputGroup
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    >
      <InputGroupAddon>$</InputGroupAddon>
      <Input width="100%" name="example" value="123" onChange={() => {}} />
      <Select
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={() => {}}
        value={{ label: 'Monkeys', value: 'bananas' }}
        width="auto"
        native
      />
      <Menu
        menu={{
          label: 'E-06/degC',
          trigger: TriggerType.DROP_DOWN_BUTTON,
          small: args.small,
          sections: [
            {
              type: MenuType.OPTION,
              label: '403.5433',
              description: 'ft',
              inline: true,
              onClick: () => {},
            },
            {
              type: MenuType.OPTION,
              label: '0.123',
              description: 'km',
              inline: true,
              onClick: () => {},
            },
            {
              type: MenuType.OPTION,
              label: '4842.4608',
              description: 'in',
              inline: true,
              onClick: () => {},
            },
          ],
        }}
      />
      <Button name="example" label="Confirm" colored onClick={() => {}} />
    </InputGroup>
  );
};
export const Default = Template.bind({});

export const FixedWidth = Template.bind({});
FixedWidth.args = { width: '480px' };

export const Small = Template.bind({});
Small.args = { small: true };

export const SelectWithButton: StoryFn<IInputGroupProps> = () => {
  return (
    <InputGroup>
      <Select
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={() => {}}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Tooltip text="This is a tooltip">
        <Button
          name="example"
          label="Confirm"
          colored
          onClick={() => {}}
          groupOrder="last"
        />
      </Tooltip>
    </InputGroup>
  );
};
