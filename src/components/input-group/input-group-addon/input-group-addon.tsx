import React, { ReactNode } from 'react';
import cx from 'classnames';
import { GroupOrder, TGroupOrder } from 'typings/common-types';
import styles from './input-group-addon.module.less';

export interface IInputGroupAddonProps {
  children: ReactNode;
  groupOrder?: TGroupOrder;
  small?: boolean;
}

export const InputGroupAddon = ({
  children,
  groupOrder,
  small = false,
}: IInputGroupAddonProps): React.ReactElement => {
  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case GroupOrder.FIRST:
          return styles.groupOrderFirst;
        case GroupOrder.LAST:
          return styles.groupOrderLast;
        default:
          return styles.groupOrderMiddle;
      }
    }
    return '';
  })();
  return (
    <span className={cx(styles.addon, order, small ? styles.small : '')}>
      {children}
    </span>
  );
};
