import React, { memo, ReactNode } from 'react';
import cx from 'classnames';
import isEqual from 'react-fast-compare';
import { Color } from 'typings/common-types';
import styles from './side-bar.module.less';
import { Tooltip } from '../tooltip/tooltip';
import { Icon } from '../icon/icon';
import { Badge } from '../badge/badge';
import { IOptionItem } from './side-bar';

interface ITooltipIconProps {
  isOpen: boolean;
  label: string | ReactNode;
  icon: any;
  invalid?: boolean;
}

const TooltipIcon = memo<ITooltipIconProps>(
  ({ isOpen, label, icon, invalid }) => {
    return (
      <>
        <Tooltip
          text={label}
          enabled={!isOpen}
          placement="right-center"
          possiblePlacements={['right-center']}
          fontSize={16}
          padding="var(--padding-sm)"
          display="block"
        >
          <span className={styles.icon}>
            <Badge small title={invalid ? '!' : undefined} margin={4}>
              <Icon icon={icon} />
            </Badge>
          </span>
        </Tooltip>
        <span className={styles.label}>{label}</span>
      </>
    );
  },
  (prevProps, nextProps) => isEqual(prevProps, nextProps),
);

export interface ISideBarLinkProps {
  isOpen: boolean;
  items: IOptionItem[];
  sectionIndex: number;
  onClick: (
    evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    value: string,
    label: string | ReactNode,
    onClickCallback?: (
      evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    ) => void,
  ) => void;
  testId?: string;
}

export const Link = ({
  isOpen,
  items,
  sectionIndex,
  onClick,
  testId,
}: ISideBarLinkProps) => {
  return (
    <div className={styles.list}>
      {items.map((link, i) => {
        const key = `${sectionIndex}_${i}`;
        return (
          <a
            href={link.value}
            className={cx(
              styles.item,
              link.isActive ? styles.active : '',
              link.isExperimental ? styles.experimental : '',
            )}
            key={key}
            onClick={(evt) =>
              onClick(evt, link.value, link.label, link.onClick)
            }
            data-testid={testId}
          >
            <TooltipIcon
              key={key}
              label={link.label}
              icon={link.icon}
              invalid={link.invalid}
              isOpen={isOpen}
            />
          </a>
        );
      })}
    </div>
  );
};
