import React, { ReactNode } from 'react';
import { Link } from './link';
import styles from './side-bar.module.less';
import { IOptionItem } from './side-bar';

interface ISection {
  heading: string;
  items: IOptionItem[];
  testId?: string;
}

export interface ISideBarSectionsProps {
  isOpen: boolean;
  sections: ISection[];
  onClick: (
    evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    value: string,
    label: string | ReactNode,
    onClickCallback?: (
      evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    ) => void,
  ) => void;
}

export const Sections = ({
  isOpen,
  sections,
  onClick,
}: ISideBarSectionsProps) => {
  return (
    <>
      {sections.map((section, i) => (
        <React.Fragment key={i}>
          <h5 className={styles.subtitle}>{section.heading.toUpperCase()}</h5>
          <Link
            isOpen={isOpen}
            items={section.items}
            sectionIndex={i}
            onClick={onClick}
            testId={section.testId}
          />
        </React.Fragment>
      ))}
    </>
  );
};
