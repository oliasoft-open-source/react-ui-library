import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IconType } from 'typings/common-types';
import { ISideBarProps, SideBar } from './side-bar';
import { Flex } from '../layout/flex/flex';
import { Badge } from '../badge/badge';
import svgIcon from '../../images/icons/icon-custom.svg';

const sections = [
  {
    heading: 'First Heading',
    items: [
      {
        label: 'Section A',
        value: '/path/to/something',
        onClick: () => {},
        icon: svgIcon,
      },
      {
        label: (
          <Flex alignItems="center" gap="var(--padding-xxs)">
            Section B
            <Badge small title="BETA" />
          </Flex>
        ),
        value: '/path/to/something',
        onClick: () => {},
        icon: IconType.HELP,
        isActive: true,
      },
      {
        label: 'Section C',
        value: '/path/to/something',
        onClick: () => {},
        icon: IconType.EDIT,
      },
      {
        label: 'Experimental Section',
        value: '/path/to/something',
        onClick: () => {},
        icon: IconType.LIBRARY,
        isExperimental: true,
      },
    ],
  },
  {
    heading: 'Second Heading',
    items: [
      {
        label: 'Section 1',
        value: '/path/to/something',
        onClick: () => {},
        icon: IconType.SETTINGS,
      },
      {
        label: 'Section 2',
        value: '/path/to/something',
        onClick: () => {},
        icon: IconType.STAR,
      },
      {
        label: 'Invalid Section',
        value: '/path/to/something',
        onClick: () => {},
        icon: IconType.NOTIFICATION,
        invalid: true,
      },
    ],
  },
];

export default {
  title: 'Navigation/SideBar',
  component: SideBar,
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  args: {
    options: {
      title: 'Title',
      sections,
    },
    startOpen: false,
  },
  argTypes: {
    top: { control: { type: 'text' } },
    startOpen: { control: { type: 'boolean' } },
  },
} as Meta;

const Template: StoryFn<ISideBarProps> = (args) => <SideBar {...args} />;

export const Default = Template.bind({});

export const StartOpen = Template.bind({});
StartOpen.args = { startOpen: true };
