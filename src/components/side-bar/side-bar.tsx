import React, { memo, MouseEvent, ReactNode, useState } from 'react';
import cx from 'classnames';
import isEqual from 'react-fast-compare';
import { ButtonPosition, IconType } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { Drawer } from '../drawer/drawer';
import { Sections } from './sections';
import styles from './side-bar.module.less';
import { Button } from '../button/button';

export interface IOptionItem {
  label: ReactNode;
  value: string;
  icon: ReactNode;
  onClick?: (evt: MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>) => void;
  isActive?: boolean;
  isExperimental?: boolean;
  invalid?: boolean;
  testId?: string;
}

interface IOptionSection {
  heading: string;
  items: IOptionItem[];
}

export interface ISideBarProps {
  options: {
    title: string;
    sections: IOptionSection[];
  };
  startOpen?: boolean;
  onShiftClickToggleOpen?: (
    evt: MouseEvent<HTMLButtonElement, globalThis.MouseEvent>,
  ) => void;
  top?: TStringOrNumber;
}

export const SideBar = memo(
  ({
    options,
    startOpen = false,
    onShiftClickToggleOpen = () => {},
    top,
  }: ISideBarProps) => {
    const [isOpen, setIsOpen] = useState(startOpen);

    const onClick = (
      evt: MouseEvent<any>,
      value: string,
      label: ReactNode,
      clickHandler?: (evt: MouseEvent<any>) => void,
    ) => {
      evt.preventDefault();
      if (typeof clickHandler === 'function') {
        evt.currentTarget.value = value;
        evt.currentTarget.label = label;
        clickHandler(evt);
      }
      setIsOpen(false);
    };

    const visible = options.sections.length > 0;
    return (
      <Drawer
        fixed
        open={isOpen}
        top={top}
        background="var(--color-background-sidebar)"
        closedWidth="var(--size-sidebar)"
        buttonPosition={ButtonPosition.BOTTOM}
        buttonAnimate
        button={
          visible && (
            <Button
              onClick={(
                evt: MouseEvent<HTMLButtonElement, globalThis.MouseEvent>,
              ) => {
                if (evt.shiftKey && onShiftClickToggleOpen) {
                  onShiftClickToggleOpen(evt);
                }
                setIsOpen(!isOpen);
              }}
              colored
              round
              icon={IconType.LEFT}
            />
          )
        }
      >
        <div className={cx(styles.sidebar, !isOpen ? styles.collapsed : '')}>
          {visible && (
            <div className={styles.inner}>
              <h4 className={styles.title}>{options.title}</h4>
              <Sections
                isOpen={isOpen}
                sections={options.sections}
                onClick={onClick}
              />
            </div>
          )}
        </div>
      </Drawer>
    );
  },
  (prevProps, nextProps) => isEqual(prevProps, nextProps),
);
