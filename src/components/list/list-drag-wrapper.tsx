import React, { useMemo, useState } from 'react';
import { createPortal } from 'react-dom';
import {
  DndContext,
  DragEndEvent,
  DragOverlay,
  DragStartEvent,
  UniqueIdentifier,
  closestCenter,
} from '@dnd-kit/core';
import {
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { ListRow } from './list-row/list-row';
import styles from './list.module.less';
import { IListData, IReorderData } from './list.interface';

export interface IListDragWrapperProps {
  children: React.ReactNode;
  draggable?: boolean;
  list: IListData;
  onListReorder: (args: IReorderData) => void;
}

export const ListDragWrapper = ({
  children,
  draggable,
  list,
  onListReorder,
}: IListDragWrapperProps) => {
  const [dragIndex, setDragIndex] = useState<UniqueIdentifier | null>(null);

  const handleDragStart = (event: DragStartEvent) => {
    setDragIndex(event.active.id);
  };

  const handleDragEnd = (event: DragEndEvent) => {
    setDragIndex(null);
    const { active, over } = event;
    const from = active?.id;
    const to = over?.id;
    if (to !== undefined && from !== to) {
      onListReorder({
        from: Number(from),
        to: Number(to),
      });
    }
  };

  const itemIds: UniqueIdentifier[] = useMemo(
    () => list.items.map((_, index) => index.toString()),
    [list.items],
  );

  if (!draggable) {
    return children;
  }

  return (
    <DndContext
      collisionDetection={closestCenter}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
    >
      <SortableContext items={itemIds} strategy={verticalListSortingStrategy}>
        {children}
      </SortableContext>
      {!!dragIndex &&
        createPortal(
          <DragOverlay dropAnimation={null}>
            <div className={styles.dragOverlay}>
              <ListRow
                draggable={draggable}
                item={list.items[Number(dragIndex)]}
                index={Number(dragIndex)}
              />
            </div>
          </DragOverlay>,
          document.body,
        )}
    </DndContext>
  );
};
