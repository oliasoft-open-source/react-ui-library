import React, { ReactNode, useCallback, useEffect, useRef } from 'react';
import cx from 'classnames';
import { useVirtualizer, Virtualizer } from '@tanstack/react-virtual';
import styles from './list.module.less';
import { IScrollDetails } from './list.interface';
import { IListItem } from './list-row/list-row.interface';

export interface IListVirtualScrollWrapperProps {
  items: IListItem[];
  scrollDetails: IScrollDetails;
  children: (props: {
    virtualizer?: Virtualizer<HTMLDivElement, Element>;
  }) => ReactNode;
}

export const ListVirtualScrollWrapper = ({
  items,
  scrollDetails,
  children,
}: IListVirtualScrollWrapperProps) => {
  const {
    scrollable,
    hideScrollbar,
    triggerScrollToActiveItem,
    infiniteScroll,
  } = scrollDetails;
  const listContainerRef = useRef<HTMLDivElement | null>(null);

  // Estimate height of list items based on properties of last item (first item may be shorter subheading)
  // Allows scrollbar to be accurately sized from the start
  const estimateItemHeight = () => {
    // TODO: Replace with design tokens once JS tokens available
    const LINE_HEIGHT = 20;
    const PADDING = 8;
    const BORDER_WIDTH = 1;
    let height = LINE_HEIGHT + PADDING * 2 + BORDER_WIDTH; // Height of list item with no details or metadata
    if (items.length > 0) {
      const item = items[items.length - 1];
      if (item.details) height += LINE_HEIGHT;
      if (item.metadata) height += LINE_HEIGHT;
    }
    return height;
  };

  const virtualizer = useVirtualizer({
    count: items.length,
    getScrollElement: () => listContainerRef.current,
    estimateSize: useCallback(estimateItemHeight, []),
    overscan: 5,
  });

  const findFirstActiveItemIndex = (items: Array<Record<string, any>>) =>
    items.findIndex((item: Record<string, any>) => item.active === true);

  const setScrollToActiveItem = (activeItemIndex: number) => {
    if (activeItemIndex >= 0) {
      virtualizer.scrollToIndex(activeItemIndex, {
        align: 'center',
        behavior: 'smooth',
      });
    }
  };

  useEffect(() => {
    if (triggerScrollToActiveItem) {
      const activeItemIndex = findFirstActiveItemIndex(items);
      setScrollToActiveItem(activeItemIndex);
    }
  }, [triggerScrollToActiveItem]);

  return (
    <div
      className={cx(
        styles.listContent,
        scrollable || infiniteScroll ? styles.scrollableList : '',
        hideScrollbar ? styles.hideScrollbar : '',
      )}
      ref={listContainerRef}
    >
      <div
        className={styles.virtualRows}
        style={{
          height: `${virtualizer.getTotalSize()}px`,
        }}
      >
        {children({ virtualizer })}
      </div>
    </div>
  );
};
