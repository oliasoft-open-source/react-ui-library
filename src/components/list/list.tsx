import React from 'react';
import cx from 'classnames';
import { TEmpty } from 'typings/common-type-definitions';
import { ListHeading } from './list-row/list-heading';
import { ListRow } from './list-row/list-row';
import styles from './list.module.less';
import { IListData, IReorderData, IScrollDetails } from './list.interface';
import { ListDragWrapper } from './list-drag-wrapper';
import { ListScrollWrapper } from './list-scroll-wrapper';

export interface IListProps {
  drawer?: boolean;
  list: IListData;
  bordered?: boolean;
  expanding?: boolean;
  narrow?: boolean;
  toggleNarrow?: boolean;
  onToggleNarrow?: TEmpty;
  invokeEditOnRowClick?: boolean;
  noHeader?: boolean;
  stickyHeader?: boolean;
  draggable?: boolean;
  onListReorder?: (args: IReorderData) => void;
  marginBottom?: number | string;
  height?: number | string;
  testId?: string;
  scrollDetails?: IScrollDetails;
}

export const List = ({
  list,
  bordered = false,
  expanding = false,
  narrow = false,
  toggleNarrow = false,
  onToggleNarrow = () => {},
  invokeEditOnRowClick = true,
  noHeader = false,
  stickyHeader,
  draggable = false,
  onListReorder = () => {},
  marginBottom = 0,
  height,
  testId,
  scrollDetails = {
    scrollable: false,
    hideScrollbar: false,
    triggerScrollToActiveItem: false,
    infiniteScroll: false,
  },
}: IListProps) => {
  return (
    <div
      className={cx(
        styles.list,
        narrow ? styles.narrow : '',
        bordered ? styles.bordered : '',
      )}
      data-testid={testId}
      style={{ height, marginBottom }}
      id="scrollableDiv"
    >
      {!noHeader && (
        <ListHeading
          name={list.name}
          actions={list.actions}
          toggleNarrow={toggleNarrow}
          onToggleNarrow={onToggleNarrow}
          stickyHeader={stickyHeader}
        />
      )}

      <ListDragWrapper
        draggable={draggable}
        list={list}
        onListReorder={onListReorder}
      >
        <ListScrollWrapper items={list.items} scrollDetails={scrollDetails}>
          {({ virtualizer }) =>
            virtualizer
              ? virtualizer.getVirtualItems().map((virtualRow) => (
                  <div
                    key={virtualRow.key}
                    data-index={virtualRow.index}
                    className={styles.virtualRow}
                    style={{
                      transform: `translateY(${virtualRow.start}px)`,
                    }}
                    ref={virtualizer.measureElement}
                  >
                    <ListRow
                      index={virtualRow.index}
                      draggable={draggable}
                      item={list.items[virtualRow.index]}
                      expanding={expanding}
                      invokeEditOnRowClick={invokeEditOnRowClick}
                    />
                  </div>
                ))
              : list.items.map((row, index) => (
                  <div key={index}>
                    <ListRow
                      index={index}
                      draggable={draggable}
                      item={row}
                      expanding={expanding}
                      invokeEditOnRowClick={invokeEditOnRowClick}
                    />
                  </div>
                ))
          }
        </ListScrollWrapper>
      </ListDragWrapper>
    </div>
  );
};
