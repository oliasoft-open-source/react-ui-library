import { ReactNode } from 'react';
import { TEmpty } from 'typings/common-type-definitions';
import { IListItem } from './list-row/list-row.interface';

interface ISubAction {
  label: string;
  icon: ReactNode;
  onClick: TEmpty;
}

interface IListAction {
  label: string;
  icon?: ReactNode;
  onClick?: TEmpty;
  primary?: boolean;
  subActions?: ISubAction[];
  childComponent?: ReactNode;
  disabled?: boolean;
  tooltip?: ReactNode | string;
}

export interface IListData {
  name?: ReactNode | string;
  actions?: IListAction[];
  items: IListItem[];
}

export interface IScrollDetails {
  scrollable?: boolean;
  hideScrollbar?: boolean;
  triggerScrollToActiveItem?: boolean;
  infiniteScroll?: boolean;
}

export interface IReorderData {
  from?: number;
  to?: number;
}

export interface IToggleNarrowProps {
  toggleNarrow?: boolean;
  onClickToggleNarrow?: TEmpty;
}
