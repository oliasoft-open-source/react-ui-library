import React, { Reducer, useReducer, useState } from 'react';
import { produce } from 'immer';
import { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter, Link, Routes, Route } from 'react-router-dom';
import { TEmpty } from 'typings/common-type-definitions';
import { Grid } from 'components/layout/grid/grid';
import { IListProps, List } from './list';
import { ListHeading } from './list-row/list-heading';
import { ListSubheading } from './list-row/list-subheading';
import * as storyData from './list.stories-data';
import { IReorderData } from './list.interface';

interface Item {
  name: string;
  type?: string;
  active?: boolean;
  id?: number;
}

interface ListSection {
  draggable?: boolean;
  name?: string;
  key: string;
  label: string;
  expanded: boolean;
  items: Item[];
}

interface DragDropAction {
  label: string;
  icon: React.ReactElement;
  onClick: TEmpty;
}

interface ListItem {
  id: number;
  name: string;
  actions?: DragDropAction[];
}

interface StateShape {
  [key: string]: ListSection;
}

interface HeadingClickedAction {
  type: 'headingClicked';
  payload: { headingKey: string };
}

interface ItemClickedAction {
  type: 'itemClicked';
  payload: { headingKey: string; itemIndex: number };
}

interface AddClickedAction {
  type: 'addClicked';
  payload: { headingKey: string };
}

interface DeleteClickedAction {
  type: 'deleteClicked';
  payload: { headingKey: string; itemIndex: number };
}

type Action =
  | HeadingClickedAction
  | ItemClickedAction
  | AddClickedAction
  | DeleteClickedAction;

export default {
  title: 'Basic/List',
  component: List,
  args: {
    list: storyData.list,
    noHeader: true,
  },
  parameters: {
    layout: 'fullscreen',
  },
} as Meta;

const Template: StoryFn<IListProps> = (args) => {
  return <List {...args} />;
};

export const Default = Template.bind({});

export const Actions = Template.bind({});
Actions.args = { list: storyData.listActions };

export const Menus = Template.bind({});
Menus.args = { list: storyData.listSubmenus };

export const Details = Template.bind({});
Details.args = { list: storyData.listDetails };

export const Expanding = Template.bind({});
Expanding.args = { list: storyData.listExpanding, expanding: true };

export const Header = Template.bind({});
Header.args = { list: storyData.listHeader, noHeader: false };

export const Badges = Template.bind({});
Badges.args = { list: storyData.listBadges };

export const BadgesSmall = Template.bind({});
BadgesSmall.args = { list: storyData.listBadgeSmall };

export const Nested = Template.bind({});
Nested.args = { list: storyData.listNested };

export const InvalidItems = Template.bind({});
InvalidItems.args = { list: storyData.listInvalid };

export const JSXTitles = Template.bind({});
JSXTitles.args = { list: storyData.listJsx, noHeader: false };

export const Bordered = Template.bind({});
Bordered.args = { bordered: true };

export const IconWithTooltip = Template.bind({});
IconWithTooltip.args = { list: storyData.listIconTooltip };

export const Metacount = Template.bind({});
Metacount.args = { list: storyData.listCounts };

export const InfiniteScroll = Template.bind({});
InfiniteScroll.args = {
  list: storyData.listLong,
  scrollDetails: {
    infiniteScroll: true,
  },
};
InfiniteScroll.decorators = [
  (story) => <div style={{ position: 'absolute', inset: 0 }}>{story()}</div>,
];
InfiniteScroll.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const HideScrollbar = Template.bind({});
HideScrollbar.args = {
  list: storyData.listLong,
  scrollDetails: {
    infiniteScroll: true,
    hideScrollbar: true,
  },
};
HideScrollbar.decorators = [
  (story) => <div style={{ position: 'absolute', inset: 0 }}>{story()}</div>,
];

export const InfiniteScrollDetails = Template.bind({});
InfiniteScrollDetails.args = {
  list: storyData.listLongDetails,
  scrollDetails: {
    infiniteScroll: true,
  },
};
InfiniteScrollDetails.decorators = [
  (story) => <div style={{ position: 'absolute', inset: 0 }}>{story()}</div>,
];
InfiniteScrollDetails.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const InfiniteScrollMetadata = Template.bind({});
InfiniteScrollMetadata.args = {
  list: storyData.listLongMetadata,
  scrollDetails: {
    infiniteScroll: true,
  },
};
InfiniteScrollMetadata.decorators = [
  (story) => <div style={{ position: 'absolute', inset: 0 }}>{story()}</div>,
];
InfiniteScrollMetadata.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const InfiniteScrollDragAndDrop = Template.bind({});
InfiniteScrollDragAndDrop.args = {
  list: storyData.listLong,
  draggable: true,
  scrollDetails: {
    infiniteScroll: true,
  },
};
InfiniteScrollDragAndDrop.decorators = [
  (story) => <div style={{ position: 'absolute', inset: 0 }}>{story()}</div>,
];
InfiniteScrollDragAndDrop.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const ScrollToActiveItem = () => {
  const reducer: Reducer<StateShape, Action> = (state, action) => {
    switch (action.type) {
      case 'headingClicked': {
        const { headingKey } = action.payload;
        return produce(state, (draft) => {
          const heading = draft[headingKey];
          heading.expanded = !heading.expanded;
        });
      }
      case 'itemClicked': {
        const { headingKey, itemIndex } = action.payload;
        return produce(state, (draft) => {
          Object.keys(draft).forEach((key) => {
            draft[key].items.forEach((item, i) => {
              item.active = key === headingKey && i === itemIndex;
            });
          });
        });
      }
      case 'addClicked': {
        const { headingKey } = action.payload;
        return produce(state, (draft) => {
          const heading = draft[headingKey];
          heading.expanded = true;
          heading.items.push({
            name: `${heading.label} #${heading.items.length + 1}`,
          });
        });
      }
      case 'deleteClicked': {
        const { headingKey, itemIndex } = action.payload;
        return produce(state, (draft) => {
          let { items } = draft[headingKey];
          delete items[itemIndex];
          draft[headingKey].items = items.filter((i) => i); //remove empty slots
        });
      }
      default:
        return state;
    }
  };
  const initialState = () => ({
    mammals: {
      draggable: true,
      name: 'Mammals',
      key: 'mammals',
      label: 'Mammals',
      expanded: true,
      items: [
        { name: 'Click item to scroll', type: 'Heading' },
        { name: 'Mammal #1', active: true },
        { name: 'Mammal #2' },
        { name: 'Mammal #3' },
        { name: 'Mammal #4' },
        { name: 'Mammal #5' },
        { name: 'Mammal #6' },
        { name: 'Mammal #7' },
        { name: 'Mammal #8' },
        { name: 'Mammal #9' },
        { name: 'Mammal #10' },
      ],
    },
    amphibians: {
      name: 'Amphibians (sticky header)',
      key: 'amphibians',
      label: 'Amphibians',
      expanded: true,
      items: [
        { name: 'Click item to scroll', type: 'Heading' },
        { name: 'Amphibian #1', active: true },
        { name: 'Amphibian #2' },
        { name: 'Amphibian #3' },
        { name: 'Amphibian #4' },
        { name: 'Amphibian #5' },
        { name: 'Amphibian #6' },
        { name: 'Amphibian #7' },
        { name: 'Amphibian #8' },
        { name: 'Amphibian #9' },
        { name: 'Amphibian #10' },
      ],
    },
  });
  const [state, dispatch] = useReducer(reducer, {}, () => initialState());
  const [triggerScroll, setTriggerScroll] = useState(false);
  const onClickItem = (headingKey: string, itemIndex: number) => {
    setTriggerScroll(true);
    dispatch({
      type: 'itemClicked',
      payload: { headingKey, itemIndex },
    });
    // will execute after stack is cleared
    setTimeout(() => setTriggerScroll(false), 0);
  };
  const onAddItem = (headingKey: string) => {
    dispatch({ type: 'addClicked', payload: { headingKey } });
  };
  const onDeleteItem = (headingKey: string, itemIndex: number) =>
    dispatch({
      type: 'deleteClicked',
      payload: { headingKey, itemIndex },
    });
  const { mammals, amphibians } = state;
  const hydrateList = (list: any) => ({
    ...list,
    actions: [
      {
        label: 'Add',
        primary: true,
        icon: 'add',
        onClick: () => onAddItem(list.key),
      },
    ],
    items: list.items.map((item: any, itemIndex: number) => {
      if (item.type == 'Heading') {
        return {
          ...item,
          id: itemIndex,
        };
      }
      return {
        ...item,
        id: itemIndex,
        onClick: () => onClickItem(list.key, itemIndex),
        actions: [
          {
            label: 'Delete',
            icon: 'delete',
            onClick: () => onDeleteItem(list.key, itemIndex),
          },
        ],
      };
    }),
  });
  const height = 300;
  return (
    <Grid columns="1fr 1fr" gap>
      <List
        list={hydrateList(mammals)}
        bordered
        height={height}
        scrollDetails={{
          infiniteScroll: true,
          triggerScrollToActiveItem: triggerScroll,
        }}
      />
      <List
        list={hydrateList(amphibians)}
        bordered
        stickyHeader
        height={height}
        scrollDetails={{
          infiniteScroll: true,
          triggerScrollToActiveItem: triggerScroll,
        }}
      />
    </Grid>
  );
};

export const DeprecatedScroll = Template.bind({});
DeprecatedScroll.args = {
  list: storyData.listLong,
  scrollDetails: {
    scrollable: true,
  },
};
DeprecatedScroll.decorators = [
  (story) => <div style={{ position: 'absolute', inset: 0 }}>{story()}</div>,
];
DeprecatedScroll.parameters = {
  docs: {
    description: {
      story:
        'Adds an internal scrollbar to the `List`, which will fill its parent height unless a `height` is set. The scrollbar can be toggled using `hideScrollbar`.',
    },
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const HeadingsManaged = () => {
  const reducer: Reducer<StateShape, Action> = (state, action) => {
    switch (action.type) {
      case 'headingClicked': {
        const { headingKey } = action.payload;
        return produce(state, (draft) => {
          const heading = draft[headingKey];
          heading.expanded = !heading.expanded;
        });
      }
      case 'itemClicked': {
        const { headingKey, itemIndex } = action.payload;
        return produce(state, (draft) => {
          Object.keys(draft).forEach((key) => {
            draft[key].items.forEach((item, i) => {
              item.active = key === headingKey && i === itemIndex;
            });
          });
        });
      }
      case 'addClicked': {
        const { headingKey } = action.payload;
        return produce(state, (draft) => {
          const heading = draft[headingKey];
          heading.expanded = true;
          heading.items.push({
            name: `${heading.label} #${heading.items.length + 1}`,
          });
        });
      }
      case 'deleteClicked': {
        const { headingKey, itemIndex } = action.payload;
        return produce(state, (draft) => {
          let { items } = draft[headingKey];
          delete items[itemIndex];
          draft[headingKey].items = items.filter((i) => i); //remove empty slots
        });
      }
      default:
        return state;
    }
  };

  const initialState: () => StateShape = () => ({
    mammals: {
      key: 'mammals',
      label: 'Mammals',
      expanded: true,
      items: [{ name: 'Aardvark', active: true }, { name: 'Kangaroo' }],
    },
    amphibians: {
      key: 'amphibians',
      label: 'Amphibians',
      expanded: true,
      items: [{ name: 'Frog' }],
    },
  });

  const [state, dispatch] = useReducer(reducer, initialState());
  const onClickHeading = (headingKey: string) =>
    dispatch({ type: 'headingClicked', payload: { headingKey } });
  const onClickItem = (headingKey: string, itemIndex: number) =>
    dispatch({
      type: 'itemClicked',
      payload: { headingKey, itemIndex },
    });
  const onAddItem = (headingKey: string) =>
    dispatch({ type: 'addClicked', payload: { headingKey } });
  const onDeleteItem = (headingKey: string, itemIndex: number) =>
    dispatch({
      type: 'deleteClicked',
      payload: { headingKey, itemIndex },
    });
  const { mammals, amphibians } = state;
  const hydrateHeading = (list: ListSection) => ({
    name: list.label,
    metaCount: list.items.length,
    expanded: list.expanded,
    onClick: () => onClickHeading(list.key),
    actions: [
      {
        label: 'Add',
        primary: true,
        icon: 'add',
        onClick: () => onAddItem(list.key),
      },
    ],
  });
  const hydrateList = (list: ListSection) => ({
    ...list,
    items: list.items.map((item, itemIndex) => ({
      ...item,
      id: itemIndex,
      onClick: () => onClickItem(list.key, itemIndex),
      actions: [
        {
          label: 'Delete',
          icon: 'delete',
          onClick: () => onDeleteItem(list.key, itemIndex),
        },
      ],
    })),
  });
  return (
    <>
      <ListHeading name="Animals" />
      <ListSubheading item={hydrateHeading(mammals)} />
      {mammals.expanded && <List noHeader list={hydrateList(mammals)} />}
      <ListSubheading item={hydrateHeading(amphibians)} />
      {amphibians.expanded && <List noHeader list={hydrateList(amphibians)} />}
    </>
  );
};

export const ManagedDragAndDrop = () => {
  const arraymove = (
    arr: ListItem[],
    from?: number,
    to?: number,
  ): ListItem[] => {
    if ((from && isNaN(from)) || (to && isNaN(to)) || from === to) {
      return arr;
    }
    const copy = [...arr];
    if (typeof to === 'number' && typeof from === 'number') {
      copy.splice(to, 0, copy.splice(from, 1)[0]);
    }
    return copy;
  };
  const beautifulDnD = {
    name: 'List Name',
    draggable: true,
    items: [...Array(10)].map((_, index) => ({
      id: index,
      name: `Aardvark #${index + 1}`,
    })),
  };
  const [reorderedItems, setReorderedItems] = useState(beautifulDnD.items);
  const onListReorder = (reorderData: IReorderData) => {
    const { to, from } = reorderData;
    setReorderedItems(arraymove(reorderedItems as any, from, to) as any);
  };
  return (
    <List
      list={{ ...beautifulDnD, items: reorderedItems }}
      draggable
      onListReorder={onListReorder}
    />
  );
};
ManagedDragAndDrop.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const AsRouterLink = () => {
  const list = {
    items: [
      {
        id: 1,
        name: 'Dashboard',
        component: Link,
        url: '/dashboard',
      },
      {
        id: 2,
        name: 'Settings',
        component: Link,
        url: '/settings',
      },
    ],
  };
  return (
    <MemoryRouter>
      <List list={list} noHeader />
      <Routes>
        <Route path="/dashboard" element={<div>Dashboard</div>} />
        <Route path="/settings" element={<div>Settings</div>} />
      </Routes>
    </MemoryRouter>
  );
};
