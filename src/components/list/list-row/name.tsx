import React from 'react';
import cx from 'classnames';
import { Icon } from '../../icon/icon';
import styles from '../list.module.less';
import { Tooltip } from '../../tooltip/tooltip';
import { INameProps } from './list-row.interface';

export const Name = ({ name, icon, testId }: INameProps) => {
  const newIcon = icon && (
    <Icon icon={icon.icon} color={icon.color || '#db2828'} />
  );

  return (
    <span className={cx(styles.name, styles.bold)} data-testid={testId}>
      {name}
      {icon && icon.tooltip && icon.tooltip.text ? (
        <span className={styles.iconTooltipMargin}>
          <Tooltip text={icon.tooltip.text as string} maxWidth="350px">
            {newIcon}
          </Tooltip>
        </span>
      ) : (
        icon &&
        icon.icon && <span className={styles.iconTooltipMargin}>{newIcon}</span>
      )}
    </span>
  );
};

export default Name;
