import React from 'react';
import { Badge } from '../../badge/badge';
import styles from '../list.module.less';
import { ILabelProps } from './list-row.interface';

export const Label = ({ label }: ILabelProps) => {
  return label ? (
    <span className={styles.label}>
      <Badge color={label.color} title={label.value} small={label.small} />
    </span>
  ) : null;
};
