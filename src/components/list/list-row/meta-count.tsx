import React from 'react';
import { Badge } from '../../badge/badge';
import { IMetaCountProps } from './list-row.interface';

export const MetaCount = ({ item }: IMetaCountProps) => {
  return item.metaCount !== undefined ? (
    <Badge color="rgba(0,0,0,0.25)" title={item.metaCount} />
  ) : null;
};
