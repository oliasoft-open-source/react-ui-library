import React, { forwardRef, useContext } from 'react';
import cx from 'classnames';
import { IconType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import { Icon } from '../../icon/icon';
import { Actions } from '../../actions/actions';
import styles from '../list.module.less';
import { Name } from './name';
import { IListSubheadingItem } from './list-row.interface';
import { MetaCount } from './meta-count';

export interface IListSubheadingProps {
  item: IListSubheadingItem;
  index?: number;
}

export const ListSubheading = forwardRef<
  (HTMLDivElement | null)[],
  IListSubheadingProps
>(({ item, index }, listRowRefs) => {
  const disabledContext = useContext(DisabledContext);

  const { actions, disabled, expanded, onClick, title, name, icon, testId } =
    item;

  return (
    <div
      ref={(el) => {
        if (
          listRowRefs &&
          'current' in listRowRefs &&
          Array.isArray(listRowRefs.current) &&
          index !== undefined
        ) {
          listRowRefs.current[index] = el;
        }
      }}
      data-testid={testId ?? null}
      className={cx(
        styles.item,
        styles.heading,
        disabled || disabledContext ? styles.disabled : '',
        onClick ? styles.action : '',
      )}
      onClick={(evt) => {
        if (onClick) {
          onClick(evt);
        }
      }}
      title={title}
    >
      <div>
        <div className={styles.itemHeader}>
          {typeof expanded === 'boolean' && (
            <div className={cx(styles.expandIcon, expanded && styles.expanded)}>
              <Icon icon={IconType.CHEVRON_RIGHT} />
            </div>
          )}
          <Name name={name} icon={icon} />

          <div className={styles.right}>
            <MetaCount item={item} />
            {!(disabled || disabledContext) && actions && (
              <div className={styles.actions}>
                <Actions actions={item.actions ?? []} />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
});
