import {
  MouseEvent,
  MutableRefObject,
  ReactElement,
  ReactNode,
  Ref,
} from 'react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { IAction } from '../../actions/actions.interface';

export interface ILabel {
  color: string;
  small?: boolean;
  value: TStringOrNumber | ReactNode;
}

export interface INameProps {
  name: ReactNode;
  icon?: {
    icon: ReactNode;
    color?: string;
    tooltip?: {
      text: ReactNode;
    };
  };
  testId?: string;
}

export interface IMetaCountProps {
  item: {
    metaCount?: number;
  };
}

export interface IMetaContentItem {
  name?: TStringOrNumber | ReactNode;
  icon?: {
    icon: ReactNode;
    color?: string;
    tooltip?: {
      text: string | ReactNode;
    };
  };
  details?: string | ReactNode;
  metadata?: string | ReactNode;
  invalid?: boolean;
  testId?: string;
}

export interface IMetaContentProps {
  item: IMetaContentItem;
}

export interface IListSubheadingItem extends Partial<IMetaContentItem> {
  actions?: IAction[];
  disabled?: boolean;
  expanded?: boolean;
  onClick?: (evt: MouseEvent<HTMLDivElement>) => void;
  title?: string;
  testId?: string;
  metaCount?: number;
}

export interface IListItem {
  name?: string | ReactElement;
  expanded?: boolean;
  cursor?: string;
  url?: string;
  active?: boolean;
  disabled?: boolean;
  onClick?: (evt: any) => void;
  title?: string;
  details?: string;
  metadata?: string;
  testId?: string;
  level?: number;
  label?: ILabel;
  actions?: IAction[];
  id?: TStringOrNumber;
  type?: string;
  metaCount?: number;
  component?: React.ElementType;
}

export interface IDraggableProps {
  innerRef?: Ref<HTMLAnchorElement>;
  draggableProps?: any;
  dragHandleProps?: any;
  style?: any;
}

export interface IListRowProps {
  item: IListItem;
  index: number;
  expanding?: boolean;
  invokeEditOnRowClick?: boolean;
  draggable?: boolean;
  ref: MutableRefObject<HTMLDivElement>[];
  onListReorder?: (args: { from?: number; to?: number }) => void;
}

export interface ILabelProps {
  label?: ILabel;
}

export interface IItemContentProps {
  item: {
    active?: boolean;
    content?: ReactNode;
  };
  expanding?: boolean;
}
