import React from 'react';
import styles from '../list.module.less';
import { IItemContentProps } from './list-row.interface';

export const ItemContent = ({ item, expanding }: IItemContentProps) => {
  const onClick = (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    evt.preventDefault();
    evt.stopPropagation();
  };

  return expanding && item.active && item.content ? (
    <div className={styles.itemContent} onClick={onClick}>
      {item.content}
    </div>
  ) : null;
};
