import React, { forwardRef, useContext } from 'react';
import cx from 'classnames';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { IconType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import { Actions } from '../../actions/actions';
import { Label } from './label';
import { MetaContent } from './meta-content';
import { MetaCount } from './meta-count';
import { ItemContent } from './item-content';
import { Icon } from '../../icon/icon';
import { ListSubheading } from './list-subheading';
import styles from '../list.module.less';
import { IListItem, IListRowProps } from './list-row.interface';

export const ListRow = forwardRef<(HTMLDivElement | null)[], IListRowProps>(
  (
    { item, index, expanding, invokeEditOnRowClick, draggable },
    listRowRefs,
  ) => {
    const disabledContext = useContext(DisabledContext);

    const listElement = (
      index: number,
      item: IListItem,
      style?: React.CSSProperties,
      attributes?: any,
      listeners?: any,
      setNodeRef?: any,
    ) => {
      const edit =
        item.actions &&
        item.actions.find(
          (a) => a.label && String(a.label)?.toLowerCase() === 'edit',
        );
      const hasOnClick: boolean = !!(edit || item.url || item.onClick);
      const indent = item.level && item.level > 1 ? (item.level - 1) * 20 : 0;
      const Component = item.component || 'a';
      return (
        <Component
          href={item.url}
          to={item.url}
          ref={setNodeRef}
          style={style}
          className={cx(
            styles.item,
            item.active ? styles.active : '',
            item.disabled || disabledContext ? styles.disabled : '',
            hasOnClick ? styles.action : '',
          )}
          onClick={(evt: React.MouseEvent) => {
            if (invokeEditOnRowClick && edit) {
              //If invokeEditOnRowClick (default) and an edit action exists
              //Then clicking the list row will also invoke the edit action
              if (edit.onClick) edit.onClick(evt, item.id);
            }
            if (item.onClick) {
              return item.onClick(evt);
            }
          }}
          key={index}
          data-id={index}
          title={item.title}
          data-testid={item.testId}
        >
          <div
            ref={(el) => {
              if (
                listRowRefs &&
                'current' in listRowRefs &&
                Array.isArray(listRowRefs.current) &&
                index !== undefined
              ) {
                listRowRefs.current[index] = el;
              }
            }}
            style={{ paddingLeft: indent }}
          >
            <div className={styles.itemHeader}>
              {draggable && (
                <div className={styles.drag} {...attributes} {...listeners}>
                  <Icon icon={IconType.DRAG} />
                </div>
              )}
              {item.level && item.level > 0 ? (
                <div className={styles.indentIcon}>
                  <Icon icon={IconType.INDENT} />
                </div>
              ) : null}
              <Label label={item.label} />
              <MetaContent item={item} />
              <div className={styles.right}>
                <MetaCount item={item} />
                {!(item.disabled || disabledContext) && item.actions && (
                  <div className={styles.actions}>
                    <Actions actions={item.actions} />
                  </div>
                )}
              </div>
            </div>
            <ItemContent item={item} expanding={expanding} />
          </div>
        </Component>
      );
    };

    const listElementWithDrag = (index: number, item: IListItem) => {
      const {
        attributes,
        listeners,
        setNodeRef,
        transform,
        transition,
        isDragging,
      } = useSortable({
        id: index.toString(),
        animateLayoutChanges: () => false,
      });

      const style = {
        transform: CSS.Translate.toString(transform),
        transition,
        opacity: isDragging ? 0 : 1,
      };

      return listElement(index, item, style, attributes, listeners, setNodeRef);
    };

    const isSubheading = item.type === 'Heading';

    return isSubheading ? (
      <ListSubheading ref={listRowRefs} item={item} index={index} key={index} />
    ) : draggable ? (
      listElementWithDrag(index, item)
    ) : (
      listElement(index, item)
    );
  },
);
