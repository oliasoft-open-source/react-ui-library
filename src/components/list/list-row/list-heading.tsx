import React, { forwardRef, ReactNode } from 'react';
import cx from 'classnames';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { ToggleNarrow } from '../toggle-narrow';
import { Heading } from '../../heading/heading';
import { Actions } from '../../actions/actions';
import styles from '../list.module.less';

export interface IListHeadingProps {
  name: TStringOrNumber | ReactNode;
  actions?: any[];
  toggleNarrow?: boolean;
  onToggleNarrow?: TEmpty;
  stickyHeader?: boolean;
}

export const ListHeading = forwardRef<HTMLDivElement, IListHeadingProps>(
  (
    {
      name,
      actions = [],
      toggleNarrow = false,
      onToggleNarrow = () => {},
      stickyHeader,
    },
    listHeadingRef,
  ) => {
    return (
      <div
        ref={listHeadingRef}
        className={cx(styles.header, stickyHeader ? styles.stickyHeader : '')}
      >
        <ToggleNarrow
          toggleNarrow={toggleNarrow}
          onClickToggleNarrow={onToggleNarrow}
        />
        {name && (
          <div className={styles.headerTitle}>
            <Heading top marginBottom={0}>
              {name}
            </Heading>
          </div>
        )}
        <div className={styles.right}>
          <div className={styles.actions}>
            <Actions actions={actions} />
          </div>
        </div>
      </div>
    );
  },
);
