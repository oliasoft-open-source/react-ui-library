import React from 'react';
import styles from '../list.module.less';
import { Badge } from '../../badge/badge';
import { Name } from './name';
import { IMetaContentProps } from './list-row.interface';

export const MetaContent = ({ item }: IMetaContentProps) => {
  const { name, icon, details, metadata, invalid, testId } = item;

  let renderedName = (
    <Name name={name} icon={icon} testId={testId && `${testId}-name`} />
  );
  if (invalid) {
    renderedName = (
      <Badge small margin="-2px" title="!">
        {renderedName}
      </Badge>
    );
  }
  return (
    <span className={styles.title}>
      {renderedName}
      {details && (
        <span
          className={styles.details}
          data-testid={testId && `${testId}-details`}
        >
          {details}
        </span>
      )}
      {metadata && (
        <span
          className={styles.metadata}
          data-testid={testId && `${testId}-metadata`}
        >
          {metadata}
        </span>
      )}
    </span>
  );
};
