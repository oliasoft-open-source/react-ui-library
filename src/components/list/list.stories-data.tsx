import React from 'react';
import { IconType, TriggerType } from 'typings/common-types';
import { Flex } from '../layout/flex/flex';
import { Menu } from '../menu/menu';
import { IListItem } from './list-row/list-row.interface';
import { Icon } from '../icon/icon';
import { MenuType } from '../menu/types';

export const items: IListItem[] = [
  {
    id: 1,
    name: 'Aardvark',
  },
  {
    id: 2,
    name: 'Kangaroo',
    active: true,
  },
  {
    id: 3,
    name: 'Jaguar',
    disabled: true,
  },
];

const itemsSubheadings: IListItem[] = [
  {
    id: 1,
    name: 'Marsupials',
    type: 'Heading',
    metaCount: 1,
  },
  {
    id: 2,
    name: 'Aardvark',
  },
  {
    id: 3,
    name: 'Kangaroo',
    active: true,
  },
  {
    id: 4,
    name: 'Mammals',
    type: 'Heading',
    metaCount: 1,
  },
  {
    id: 5,
    name: 'Jaguar',
  },
];

const itemsNested: IListItem[] = [
  {
    id: 1,
    name: 'Aardvark',
  },
  {
    id: 2,
    name: 'Kangaroo',
    active: true,
  },
  {
    id: 3,
    name: 'Grey',
    level: 1,
  },
  {
    id: 4,
    name: 'Eastern',
    level: 2,
  },
  {
    id: 5,
    name: 'Macropus giganteus giganteus',
    title: 'Macropus giganteus giganteus',
    level: 3,
  },
  {
    id: 6,
    name: 'Macropus giganteus tasmaniensis',
    title: 'Macropus giganteus tasmaniensis',
    level: 3,
  },
  {
    id: 7,
    name: 'Western',
    level: 2,
  },
  {
    id: 8,
    name: 'Red',
    level: 1,
  },
  {
    id: 9,
    name: 'Jaguar',
  },
];

export const list = {
  items,
};

export const listHeader = {
  name: 'Animals',
  actions: [
    {
      label: 'More',
      subActions: [
        {
          label: 'Item',
          icon: 'up',
          onClick: () => {},
        },
        {
          label: 'Item',
          icon: 'down',
          onClick: () => {},
        },
      ],
    },

    {
      label: 'Add',
      primary: true,
      icon: 'add',
      onClick: () => {},
    },
  ],
  items,
};

export const listSubheadings = {
  ...listHeader,
  items: itemsSubheadings,
};

export const listJsx = {
  ...listHeader,
  name: (
    <Flex gap="var(--padding-xs)" alignItems="center">
      Animals
      <Icon icon={IconType.STAR} />
    </Flex>
  ),
  items: items.map((item) => ({
    ...item,
    name: (
      <Flex gap="var(--padding-xs)" alignItems="center">
        {item.name}
        <Icon icon={IconType.STAR} />
      </Flex>
    ),
  })),
};

export const listSubcomponents = {
  items: items.map((item) => ({
    ...item,
    actions: [
      {
        childComponent: (
          <Menu
            menu={{
              trigger: TriggerType.COMPONENT,
              component: <Icon icon={IconType.MENU} />,
              placement: 'bottom-end',
              sections: [
                {
                  type: MenuType.OPTION,
                  label: 'Item',
                  onClick: () => {},
                },
                {
                  type: MenuType.OPTION,
                  label: 'Item',
                  onClick: () => {},
                },
              ],
            }}
          />
        ),
      },
    ],
  })),
};

export const listSubmenus = {
  items: items.map((item) => ({
    ...item,
    actions: [
      {
        label: 'More',
        subActions: [
          {
            label: 'Move Up',
            icon: 'up',
            onClick: () => {},
          },
          {
            label: 'Move Down',
            icon: 'down',
            onClick: () => {},
          },
        ],
      },
    ],
  })),
};

export const listBadges = {
  items: items.map((item, index) => ({
    ...item,
    label: {
      value: String(index + 1),
      color: 'var(--color-text-info)',
    },
  })),
};

export const listBadgeSmall = {
  items: items.map((item) => ({
    ...item,
    label: {
      value: <>&nbsp;</>,
      color: 'var(--color-text-info)',
      small: true,
    },
  })),
};

export const listSubheadingsBadges = {
  ...listSubheadings,
  items: listSubheadings.items.map((item, index) => ({
    ...item,
    label: {
      value: String(index + 1),
      color: 'var(--color-text-info)',
    },
  })),
};

export const listActions = {
  items: items.map((item) => ({
    ...item,
    actions: [
      {
        label: 'Delete',
        icon: 'delete',
        onClick: () => {},
      },
    ],
  })),
};

export const listDetails = {
  items: items.map((item, itemIx) => ({
    ...item,
    details: 'Description goes here',
    metadata: '2019-05-03 09:03:51',
    testId: `story-list-item-${itemIx}`,
  })),
};

export const listExpanding = {
  items: items.map((item) => ({
    ...item,
    content: <div>Example content element (for show/hide).</div>,
  })),
};

export const listNested = {
  items: itemsNested,
};

export const listInvalid = {
  items: [
    ...items,
    {
      id: 4,
      name: 'Dodo',
      invalid: true,
    },
  ],
};

export const listIconTooltip = {
  items: items.map((item) => ({
    ...item,
    icon: {
      icon: 'success',
      color: 'var(--color-text-success)',
      tooltip: {
        text: <>Tooltip goes here</>,
      },
    },
  })),
};

export const listCounts = {
  items: items.map((item) => ({
    ...item,
    metaCount: 3,
  })),
};

export const listLong = {
  name: 'Animals',
  items: [...Array(500)].map((_, index) => ({
    id: index,
    name: `Aardvark #${index + 1}`,
  })),
};

export const listLongDetails = {
  name: 'Animals',
  items: [...Array(500)].map((_, index) => ({
    id: index,
    name: `Aardvark #${index + 1}`,
    details: 'Description goes here',
  })),
};

export const listLongMetadata = {
  name: 'Animals',
  items: [...Array(500)].map((_, index) => ({
    id: index,
    name: `Aardvark #${index + 1}`,
    details: 'Description goes here',
    metadata: '2019-05-03 09:03:51',
  })),
};
