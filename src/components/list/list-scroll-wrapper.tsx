import React, { ReactNode } from 'react';
import cx from 'classnames';
import { Virtualizer } from '@tanstack/react-virtual';
import styles from './list.module.less';
import { IScrollDetails } from './list.interface';
import { IListItem } from './list-row/list-row.interface';
import { ListVirtualScrollWrapper } from './list-virtual-scroll-wrapper';

export interface IListScrollWrapperProps {
  items: IListItem[];
  scrollDetails: IScrollDetails;
  children: (props: {
    virtualizer?: Virtualizer<HTMLDivElement, Element>;
  }) => ReactNode;
}

export const ListScrollWrapper = ({
  items,
  scrollDetails,
  children,
}: IListScrollWrapperProps) => {
  const { scrollable, hideScrollbar, infiniteScroll } = scrollDetails;

  return infiniteScroll ? (
    <ListVirtualScrollWrapper items={items} scrollDetails={scrollDetails}>
      {({ virtualizer }) => children({ virtualizer })}
    </ListVirtualScrollWrapper>
  ) : (
    <div
      className={cx(
        styles.listContent,
        scrollable ? styles.scrollableList : '',
        hideScrollbar ? styles.hideScrollbar : '',
      )}
    >
      <div className={styles.virtualRows}>{children({})}</div>
    </div>
  );
};
