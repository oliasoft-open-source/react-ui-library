import React from 'react';
import { IconType } from 'typings/common-types';
import { Icon } from '../icon/icon';
import styles from './list.module.less';
import { IToggleNarrowProps } from './list.interface';

export const ToggleNarrow = ({
  toggleNarrow,
  onClickToggleNarrow,
}: IToggleNarrowProps) => {
  return toggleNarrow ? (
    <a className={styles.toggleNarrow} onClick={onClickToggleNarrow}>
      <Icon icon={IconType.CHEVRON_LEFT} />
    </a>
  ) : null;
};
