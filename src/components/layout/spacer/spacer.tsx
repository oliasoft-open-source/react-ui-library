import React from 'react';
import {
  TStringOrNumber,
  TStringNumberNull,
} from 'typings/common-type-definitions';

export interface ISpacerProps {
  height?: TStringOrNumber;
  width?: TStringNumberNull;
  flex?: boolean;
}

export const Spacer = ({
  height = 'var(--padding)',
  width = null,
  flex = false,
}: ISpacerProps) => (
  <div
    style={{
      height,
      width: width || 'auto',
      flexGrow: flex ? 1 : 0,
      flexShrink: 0,
      display: width ? 'inline-block' : 'block',
    }}
  />
);
