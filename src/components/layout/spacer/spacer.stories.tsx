import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ISpacerProps, Spacer } from './spacer';
import { Flex } from '../flex/flex';
import { Button } from '../../button/button';

export default {
  title: 'Layout/Spacer',
  component: Spacer,
  argTypes: { width: { control: 'text' }, height: { control: 'text' } },
} as Meta;

const Template: StoryFn<ISpacerProps> = (args: ISpacerProps) => (
  <Spacer {...args} />
);

export const Default = Template.bind({});
Default.decorators = [
  (story) => (
    <>
      <Button label="Button" />
      {story()}
      <Button label="Button" />
    </>
  ),
];

export const Height = Template.bind({});
Height.args = { height: 'var(--padding-xxs)' };
Height.decorators = [
  (story) => (
    <>
      <Button label="Button" />
      {story()}
      <Button label="Button" />
    </>
  ),
];

export const Width = Template.bind({});
Width.args = { width: 'var(--padding)' };
Width.decorators = [
  (story) => (
    <Flex>
      <Button label="Button" />
      {story()}
      <Button label="Button" />
    </Flex>
  ),
];
