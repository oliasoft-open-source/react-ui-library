import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Column } from './column/column';
import { Row } from './row/row';
import { Heading } from '../heading/heading';

interface TemplateArgs {
  height?: string;
  wrap?: boolean;
  width?: string;
  padding?: boolean | string;
  borderRight?: boolean;
  scroll?: boolean;
  showScrollbar?: boolean;
  widthTablet?: string;
  widthMobile?: string;
  borderLeft?: boolean;
}

export default {
  title: 'Layout/Columns & Rows',
  component: Column,
  parameters: {
    layout: 'fullscreen',
  },
  args: {
    width: '50%',
    padding: true,
    borderRight: true,
  },
  argTypes: {
    width: { control: { type: 'text' } },
    padding: { control: { type: 'text' } },
    borderLeft: { control: { type: 'boolean' } },
    borderRight: { control: { type: 'boolean' } },
  },
} as Meta;

const Template: StoryFn<TemplateArgs> = (args) => {
  const { height, wrap, ...columnArgs } = args;
  return (
    <Row spacing={0} height={height} wrap={wrap}>
      <Column {...columnArgs}>
        <Heading top>Column</Heading>
        {'Example content goes here lorem ipsum. '.repeat(20)}
      </Column>
      <Column padding background="var(--color-background)" />
    </Row>
  );
};

export const Default = Template.bind({});

export const NoPadding = Template.bind({});
NoPadding.args = {
  padding: false,
};

export const FixedWidth = Template.bind({});
FixedWidth.args = {
  width: '400px',
};

export const Scroll = Template.bind({});
Scroll.args = {
  scroll: true,
  height: '200px',
};

export const HideScrollbar = Template.bind({});
HideScrollbar.args = {
  scroll: true,
  showScrollbar: false,
  height: '200px',
};

export const ResponsiveWidth = Template.bind({});
ResponsiveWidth.args = {
  wrap: true,
  widthTablet: '50%',
  widthMobile: '100%',
  borderRight: false,
};

export const TestNumberSpacing = () => (
  <Row spacing={100}>
    <Column>
      <Heading top>Column</Heading>
      {'Example content goes here lorem ipsum. '.repeat(20)}
    </Column>
    <Column>
      <Heading top>Column</Heading>
      {'Example content goes here lorem ipsum. '.repeat(20)}
    </Column>
  </Row>
);
