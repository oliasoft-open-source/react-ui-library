import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Page } from './page/page';
import { TopBar } from '../top-bar/top-bar';
import { SideBar } from '../side-bar/side-bar';
import { Heading } from '../heading/heading';
import { PrintHeader } from './print-header/print-header';
import { placeholder } from './placeholder'; // Assuming placeholder has default export.
import LogoSVG from '../../images/logo.svg';

export default {
  title: 'Layout/Page',
  component: Page,
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  argTypes: {
    top: { control: 'text' },
    left: { control: 'text' },
    padding: { control: { type: 'boolean' } },
  },
} as Meta;

export const Default: StoryFn = () => (
  <>
    <TopBar />
    <SideBar options={{ title: '', sections: [] }} />
    <Page>
      <Heading top>Heading</Heading>
      {placeholder}
    </Page>
  </>
);

export const NoPadding = () => (
  <>
    <TopBar />
    <SideBar options={{ title: '', sections: [] }} />
    <Page padding={false}>
      <Heading top>Heading</Heading>
      {placeholder}
    </Page>
  </>
);

export const NoScroll = () => (
  <>
    <TopBar />
    <SideBar options={{ title: '', sections: [] }} />
    <Page scroll={false}>
      <Heading top>Heading</Heading>
      {placeholder}
    </Page>
  </>
);

export const NoTopBar = () => (
  <>
    <SideBar options={{ title: '', sections: [] }} />
    <Page top={0}>
      <Heading top>Heading</Heading>
      {placeholder}
    </Page>
  </>
);

export const NoSideBar = () => (
  <>
    <TopBar />
    <Page left={0}>
      <Heading top>Heading</Heading>
      {placeholder}
    </Page>
  </>
);

export const WithPrintHeader = () => (
  <>
    <PrintHeader logo={LogoSVG} logoWidth="60px" alt="company logo" />
    <TopBar />
    <SideBar options={{ title: '', sections: [] }} />
    <Page>
      <Heading top>Heading</Heading>
      {placeholder}
    </Page>
  </>
);
