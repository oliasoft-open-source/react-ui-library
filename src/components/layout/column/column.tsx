import React from 'react';
import { StyledColumn as ImportedStyledColumn } from './styles';

export interface ILayoutColumnProps {
  background?: string;
  borderLeft?: boolean | string;
  borderRight?: boolean | string;
  children?: React.ReactNode;
  flex?: boolean;
  flexbox?: boolean;
  padding?: boolean | string;
  scroll?: boolean;
  showScrollbar?: boolean;
  spacing?: number | string;
  width?: number | string;
  widthMobile?: number | string;
  widthTablet?: number | string;
  testId?: string;
}

export const Column = ({
  background = 'transparent',
  borderLeft,
  borderRight,
  children = null,
  flex = true,
  flexbox = false,
  padding = false,
  scroll = false,
  showScrollbar = true,
  spacing = 'var(--padding)',
  width,
  widthMobile,
  widthTablet,
  testId,
}: ILayoutColumnProps) => {
  const getWidthString = (w?: number | string): string | undefined => {
    return typeof w === 'string'
      ? w
      : typeof w === 'number'
      ? `${w}px`
      : undefined;
  };

  const StyledColumn = ImportedStyledColumn as any;

  return (
    <StyledColumn
      $background={background}
      $borderLeft={borderLeft}
      $borderRight={borderRight}
      $flex={flex}
      $flexbox={flexbox}
      $padding={padding}
      $scroll={scroll}
      $showScrollbar={showScrollbar}
      $spacing={spacing}
      $width={getWidthString(width)}
      $widthTablet={getWidthString(widthTablet)}
      $widthMobile={getWidthString(widthMobile)}
    >
      <div className="inner" data-testid={testId}>
        {children}
      </div>
    </StyledColumn>
  );
};
