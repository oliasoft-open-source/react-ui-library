import styled, { css } from 'styled-components';

const noScrollbars = css`
  scrollbar-width: none;
  -ms-overflow-style: none;
  &::-webkit-scrollbar {
    display: none;
  }
`;

// Use transient props to avoid passing them to the DOM (https://styled-components.com/docs/api#transient-props)

export interface ILayoutStyledColumnProps {
  $width?: string;
  $flex?: boolean;
  $scroll?: boolean;
  $background?: string;
  $borderLeft?: boolean | string;
  $borderRight?: boolean | string;
  $spacing?: number;
  $showScrollbar?: boolean;
  $widthTablet?: string;
  $widthMobile?: string;
  $flexbox?: boolean;
  $padding?: boolean | string;
}

export const StyledColumn = styled.div<ILayoutStyledColumnProps>`
  position: relative; // for react-laag
  display: flex;
  flex-direction: column;
  min-width: 0;
  flex-basis: ${(p) => p.$width || 'auto'};
  flex-grow: ${(p) => (!p.$width && p.$flex ? '1' : '0')};
  flex-shrink: ${(p) => (p.$width || !p.$flex ? '0' : '1')};
  overflow: ${(p) => (p.$scroll ? 'auto' : 'initial')};
  background: ${(p) => p.$background};
  border-left: ${(p) =>
    p.$borderLeft === true ? '1px solid var(--color-border)' : p.$borderLeft};
  border-right: ${(p) =>
    p.$borderRight === true ? '1px solid var(--color-border)' : p.$borderRight};
  padding: ${(p) => (p.$spacing ? `0 calc(${p.$spacing} / 2)` : '0')};
  ${(p) => !p.$showScrollbar && noScrollbars}

  @media (max-width: 992px) {
    ${(p) => p.$widthTablet && `flex-basis: ${p.$widthTablet};`}
  }

  @media (max-width: 575px) {
    ${(p) => p.$widthMobile && `flex-basis: ${p.$widthMobile};`}
  }

  > .inner {
    flex-basis: 100%;
    position: relative;
    z-index: 0;
    min-height: 0;
    display: ${(p) => (p.$flexbox && !p.$scroll ? 'flex' : 'block')};
    ${(p) => p.$flexbox && `flex-direction: column;`}
    padding: ${(p) => (p.$padding === true ? 'var(--padding)' : p.$padding)};
  }
`;
