import React, {
  ReactNode,
  isValidElement,
  ReactElement,
  CSSProperties,
} from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './row.module.less';

export interface IRowProps {
  alignItems?: CSSProperties['alignItems'];
  justifyContent?: CSSProperties['justifyContent'];
  children?: ReactNode;
  flex?: boolean;
  height?: TStringOrNumber;
  marginBottom?: TStringOrNumber;
  marginTop?: TStringOrNumber;
  spacing?: TStringOrNumber;
  wrap?: boolean;
  testId?: string | null;
}

export const Row = ({
  alignItems = 'initial',
  justifyContent = 'initial',
  children = null,
  flex = false,
  height = 'auto',
  marginBottom = '0',
  marginTop = '0',
  spacing: spacingProp = 'var(--padding)',
  wrap = false,
  testId = null,
}: IRowProps) => {
  // Convert spacing to a px string if it's a number
  const spacing =
    typeof spacingProp === 'number' ? `${spacingProp}px` : spacingProp;

  const childElements: ReactElement[] =
    children === null || children === false
      ? []
      : Array.isArray(children)
      ? children.filter((c) => isValidElement(c))
      : [children as ReactElement];

  const columns = childElements.map((item, index) => (
    <React.Fragment key={index}>
      {React.cloneElement(item, { spacing })}
    </React.Fragment>
  ));

  return (
    <div
      className={cx(styles.row)}
      style={{
        alignItems,
        justifyContent,
        flexGrow: flex ? 1 : 0,
        flexShrink: flex ? 1 : 0,
        flexWrap: wrap ? 'wrap' : 'nowrap',
        height,
        marginLeft: `calc(${spacing} * -0.5)`,
        marginRight: `calc(${spacing} * -0.5)`,
        marginBottom,
        marginTop,
      }}
      data-testid={testId}
    >
      {columns}
    </div>
  );
};
