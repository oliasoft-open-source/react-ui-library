import React from 'react';
import { IconType } from 'typings/common-types';
import { TopBar } from '../../top-bar/top-bar';
import { SideBar } from '../../side-bar/side-bar';
import { Heading } from '../../heading/heading';
import { Button } from '../../button/button';
import { Card } from '../../card/card';
import { Page } from '../page/page';
import { Row } from '../row/row';
import { Column } from '../column/column';
import { Spacer } from '../spacer/spacer';

const ImagePlaceholder = ({ width, height }) => (
  <div style={{ background: '#ddd', width, height }}></div>
);

export default {
  title: 'Layout/Layout Examples',
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
};

export const Blowout = () => {
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding="var(--padding)">
        <Row>
          <Column width="80px">
            <ImagePlaceholder width={60} height={80} />
          </Column>
          <Column>
            <Heading top>Blowout Simulation</Heading>
            <Button colored label="Save & run simulation" />
          </Column>
          <Column flex={false}>
            <Button onClick={() => {}} round icon={IconType.HELP} />
          </Column>
        </Row>
        <Spacer />
        <Row>
          <Column>
            <Card heading={<Heading>Reservoir Zones</Heading>}>Content</Card>
            <Spacer />
            <Card heading={<Heading>Simulation Models</Heading>}>Content</Card>
            <Spacer />
            <Card heading={<Heading>Simulation Options</Heading>}>Content</Card>
          </Column>
          <Column>
            <Card heading={<Heading>Drill String</Heading>}>Content</Card>
            <Spacer />
            <Card heading={<Heading>Surface Roughness</Heading>}>Content</Card>
            <Spacer />
            <Card heading={<Heading>Weighted Rates</Heading>}>Content</Card>
          </Column>
        </Row>
      </Page>
    </>
  );
};
