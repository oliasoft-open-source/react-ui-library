import React, { useState } from 'react';
import { IconType } from 'typings/common-types';
import { TopBar } from '../../top-bar/top-bar';
import { SideBar } from '../../side-bar/side-bar';
import { Heading } from '../../heading/heading';
import { Button } from '../../button/button';
import { Drawer } from '../../drawer/drawer';
import { List } from '../../list/list';
import { Field } from '../../form/field';
import { Input } from '../../input/input';
import { Modal } from '../../modal/modal';
import { Dialog } from '../../dialog/dialog';
import { CheckBox } from '../../check-box/check-box';
import { FormRow } from '../form-row/form-row';
import { Page } from '../page/page';
import { Row } from '../row/row';
import { Column } from '../column/column';
import { Select } from '../../select/select';
import { Spacer } from '../spacer/spacer';
import { Tabs } from '../../tabs/tabs';
import { Table } from '../../table/table';

export default {
  title: 'Layout/Layout Examples',
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
};

export const CasingLoads = () => {
  const [dialogOpen, setDialogOpen] = useState(false);

  const SchematicPlaceholder = () => (
    <div
      style={{
        color: '#aaa',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        overflow: 'hidden',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      }}
    >
      SCHEMATIC
    </div>
  );

  const casingTabs = [
    {
      label: 'Casing Loads',
      value: 'casingloads',
    },
    {
      label: 'Plot Charts',
      value: 'charts',
    },
  ];

  const casingListData = {
    name: 'Casing Loads',
    items: [
      {
        id: 1,
        name: 'Sections',
        type: 'Heading',
      },
      {
        id: 2,
        name: '30" Conductor Casing',
        active: true,
      },
      {
        id: 3,
        name: 'Ipsum',
      },
      {
        id: 4,
        name: 'Dolor',
      },
    ],
  };

  const loadsTableData = {
    columnWidths: ['auto', 'auto', 'auto', 'auto', '1%'],
    headers: [
      {
        cells: [
          { value: 'Load case name' },
          { value: 'Internal profile' },
          { value: 'External profile' },
          { value: 'Temperature profile' },
          { value: '' },
        ],
      },
    ],
    rows: [
      {
        cells: [
          { value: 'Load case 1' },
          { value: 'Profile 1' },
          { value: 'Profile 2' },
          { value: 'Profile 3' },
          { value: 'Edit', type: 'Link', onClick: () => setDialogOpen(true) },
        ],
      },
      {
        cells: [
          { value: 'Load case 2' },
          { value: 'Profile 1' },
          { value: 'Profile 2' },
          { value: 'Profile 3' },
          { value: 'Edit', type: 'Link', onClick: () => setDialogOpen(true) },
        ],
      },
    ],
  };

  const LoadCaseForm = () => (
    <>
      <Field label="Name">
        <Input value="Load case 1" />
      </Field>
      <Spacer />
      <Heading>Internal Profile</Heading>
      <Spacer />
      <FormRow>
        <Field label="Profile">
          <Select
            native
            width="auto"
            options={[
              { label: 'Profile 1', value: '1' },
              { label: 'Profile 2', value: '2' },
            ]}
          />
        </Field>
        <Field label="Influx depth">
          <Input />
        </Field>
        <Field label="Gas gradient">
          <Input />
        </Field>
        <Field label="Mud weight">
          <Input />
        </Field>
        <Field label="Input n">
          <Input />
        </Field>
        <Field>
          <CheckBox label="Limit gas at shoe" />
        </Field>
      </FormRow>
      <Spacer />
      <Heading>External Profile</Heading>
      <Spacer />
      <FormRow>
        <Field label="Profile">
          <Select
            native
            width="auto"
            options={[
              { label: 'Profile 1', value: '1' },
              { label: 'Profile 2', value: '2' },
            ]}
          />
        </Field>
        <Field label="FG above TOC">
          <Input />
        </Field>
        <Field label="FG below TOC">
          <Input />
        </Field>
      </FormRow>
      <Spacer />
      <Heading>Temperature Profile</Heading>
      <Spacer />
      <FormRow>
        <Field label="Profile">
          <Select
            native
            width="auto"
            options={[
              { label: 'Profile 1', value: '1' },
              { label: 'Profile 2', value: '2' },
            ]}
          />
        </Field>
        <Field label="Custome table">
          <Select
            native
            width="auto"
            options={[
              { label: 'Custome table 1', value: '1' },
              { label: 'Custome table 2', value: '2' },
            ]}
          />
        </Field>
      </FormRow>
    </>
  );

  const CasingLoadsList = () => {
    const [open, setOpen] = useState(true);
    return (
      <Drawer
        button={
          <Button
            onClick={() => setOpen(!open)}
            round
            icon={IconType.CHEVRON_LEFT}
          />
        }
        open={open}
        width={300}
        closedWidth="var(--size-sidebar)"
      >
        <List drawer narrow={!open} list={casingListData} />
      </Drawer>
    );
  };

  const CasingLoadsSchematic = () => {
    const [open, setOpen] = useState(true);
    return (
      <Drawer
        right
        background="white"
        button={
          <Button
            onClick={() => setOpen(!open)}
            round
            icon={IconType.CHEVRON_RIGHT}
          />
        }
        open={open}
        width={300}
        closedWidth="var(--size-sidebar)"
      >
        <SchematicPlaceholder />
      </Drawer>
    );
  };

  const CasingLoadsEditModal = () => (
    <Modal visible={dialogOpen}>
      <Dialog
        dialog={{
          heading: 'Load case 1',
          content: <LoadCaseForm />,
          footer: (
            <>
              <Button
                label="Save"
                colored
                onClick={() => setDialogOpen(false)}
              />
              <Button label="Cancel" onClick={() => setDialogOpen(false)} />
            </>
          ),
          onClose: () => setDialogOpen(false),
        }}
      />
    </Modal>
  );

  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false} scroll={false}>
        <Row spacing={0} flex>
          <CasingLoadsList />
          <Column borderLeft borderRight padding scroll>
            <Heading top marginBottom={0}>
              30&quot; Conductor Casing
            </Heading>
            <Tabs margin={false} options={casingTabs} value={casingTabs[0]} />
            <Spacer />
            <Table table={loadsTableData} />
          </Column>
          <CasingLoadsSchematic />
        </Row>
      </Page>
      <CasingLoadsEditModal />
    </>
  );
};
