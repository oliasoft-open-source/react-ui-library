import React, { useState } from 'react';
import { IconType } from 'typings/common-types';
import { TopBar } from '../../top-bar/top-bar';
import { SideBar } from '../../side-bar/side-bar';
import { Heading } from '../../heading/heading';
import { Button } from '../../button/button';
import { Drawer } from '../../drawer/drawer';
import { List } from '../../list/list';
import { Field } from '../../form/field';
import { Input } from '../../input/input';
import { Accordion } from '../../accordion/accordion';
import { CheckBox } from '../../check-box/check-box';
import { FormRow } from '../form-row/form-row';
import { Page } from '../page/page';
import { Row } from '../row/row';
import { Column } from '../column/column';
import { Select } from '../../select/select';
import { Spacer } from '../spacer/spacer';
import { Tabs } from '../../tabs/tabs';
import { InputGroup } from '../../input-group/input-group';
import { InputGroupAddon } from '../../input-group/input-group-addon/input-group-addon';

const options = [
  {
    label: 'Settings',
    value: 'settings',
  },
  {
    label: 'Simulation Results',
    value: 'results',
  },
];
const casingOptions = [
  '30, Conductor, Casing',
  '20, Surface, Casing',
  '13 3/8, Intermediate, Casing',
  '11 3/4, Surface, Casing',
  '9 5/8, Production, Casing',
];

export default {
  title: 'Layout/Layout Examples',
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
};

export const AFESimulation = () => {
  const [open, setOpen] = useState(true);
  const [selectedTab, setSelectedTab] = useState(options[0]);
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false} scroll={false}>
        <Row spacing={0} height="100%">
          <Drawer
            button={
              <Button
                onClick={() => setOpen(!open)}
                round
                icon={IconType.CHEVRON_LEFT}
              />
            }
            open={open}
            width={300}
            closedWidth="var(--size-sidebar)"
          >
            <List
              drawer
              narrow={!open}
              list={{
                name: 'AFE Simulation',
                items: [
                  {
                    id: 1,
                    name: 'Simulation name here',
                    active: true,
                  },
                  {
                    id: 2,
                    name: 'Ipsum',
                  },
                ],
              }}
            />
          </Drawer>
          <Column scroll padding borderLeft>
            <Heading top>Simulation name here</Heading>
            <Tabs
              options={options}
              value={selectedTab}
              onChange={(evt) => {
                const { value, label } = evt.target;
                setSelectedTab({ value, label });
              }}
              margin={0}
            />
            <Spacer />
            <FormRow>
              <Field label="Name">
                <Input value="Simulation name here" width="200px" />
              </Field>
              <Field label="Initial condition">
                <Select
                  options={[
                    {
                      label: 'E3 - Circulation through drillpipe',
                      value: 'e3',
                    },
                  ]}
                  value={{
                    label: 'E3 - Circulation through drillpipe',
                    value: 'e3',
                  }}
                  native
                />
              </Field>
            </FormRow>
            {casingOptions.map((option) => (
              <div key={option}>
                <Accordion
                  heading={<Heading>{option}</Heading>}
                  managed
                  bordered
                >
                  <FormRow>
                    <Field label="Leak off @TVD 425">
                      <CheckBox label="Use formation data" checked />
                    </Field>
                    <Field>
                      <InputGroup>
                        <Input
                          width="200px"
                          disabled
                          value="1.532142857142857"
                        />
                        <InputGroupAddon>m</InputGroupAddon>
                      </InputGroup>
                    </Field>
                    <Field>
                      <CheckBox label="Gas cap" />
                    </Field>
                    <Field>
                      <CheckBox label="Max allowable pressure buildup" />
                    </Field>
                  </FormRow>
                </Accordion>
                <Spacer />
              </div>
            ))}
            <Field label="Final condition">
              <Select
                options={[
                  {
                    label: 'Undisturbed',
                    value: 'Undisturbed',
                  },
                ]}
                value={{
                  label: 'Undisturbed',
                  value: 'Undisturbed',
                }}
                native
                width="auto"
              />
            </Field>
            <Button label="Simulate" colored />
          </Column>
        </Row>
      </Page>
    </>
  );
};
