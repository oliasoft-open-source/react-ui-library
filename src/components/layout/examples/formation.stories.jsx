import React, { useState } from 'react';
import { TopBar } from '../../top-bar/top-bar';
import { SideBar } from '../../side-bar/side-bar';
import { Heading } from '../../heading/heading';
import { Page } from '../page/page';
import { Row } from '../row/row';
import { Column } from '../column/column';
import { Spacer } from '../spacer/spacer';
import { Tabs } from '../../tabs/tabs';
import { Table } from '../../table/table';

const ChartPlaceholder = () => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: 'rgba(0,0,0,0.05)',
      height: '100%',
      minHeight: 400,
      textAlign: 'center',
    }}
  >
    CHART <br />
    (fills height, minHeight 400px)
  </div>
);

export default {
  title: 'Layout/Layout Examples',
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
};

export const Formation = () => {
  const options = [
    {
      label: 'Monkeys',
      value: 'monkeys',
    },
    {
      label: 'Bananas',
      value: 'bananas',
    },
    {
      label: 'Squirrels',
      value: 'squirrels',
    },
    {
      label: 'Pomegranates',
      value: 'pomegranates',
    },
  ];
  const [selectedTab, setSelectedTab] = useState(options[0]);
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding="var(--padding)" scroll>
        <Heading top marginBottom={0}>
          Formation Inputs
        </Heading>
        <Tabs
          margin={false}
          options={options}
          value={selectedTab}
          onChange={(evt) => {
            const { value, label } = evt.target;
            setSelectedTab({ value, label });
          }}
        />
        <Spacer />
        <Row flex>
          <Column width={400}>
            <Table
              table={{
                headers: [
                  {
                    cells: [{ value: 'Name' }, { value: 'Origin' }],
                  },
                ],
                rows: [
                  {
                    cells: [{ value: 'Brown rice' }, { value: 'Vietnam' }],
                  },
                  {
                    cells: [{ value: 'Buckwheat' }, { value: 'Poland' }],
                  },
                  {
                    cells: [{ value: 'Couscous' }, { value: 'France' }],
                  },
                ],
              }}
            />
          </Column>
          <Column>
            <ChartPlaceholder />
          </Column>
        </Row>
      </Page>
    </>
  );
};
