import React, { useState } from 'react';
import { IconType } from 'typings/common-types';
import { TopBar } from '../../top-bar/top-bar';
import { SideBar } from '../../side-bar/side-bar';
import { Heading } from '../../heading/heading';
import { Button } from '../../button/button';
import { Drawer } from '../../drawer/drawer';
import { List } from '../../list/list';
import { Field } from '../../form/field';
import { Input } from '../../input/input';
import { CheckBox } from '../../check-box/check-box';
import { Page } from '../page/page';
import { Row } from '../row/row';
import { Column } from '../column/column';
import { Accordion } from '../../accordion/accordion';
import { Spacer } from '../spacer/spacer';
import { Tabs } from '../../tabs/tabs';

const options = [
  {
    label: 'Import',
    value: 'import',
    right: true,
  },
  {
    label: 'Settings',
    value: 'settings',
    right: true,
  },
];

const MapPlaceholder = () => (
  <div
    style={{
      background: '#AAD3DF',
      borderLeft: '1px solid #8AB3BF',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: 'white',
      height: '100%',
    }}
  >
    MAP
  </div>
);

export default {
  title: 'Layout/Layout Examples',
  parameters: {
    layout: 'fullscreen',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
};

export const Site = () => {
  const [open, setOpen] = useState(true);
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} width="250px" />
      <Page padding={false} scroll={false}>
        <Row spacing={0} flex>
          <Drawer
            button={
              <Button
                onClick={() => setOpen(!open)}
                round
                icon={IconType.CHEVRON_LEFT}
              />
            }
            open={open}
            width={300}
            closedWidth="var(--size-sidebar)"
          >
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Sites',
                items: [
                  {
                    id: 1,
                    name: 'Main Site',
                    type: 'Heading',
                  },
                  {
                    id: 2,
                    name: 'Site name here',
                    active: true,
                    label: {
                      value: 1,
                      color: '#69779b',
                    },
                  },
                  {
                    id: 3,
                    name: 'Locations',
                    type: 'Heading',
                  },
                  {
                    id: 4,
                    name: 'Ipsum',
                    label: {
                      value: 2,
                      color: '#69779b',
                    },
                  },
                  {
                    id: 5,
                    name: 'Dolor',
                    label: {
                      value: 3,
                      color: '#69779b',
                    },
                  },
                ],
              }}
            />
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Wells',
                items: [
                  {
                    id: 1,
                    name: 'Main Wellbore',
                    type: 'Heading',
                  },
                  {
                    id: 2,
                    name: 'Lorem',
                    label: {
                      value: 1,
                      color: '#77699b',
                    },
                  },
                ],
              }}
            />
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Targets',
                items: [
                  {
                    id: 1,
                    name: 'Lorem',
                    label: {
                      value: 1,
                      color: '#9b6977',
                    },
                  },
                ],
              }}
            />
          </Drawer>
          <Column borderLeft flexbox>
            <Spacer />
            <Tabs padding margin={false} options={options} />
            <Row spacing={0} flex>
              <Column scroll padding width={550}>
                <Heading top>Site name here</Heading>
                <Field label="Name">
                  <Input value="Site name here" width="200px" />
                </Field>
                <Accordion
                  heading={<Heading>Coordinate Reference System</Heading>}
                  managed
                >
                  <Field label="Label" labelLeft labelWidth="50px">
                    <Input value="Value" width="100%" />
                  </Field>
                  <Field label="Label" labelLeft labelWidth="50px">
                    <Input value="Value" width="100%" />
                  </Field>
                  <Field label="Label" labelLeft labelWidth="50px">
                    <Input value="Value" width="100%" />
                  </Field>
                  <Field>
                    <CheckBox label="Dolor" />
                  </Field>
                </Accordion>
                <Spacer />
                <Heading marginBottom="var(--padding)">Location</Heading>
                <Row>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                </Row>
                <Spacer />
                <Heading marginBottom="var(--padding)">Model</Heading>
                <Row>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                </Row>
                <Accordion heading={<Heading>Settings</Heading>} managed>
                  <Field>
                    <CheckBox label="Dolor" />
                  </Field>
                  <Row>
                    <Column>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                    </Column>
                    <Column>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                    </Column>
                  </Row>
                </Accordion>
              </Column>
              <Column>
                <MapPlaceholder />
              </Column>
            </Row>
          </Column>
        </Row>
      </Page>
    </>
  );
};
