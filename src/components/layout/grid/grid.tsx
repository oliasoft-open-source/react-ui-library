import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { TStringOrNumber } from 'typings/common-type-definitions';

export interface IGridProps {
  rows?: string;
  columns?: string;
  columnsTablet?: string | null;
  columnsMobile?: string | null;
  gap?: boolean | TStringOrNumber;
  height?: TStringOrNumber;
  children?: ReactNode;
}

// Use transient props to avoid passing them to the DOM (https://styled-components.com/docs/api#transient-props)
export interface IStyledGridProps {
  $rows?: string;
  $columns?: string;
  $columnsTablet?: string | null;
  $columnsMobile?: string | null;
  $gap?: boolean | TStringOrNumber;
  $height?: TStringOrNumber;
  $children?: ReactNode;
}

const getGapValue = (gap?: boolean | TStringOrNumber): string => {
  if (typeof gap === 'boolean') {
    return gap ? '20px' : '0px';
  } else if (typeof gap === 'number') {
    return `${gap}px`;
  } else {
    return gap || 'initial';
  }
};

const getSizeValue = (size?: TStringOrNumber): string => {
  if (typeof size === 'number') {
    return `${size}px`;
  } else {
    return size || 'initial';
  }
};

const StyledGrid: any = styled.div<IStyledGridProps>`
  display: grid;
  gap: ${(props) => getGapValue(props.$gap)};
  grid-template-rows: ${(props) => getSizeValue(props.$rows)};
  grid-template-columns: ${(props) => getSizeValue(props.$columns)};
  height: ${(props) => getSizeValue(props.$height)};

  @media (max-width: 992px) {
    ${(p) =>
      p.$columnsTablet &&
      `grid-template-columns: ${getSizeValue(p.$columnsTablet)};`}
  }

  @media (max-width: 575px) {
    ${(p) =>
      p.$columnsMobile &&
      `grid-template-columns: ${getSizeValue(p.$columnsMobile)};`}
  }
`;

export const Grid = ({
  rows = 'initial',
  columns = 'initial',
  columnsTablet = null,
  columnsMobile = null,
  gap = false,
  height = 'initial',
  children,
}: IGridProps) => {
  return (
    <StyledGrid
      $rows={rows}
      $columns={columns}
      $columnsTablet={columnsTablet}
      $columnsMobile={columnsMobile}
      $gap={gap === false ? '0px' : gap === true ? 'var(--padding)' : gap}
      $height={typeof height === 'number' ? `${height}px` : height}
    >
      {children}
    </StyledGrid>
  );
};
