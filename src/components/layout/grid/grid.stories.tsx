import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Grid, IGridProps } from './grid';
import { Card } from '../../card/card';

export default {
  title: 'Layout/Grid',
  component: Grid,
  argTypes: {
    alignItems: {
      control: {
        type: 'inline-radio',
      },
      options: ['flex-start', 'center', 'flex-end'],
    },
    justifyContent: {
      control: {
        type: 'inline-radio',
      },
      options: ['flex-start', 'center', 'flex-end', 'space-between'],
    },
    gap: { control: { type: 'boolean' } },
  },
  args: {
    columns: '1fr 1fr 1fr',
    children: (
      <>
        <Card>Item 1</Card>
        <Card>Item 2</Card>
        <Card>Item 3</Card>
        <Card>Item 4</Card>
        <Card>Item 5</Card>
        <Card>Item 6</Card>
      </>
    ),
  },
} as Meta;

const Template: StoryFn<IGridProps> = (args) => (
  <Grid
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);

export const Default = Template.bind({});

export const DefaultGap = Template.bind({});
DefaultGap.args = { gap: true };

export const CustomGap = Template.bind({});
CustomGap.args = { gap: '50px' };

export const CustomColumnWidth = Template.bind({});
CustomColumnWidth.args = {
  columns: '200px 50% 1fr',
};

export const Height = Template.bind({});
Height.args = {
  height: '200px',
};

export const Responsive = Template.bind({});
Responsive.args = {
  columns: '1fr 1fr 1fr',
  columnsTablet: '1fr 1fr',
  columnsMobile: '1fr',
};
