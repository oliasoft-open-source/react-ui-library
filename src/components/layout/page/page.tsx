import React, { ReactNode } from 'react';
import cx from 'classnames';

import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './page.module.less';

export interface ILayoutPageProps {
  children?: ReactNode;
  padding?: boolean | TStringOrNumber;
  left?: TStringOrNumber;
  scroll?: boolean;
  top?: TStringOrNumber;
}

export const Page = ({
  children,
  left = 'var(--size-sidebar)',
  padding: paddingProp = true,
  scroll = true,
  top = 'var(--size-topbar)',
}: ILayoutPageProps) => {
  const padding =
    typeof paddingProp === 'string'
      ? paddingProp
      : paddingProp === true
      ? 'var(--padding)'
      : '0';

  return (
    <div
      className={cx(styles.page, scroll ? styles.scroll : '')}
      style={{ left, padding, top }}
    >
      {children}
    </div>
  );
};
