import React, { ReactNode } from 'react';
import styles from './form-row.module.less';

export interface IFormRowProps {
  children?: ReactNode;
}

export const FormRow = ({ children }: IFormRowProps) => {
  return <div className={styles.formRow}>{children}</div>;
};
