import React from 'react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './print-header.module.less';

export interface IPrintHeaderProps {
  logo: string;
  logoWidth?: TStringOrNumber;
  alt: string;
}

export const PrintHeader = ({
  logo,
  alt,
  logoWidth = '100px',
}: IPrintHeaderProps) => (
  <img
    src={logo}
    alt={alt}
    className={styles.printHeader}
    style={{ width: logoWidth }}
  />
);
