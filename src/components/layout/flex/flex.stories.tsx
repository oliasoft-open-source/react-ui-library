import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Flex, IFlexProps } from './flex';
import { Button } from '../../button/button';
import { Text } from '../../text/text';

export default {
  title: 'Layout/Flex',
  component: Flex,
  argTypes: {
    alignItems: {
      control: {
        type: 'inline-radio',
      },
      options: ['flex-start', 'center', 'flex-end'],
    },
    justifyContent: {
      control: {
        type: 'inline-radio',
      },
      options: ['flex-start', 'center', 'flex-end', 'space-between'],
    },
    gap: { control: { type: 'boolean' } },
  },
  args: {
    alignItems: undefined,
    justifyContent: undefined,
    children: (
      <>
        <Button label="Button" />
        <Text>Text</Text>
      </>
    ),
  },
} as Meta;

const Template: StoryFn<IFlexProps> = (args) => (
  <Flex
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);

export const Default = Template.bind({});

export const DefaultGap = Template.bind({});
DefaultGap.args = { gap: true };

export const CustomGap = Template.bind({});
CustomGap.args = { gap: '50px' };

export const Center = Template.bind({});
Center.args = {
  height: '200px',
  alignItems: 'center',
  justifyContent: 'center',
};

export const SpaceBetween = Template.bind({});
SpaceBetween.args = {
  justifyContent: 'space-between',
};

export const JustifyEnd = Template.bind({});
JustifyEnd.args = {
  justifyContent: 'flex-end',
};

export const Column = Template.bind({});
Column.args = { direction: 'column' };
