import React from 'react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { Property } from 'csstype';

export interface IFlexProps {
  alignItems?: string;
  justifyContent?: string;
  direction?: Property.FlexDirection;
  height?: TStringOrNumber;
  children?: React.ReactNode;
  wrap?: boolean;
  gap?: boolean | TStringOrNumber;
}

export const Flex = ({
  alignItems = 'initial',
  justifyContent = 'initial',
  direction = 'initial',
  height = 'initial',
  children = null,
  wrap = true,
  gap = false,
}: IFlexProps) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: direction,
        alignItems,
        justifyContent,
        height: typeof height === 'number' ? `${height}px` : height,
        flexWrap: wrap ? 'wrap' : 'nowrap',
        gap:
          gap === false
            ? 0
            : gap === true
            ? 'var(--padding)'
            : typeof gap === 'number'
            ? `${gap}px`
            : gap,
      }}
    >
      {children}
    </div>
  );
};
