import React, { ReactNode } from 'react';
import cx from 'classnames';
import styles from './tabs.module.less';

export interface ITabContentProps {
  children?: ReactNode[] | ReactNode;
  activeTabIndex: number;
  contentPadding?: boolean;
}

export const Content = ({
  children,
  activeTabIndex,
  contentPadding,
}: ITabContentProps) => {
  return Array.isArray(children) ? (
    <div className={cx(contentPadding ? styles.contentPadding : '')}>
      {children.map((child, index) => activeTabIndex === index && child)}
    </div>
  ) : null;
};
