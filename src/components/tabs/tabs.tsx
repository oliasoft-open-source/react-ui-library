import React, { useContext } from 'react';
import cx from 'classnames';
import {
  TChangeEvent,
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { noop } from 'lodash';
import { Label } from './label';
import { Content } from './content';
import styles from './tabs.module.less';
import { standardizeInputs } from '../select/select.input';

interface ITabOption {
  key?: string;
  label: TStringOrNumber;
  value: TStringOrNumber;
  url?: string;
  hidden?: boolean;
  disabled?: boolean;
  invalid?: boolean;
  badge?: TStringOrNumber;
  right?: boolean;
  testId?: string;
}

interface ITabSelectedOption {
  label: TStringOrNumber;
  value: TStringOrNumber;
}

export interface ITabsProps {
  name?: string;
  options?: ITabOption[] | null;
  children?: any;
  value?: ITabSelectedOption;
  onChange?: TChangeEventHandler;
  padding?: boolean;
  margin?: boolean;
  contentPadding?: boolean;
  // deprecated:
  activeTabIndex?: number;
  tabs?: ITabOption[] | undefined;
  onChangeTab?: (index: number, value: TStringOrNumber | null) => void;
  testId?: string;
}

export const Tabs = ({
  name = undefined,
  options: rawOptions = null,
  children = null,
  value: rawValue = undefined,
  onChange = noop,
  padding = false,
  margin = true,
  contentPadding = true,
  //deprecated:
  onChangeTab,
  activeTabIndex: rawActiveTabIndex = undefined,
  tabs: tabsConfig = undefined,
  testId,
}: ITabsProps) => {
  const disabledContext = useContext(DisabledContext);

  //----------------------------------------------------------------------------
  // Deprecation / backwards-compatibility
  const isDeprecated = onChangeTab !== undefined;

  const rawTabs = children
    ? children.map((c: any, i: number) => ({
        value: i,
        url: c.props.url,
        label: c.props.label,
        hidden: c.props.hidden,
        disabled: c.props.disabled || disabledContext,
        badge: c.props.badge,
        testId: c.props?.testId && `${c.props.testId}-${i}`,
      }))
    : rawOptions || tabsConfig;
  const {
    simpleInputs,
    options: tabs,
    selectedOptions: value,
  } = isDeprecated
    ? {
        simpleInputs: false,
        options: rawTabs,
        selectedOptions: undefined,
      }
    : standardizeInputs(rawTabs as any, rawValue as any);
  const selectedValue =
    isDeprecated || value === undefined || value === null
      ? undefined
      : simpleInputs
      ? value
      : (value as any).value;
  const activeTabIndexBase =
    rawActiveTabIndex !== undefined
      ? rawActiveTabIndex
      : tabs?.findIndex((t: any) => t.value === selectedValue);
  const activeTabIndex = activeTabIndexBase === -1 ? 0 : activeTabIndexBase;
  //----------------------------------------------------------------------------

  const handleTabClick = (
    index: number,
    tab: ITabOption,
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
  ) => {
    if (isDeprecated) {
      const value = tab.key || tab.label || null; // Return key if it exists, otherwise use label
      onChangeTab(index, value);
    } else {
      const target = event.target as any;
      target.name = name;
      target.value = tab.value;
      target.label = tab.label;
      onChange(event as unknown as TChangeEvent);
    }
  };

  const renderTab = (tab: ITabOption, index: number) => {
    const {
      label,
      url,
      hidden,
      disabled,
      value,
      right,
      invalid,
      badge,
      testId,
    } = tab;
    return (
      <Label
        label={label}
        url={url}
        key={index}
        hidden={hidden}
        disabled={disabled || disabledContext}
        invalid={invalid}
        badge={badge}
        right={right}
        active={
          isDeprecated ? index === activeTabIndex : value === selectedValue
        }
        onClick={(event) => handleTabClick(index, tab, event)}
        testId={testId}
      />
    );
  };

  if (tabs) {
    return (
      <div>
        <div
          className={cx(
            styles.tabs,
            padding ? styles.padding : '',
            margin ? styles.margin : '',
          )}
          data-testid={testId}
        >
          {(tabs as ITabOption[]).map(renderTab)}
        </div>
        <Content
          activeTabIndex={activeTabIndex ?? 0}
          contentPadding={contentPadding}
        >
          {children}
        </Content>
      </div>
    );
  }
  return null;
};
