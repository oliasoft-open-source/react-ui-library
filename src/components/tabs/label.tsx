import React, { MouseEvent } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './tabs.module.less';
import { Badge } from '../badge/badge';

export interface ITabLabelProps {
  label: TStringOrNumber;
  url?: string;
  hidden?: boolean;
  disabled?: boolean;
  active?: boolean;
  onClick?: (evt: MouseEvent<HTMLAnchorElement>) => void;
  right?: boolean;
  invalid?: boolean;
  badge?: TStringOrNumber;
  testId?: string;
}

export const Label = ({
  label,
  url,
  hidden,
  disabled,
  active,
  onClick,
  right,
  invalid,
  badge,
  testId,
}: ITabLabelProps) => {
  const col_danger = getComputedStyle(document.body).getPropertyValue(
    '--color-text-error',
  );
  return (
    <a
      id={label?.toString()}
      href={url}
      className={cx(
        styles.item,
        active ? styles.active : '',
        disabled ? styles.disabled : '',
        hidden && !active ? styles.hidden : '',
        right ? styles.right : styles.left,
      )}
      onClick={onClick}
      data-testid={testId}
    >
      {invalid ? (
        <Badge small margin="-4px" color={col_danger} title="!">
          {label}
        </Badge>
      ) : badge ? (
        <Badge small margin="-4px" title={badge?.toString()}>
          {label}
        </Badge>
      ) : (
        label
      )}
    </a>
  );
};
