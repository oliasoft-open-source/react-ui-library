import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ITabsProps, Tabs } from './tabs';

export default {
  title: 'Navigation/Tabs',
  component: Tabs,
  args: {
    name: 'example',
    options: [
      { label: 'Tab', value: 0 },
      { label: 'Tab', value: 1 },
      { label: 'With Badge', value: 2, badge: '3' },
      { label: 'Invalid', value: 3, invalid: true },
      { label: 'Disabled', value: 4, disabled: true },
      { label: 'Hidden tab', value: 5, hidden: true },
    ],
    value: 0,
  },
} as Meta;

const Template: StoryFn<ITabsProps> = (args) => <Tabs {...args} />;
export const Default: StoryFn<ITabsProps> = Template.bind({});

// TODO - @Jack & @Oleg & @Mark - revisit, why label and disabled used for div
// JACK - this looks like a deprecated usage (the tab names & states are grabbed from 'label', etc but it seems hacky). I can't find any evidence of it on WD. If the other apps don't use it either we should consider removing it
export const WithChildren = Template.bind({});
WithChildren.args = {
  children: [
    // @ts-ignore: write explicit reason
    <div label="Tab 0">
      <h3>Tab 0 Content</h3>
    </div>,
    // @ts-ignore: write explicit reason
    <div label="Tab 1" hidden>
      <h3>Tab 1 Content</h3>
    </div>,
    // @ts-ignore: write explicit reason
    <div label="Tab 2" disabled>
      <h3>Tab 2 Content</h3>
    </div>,
    // @ts-ignore: write explicit reason
    <div label="Tab 3">
      <h3>Tab 3 Content</h3>
    </div>,
    // @ts-ignore: write explicit reason
    <div label="Tab 4">
      <h3>Tab 4 Content</h3>
    </div>,
  ],
};

export const NoMargin = Template.bind({});
NoMargin.args = { margin: false };

export const HorizontalPadding = Template.bind({});
HorizontalPadding.parameters = {
  layout: 'fullscreen',
  docs: {
    description: {
      story: 'When you want the divider to fill the edges of its container.',
    },
  },
};
HorizontalPadding.args = { padding: true };

export const Alignment = Template.bind({});
Alignment.args = {
  options: [
    { label: 'Tab 0', value: 0 },
    { label: 'Tab 1', value: 1 },
    { label: 'Tab 2', value: 2 },
    { label: 'Tab 3', value: 3, right: true },
    { label: 'Tab 4', value: 4, right: true },
  ],
};

export const Managed = () => {
  const options = [
    {
      label: 'Strawberries',
      value: 'strawberries',
    },
    {
      label: 'Bananas',
      value: 'bananas',
    },
    {
      label: 'Apples',
      value: 'apples',
      disabled: true,
    },
    {
      label: 'Pomegranates',
      value: 'pomegranates',
    },
  ];
  const [selectedTab, setSelectedTab] = useState(options[0]);
  return (
    <>
      <Tabs
        name="example"
        value={selectedTab}
        options={options}
        onChange={(evt) => {
          const { value, label } = (evt?.target ?? {}) as any;
          setSelectedTab({ value, label });
        }}
      />
      <span>Eating {selectedTab.value}...</span>
    </>
  );
};

export const TestTitleCase = Template.bind({});
TestTitleCase.args = {
  options: [
    { label: 'Title Case Label', value: 0 },
    { label: 'Mixed case label', value: 1 },
    { label: 'lower case label', value: 2 },
  ],
};
