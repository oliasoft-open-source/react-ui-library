import React from 'react';
import { StoryFn } from '@storybook/react';
import { TriggerType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import { Field } from './field';
import { Input } from '../input/input';
import { Select } from '../select/select';
import { CheckBox } from '../check-box/check-box';
import { ButtonGroup } from '../button-group/button-group';
import { Button } from '../button/button';
import { Toggle } from '../toggle/toggle';
import { InputGroup } from '../input-group/input-group';
import { InputGroupAddon } from '../input-group/input-group-addon/input-group-addon';
import { Slider } from '../slider/slider';
import { TextArea } from '../textarea/textarea';
import { RadioButton } from '../radio-button/radio-button';
import { Menu } from '../menu/menu';
import { FormRow } from '../layout/form-row/form-row';
import { MenuType } from '../menu/types';

interface FieldArgs {
  small?: boolean;
  labelLeft?: boolean;
  labelWidth?: number | string;
  row?: boolean;
  disabled: boolean;
}

export default {
  title: 'Forms/Layout',
  argTypes: {
    labelWidth: 'text',
  },
  args: {
    small: false,
    row: false,
    labelLeft: false,
    labelWidth: undefined,
    disabled: false,
  },
};

const renderFields = (args: FieldArgs) => {
  const { small, labelLeft, labelWidth } = args;
  return (
    <>
      <Field
        label="Input"
        helpText="Some help text"
        labelLeft={labelLeft}
        labelWidth={labelWidth}
      >
        <Input value="Value" small={small} />
      </Field>
      <Field
        label="TextArea"
        helpText="Some very long help text that needs to have its width constrained so it doesn't look silly"
        helpTextMaxWidth={200}
        labelLeft={labelLeft}
        labelWidth={labelWidth}
      >
        <TextArea value="Value" />
      </Field>
      <Field
        label="Select (native)"
        labelLeft={labelLeft}
        labelWidth={labelWidth}
      >
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={{ label: 'Option 2', value: '2' }}
          native
          small={small}
          width="auto"
        />
      </Field>
      <Field
        label="Select (custom)"
        labelLeft={labelLeft}
        labelWidth={labelWidth}
      >
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={{ label: 'Option 2', value: '2' }}
          small={small}
          width="auto"
        />
      </Field>
      <Field
        label="Select (multi)"
        labelLeft={labelLeft}
        labelWidth={labelWidth}
      >
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          small={small}
        />
      </Field>
      <Field label="Slider" labelLeft={labelLeft} labelWidth={labelWidth}>
        <Slider
          width="300px"
          name="example"
          label="Number of aardvarks"
          value={35}
          min={0}
          max={100}
          step={1}
          onChange={() => {}}
          showArrows
          showTooltip
          small={small}
        />
      </Field>
      <Field label="ButtonGroup" labelLeft={labelLeft} labelWidth={labelWidth}>
        <ButtonGroup
          items={[
            { key: '0', label: 'Option 1', value: '1' },
            { key: '1', label: 'Option 2', value: '2' },
          ]}
          value={0}
          small={small}
        />
      </Field>
      <Field label="InputGroup" labelLeft={labelLeft} labelWidth={labelWidth}>
        <InputGroup small={small}>
          <InputGroupAddon>$</InputGroupAddon>
          <Input name="example" value="123" onChange={() => {}} />
          <InputGroupAddon>each</InputGroupAddon>
        </InputGroup>
      </Field>
      <Field label="Menu" labelLeft={labelLeft} labelWidth={labelWidth}>
        <Menu
          menu={{
            label: 'Menu',
            trigger: TriggerType.DROP_DOWN_BUTTON,
            sections: [
              { type: MenuType.OPTION, label: 'Lorem' },
              { type: MenuType.OPTION, label: 'Ipsum' },
            ],
            small,
          }}
        />
      </Field>
      <Field label="CheckBox" labelLeft={labelLeft} labelWidth={labelWidth}>
        <CheckBox label="CheckBox" checked small={small} onChange={() => {}} />
      </Field>
      <Field label="RadioButton" labelLeft={labelLeft} labelWidth={labelWidth}>
        <RadioButton
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Monkeys', value: 'bananas' },
          ]}
          value={{ label: 'Monkeys', value: 'bananas' }}
          inline
          small={small}
        />
      </Field>
      <Field label="Toggle" labelLeft={labelLeft} labelWidth={labelWidth}>
        <Toggle label="Toggle" checked onChange={() => {}} small={small} />
      </Field>
      <Field labelLeft={labelLeft} labelWidth={labelWidth}>
        <Button colored label="Button" onClick={() => {}} small={small} />
      </Field>
    </>
  );
};

const Template: StoryFn<FieldArgs> = (args) => {
  const { row, disabled } = args;
  return (
    <DisabledContext.Provider value={disabled}>
      {row ? <FormRow>{renderFields(args)}</FormRow> : renderFields(args)}
    </DisabledContext.Provider>
  );
};
export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const LabelsOnLeft = Template.bind({});
LabelsOnLeft.args = { labelLeft: true, labelWidth: 100 };

export const Row = Template.bind({});
Row.args = { row: true };

export const SmallRow = Template.bind({});
SmallRow.args = { row: true, small: true };

export const DisabledWithContext = Template.bind({});
DisabledWithContext.args = { disabled: true };
