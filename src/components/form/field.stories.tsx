import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Field, IFieldProps } from './field';
import { Input } from '../input/input';
import { Text } from '../text/text';

export default {
  title: 'Forms/Field',
  component: Field,
  args: {
    label: 'Label',
    labelLeft: false,
  },
} as Meta;

const Template: StoryFn<IFieldProps> = (args) => (
  <Field {...args}>
    <Input />
  </Field>
);
export const Default = Template.bind({});

export const LabelOnLeft = Template.bind({});
LabelOnLeft.args = {
  labelLeft: true,
};

export const LabelOnLeftFixedWidth = Template.bind({});
LabelOnLeftFixedWidth.args = {
  labelLeft: true,
  labelWidth: '80px',
};

export const HelpIconTooltip = Template.bind({});
HelpIconTooltip.args = {
  helpText: 'Tooltip goes here',
};

export const HelpIconClickable = Template.bind({});
HelpIconClickable.args = {
  onClickHelp: () => {},
};

export const HelpTextBelow: StoryFn<IFieldProps> = (args) => (
  <>
    <Field {...args}>
      <Input />
      <Text small faint>
        Help text goes here
      </Text>
    </Field>
    <Field {...args}>
      <Input error="Error text goes here" />
      <Text small error>
        Error text goes here
      </Text>
    </Field>
    <Field {...args}>
      <Input warning="Error text goes here" />
      <Text small warning>
        Warning text goes here
      </Text>
    </Field>
  </>
);

export const InfoIcon = Template.bind({});
InfoIcon.args = {
  info: 'Info goes here',
};

export const LockIcon = () => {
  const [locked, setLocked] = useState(false);
  const handleToggleLock = () => setLocked(!locked);
  return (
    <Field
      label="Label"
      lock={{
        visible: true,
        active: locked,
        onClick: handleToggleLock,
        tooltip: locked ? 'Unlock' : 'Lock',
        testId: 'testId',
      }}
    >
      <Input disabled={locked} />
    </Field>
  );
};

export const LibraryIcon = Template.bind({});
LibraryIcon.args = {
  libraryIcon: {
    onClick: () => {},
    tooltip: 'View in library',
  },
};

export const TestWrappingFields = () => (
  <Field labelLeft label="Label">
    <Input />
    <Input />
  </Field>
);
