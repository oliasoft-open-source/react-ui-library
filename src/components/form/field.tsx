import React, { ReactNode, MouseEvent } from 'react';
import cx from 'classnames';
import {
  TStringNumberNull,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { ILabelIcon, ILabelLockProps, Label } from '../label/label';
import styles from './form.module.less';

export interface IFieldProps {
  label?: TStringNumberNull | ReactNode;
  labelLeft?: boolean;
  labelWidth?: TStringOrNumber;
  children: ReactNode;
  helpText?: TStringOrNumber | ReactNode;
  helpTextMaxWidth?: TStringOrNumber;
  onClickHelp?: (event?: MouseEvent<HTMLDivElement>) => void;
  lock?: ILabelLockProps;
  info?: string;
  libraryIcon?: ILabelIcon;
  testId?: string;
}

export const Field = ({
  label,
  labelLeft = false,
  labelWidth = 'auto',
  children,
  helpText,
  helpTextMaxWidth = '300px',
  onClickHelp,
  lock = {
    visible: false,
    active: false,
    onClick: () => {},
    tooltip: '',
    testId: undefined,
  },
  info,
  libraryIcon,
  testId,
}: IFieldProps) => {
  return (
    <div
      className={cx(styles.field, labelLeft ? styles.labelLeft : '')}
      data-testid={testId}
    >
      {(label || labelLeft) && (
        <Label
          label={label}
          width={labelWidth}
          helpText={helpText}
          helpTextMaxWidth={helpTextMaxWidth}
          onClickHelp={onClickHelp}
          lock={lock}
          info={info}
          libraryIcon={libraryIcon}
          labelLeft={labelLeft}
        />
      )}
      <div className={styles.fieldInput}>{children}</div>
    </div>
  );
};
