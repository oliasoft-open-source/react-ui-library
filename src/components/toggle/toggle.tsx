import React, { useContext } from 'react';
import cx from 'classnames';
import { InputType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import {
  TChangeEvent,
  TChangeEventHandler,
} from 'typings/common-type-definitions';
import styles from './toggle.module.less';
import { HelpIcon } from '../help-icon/help-icon';

export interface IToggleProps {
  name?: string;
  label?: string | React.ReactElement | null;
  checked?: boolean;
  disabled?: boolean;
  display?: string;
  small?: boolean;
  onChange: TChangeEventHandler;
  noMargin?: boolean;
  testId?: string;
  helpText?: string;
  onClickHelp?: (evt: any) => void;
}

export const Toggle = ({
  name,
  label,
  checked = false,
  disabled,
  display,
  small = false,
  onChange,
  noMargin = false,
  testId,
  helpText,
  onClickHelp,
}: IToggleProps) => {
  const disabledContext = useContext(DisabledContext);
  const showHelp = helpText || onClickHelp;
  const isDisabled = disabled || disabledContext;

  return (
    <div
      className={cx(
        styles.toggle,
        isDisabled ? styles.disabled : null,
        small ? styles.small : null,
        noMargin ? styles.noMargin : null,
      )}
      style={{ display }}
      onClick={(evt) => {
        if (!isDisabled) {
          (evt.target as any).name = name;
          (evt.target as any).checked = !checked;
          (evt.target as any).value = !checked;
          onChange(evt as unknown as TChangeEvent);
        }
      }}
      data-testid={testId}
    >
      <input
        type={InputType.CHECKBOX}
        name={name}
        value={checked?.toString()}
        disabled={isDisabled}
        checked={checked}
        onChange={() => {}}
      />
      <label>{label}</label>
      {showHelp && (
        <div className={styles.helpIconEnabled} onClick={onClickHelp}>
          <HelpIcon text={helpText} />
        </div>
      )}
    </div>
  );
};
