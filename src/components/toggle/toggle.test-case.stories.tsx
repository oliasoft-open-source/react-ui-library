import React, { useState } from 'react';
import { useArgs } from '@storybook/preview-api';
import { expect, userEvent, waitFor, within } from '@storybook/test';
import { Meta, StoryFn } from '@storybook/react';
import { IToggleProps, Toggle } from './toggle';

export default {
  title: 'Forms/Toggle/Test Cases',
  component: Toggle,
  args: {
    label: 'Label',
  },
  tags: ['!autodocs'],
} as Meta;

const Template: StoryFn<IToggleProps> = (args) => {
  const [{ checked }, updateArgs] = useArgs();
  const handleChange = () => {
    updateArgs({ checked: !checked });
  };
  return (
    <Toggle
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
      onChange={handleChange}
    />
  );
};

export const TestClicked: StoryFn<IToggleProps> = () => {
  const [checked, setChecked] = useState(false);
  return (
    <Toggle
      name="name"
      label="Label"
      checked={checked}
      onChange={(evt) => {
        const { name, value, checked } = evt.target;
        setChecked(checked);
        console.log(name, value, checked);
      }}
    />
  );
};

export const Test = Template.bind({});
Test.args = { testId: 'testId' };
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step('Clicking checkbox should toggle it', async () => {
    const component = canvas.getByTestId('testId');
    const input = component.querySelector('input');

    await userEvent.click(component);
    await waitFor(() => {
      expect(input).toBeChecked();
    });

    await userEvent.click(component);
    await waitFor(() => {
      expect(input).not.toBeChecked();
    });
  });
};

export const TestDisabled = Template.bind({});
TestDisabled.args = { testId: 'testId', disabled: true };
TestDisabled.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step("Clicking disabled checkbox shouldn't toggle it", async () => {
    const component = canvas.getByTestId('testId');
    await component.click();
    await expect(component.querySelector('input')).not.toBeChecked();
  });
};
