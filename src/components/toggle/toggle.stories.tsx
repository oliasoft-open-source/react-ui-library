import React, { useState } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { Flex } from 'components/layout/flex/flex';
import { Text } from 'components/text/text';
import { IToggleProps, Toggle } from './toggle';

export default {
  title: 'Forms/Toggle',
  component: Toggle,
  args: {
    label: 'Label',
  },
} as Meta;

const Template: StoryFn<IToggleProps> = (args) => {
  const [{ checked }, updateArgs] = useArgs();
  const handleChange = () => {
    updateArgs({ checked: !checked });
  };
  return (
    <Toggle
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
      onChange={handleChange}
    />
  );
};
export const Default = Template.bind({});

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const DisabledChecked = Template.bind({});
DisabledChecked.args = { checked: true, disabled: true };

export const Small = Template.bind({});
Small.args = { small: true };

export const Standalone = Template.bind({});
Standalone.args = { label: null };

export const HelpText = Template.bind({});
HelpText.args = {
  helpText: 'Help text',
};

export const OnClickHelp = Template.bind({});
OnClickHelp.args = {
  onClickHelp: () => console.log('Help clicked'),
};

export const Display = () => (
  <>
    <Toggle
      display="block"
      label={
        <Flex justifyContent="space-between" wrap={false} gap>
          <Text bold>Label</Text>
          <Text success>Released yesterday</Text>
        </Flex>
      }
      onChange={() => {}}
    />
    <Toggle
      label={
        <Flex justifyContent="space-between" wrap={false} gap>
          <Text bold>Label</Text>
          <Text success>Released yesterday</Text>
        </Flex>
      }
      onChange={() => {}}
    />
  </>
);

export const DisabledWithHelpText = Template.bind({});
DisabledWithHelpText.args = {
  helpText: 'Help text',
  disabled: true,
};
