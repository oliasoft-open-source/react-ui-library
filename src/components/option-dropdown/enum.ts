export enum DropdownType {
  HEADING = 'Heading',
  DIVIDER = 'Divider',
  DEFAULT = 'Default',
}
