import React from 'react';
import styles from './option-dropdown.module.less';

export interface IOptDropdownHeadingProps {
  label: string;
}

export const Heading = ({ label }: IOptDropdownHeadingProps) => (
  <div className={styles.heading}>{label}</div>
);
