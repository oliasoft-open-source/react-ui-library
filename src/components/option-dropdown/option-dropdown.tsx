import React, { ReactElement, ChangeEvent } from 'react';
import { TriggerType } from 'typings/common-types';
import {
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { Menu } from '../menu/menu';
import { Layer } from './layer';

interface IOptionType {
  label?: string | null;
  value?: TStringOrNumber;
  selected?: boolean;
}

export interface IOptionDropdownProps {
  name?: string;
  label: string;
  options: IOptionType[];
  onChange: TChangeEventHandler;
  showHeader?: boolean;
  maxHeight?: string;
  small?: boolean;
  testId?: string;
}

export const OptionDropdown = ({
  name,
  label,
  options,
  onChange,
  small = false,
  showHeader = true,
  maxHeight = '40vh',
  testId,
}: IOptionDropdownProps): ReactElement => {
  return (
    <Menu
      testId={testId}
      menu={{
        testId: testId ? `${testId}-menu-layer` : undefined,
        small,
        label,
        trigger: TriggerType.DROP_DOWN_BUTTON,
        possiblePlacements: ['top-start', 'bottom-start'],
        sections: (
          <Layer
            testId={testId}
            options={options}
            onChangeOptions={(evt: any, nextOptions) => {
              evt.target.name = name;
              evt.target.value = nextOptions;
              onChange(evt);
            }}
            showHeader={showHeader}
            maxHeight={maxHeight}
          />
        ),
      }}
    />
  );
};
