import React, { ReactElement } from 'react';
import { TChangeEventHandler } from 'typings/common-type-definitions';
import { CheckBox, TCheckBoxClickHandler } from '../check-box/check-box';
import styles from './option-dropdown.module.less';

export interface IOptDropdownOptionProps {
  option: {
    key?: string;
    label?: string | null;
    selected?: boolean;
  };
  onChange: TCheckBoxClickHandler;
}

export const Option = ({
  option,
  onChange,
}: IOptDropdownOptionProps): ReactElement => {
  const { key, label, selected } = option ?? {};
  return (
    <div key={key} className={styles.item}>
      <CheckBox label={label} noMargin checked={selected} onChange={onChange} />
    </div>
  );
};
