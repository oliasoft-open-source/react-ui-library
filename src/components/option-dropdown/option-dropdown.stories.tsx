import React from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { IOptionDropdownProps, OptionDropdown } from './option-dropdown';

const options = [
  { label: 'Animals', type: 'Heading' },
  { type: 'Divider' },
  { label: 'Aardvarks', value: 'termites' },
  { label: 'Kangaroos', value: 'grass' },
  { label: 'Monkeys', value: 'bananas' },
];

export default {
  title: 'Forms/OptionDropdown',
  component: OptionDropdown,
  args: {
    label: 'Label',
    options,
  },
} as Meta;

const Template: StoryFn<IOptionDropdownProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: any) => {
    updateArgs({ options: evt.target.value });
  };
  return (
    <OptionDropdown
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
      onChange={handleChange}
    />
  );
};
export const Default = Template.bind({});

export const ScrollOverflow = Template.bind({});
ScrollOverflow.args = {
  options: [...Array(50)].map((_, index) => ({
    label: `Item #${index}`,
    value: index,
  })),
};

export const SmallSize = Template.bind({});
SmallSize.args = {
  small: true,
  options: [...Array(50)].map((_, index) => ({
    label: `Item #${index}`,
    value: index,
  })),
};

export const TestId = Template.bind({});
TestId.args = {
  testId: 'option-dropdown',
};
