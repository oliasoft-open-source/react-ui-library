import React, { ReactElement } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { Option } from './option';
import { Divider } from '../divider/divider';
import styles from './option-dropdown.module.less';
import { Heading } from './heading';
import { DropdownType } from './enum';

interface IOptionType {
  type?: DropdownType;
  label?: string | null;
  value?: any;
  selected?: boolean;
}

export interface IOptDropdownLayerProps {
  options: IOptionType[];
  onChangeOptions: (
    evt: React.MouseEvent<HTMLAnchorElement>,
    options: IOptionType[],
  ) => void;
  showHeader?: boolean;
  maxHeight?: TStringOrNumber;
  testId?: string;
}

export const Layer = ({
  options,
  onChangeOptions,
  showHeader,
  maxHeight,
  testId,
}: IOptDropdownLayerProps): ReactElement => {
  return (
    <div
      className={cx('menu', styles.menu)}
      style={{ maxHeight }}
      data-testid={testId ? `${testId}-option-dropdown-layer` : undefined}
    >
      {showHeader && (
        <>
          <div className={styles.header}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a
              onClick={(evt) => {
                const next = options.map((option) => ({
                  ...option,
                  selected: true,
                }));
                onChangeOptions(evt, next);
              }}
            >
              All
            </a>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a
              onClick={(evt) => {
                const next = options.map((option) => ({
                  ...option,
                  selected: false,
                }));
                onChangeOptions(evt, next);
              }}
            >
              Clear
            </a>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a
              onClick={(evt) => {
                const next = options.map((option) => ({
                  ...option,
                  selected: !option?.selected,
                }));
                onChangeOptions(evt, next);
              }}
            >
              Invert
            </a>
          </div>
          <Divider margin={0} />
        </>
      )}

      {options.map((option, index) => {
        switch (option.type) {
          case DropdownType.HEADING:
            return <Heading key={index} label={option.label!} />;
          case DropdownType.DIVIDER:
            return <Divider key={index} margin={0} />;
          default:
            return (
              <Option
                key={index}
                option={option}
                onChange={(evt: any) => {
                  const { value } = option;
                  const next = options.map((option) =>
                    option.value !== value
                      ? option
                      : { ...option, selected: !option?.selected },
                  );
                  onChangeOptions(evt, next);
                }}
              />
            );
        }
      })}
    </div>
  );
};
