import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Breadcrumb, IBreadcrumbProps } from './breadcrumb';

export default {
  title: 'Navigation/Breadcrumb',
  component: Breadcrumb,
  args: {
    links: [
      {
        label: 'Animals',
        onClick: () => console.log('label clicked'),
      },
      {
        label: 'Mammals',
        url: 'animals/mammals',
        disabled: true,
      },
      {
        label: 'Cats',
        url: 'animals/mammals/cats',
      },
      {
        type: 'custom', //e.g. react router <Link>
        element: <span>Large</span>,
      },
      {
        label: 'Tiger',
        url: 'animals/mammals/cats/tiger',
        active: true,
      },
    ],
  },
} as Meta;

const Template: StoryFn<IBreadcrumbProps> = (args) => <Breadcrumb {...args} />;

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };
