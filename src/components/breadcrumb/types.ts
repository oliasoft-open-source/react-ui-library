export enum BreadcrumbLinkType {
  LABEL = 'label',
  LINK = 'link',
  CUSTOM = 'custom',
}

export type TBreadcrumbLinkType = 'label' | 'link' | 'custom';
