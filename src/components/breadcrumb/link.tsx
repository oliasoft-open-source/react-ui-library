import React, { isValidElement, useContext, ReactNode } from 'react';
import cx from 'classnames';
import { TEmpty, TStringNumberNull } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import styles from './breadcrumb.module.less';
import { BreadcrumbLinkType, TBreadcrumbLinkType } from './types';

export interface ILinkProps {
  type?: TBreadcrumbLinkType | string;
  label?: TStringNumberNull;
  url?: string;
  onClick?: TEmpty;
  active?: boolean;
  disabled?: boolean;
  element?: ReactNode;
}

export const Link = ({
  type = BreadcrumbLinkType.LABEL,
  label,
  url,
  onClick,
  active,
  disabled,
  element,
}: ILinkProps) => {
  const disabledContext = useContext(DisabledContext);

  const isLink = type === BreadcrumbLinkType.LINK && url !== undefined;
  const isCustom =
    type === BreadcrumbLinkType.CUSTOM && isValidElement(element);

  return (
    <span
      onClick={onClick}
      className={cx(
        styles.labelContainer,
        active && styles.active,
        (disabled || disabledContext) && styles.disabled,
      )}
    >
      {isLink ? <a href={url!}>{label}</a> : isCustom ? element : label}
    </span>
  );
};
