import React from 'react';
import cx from 'classnames';
import { Link, ILinkProps } from './link';
import styles from './breadcrumb.module.less';

export interface IBreadcrumbProps {
  links: ILinkProps[];
  small?: boolean;
}

export const Breadcrumb = ({ links, small = false }: IBreadcrumbProps) => {
  return (
    <div className={cx(styles.breadcrumb, small && styles.small)}>
      {links.map((link, i) => {
        const isFinal = links.length === i + 1;
        const { type, label, url, onClick, active, disabled, element } = link;
        return (
          <span key={i}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <Link
              type={type}
              label={label}
              url={url}
              onClick={onClick}
              active={active}
              disabled={disabled}
              element={element}
            />
            {!isFinal && <span className={styles.separator}>/</span>}
          </span>
        );
      })}
    </div>
  );
};
