import React, { ChangeEvent } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { roundToFixed } from '@oliasoft-open-source/units';
import { INumberInputProps, NumberInput } from './number-input';

export default {
  title: 'Forms/NumberInput',
  component: NumberInput,
  parameters: {
    canvas: {
      page: null,
    },
  },
  args: {
    label: 'Label',
  },
} as Meta;

const Template: StoryFn<INumberInputProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: ChangeEvent<HTMLInputElement>) => {
    updateArgs({ value: evt.target.value });
  };
  return <NumberInput onChange={handleChange} value={123} {...args} />;
};

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const Width = Template.bind({});
Width.args = { width: '150px' };

export const Left = Template.bind({});
Left.args = { left: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const AllowEmpty = Template.bind({});
AllowEmpty.args = { allowEmpty: true, value: '' };

export const Warning = Template.bind({});
Warning.args = { warning: 'This input number is almost 100!', value: '99' };

export const Error = Template.bind({});
Error.args = { error: 'This is a error message' };

export const BadInput = Template.bind({});
BadInput.args = { value: '123test' };

export const WithDisplayRounding = Template.bind({});
WithDisplayRounding.args = {
  value: '123.123456789',
  enableDisplayRounding: true,
};

export const WithCustomDisplayRounding = Template.bind({});
WithCustomDisplayRounding.args = {
  value: '123.123456789',
  enableDisplayRounding: true,
  roundDisplayValue: (value) => roundToFixed(value, 4),
};

export const DisableSelectOnFocus = Template.bind({});
DisableSelectOnFocus.args = {
  value: '123.123456789',
  selectOnFocus: false,
};

export const SelectOnFocusWithRounding = Template.bind({});
SelectOnFocusWithRounding.args = {
  value: '123.123456789',
  enableDisplayRounding: true,
  selectOnFocus: true,
};
