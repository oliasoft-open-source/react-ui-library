import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { expect, fireEvent, userEvent, within, waitFor } from '@storybook/test';
import { Spacer } from 'components/layout/spacer/spacer';
import { Grid } from 'components/layout/grid/grid';
import { Card } from 'components/card/card';
import { Button } from 'components/button/button';
import { Heading } from 'components/heading/heading';
import { InputGroupAddon } from 'components/input-group/input-group-addon/input-group-addon';
import { Select } from 'components/select/select';
import { InputGroup } from 'components/input-group/input-group';
import { Field } from 'components/form/field';
import { Toggle } from 'components/toggle/toggle';
import { Text } from 'components/text/text';
import { Message } from 'components/message/message';
import { Icon } from 'components/icon/icon';
import { INumberInputProps, NumberInput } from './number-input';

export default {
  title: 'Forms/NumberInput/Test Cases',
  component: NumberInput,
  parameters: {
    canvas: {
      page: null,
    },
  },
  args: {
    label: 'Label',
  },
  tags: ['!autodocs'],
} as Meta;

export const TestInputGroup = () => (
  <InputGroup small>
    <NumberInput value={123} />
    <InputGroupAddon>to</InputGroupAddon>
    <NumberInput value={123} />
    <Select
      options={[{ value: 'units', label: 'units' }]}
      width="auto"
      value="units"
    />
  </InputGroup>
);

export const Placeholder: StoryFn<INumberInputProps> = () => {
  const [value, setValue] = useState('123|m');
  return (
    <NumberInput
      name="example"
      onChange={(evt) => setValue(evt.target.value)}
      placeholder="Type a value..."
      value={value}
      testId="testId"
    />
  );
};
Placeholder.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); //for tooltip portal elements https://github.com/storybookjs/storybook/issues/16971
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123x45s6');
    await expect(canvas.getByDisplayValue('123x45s6')).toBeInTheDocument();
    await fireEvent.mouseOver(input);
    await expect(input.getAttribute('data-error')).toEqual(
      'Must be a numerical value',
    );
    // check that tooltip shows (OW-17693)
    await waitFor(() => {
      expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
    });
  });
};

export const TestDisplayRounding: StoryFn<INumberInputProps> = () => {
  const [value, setValue] = useState('123');
  return (
    <NumberInput
      value={value}
      onChange={(evt) => {
        setValue(evt.target.value);
      }}
      testId="testId"
      enableDisplayRounding
    />
  );
};
TestDisplayRounding.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step('Value should be rounded on blur', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123.123456789');
    await userEvent.click(canvasElement);
    await expect(canvas.getByDisplayValue('123.1')).toBeInTheDocument();
  });
  await step('Full value shown on focus', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.click(input);
    await expect(canvas.getByDisplayValue('123.123456789')).toBeInTheDocument();
  });
};

export const ManagedTestCase = () => {
  const [visible, setVisible] = useState(true);
  const triggerRerender = () => {
    setVisible(false);
    setTimeout(() => setVisible(true), 0);
  };
  const [value, setValue] = useState('123');
  return (
    <Grid columns="1fr 1fr" gap>
      <Card heading={<Heading>Storage State (Redux)</Heading>}>
        {value}
        <Spacer />
        <Button
          label="Change value"
          onClick={() => setValue(String(Math.floor(Math.random() * 100)))}
        />
      </Card>
      <Card
        heading={
          <Heading icon={<Icon icon="refresh" />} onIconClick={triggerRerender}>
            NumberInput
          </Heading>
        }
      >
        {visible && (
          <NumberInput
            value={value}
            onChange={(evt) => {
              setValue(evt.target.value);
            }}
            error={Number(value) >= 0 ? '' : 'Value must be positive'}
          />
        )}
      </Card>
    </Grid>
  );
};

export const DemoCosmeticRounding: StoryFn<INumberInputProps> = () => {
  const [visible, setVisible] = useState(true);
  const triggerRerender = () => {
    setVisible(false);
    setTimeout(() => setVisible(true), 0);
  };
  const [enableDisplayRounding, setEnableDisplayRounding] = useState(false);
  const [enableCosmeticRounding, setEnableCosmeticRounding] = useState(true);
  const [valueInFeet, setValueInFeet] = useState('0.6889763779527558');
  const [callBackError, setCallBackError] = useState('');
  const valueInMeters = Number(valueInFeet) * 0.3048;
  return (
    <Grid columns="1fr 1fr" gap>
      <Card heading={<Heading>Settings</Heading>}>
        <Field label="Enable display rounding:" labelLeft labelWidth={200}>
          <Toggle
            checked={enableDisplayRounding}
            onChange={(evt) => setEnableDisplayRounding(evt.target.checked)}
          />
        </Field>
        <Field label="Enable cosmetic rounding:" labelLeft labelWidth={200}>
          <Toggle
            checked={enableCosmeticRounding}
            onChange={(evt) => setEnableCosmeticRounding(evt.target.checked)}
          />
        </Field>
      </Card>
      <Card
        heading={
          <Heading
            icon={<Icon icon="refresh" testId="triggerRerenderTestId" />}
            onIconClick={triggerRerender}
          >
            NumberInput (displayed in Meters)
          </Heading>
        }
      >
        <Text faint>
          Cosmetic Rounding Test Cases: type 0.21 then return (compare with
          enableCosmeticRounding on and off)
        </Text>
        <Spacer />
        {visible && (
          <NumberInput
            name="Example"
            value={valueInMeters}
            onChange={(evt) => {
              const nextValueInFeet = String(Number(evt.target.value) / 0.3048);
              setValueInFeet(nextValueInFeet); // convert back to ft
            }}
            enableDisplayRounding={enableDisplayRounding}
            enableCosmeticRounding={enableCosmeticRounding}
            error={Number(valueInFeet) >= 0 ? '' : 'Value must be positive'}
            validationCallback={(name, error) => {
              setCallBackError(error ?? '');
            }}
            testId="testId"
          />
        )}
      </Card>
      <Card heading={<Heading>Storage State / Redux (stored in Feet)</Heading>}>
        {valueInFeet}
        <Spacer />
        <Button
          label="Change value"
          onClick={() =>
            setValueInFeet(String(Math.floor(Math.random() * 100)))
          }
        />
      </Card>
      <Card heading={<Heading>Test Validation Callback</Heading>}>
        {callBackError && (
          <Message
            message={{
              content: callBackError,
              icon: true,
              type: 'Error',
              visible: true,
            }}
          />
        )}
      </Card>
    </Grid>
  );
};
DemoCosmeticRounding.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); //for portal layer elements https://github.com/storybookjs/storybook/issues/16971
  await step('Can enter decimal values', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123.123456789');
    await userEvent.click(canvasElement);
    await expect(canvas.getByDisplayValue('123.123456789')).toBeInTheDocument();
  });
  await step(
    'Can enter scientific notation when storage unit is different',
    async () => {
      const input = canvas.getByTestId('testId');
      await userEvent.clear(input);
      await userEvent.type(input, '-1.12e-3');
      await expect(canvas.getByDisplayValue('-1.12e-3')).toBeInTheDocument();
      /*
        After we re-render the component from scratch (e.g. after navigating away), it shows in decimal notation (expected)
        It's not practical to "persist" scientific notation of display values when the storage unit is different
      */
      const trigger = canvas.getByTestId('triggerRerenderTestId');
      await trigger.click();
      await waitFor(() => {
        expect(canvas.getByDisplayValue(-0.00112)).toBeInTheDocument();
      });
    },
  );
  await step('Can enter comma decimals (converts to dot)', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123,456');
    await expect(canvas.getByDisplayValue('123.456')).toBeInTheDocument();
  });
  await step('Can enter small numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '0.0000000023');
    await expect(canvas.getByDisplayValue('0.0000000023')).toBeInTheDocument();
  });
  await step('Can enter large numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '999999999999');
    await expect(canvas.getByDisplayValue('999999999999')).toBeInTheDocument();
  });
  await step('Can enter explicit + symbol in numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '+1.23e+3');
    await expect(canvas.getByDisplayValue('+1.23e+3')).toBeInTheDocument();
  });
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123x45s6');
    await userEvent.click(canvasElement);
    await expect(canvas.getByDisplayValue('123x45s6')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toEqual(
      'Must be a numerical value',
    );
    await fireEvent.mouseOver(input);
    await waitFor(() => {
      expect(body.getAllByText('Must be a numerical value').length).toEqual(2); //tooltip plus validation callback instance
    });
  });
};

export const TestUserInput: StoryFn<INumberInputProps> = () => {
  const [value, setValue] = useState('123');
  return (
    <NumberInput
      value={value}
      onChange={(evt) => {
        setValue(evt.target.value);
      }}
      testId="testId"
    />
  );
};
TestUserInput.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body); //for portal layer elements https://github.com/storybookjs/storybook/issues/16971
  await step('Can enter decimal values', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123.123456789');
    await expect(canvas.getByDisplayValue('123.123456789')).toBeInTheDocument();
  });
  await step('Can enter scientific notation', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '-1.12e3');
    await expect(canvas.getByDisplayValue('-1.12e3')).toBeInTheDocument();
  });
  await step('Can enter comma decimals (converts to dot)', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123,456');
    await expect(canvas.getByDisplayValue('123.456')).toBeInTheDocument();
  });
  await step('Can enter small numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '0.0000000023');
    await expect(canvas.getByDisplayValue('0.0000000023')).toBeInTheDocument();
  });
  await step('Can enter large numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '999999999999');
    await expect(canvas.getByDisplayValue('999999999999')).toBeInTheDocument();
  });
  await step('Can enter explicit + symbol in numbers', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '+1.23e+3');
    await expect(canvas.getByDisplayValue('+1.23e+3')).toBeInTheDocument();
  });
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '123x45s6');
    await expect(canvas.getByDisplayValue('123x45s6')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toEqual(
      'Must be a numerical value',
    );
    await fireEvent.mouseOver(input);
    await waitFor(() => {
      expect(body.getByText('Must be a numerical value')).toBeInTheDocument();
    });
  });
};

export const InfinityTest: StoryFn<INumberInputProps> = () => {
  const [value, setValue] = useState('123|m');
  return (
    <NumberInput
      name="example"
      onChange={(evt) => setValue(evt.target.value)}
      placeholder="Type a value..."
      value={value}
      testId="testId"
    />
  );
};
InfinityTest.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, 'Infinity');
    await expect(canvas.getByDisplayValue('Infinity')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toBeNull();
  });

  await step('Shows validation error for non-numeric input', async () => {
    const input = canvas.getByTestId('testId');
    await userEvent.clear(input);
    await userEvent.type(input, '-Infinity');
    await expect(canvas.getByDisplayValue('-Infinity')).toBeInTheDocument();
    await expect(input.getAttribute('data-error')).toBeNull();
  });
};
