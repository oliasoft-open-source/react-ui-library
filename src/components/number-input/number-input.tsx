import React, {
  useState,
  useEffect,
  ChangeEvent,
  FocusEvent,
  useRef,
  KeyboardEvent,
} from 'react';
import {
  validateNumber,
  roundToFixed,
  roundByMagnitude,
  cleanNumStr,
  stripLeadingZeros,
  toNum,
} from '@oliasoft-open-source/units';
import {
  safeToString,
  roundNumberCosmetic,
} from 'helpers/cosmetic-rounding/cosmetic-rounding';
import type {
  TStringOrNumber,
  TChangeEventHandler,
} from 'typings/common-type-definitions';
import { isArray } from 'lodash';
import { EventKey } from 'components/select/custom-select/hooks/enum';
import { isEmptyNullOrUndefined } from 'helpers/helpers';
import { TGroupOrder } from 'typings/common-types';
import { Input } from '../input/input';

export interface INumberInputProps {
  name?: string | { fieldName: string };
  placeholder?: string;
  disabled?: boolean;
  error?: string | boolean;
  left?: boolean;
  small?: boolean;
  width?: TStringOrNumber;
  value?: TStringOrNumber;
  onChange?: TChangeEventHandler;
  onFocus?: (evt: FocusEvent<HTMLInputElement>) => void;
  onBlur?: (evt: FocusEvent<HTMLInputElement>) => void;
  selectOnFocus?: boolean;
  tabIndex?: number;
  testId?: string;
  tooltip?: TStringOrNumber | React.ReactNode;
  warning?: string | boolean;
  validationCallback?: (
    name: string | { fieldName: string },
    error: string | null,
  ) => void;
  allowEmpty?: boolean;
  isInTable?: boolean;
  groupOrder?: TGroupOrder;
  enableCosmeticRounding?: boolean;
  enableDisplayRounding?: boolean;
  roundDisplayValue?: (args: TStringOrNumber) => TStringOrNumber;
  disableInternalErrorValidationMessages?: boolean;
}

const getStringName = (name?: string | { fieldName: string }): string => {
  if (!name) return '';
  if (typeof name === 'string') {
    return name;
  } else if (isArray(name)) {
    return name[0];
  }
  return name.fieldName;
};

export const NumberInput = ({
  name,
  placeholder = '',
  disabled = false,
  error = false,
  left = false,
  small = false,
  width = '100%',
  value = '',
  onChange = () => {},
  onFocus = () => {},
  onBlur = () => {},
  selectOnFocus,
  tabIndex = 0,
  testId,
  tooltip = null,
  warning = false,
  validationCallback = () => {},
  allowEmpty = false,
  isInTable,
  groupOrder,
  enableCosmeticRounding = true,
  enableDisplayRounding = false,
  roundDisplayValue,
  disableInternalErrorValidationMessages = false,
}: INumberInputProps) => {
  const validateInputValue = (value: TStringOrNumber) => {
    const validation = validateNumber(value);
    const isPlaceholder = placeholder && isEmptyNullOrUndefined(value);
    const isInfinity = [Infinity, -Infinity].includes(Number(value));

    if (isInfinity) {
      return { valid: true, errors: [] };
    }

    if (isPlaceholder) {
      return { valid: true, errors: [] };
    }

    if ((allowEmpty && value === '') || validation.valid) {
      return { ...validation, valid: true, errors: undefined };
    } else {
      return validation;
    }
  };

  const cosmeticRound = (value: string): string => {
    /*
      Optional "Excel-style" cosmetic rounding (on by default)
      Here we display slightly less precision than the underlying data type to resolve "rounding noise" issues when
      displaying values that are stored in a different unit to their display unit
    */
    const displayValueEndsWithZero = !!value?.endsWith('0');
    const shouldCosmeticallyRound =
      enableCosmeticRounding && // only when prop is enabled
      value && // not when empty typed value
      !displayValueEndsWithZero && // not when intentionally trying to type trailing zeros as interim values (OW-17109)
      validateInputValue(value)?.valid; // not for invalid values
    const valueWithCosmeticRounding = shouldCosmeticallyRound
      ? roundNumberCosmetic(value)
      : value;
    return safeToString(valueWithCosmeticRounding);
  };

  const inputRef = useRef<HTMLInputElement | null>(null);
  const [displayValue, setDisplayValue] = useState(
    cosmeticRound(safeToString(value)),
  );
  const [focus, setFocus] = useState(false);
  const stringName = getStringName(name);

  const { valid, errors } = validateInputValue(displayValue);
  const firstError =
    errors && errors.length && !disableInternalErrorValidationMessages
      ? errors[0]
      : null;

  useEffect(() => {
    const nextValue = cosmeticRound(safeToString(value));
    const hasChanged = toNum(displayValue) !== toNum(nextValue);
    if (hasChanged) {
      /*
        Only reset the display value if the storage value and display value are actually numerically different
        Otherwise it's hard to type values without "overwrite" UX glitches:
          - backspace zero bug OW-17109
          - scientific notation when storage unit is different
      */
      setDisplayValue(nextValue);
    }
  }, [value, enableCosmeticRounding, enableDisplayRounding]);

  useEffect(() => {
    if (!focus && !valid) {
      validationCallback(stringName, firstError as any);
    } else {
      validationCallback(stringName, null);
    }
  }, [focus, valid, error]);

  const onChangeValue = (
    evt: ChangeEvent<HTMLInputElement> | KeyboardEvent<HTMLInputElement>,
  ) => {
    const value = 'key' in evt ? displayValue : evt?.target?.value;
    const inputValue = cleanNumStr(
      value.replaceAll(' ', '').replaceAll('|', ''),
    );
    setDisplayValue(inputValue);
    /*
      Don't trigger onChange callback to parent when:
        - user input is not valid
     */
    if (
      validateInputValue(inputValue)?.valid ||
      (placeholder && isEmptyNullOrUndefined(value))
    ) {
      const event = {
        ...evt,
        target: {
          ...evt.target,
          value: stripLeadingZeros(safeToString(inputValue)),
          name: stringName,
        },
      } as ChangeEvent<HTMLInputElement>;
      onChange(event);
    }
  };

  useEffect(() => {
    /*
      When display rounding is enabled, set the cursor position to the end on focus
      (because we show more precision when the user clicks the input and want to avoid arbitrary cursor jumps)
    */
    if (inputRef && inputRef?.current && enableDisplayRounding && focus) {
      const textInput = inputRef.current;
      if (textInput && !selectOnFocus) {
        setTimeout(() => {
          /*
            We need setTimeout to queue the custom cursor positioning until after the React commit phase
            https://giacomocerquone.com/blog/keep-input-cursor-still
          */
          const { length } = safeToString(displayValue);
          textInput.setSelectionRange(length, length);
        }, 0);
      }
    }
  }, [focus]);

  const onInputFocus = (evt: FocusEvent<HTMLInputElement>) => {
    setFocus(true);
    onFocus(evt);
  };
  const onInputBlur = (evt: FocusEvent<HTMLInputElement>) => {
    setFocus(false);
    onBlur(evt);
  };

  /*
    Optional "display rounding" (by default, 2 decimal place fixed-point rounding so numbers align in tables)
    The rounding function can be enabled or customised via a prop
    This rounding applies only when input is not focused (we show full precision when input is focused)
  */
  const defaultDisplayRound = (value: TStringOrNumber) =>
    isInTable ? roundToFixed(value, 2) : roundByMagnitude(value, 4);
  const shouldDisplayRound = enableDisplayRounding && !focus && valid;
  const valueWithDisplayRounding = shouldDisplayRound
    ? roundDisplayValue
      ? safeToString(roundDisplayValue(displayValue)) // custom display rounding (if prop provided)
      : defaultDisplayRound(value) // default display rounding
    : displayValue;

  const onKeyDown = (evt: KeyboardEvent<HTMLInputElement>) => {
    if (evt.key === EventKey.ENTER) {
      onChangeValue(evt);
    }
  };

  return (
    <Input
      type="text"
      name={stringName}
      key={stringName}
      testId={testId}
      disabled={disabled}
      placeholder={placeholder}
      value={valueWithDisplayRounding}
      onChange={onChangeValue}
      onFocus={onInputFocus}
      onBlur={onInputBlur}
      onKeyDown={onKeyDown}
      error={firstError || error}
      warning={warning}
      right={!left}
      small={small}
      width={width}
      isInTable={isInTable}
      groupOrder={groupOrder}
      selectOnFocus={selectOnFocus}
      tabIndex={tabIndex}
      tooltip={tooltip}
      ref={inputRef}
    />
  );
};
