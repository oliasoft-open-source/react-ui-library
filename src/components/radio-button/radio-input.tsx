import React from 'react';
import cx from 'classnames';
import { InputType } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { HelpIcon } from '../help-icon/help-icon';
import styles from './radio-button.module.less';

interface IRadioInputProps {
  name: TStringOrNumber;
  label: TStringOrNumber;
  value: TStringOrNumber;
  selected: boolean;
  disabled?: boolean;
  small?: boolean;
  onChange?: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  noMargin?: boolean;
  testId?: string;
  helpText?: string;
  onClickHelp?: (event?: any) => void;
}

export const RadioInput = ({
  name,
  label,
  value,
  selected,
  disabled = false,
  small = false,
  onChange,
  noMargin = false,
  testId,
  helpText,
  onClickHelp,
}: IRadioInputProps) => {
  const showHelp = helpText || onClickHelp;

  return (
    <div
      className={cx(
        styles.radio,
        disabled ? styles.disabled : null,
        small ? styles.small : null,
        noMargin ? styles.noMargin : null,
      )}
      onClick={onChange}
      data-testid={testId}
    >
      <input
        type={InputType.RADIO}
        value={value}
        checked={selected}
        onChange={() => {}}
        disabled={disabled}
      />
      <label data-name={name} data-value={value}>
        {label}
      </label>
      {showHelp && (
        <div className={styles.helpIconEnabled} onClick={onClickHelp}>
          <HelpIcon text={helpText} />
        </div>
      )}
    </div>
  );
};
