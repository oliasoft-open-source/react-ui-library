import React, { ChangeEvent, MouseEvent, useContext } from 'react';
import cx from 'classnames';
import {
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { standardizeInputs } from '../select/select.input';
import { RadioInput } from './radio-input';
import styles from './radio-button.module.less';

export interface IRadioButtonOption {
  label: TStringOrNumber;
  value: TStringOrNumber;
  testId?: string;
  helpText?: string;
  onClickHelp?: (evt: MouseEvent<HTMLButtonElement>) => void;
  name?: string;
  checked?: boolean;
  disabled?: boolean;
}

export interface IRadioButtonProps {
  name?: string;
  label?: string | null;
  options: IRadioButtonOption[];
  value: TStringOrNumber | IRadioButtonOption;
  inline?: boolean;
  disabled?: boolean;
  small?: boolean;
  onChange?: TChangeEventHandler;
  noMargin?: boolean;
  //   Deprecated
  mainLabel?: string;
  onClick?: (target: HTMLInputElement) => void;
  radioButtonsData?: IRadioButtonOption[];
  classForContainer?: 'grouped fields' | 'inline fields';
}

export const RadioButton = ({
  name,
  label: rawLabel,
  options: rawOptions,
  value: rawValue,
  onChange,
  disabled = false,
  small = false,
  noMargin = false,
  onClick = () => {},
  // Deprecated
  inline = false,
  mainLabel = '',
  radioButtonsData,
  classForContainer = 'grouped fields', // Deprecated
}: IRadioButtonProps) => {
  const disabledContext = useContext(DisabledContext);

  const isDeprecated = radioButtonsData !== undefined;
  const {
    simpleInputs,
    options,
    selectedOptions: value,
  } = isDeprecated
    ? {
        simpleInputs: false,
        options: radioButtonsData,
        selectedOptions: undefined,
      }
    : standardizeInputs(rawOptions, rawValue);
  const selectedValue =
    isDeprecated || value === undefined || value === null
      ? undefined
      : simpleInputs
      ? value
      : (value as any).value;
  const label = isDeprecated ? mainLabel : rawLabel;

  return (
    <div
      className={cx(
        styles.wrapper,
        inline ? styles.inline : null,
        classForContainer === 'inline fields' ? styles.inline : null, //deprecated
      )}
    >
      {mainLabel && (
        <label className={cx(inline && styles.labelMargin)}>{label}</label>
      )}
      {(options as IRadioButtonOption[]).map(
        (option: IRadioButtonOption, index: number) => {
          const selected =
            option?.checked || option?.value === selectedValue || false;
          return (
            <RadioInput
              key={index}
              name={option.name || option.value} //name must be unique
              label={option.label}
              value={option.value}
              selected={selected}
              disabled={disabled || option.disabled || disabledContext}
              small={small}
              noMargin={noMargin}
              onChange={
                !option.disabled
                  ? (evt: any) => {
                      if (isDeprecated) {
                        onClick(evt.target);
                      } else {
                        evt.target.name = name!;
                        evt.target.value = option.value.toString();
                        evt.target.label = option.label.toString();
                        if (onChange) {
                          onChange(evt);
                        }
                      }
                    }
                  : undefined
              }
              testId={option.testId}
              helpText={option.helpText}
              onClickHelp={option.onClickHelp}
            />
          );
        },
      )}
    </div>
  );
};
