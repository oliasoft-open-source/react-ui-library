import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { TEmpty } from 'typings/common-type-definitions';
import { RadioButton } from './radio-button';

interface OptionType {
  label: string;
  value: string;
  disabled?: boolean;
  helpText?: string;
  onClickHelp?: TEmpty;
}

const options: OptionType[] = [
  { label: 'Aardvarks', value: 'termites' },
  { label: 'Kangaroos', value: 'grass' },
  { label: 'Monkeys', value: 'bananas', disabled: true },
];

export default {
  title: 'Forms/RadioButton',
  component: RadioButton,
  args: {
    options,
    inline: false,
    noMargin: false,
    disabled: false,
    small: false,
  },
} as Meta;

const Template: StoryFn<any> = (args) => {
  const [value, setValue] = useState<OptionType>(options[0]);
  const handleChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const selectedValue = evt.target.value;
    const selectedOption = options.find((opt) => opt.value === selectedValue);
    if (selectedOption) {
      setValue(selectedOption);
    }
  };
  return <RadioButton {...args} value={value} onChange={handleChange} />;
};

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const HelpText = Template.bind({});
HelpText.args = {
  options: options.map((option, index) => ({
    ...option,
    helpText: `Help text #${index}`,
  })),
};

export const OnClickHelp = Template.bind({});
OnClickHelp.args = {
  options: options.map((option, index) => ({
    ...option,
    onClickHelp: () => console.log(`Help #${index} clicked`),
  })),
};

export const Inline = Template.bind({});
Inline.args = {
  inline: true,
};

export const DisabledWithHelpText = Template.bind({});
DisabledWithHelpText.args = {
  options: options.map((option, index) => ({
    ...option,
    helpText: `Help text #${index}`,
    disabled: true,
  })),
};
