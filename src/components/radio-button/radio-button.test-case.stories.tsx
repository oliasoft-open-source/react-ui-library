import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { expect, userEvent, within } from '@storybook/test';
import { TEmpty } from 'typings/common-type-definitions';
import { RadioButton } from './radio-button';

interface OptionType {
  label: string;
  value: string;
  disabled?: boolean;
  helpText?: string;
  onClickHelp?: TEmpty;
}

const options: OptionType[] = [
  { label: 'Aardvarks', value: 'termites' },
  { label: 'Kangaroos', value: 'grass' },
  { label: 'Monkeys', value: 'bananas', disabled: true },
];

export default {
  title: 'Forms/RadioButton/Test Cases',
  component: RadioButton,
  args: {
    options,
    inline: false,
    noMargin: false,
    disabled: false,
    small: false,
  },
  tags: ['!autodocs'],
} as Meta;

const Template: StoryFn<any> = (args) => {
  const [value, setValue] = useState<OptionType>(options[0]);
  const handleChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const selectedValue = evt.target.value;
    const selectedOption = options.find((opt) => opt.value === selectedValue);
    if (selectedOption) {
      setValue(selectedOption);
    }
  };
  return <RadioButton {...args} value={value} onChange={handleChange} />;
};

export const Test = Template.bind({});
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  await step('Clicking a radio should activate it', async () => {
    const radio = canvas.getByDisplayValue('grass');
    await userEvent.click(radio);
    await expect(radio).toBeChecked();
  });
  await step("Clicking a disabled radio shouldn't activate it", async () => {
    const radio = canvas.getByDisplayValue('bananas');
    await radio.click();
    await expect(radio).not.toBeChecked();
  });
};
