import React, { ReactNode, useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Theme } from 'typings/common-types';
import { ILoaderProps, Loader } from './loader';
import { ProgressBar } from '../progress-bar/progress-bar';
import { Spinner } from '../spinner/spinner';
import { Spacer } from '../layout/spacer/spacer';
import { Button } from '../button/button';
import { Card } from '../card/card';

export default {
  title: 'Progress/Loader',
  component: Loader,
  argTypes: {
    theme: {
      control: {
        type: 'inline-radio',
      },
      options: ['white', 'light', 'dark'],
    },
  },
  args: {
    theme: 'white',
    text: 'Loading...',
    width: '100%',
    height: '100%',
    fullViewPortSize: false,
  },
  decorators: [
    (story: () => ReactNode) => (
      <div style={{ height: '200px', position: 'relative' }}>{story()}</div>
    ),
  ],
  parameters: {
    docs: {
      source: {
        excludeDecorators: true,
      },
    },
  },
} as Meta;

const SpinnerTemplate: StoryFn<ILoaderProps> = (args: ILoaderProps) => {
  const { theme } = args;
  return (
    <Loader {...args}>
      <Spinner dark={theme !== 'dark'} />
    </Loader>
  );
};

const ProgressBarTemplate: StoryFn<ILoaderProps> = (args: ILoaderProps) => {
  const { theme } = args;
  return (
    <Loader {...args}>
      <ProgressBar width="300px" inverted={theme === 'dark'} percentage={40} />
    </Loader>
  );
};

export const Default = SpinnerTemplate.bind({});
Default.args = { testId: 'story-default-spinner' };

export const WithProgressBar = ProgressBarTemplate.bind({});

export const TintedBackground = SpinnerTemplate.bind({});
TintedBackground.args = { theme: Theme.LIGHT };

export const FullViewport = SpinnerTemplate.bind({});
FullViewport.args = {
  theme: Theme.DARK,
  fullViewPortSize: true,
};

export const FullViewportWithProgressBar = ProgressBarTemplate.bind({});
FullViewportWithProgressBar.args = {
  theme: Theme.DARK,
  fullViewPortSize: true,
};

export const WithExtraTextDetails = ProgressBarTemplate.bind({});
WithExtraTextDetails.args = { details: 'Some extra info' };

export const WithExtraJSXDetails = ProgressBarTemplate.bind({});
WithExtraJSXDetails.args = {
  text: '',
  details: (
    <>
      <div style={{ opacity: 0.5 }}>Step 1: Lorem ipsum...DONE</div>
      <Spacer height="var(--padding-xs)" />
      <div>Step 2: Dolor est</div>
      <Spacer height="var(--padding-xs)" />
      <div style={{ opacity: 0.5 }}>Step 3: Compendum</div>
      <Spacer height="var(--padding-xs)" />
      <Button label="Cancel" onClick={() => {}} />
    </>
  ),
};

export const CoversCardContent = () => {
  const [loading, setLoading] = useState<boolean>(true);
  return (
    <>
      <Button label="Toggle loading" onClick={() => setLoading(!loading)} />
      <Spacer />
      <Card>
        <div style={{ position: 'relative' }}>
          {loading && (
            <Loader cover theme={Theme.WHITE}>
              <Spinner dark />
            </Loader>
          )}
          {'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '.repeat(
            20,
          )}
        </div>
      </Card>
    </>
  );
};
CoversCardContent.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 400,
    },
  },
};

export const ReplacesCardContent = () => {
  const [loading, setLoading] = useState<boolean>(true);
  return (
    <>
      <Button label="Toggle loading" onClick={() => setLoading(!loading)} />
      <Spacer />
      <Card>
        <div style={{ position: 'relative' }}>
          {loading ? (
            <Loader theme={Theme.WHITE} height="200px">
              <Spinner dark />
            </Loader>
          ) : (
            <div>
              {'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '.repeat(
                20,
              )}
            </div>
          )}
        </div>
      </Card>
    </>
  );
};
ReplacesCardContent.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 400,
    },
  },
};
