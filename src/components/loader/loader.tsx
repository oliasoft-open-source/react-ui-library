import React, { ReactNode } from 'react';
import cx from 'classnames';
import { Theme, TTheme } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './loader.module.less';

export interface ILoaderProps {
  fullViewPortSize?: boolean;
  cover?: boolean;
  width?: TStringOrNumber;
  height?: TStringOrNumber;
  text?: string;
  details?: string | ReactNode;
  children?: ReactNode;
  testId?: string | null;
  theme?: TTheme | string;
}

export const Loader = ({
  width,
  height,
  text = '',
  details = '',
  fullViewPortSize = false,
  cover = false,
  children = null,
  theme = Theme.DARK,
  testId = null,
}: ILoaderProps) => {
  const color = theme === Theme.DARK ? Theme.WHITE : Theme.INHERIT;

  const background =
    theme === 'white'
      ? 'var(--color-background-raised)'
      : theme === 'light'
      ? 'var(--color-background)'
      : 'black';

  return (
    <div
      className={cx(styles.loader, cover ? styles.cover : '', {
        [styles.fullViewPortSize]: fullViewPortSize,
      })}
      style={{ width, height }}
      data-testid={testId}
    >
      <div className={styles.dimmer} style={{ background, color }}>
        <div
          className={styles.content}
          data-testid={testId && `${testId}-content`}
        >
          {children}
          {text && (
            <div
              className={styles.text}
              data-testid={testId && `${testId}-text`}
            >
              {text}
            </div>
          )}
          {details && (
            <div
              className={styles.details}
              data-testid={testId && `${testId}-details`}
            >
              {details}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
