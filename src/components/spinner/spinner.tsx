import React from 'react';
import styles from './spinner.module.less';

export interface ISpinnerProps {
  small?: boolean;
  colored?: boolean;
  dark?: boolean;
  tiny?: boolean;
}

export const Spinner = ({
  small = false,
  colored = false,
  dark = false,
  tiny = false,
}: ISpinnerProps) => {
  const color = colored
    ? 'var(--color-text-primary)'
    : dark
    ? 'var(--color-text-muted)'
    : 'white';
  const fontSize = small
    ? 'var(--font-size)'
    : tiny
    ? 'var(--font-size-xs)'
    : 'var(--size)';
  return <div className={styles.spinner} style={{ color, fontSize }} />;
};
