import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ISpinnerProps, Spinner } from './spinner';

export default {
  title: 'Progress/Spinner',
  component: Spinner,
  args: {
    dark: true,
    colored: false,
    small: false,
    tiny: false,
  },
} as Meta;

const Template: StoryFn<ISpinnerProps> = (args) => (
  <div
    style={{ background: 'var(--color-background)', padding: 'var(--padding)' }}
  >
    <Spinner {...args} />
  </div>
);

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = {
  small: true,
};

export const Tiny = Template.bind({});
Tiny.args = {
  tiny: true,
};

export const White = Template.bind({});
White.args = {
  dark: false,
};

export const Colored = Template.bind({});
Colored.args = {
  colored: true,
};
