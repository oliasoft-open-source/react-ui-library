import { ReactNode, useLayoutEffect, useState } from 'react';
import { createPortal } from 'react-dom';

export interface IPortalProps {
  id: string;
  children: ReactNode | any;
}

export const Portal = ({ id, children }: IPortalProps) => {
  const parent = document.querySelector(`#${id}`);
  const [mounted, setMounted] = useState(false);

  useLayoutEffect(() => {
    if (!mounted) setMounted(true);
  }, [id]);

  return parent ? createPortal(children, parent) : null;
};
