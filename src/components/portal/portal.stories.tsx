import React, { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';
import { Meta, StoryFn } from '@storybook/react';
import { IPortalProps, Portal } from './portal';

export default {
  title: 'Modals/Portal',
  component: Portal,
  args: {
    children: <div>Portal content</div>,
    id: 'portalContainer',
  },
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
} as Meta;

const Template: StoryFn<IPortalProps> = (args) => (
  <>
    <Portal {...args} />
    <div id={args.id} />
  </>
);

export const Default = Template.bind({});

export const CreatePortal: StoryFn<IPortalProps> = () => {
  const parent = document.querySelector('#root');
  return (
    <>
      <div id="portalContainer" />
      {parent && createPortal(<div>Portal content</div>, parent)}
    </>
  );
};

CreatePortal.parameters = {
  docs: {
    source: { type: 'code' },
    description: {
      story: 'How to use React `createPortal()` directly',
    },
  },
};

export const CreatePortalWithDynamicDOMNode: StoryFn<IPortalProps> = () => {
  const parent = document.querySelector('#portalContainer');
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (!visible) setVisible(true);
  }, []);

  return (
    <>
      <div id="portalContainer" />
      {visible && parent && createPortal(<div>Portal content</div>, parent)}
    </>
  );
};

CreatePortalWithDynamicDOMNode.parameters = {
  docs: {
    source: { type: 'code' },
    description: {
      story:
        'This story shows how to use React `createPortal()` directly. The domNode for `createPortal()` must exist. For a dynamic container, this is not the case on the first render.',
    },
  },
};
