import React, { ReactNode } from 'react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { Text } from '../text/text';
import styles from './empty.module.less';

export interface IEmptyProps {
  width?: TStringOrNumber;
  height?: TStringOrNumber;
  text?: string | ReactNode;
  children?: ReactNode;
  testId?: string;
}

export const Empty = ({
  width = 'auto',
  height = 'auto',
  text = 'No data',
  children,
  testId,
}: IEmptyProps) => {
  return (
    <div
      className={styles.empty}
      style={{ width, height }}
      data-testid={testId}
    >
      <svg xmlns="http://www.w3.org/2000/svg" width="64" height="41">
        <g transform="translate(0 1)" fill="none" fillRule="evenodd">
          <ellipse
            cx="32"
            cy="33"
            rx="32"
            ry="7"
            fill="rgba(0,0,0,0.1)"
          ></ellipse>
          <g fillRule="nonzero" stroke="var(--color-text)" strokeOpacity="0.2">
            <path
              d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"
              fill="rgba(0,0,0,0.05)"
            ></path>
            <path
              d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z"
              fill="var(--color-background-raised)"
            ></path>
          </g>
        </g>
      </svg>
      {text !== '' && (
        <div className={styles.text}>
          <Text faint>{text}</Text>
        </div>
      )}
      {children}
    </div>
  );
};
