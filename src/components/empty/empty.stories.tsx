import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Empty, IEmptyProps } from './empty';
import { Button } from '../button/button';

export default {
  title: 'Progress/Empty',
  component: Empty,
  args: {
    text: 'No data',
    width: '100%',
    height: '100%',
  },
} as Meta;

const Template: StoryFn<IEmptyProps> = (args) => <Empty {...args} />;

export const Default = Template.bind({});

export const WithButton = Template.bind({});
WithButton.args = {
  text: 'Please apply at least one casing in Well Schematics to perform drilling mechanics',
  children: <Button colored label="Add casing" />,
  testId: 'empty-with-button',
};
