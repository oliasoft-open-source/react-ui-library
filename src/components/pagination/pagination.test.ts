import {
  selectedRowsPerPage,
  numberOfPages,
  validateSelectedPage,
} from './pagination.viewdata';

describe('pagination', () => {
  test('selected rows per page, by value', () => {
    const options = [{}, {}, { value: 30, selected: true }, {}];
    const value = 66;
    expect(selectedRowsPerPage(options, value)).toBe(66);
  });
  test('selected rows per page, by selected option', () => {
    const options = [{}, {}, { value: 30, selected: true }, {}];
    expect(selectedRowsPerPage(options)).toBe(30);
  });
  test('default selected rows per page', () => {
    expect(selectedRowsPerPage()).toBe(10);
  });

  test('number of pages defaults to 10', () => {
    expect(numberOfPages()).toBe(10);
  });
  test('number of pages is 1 if rows per page is 0', () => {
    expect(numberOfPages(Infinity, { value: 0 })).toBe(1);
  });
  test('number of pages works with options', () => {
    expect(
      numberOfPages(5, {
        options: [
          {},
          { value: 1 },
          { value: 2, selected: false },
          { value: 3, selected: true },
          {},
        ],
      }),
    ).toBe(2);
  });
  test('number of pages otherwise behaves as expected', () => {
    expect(numberOfPages(5, { value: 1 })).toBe(5);
    expect(numberOfPages(5, { value: 2 })).toBe(3);
    expect(numberOfPages(5, { value: 5 })).toBe(1);
    expect(numberOfPages(5, { value: 7 })).toBe(1);
  });
  test('validateSelectedPage', () => {
    expect(validateSelectedPage('4', 12)).toStrictEqual({
      valid: true,
      error: null,
    });
    expect(validateSelectedPage('', 12)).toStrictEqual({
      valid: true,
      error: null,
    });
    expect(validateSelectedPage('1', 1)).toStrictEqual({
      valid: true,
      error: null,
    });
    expect(validateSelectedPage('13', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('x', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('1.', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('1.0', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('1.1', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('1,1', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('1x', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
    expect(validateSelectedPage('-1', 12)).toStrictEqual({
      valid: false,
      error: 'Selected page must be a positive whole number between 1 and 12',
    });
  });
});
