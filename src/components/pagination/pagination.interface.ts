import React from 'react';
import {
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';

export interface IPaginationValidationResult {
  valid: boolean;
  error: string | null;
}

export interface IPaginationOptionType {
  label?: string | null;
  value?: TStringOrNumber;
  selected?: boolean;
}

export interface IPaginationPerPageType {
  onChange?: TChangeEventHandler;
  options?: IPaginationOptionType[];
  value?: TStringOrNumber;
}
