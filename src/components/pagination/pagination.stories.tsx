import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IPaginationProps, Pagination } from './pagination';

const pagination = {
  rowCount: 500,
  selectedPage: 1,
  onSelectPage: () => {},
};

export default {
  title: 'Basic/Pagination',
  component: Pagination,
  args: {
    pagination,
  },
} as Meta;

export const Default: StoryFn<IPaginationProps> = () => {
  const [selectedPage, setSelectedPage] = useState(1);
  return (
    <Pagination
      pagination={{
        rowCount: 500,
        selectedPage,
        onSelectPage: (evt) => setSelectedPage(evt),
      }}
    />
  );
};

export const RowsPerPage = () => {
  const [selectedPage, setSelectedPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(20);
  return (
    <Pagination
      pagination={{
        rowCount: 500,
        selectedPage,
        rowsPerPage: {
          onChange: (evt) => setRowsPerPage(Number(evt.target.value)),
          options: [
            { label: '10 / page', value: 10 },
            { label: '20 / page', value: 20 },
            { label: '50 / page', value: 50 },
            { label: 'Show all', value: 0 },
          ],
          value: rowsPerPage,
        },
        onSelectPage: (evt) => setSelectedPage(evt),
      }}
    />
  );
};

export const Small = () => {
  const [selectedPage, setSelectedPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(20);
  return (
    <Pagination
      pagination={{
        small: true,
        rowCount: 500,
        selectedPage,
        rowsPerPage: {
          onChange: (evt) => setRowsPerPage(Number(evt.target.value)),
          options: [
            { label: '10 / page', value: 10 },
            { label: '20 / page', value: 20 },
            { label: '50 / page', value: 50 },
            { label: 'Show all', value: 0 },
          ],
          value: rowsPerPage,
        },
        onSelectPage: (evt) => setSelectedPage(evt),
      }}
    />
  );
};
