import React, { useEffect, useState } from 'react';
import { IconType } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { Select } from '../select/select';
import { Input } from '../input/input';
import { numberOfPages, validateSelectedPage } from './pagination.viewdata';
import styles from './pagination.module.less';
import { Button } from '../button/button';
import { InputGroup } from '../input-group/input-group';
import { InputGroupAddon } from '../input-group/input-group-addon/input-group-addon';
import { IPaginationPerPageType } from './pagination.interface';

export interface IPagination {
  rowCount: number;
  selectedPage: TStringOrNumber;
  onSelectPage?: (value: number) => void;
  rowsPerPage?: IPaginationPerPageType;
  small?: boolean;
  errorMessageTemplate?: string;
  testIds?: {
    doubleRightBtn?: string;
  };
}
export interface IPaginationProps {
  pagination: IPagination;
}

export const Pagination = ({ pagination }: IPaginationProps) => {
  const {
    rowCount,
    selectedPage,
    small,
    onSelectPage,
    rowsPerPage,
    errorMessageTemplate,
    testIds,
  } = pagination;
  const showAll = Number(rowsPerPage?.value) === 0;
  const numPages = numberOfPages(rowCount, rowsPerPage);
  const [unvalidatedSelectedPage, setUnvalidatedSelectedPage] = useState(
    String(selectedPage),
  );

  useEffect(() => {
    setUnvalidatedSelectedPage(String(selectedPage));
  }, [selectedPage]);

  const { error } = validateSelectedPage(
    unvalidatedSelectedPage,
    numPages,
    errorMessageTemplate,
  );

  const setSelectedPage = (value: string) => {
    const { valid } = validateSelectedPage(
      value,
      numPages,
      errorMessageTemplate,
    );
    setUnvalidatedSelectedPage(value);
    if (valid && value !== '' && onSelectPage) {
      onSelectPage(Number(value));
    }
  };

  return (
    <div className={styles.paginationContainer}>
      {rowsPerPage && (
        <div style={{ flexShrink: 0 }}>
          <Select
            onChange={(evt) => {
              if (rowsPerPage.onChange) {
                rowsPerPage.onChange(evt);
              }
              setSelectedPage(String(1));
            }}
            options={rowsPerPage.options}
            native={!!rowsPerPage.options}
            small={small}
            width="auto"
            value={rowsPerPage.value}
          />
        </div>
      )}
      <Button
        small={small}
        round
        basic
        icon={IconType.CHEVRON_DOUBLE_LEFT}
        onClick={() => setSelectedPage(String(1))}
        disabled={showAll || selectedPage === 1}
      />
      <Button
        small={small}
        round
        basic
        icon={IconType.CHEVRON_LEFT}
        onClick={() => setSelectedPage(String(Number(selectedPage) - 1))}
        disabled={showAll || selectedPage === 1}
      />
      <InputGroup small={small} width="auto">
        <Input
          right
          value={unvalidatedSelectedPage}
          placeholder={String(selectedPage)}
          onChange={(evt) => setSelectedPage(evt.target.value)}
          width="50px"
          disabled={showAll || numPages <= 1}
          error={error}
        />
        <InputGroupAddon>of {numPages}</InputGroupAddon>
      </InputGroup>
      <Button
        small={small}
        round
        basic
        icon={IconType.CHEVRON_RIGHT}
        onClick={() => setSelectedPage(String(Number(selectedPage) + 1))}
        disabled={showAll || selectedPage === numPages}
      />
      <Button
        small={small}
        round
        basic
        icon={IconType.CHEVRON_DOUBLE_RIGHT}
        onClick={() => setSelectedPage(String(numPages))}
        disabled={showAll || selectedPage === numPages}
        testId={testIds?.doubleRightBtn}
      />
    </div>
  );
};
