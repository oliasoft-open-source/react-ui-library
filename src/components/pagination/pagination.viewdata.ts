import {
  IPaginationOptionType,
  IPaginationPerPageType,
  IPaginationValidationResult,
} from './pagination.interface';

export const selectedRowsPerPage = (
  options?: IPaginationOptionType[],
  value?: number,
): number => {
  return (
    value ||
    (options &&
      options.reduce((acc: number, row: IPaginationOptionType) => {
        return row?.selected ? Number(row?.value) : acc;
      }, 10)) ||
    10
  );
};

/**
 * Calculate number of pages
 * @param numberOfRows Number of rows
 * @param pageContainer Object containing page 'options' and rows per page 'value'.
 * @returns Number of pages
 */

export const numberOfPages = (
  numberOfRows?: number,
  pageContainer?: IPaginationPerPageType,
): number => {
  // Default
  if (!pageContainer || !numberOfRows) {
    return 10;
  }

  const { options, value } = pageContainer;
  const valueToNum = Number(value);

  // Show all
  if (valueToNum === 0) {
    return 1;
  }

  return Math.ceil(numberOfRows / selectedRowsPerPage(options, valueToNum));
};

/**
 * Validate the selected page input value
 * @param selectedPage Selected page
 * @param numPages Number of pages
 * @param errorMessageTemplate Error message template
 * @returns Validation object
 */

export const validateSelectedPage = (
  selectedPage: string,
  numPages: number,
  errorMessageTemplate = 'Selected page must be a positive whole number between 1 and',
): IPaginationValidationResult => {
  const error = `${errorMessageTemplate} ${numPages}`;
  const valid: IPaginationValidationResult = {
    valid: true,
    error: null,
  };
  if (selectedPage === '') {
    return valid;
  }
  if (
    !selectedPage.match(/^\d+$/g) ||
    parseInt(selectedPage) <= 0 ||
    parseInt(selectedPage) > numPages
  ) {
    return {
      valid: false,
      error,
    };
  }
  return valid;
};
