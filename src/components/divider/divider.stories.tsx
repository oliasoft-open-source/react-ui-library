import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Align } from 'typings/common-types';
import { Text } from '../text/text';
import { Divider, IDividerProps } from './divider';

export default {
  title: 'Layout/Divider',
  component: Divider,
  argTypes: {
    align: {
      control: {
        type: 'inline-radio',
      },
      options: ['left', 'center', 'right'] as const,
    },
  },
  decorators: [(story) => <>content{story()}more content</>],
  parameters: {
    docs: {
      source: {
        excludeDecorators: true,
      },
    },
  },
} as Meta;

const Template: StoryFn<IDividerProps> = (args) => <Divider {...args} />;

export const Default = Template.bind({});

export const WithText = Template.bind({});
WithText.args = {
  children: 'Heading',
  align: Align.LEFT,
};

export const Margin = Template.bind({});
Margin.args = {
  margin: 0,
};

export const CustomColor = Template.bind({});
CustomColor.args = {
  color: 'var(--color-text-success)',
  children: <Text success>Colored text</Text>,
  align: Align.LEFT,
};
