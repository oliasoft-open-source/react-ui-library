import React from 'react';
import { Align, TAlign } from 'typings/common-types';
import styles from './divider.module.less';
import { Text } from '../text/text';

export interface IDividerProps {
  margin?: number | string;
  color?: string;
  align?: TAlign | string;
  children?: React.ReactNode;
}

const cssAlignClass = (align?: string) => {
  switch (align) {
    case Align.LEFT:
      return styles.alignLeft;
    case Align.RIGHT:
      return styles.alignRight;
    default:
      return '';
  }
};

export const Divider = ({
  children,
  margin = 'var(--padding)',
  color = 'var(--color-border)',
  align = Align.CENTER,
}: IDividerProps) => (
  <div
    className={`${styles.divider} ${cssAlignClass(align)}`}
    style={
      {
        marginTop: margin,
        marginBottom: margin,
        '--color': color,
      } as React.CSSProperties
    }
  >
    {typeof children === 'string' ? <Text muted>{children}</Text> : children}
  </div>
);
