import { isEmpty, noop } from 'lodash';
import React, { ReactNode, useContext } from 'react';
import RcSlider, { SliderProps } from 'rc-slider';
import memoizeOne from 'memoize-one';
import isEqual from 'react-fast-compare';
import cx from 'classnames';
import Handle from 'rc-slider/lib/Handles/Handle.js';
import { IconType } from 'typings/common-types';
import {
  TChangeEvent,
  TChangeEventHandler,
  TStringNumberNull,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import './rc-slider.less';
import styles from './slider.module.less';
import { Tooltip } from '../tooltip/tooltip';
import { Button } from '../button/button';

export type TSliderMark = {
  value: number | string;
  label?: number | string | ReactNode;
  tooltip?: number | string | ReactNode;
};

export interface ISliderProps {
  name?: string;
  label?: TStringNumberNull | ReactNode;
  width?: TStringOrNumber;
  labelWidth?: TStringOrNumber;
  value: number | number[];
  min: number;
  max: number;
  step?: number;
  marks?: TSliderMark[];
  showArrows?: boolean;
  showTooltip?: boolean;
  tooltipFormatter?: (value: number | number[]) => TStringOrNumber | ReactNode;
  disabled?: boolean;
  range?: boolean;
  small?: boolean;
  //for vertical variant:
  vertical?: {
    enabled: boolean;
    width?: string;
    height?: string;
  };
  onChange: TChangeEventHandler;
}

const formatMarkers = (marks: any[]): Record<TStringOrNumber, ReactNode> => {
  return marks.reduce((acc, { label, tooltip, value }) => {
    acc[value] = (
      <Tooltip
        text={
          <>
            {/* Display tooltip text and colon if set */}
            {tooltip}
            {tooltip ? ': ' : ''}
            {/* Display raw value if custom label not set */}
            {label ?? value}
          </>
        }
      >
        <div className={styles.dot} />
      </Tooltip>
    );
    return acc;
  }, {});
};

const memoizedFormatMarkers = memoizeOne(formatMarkers, isEqual);

export const Slider = ({
  name,
  label,
  width = '100%',
  labelWidth = 'auto',
  value,
  min,
  max,
  step = 1,
  marks = [],
  showArrows = false,
  showTooltip = false,
  tooltipFormatter = (v) => v,
  disabled = false,
  range = false,
  small = false,
  vertical = {
    enabled: false,
    width: '100px',
    height: '400px',
  },
  onChange = noop,
}: ISliderProps) => {
  const disabledContext = useContext(DisabledContext);

  const formattedMarks = memoizedFormatMarkers(marks);

  const onChangeValue = (value: any) => {
    const eventLabel = typeof label === 'string' ? label : String(label);
    const eventName = name ?? '';
    //rc-slider doesn't pass the Synthetic event
    //but we can still follow the standard event format
    onChange({
      target: {
        name: eventName,
        value,
        label: eventLabel,
      },
    } as TChangeEvent);
  };

  const ButtonMin = () => (
    <div className={styles.button}>
      <Button
        basic
        small
        round
        onClick={() =>
          onChangeValue(range ? [min, (value as number[])?.[1]] : min)
        }
        disabled={disabled || value === min || disabledContext}
        icon={IconType.FAST_BACKWARD}
      />
    </div>
  );

  const ButtonMax = () => (
    <div className={styles.button}>
      <Button
        basic
        small
        round
        onClick={() =>
          onChangeValue(range ? [(value as number[])?.[0], max] : max)
        }
        disabled={disabled || value === max || disabledContext}
        icon={IconType.FAST_FORWARD}
      />
    </div>
  );

  // From https://github.com/react-component/slider/blob/master/docs/examples/components/TooltipSlider.tsx
  const handleRender: SliderProps['handleRender'] = (node, handleProps) => {
    const isTooltipVisible = showTooltip && handleProps.dragging;
    return (
      // TODO: revisit - fix handle warning
      // @ts-ignore: revisit - fix handle warning
      <Handle {...node.props} {...handleProps}>
        {isTooltipVisible && (
          <Tooltip display="block" text={tooltipFormatter(value)} forceOpen>
            {/* Pin child to handle edges for tooltip positioning */}
            <div style={{ position: 'absolute', inset: 0 }} />
          </Tooltip>
        )}
      </Handle>
    );
  };

  return (
    <div
      className={cx(
        styles.container,
        small ? styles.small : '',
        vertical.enabled ? styles.vertical : '',
      )}
      style={
        vertical.enabled
          ? { width: vertical.width, height: vertical.height }
          : { width }
      }
    >
      {showArrows && (vertical.enabled ? <ButtonMax /> : <ButtonMin />)}
      <RcSlider
        range={range}
        allowCross={false}
        className={cx(
          (disabled || disabledContext) && styles.rcSliderDisabled,
          showArrows && styles.hasArrows,
        )}
        value={value}
        max={max}
        min={min}
        step={step}
        marks={formattedMarks}
        onChange={(v) => onChangeValue(v)}
        disabled={disabled || disabledContext}
        vertical={vertical.enabled}
        handleRender={handleRender}
      />
      {showArrows && (vertical.enabled ? <ButtonMin /> : <ButtonMax />)}
      {label && (
        <label className={styles.label} style={{ width: labelWidth }}>
          {label}
        </label>
      )}
    </div>
  );
};
