import React, { useState } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { ISliderProps, Slider } from './slider';
import { Text } from '../text/text';

const marks = [
  { label: 'S1...', tooltip: 'S1 Production', value: 20 },
  { label: 'S2...', tooltip: 'S2 Cementing', value: 40 },
  {
    label: 'S3...',
    tooltip: (
      <span>
        <strong>S3</strong> Drilling
      </span>
    ),
    value: 60,
  },
  { label: 'S4...', tooltip: 'S4 Shut in', value: 80 },
  { label: 'S5...', tooltip: 'S5 Circulate', value: 100 },
];

export default {
  title: 'Forms/Slider',
  component: Slider,
  args: {
    value: 50,
    min: 0,
    max: 100,
  },
} as Meta;

const Template: StoryFn<ISliderProps> = (args) => {
  const [value, setValue] = useState(args.value);
  return (
    <div style={{ paddingTop: 40 }}>
      <Slider
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...args}
        value={value}
        onChange={(evt) => setValue(evt.target.value)}
      />
    </div>
  );
};
export const Default = Template.bind({});

export const Range = Template.bind({});
Range.args = { range: true, value: [10, 40] };

export const Label = Template.bind({});
Label.args = { label: 'Number of aardvarks' };

export const RangeWithArrows = Template.bind({});
RangeWithArrows.args = { range: true, value: [10, 40], showArrows: true };

export const JSXLabel: StoryFn<ISliderProps> = (args) => {
  const [{ value }, updateArgs] = useArgs();
  const handleChange = (evt: any) => {
    updateArgs({ value: evt.target.value });
  };
  return (
    <>
      <Slider
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...args}
        label={
          <span>
            Fixed width label: <strong>{value * Math.random()}</strong>
          </span>
        }
        labelWidth="400px"
        value={value}
        onChange={handleChange}
      />
    </>
  );
};

export const Tooltip = Template.bind({});
Tooltip.args = { showTooltip: true };

export const FormattedTooltip = Template.bind({});
FormattedTooltip.args = {
  showTooltip: true,
  tooltipFormatter: (v) => <Text warning>{`${v} extra info lorem ipsum`}</Text>,
};

export const Arrows = Template.bind({});
Arrows.args = { showArrows: true };

export const Marks = Template.bind({});
Marks.args = {
  marks: [
    { value: 20 },
    { value: 40 },
    { value: 60 },
    { value: 80 },
    { value: 100 },
  ],
};

export const MarksWithExtraTooltip = Template.bind({});
MarksWithExtraTooltip.args = {
  marks: [
    { tooltip: 'S1 Production', value: 20 },
    { tooltip: 'S2 Cementing', value: 40 },
    {
      tooltip: (
        <>
          <strong>S3</strong> Drilling
        </>
      ),
      value: 60,
    },
    { tooltip: 'S4 Shut in', value: 80 },
    { tooltip: 'S5 Circulate', value: 100 },
  ],
};

export const MarksWithCustomLabel = Template.bind({});
MarksWithCustomLabel.args = {
  marks: [
    { label: '20m', value: 20 },
    { label: '40m', value: 40 },
    { label: '60m', value: 60 },
    { label: '80m', value: 80 },
    { label: '100m', value: 100 },
  ],
};

export const Small = Template.bind({});
Small.args = { small: true };
Small.parameters = {
  docs: {
    description: {
      story:
        'Reduces the vertical height of the `Slider` container to align with other `small` elements.',
    },
  },
};

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const Vertical = Template.bind({});
Vertical.args = {
  marks,
  vertical: {
    enabled: true,
    width: '100px',
    height: '400px',
  },
};
