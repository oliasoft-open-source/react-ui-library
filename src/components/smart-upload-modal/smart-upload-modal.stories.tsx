import { Meta } from '@storybook/react';
import React, { useState } from 'react';
import { Button } from 'components/button/button';
import { SmartUploadModal } from './smart-upload-modal';

export default {
  title: 'Ai/SmartUploadModal',
  component: SmartUploadModal,
  args: {
    visible: true,
    prompt: 'This is a prompt',
    onCloseModal: () => {},
    onUpload: () => {},
    onComplete: () => {},
  },
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
} as Meta;

export const SmartUpload = () => {
  const [visible, setVisible] = useState(true);

  const openModal = () => {
    setVisible(true);
  };

  const closeModal = () => {
    setVisible(false);
  };

  const onUpload = () => {
    console.log('Uploading');
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve([]);
      }, 4000);
    });
  };

  const onComplete = () => {
    console.log('Completed');
    closeModal();
  };

  return (
    <>
      <SmartUploadModal
        visible={visible}
        prompt="This is a prompt"
        onCloseModal={closeModal}
        onUpload={onUpload}
        onComplete={onComplete}
      />
      <Button label="Open Modal" onClick={openModal} />
    </>
  );
};
