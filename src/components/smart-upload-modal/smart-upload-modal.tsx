import React, { useEffect, useState } from 'react';
import { Button } from 'components/button/button';
import { Dialog } from 'components/dialog/dialog';
import { FileInput } from 'components/file-input/file-input';
import { Spacer } from 'components/layout/spacer/spacer';
import { Modal } from 'components/modal/modal';
import { Text } from 'components/text/text';
import { useKeyboardEvent } from 'hooks/use-keyboard-event';
import { ProgressBar } from 'components/progress-bar/progress-bar';
import { roundByMagnitudeToFixed } from '@oliasoft-open-source/units';

const INTERVAL_TIME = 250;
const REQUEST_TIME = 40000;
const FINISH_INTERVALS = 20;
const FINISH_TIME = 500;

export type ISmartUploadModalProps = {
  visible: boolean;
  prompt: string;
  onCloseModal: () => void;
  onUpload: (prompt: string, file: File) => any;
  onComplete: (data: { [key: string]: string }[]) => void;
  onFailed?: () => void;
  onError?: (error: any) => void;
  width?: number;
  heading?: string;
  dialogText?: string;
  fileInputText?: string;
  fileInputPlaceholder?: string;
  uploadText?: string;
  cancelText?: string;
  testId?: string;
};

export const SmartUploadModal = ({
  visible,
  prompt,
  onCloseModal,
  onUpload,
  onComplete,
  onFailed,
  onError,
  width,
  heading,
  dialogText,
  fileInputText,
  fileInputPlaceholder,
  uploadText,
  cancelText,
  testId,
}: ISmartUploadModalProps) => {
  const [file, setFile] = useState<File | null>(null);
  const [isFetching, setFetching] = useState(false);
  const [requestTimer, setRequestTimer] = useState(0);
  let timer = 0;

  const updateTimer = () => {
    const newTime = timer + INTERVAL_TIME;
    timer = newTime;
    setRequestTimer(timer);
  };

  const finishTimer = (deltaTime: number) => {
    let newTime = timer + deltaTime;
    if (newTime >= 0.99 * REQUEST_TIME) {
      newTime = REQUEST_TIME;
    }
    timer = newTime;
    setRequestTimer(timer);
  };

  useEffect(() => {
    setFile(null);
  }, [visible]);

  const handleFileChange = (event: any) => {
    if (!event.target.value) return;
    setFile(event.target.value);
  };

  const handleUpload = async () => {
    setRequestTimer(0);
    const id = setInterval(updateTimer, INTERVAL_TIME);
    try {
      if (!file) return;

      setFetching(true);

      const data = await onUpload(prompt, file);

      if (!visible) return;

      if (data) {
        // Clear first interval and set another to hurry up the progress bar.
        clearInterval(id);
        const id2 = setInterval(
          finishTimer,
          FINISH_TIME / FINISH_INTERVALS,
          (REQUEST_TIME - timer) / (FINISH_INTERVALS - 1),
        );
        setTimeout(() => {
          onComplete(data);
          clearInterval(id2);
          setFetching(false);
        }, FINISH_TIME);
      } else {
        onFailed?.();
        setFetching(false);
      }
    } catch (error) {
      onError?.(error);
      setFetching(false);
    } finally {
      clearInterval(id);
    }
  };

  const onConfirm = () => {
    handleUpload();
  };

  const onClose = () => {
    onCloseModal();
  };

  const content = (
    <>
      <Text>{dialogText || 'Upload file and extract data using a LLM'}</Text>
      <Spacer />
      <FileInput
        placeholder={fileInputPlaceholder || 'No file'}
        label={fileInputText || 'Select'}
        file={file || undefined}
        onChange={handleFileChange}
        testId={`${testId}-file-input`}
        accept="image/*"
      />
      {isFetching && (
        <>
          <Spacer />
          <ProgressBar
            percentage={roundByMagnitudeToFixed(
              (100 * requestTimer) / REQUEST_TIME,
              3,
            )}
          />
        </>
      )}
    </>
  );

  const footer = (
    <>
      <Button
        label={uploadText || 'Upload'}
        colored
        onClick={onConfirm}
        disabled={!file || isFetching}
        testId={`${testId}-button-upload`}
      />
      <Button
        label={cancelText || 'Cancel'}
        onClick={onClose}
        testId={`${testId}-button-cancel`}
      />
    </>
  );

  useKeyboardEvent('Escape', () => onClose());

  return (
    <Modal visible={visible} centered>
      <Dialog
        dialog={{
          scroll: true,
          heading: heading || 'Smart Upload',
          content,
          footer,
          onClose,
          width: width || 350,
          testId,
        }}
      />
    </Modal>
  );
};
