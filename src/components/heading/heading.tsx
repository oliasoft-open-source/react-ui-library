import React, { ReactNode, MouseEvent } from 'react';
import cx from 'classnames';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import styles from './heading.module.less';
import { HelpIcon } from '../help-icon/help-icon';

interface ILibraryIconProps {
  onClick: TEmpty;
  tooltip?: string;
}

export interface IHeadingProps {
  children: ReactNode;
  helpText?: string;
  onClick?: (evt: MouseEvent<HTMLDivElement>) => void;
  onClickHelp?: TEmpty;
  onIconClick?: TEmpty;
  icon?: ReactNode;
  libraryIcon?: ILibraryIconProps;
  marginBottom?: TStringOrNumber;
  top?: boolean;
  testId?: string;
}

export const Heading = ({
  children,
  helpText,
  onClick,
  onClickHelp,
  onIconClick,
  icon,
  libraryIcon,
  marginBottom,
  top = false,
  testId,
}: IHeadingProps) => {
  const isHelpIconDisplayed = !!helpText || !!onClickHelp;
  return (
    <div
      className={cx(styles.heading, top ? styles.top : '')}
      style={{ marginBottom }}
      onClick={onClick}
      data-testid={testId}
    >
      {children}
      <div className={styles.icons}>
        {icon && <HelpIcon onClick={onIconClick} icon={icon} />}
        {isHelpIconDisplayed && (
          <HelpIcon text={helpText} onClick={onClickHelp} />
        )}
        {libraryIcon && (
          <HelpIcon
            text={libraryIcon?.tooltip ?? ''}
            onClick={libraryIcon.onClick}
            icon={IconType.LIBRARY}
          />
        )}
      </div>
    </div>
  );
};
