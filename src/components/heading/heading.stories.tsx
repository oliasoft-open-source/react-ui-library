import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IconType } from 'typings/common-types';
import { Heading, IHeadingProps } from './heading';
import { Flex } from '../layout/flex/flex';
import { Icon } from '../icon/icon';

export default {
  title: 'Basic/Heading',
  component: Heading,
  args: {
    children: 'Heading',
    top: false,
  },
} as Meta;

const Template: StoryFn<IHeadingProps> = (args) => (
  <Heading
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);

export const Default = Template.bind({});

export const Top = Template.bind({});
Top.args = {
  top: true,
};

export const CustomMargin = Template.bind({});
CustomMargin.args = {
  marginBottom: 50,
};

export const Help = Template.bind({});
Help.args = {
  helpText: 'Help!',
  onClickHelp: () => console.log('Help received :)'),
};

export const LibraryIcon = Template.bind({});
LibraryIcon.args = {
  libraryIcon: {
    onClick: () => {},
    tooltip: 'Edit in library',
  },
};

export const CustomIcon = Template.bind({});
CustomIcon.args = {
  icon: <Icon icon={IconType.ADD} />,
  onIconClick: () => console.log('clicked icon!'),
};

export const JSXContent = Template.bind({});
JSXContent.args = {
  children: (
    <Flex alignItems="center" gap="var(--padding-xxs)">
      Subheading
      <Icon icon={IconType.SUCCESS} />
    </Flex>
  ),
};
