import React, { ReactNode, useRef } from 'react';
import cx from 'classnames';
import { get } from 'lodash';
import { TAlign } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './table.module.less';
import { Title } from './title/title';
import { Footer, IFooterProps } from './footer/footer';
import {
  getColumnCount,
  hasRowActions,
  TRowsType,
  TRowType,
} from './table.viewdata';
import { Row } from './row/row';
import { TableDragWrapper } from './table-drag-wrapper';
import { TableScrollWrapper } from './table-scroll-wrapper';

export interface ITableTableProps {
  name?: TStringOrNumber | ReactNode;
  actionsRight?: boolean;
  fixedWidth?: TStringOrNumber;
  maxHeight?: TStringOrNumber;
  columnWidths?: string[];
  className?: string;
  columnHeaderAlignments?: Array<TAlign | string>;
  columnAlignment?: Array<TAlign | string>;
  infiniteScroll?: boolean;
  headers?: any[];
  rows?: TRowsType;
  footer?: IFooterProps;
  draggable?: boolean;
  bordered?: boolean;
  striped?: boolean;
  testId?: string;
  onAddRow?: any;
  defaultEmptyRow?: any;
  stickyHeaders?: string | boolean;
  actions?: any;
}

export interface ITableProps {
  onListReorder?: (obj: { from: number; to: number }) => void;
  canListReorder?: (obj: { from: number; to: number }) => boolean;
  table: ITableTableProps;
}

export const Table = (props: ITableProps) => {
  const {
    onListReorder = () => {},
    canListReorder = () => true,
    table: propTable,
  } = props;
  const {
    columnWidths,
    footer,
    name,
    rows: propRows = [],
    actions,
    actionsRight,
    columnHeaderAlignments,
    columnAlignment,
    striped = true,
    testId,
    draggable,
    defaultEmptyRow = null,
    onAddRow = null,
    bordered = true,
    maxHeight,
    fixedWidth: width,
  } = propTable;
  const theadRef = useRef<HTMLTableSectionElement | null>(null);
  const tbodyRef = useRef<HTMLTableSectionElement>(null);

  const headers = get(props, 'table.headers', []);
  const columnCount = getColumnCount(propRows, headers);
  const rowActions = hasRowActions(propRows, headers);
  const colSpan = columnCount + (rowActions ? 1 : 0);

  const isLastPage = footer
    ? footer?.pagination?.rowsPerPage?.value === 0 ||
      Number(footer?.pagination?.selectedPage) >=
        Number(footer?.pagination?.rowCount) /
          Number(footer?.pagination?.rowsPerPage?.value)
    : true;

  const addEmptyTempRow =
    defaultEmptyRow?.cells?.length && onAddRow && isLastPage;
  const tempEmptyCells = addEmptyTempRow
    ? defaultEmptyRow.cells.map((cell: any, i: number) => {
        if (cell?.type === 'Input') {
          return {
            ...cell,
            onChange: (evt: any) => {
              const value = evt?.target?.value;
              const newRow = {
                ...defaultEmptyRow,
                cells: defaultEmptyRow.cells.map(
                  (cell: any, cellIndex: number) => {
                    return {
                      ...cell,
                      value: cellIndex === i ? value : cell?.value,
                    };
                  },
                ),
              };
              onAddRow({ newRow });
            },
          };
        }
        return {
          ...cell,
          disabled: true, // Just input cell would be editable
        };
      })
    : null;

  const newEmptyRow: TRowType = {
    cells: tempEmptyCells,
    testId: testId ? `${testId}-last-empty-row` : undefined,
  };

  const rows = addEmptyTempRow ? propRows.concat(newEmptyRow) : propRows;

  const table = {
    ...propTable,
    rows,
  };

  const wrapperClass = cx(styles.wrapper, bordered ? styles.bordered : '');

  const wrapperStyles = {
    maxHeight,
    width,
    // Collapse width if width = auto
    display: width === 'auto' ? 'inline-flex' : 'flex',
  };

  return (
    <div className={wrapperClass} style={wrapperStyles}>
      <Title
        actions={actions}
        actionsRight={actionsRight}
        name={name}
        testId={testId && `${testId}-title`}
      />

      <TableDragWrapper
        colSpan={colSpan}
        columnAlignment={columnAlignment}
        columnCount={columnCount}
        draggable={draggable}
        onListReorder={onListReorder}
        rowActions={rowActions}
        rows={rows}
        tbodyRef={tbodyRef}
        canListReorder={canListReorder}
      >
        {({ dropDisabled }) => (
          <TableScrollWrapper table={table} theadRef={theadRef}>
            {({ virtualizer, tableStyle }) => (
              <table
                className={cx(styles.table, striped ? styles.striped : '')}
                data-testid={testId}
                style={tableStyle}
              >
                <thead ref={theadRef}>
                  {headers.map((row, rowIndex) => {
                    return (
                      <Row
                        rowIndex={rowIndex}
                        isHeader
                        row={row}
                        columnCount={columnCount}
                        columnWidths={columnWidths}
                        colSpan={colSpan}
                        hasRowActions={rowActions}
                        key={`0_${rowIndex}`}
                        columnAlignment={columnAlignment}
                        columnHeaderAlignments={columnHeaderAlignments}
                        draggableTable={draggable}
                      />
                    );
                  })}
                </thead>
                <tbody ref={tbodyRef}>
                  {virtualizer
                    ? virtualizer
                        .getVirtualItems()
                        .map((virtualRow) => (
                          <Row
                            rowIndex={virtualRow.index}
                            row={rows[virtualRow.index]}
                            columnCount={columnCount}
                            columnWidths={columnWidths}
                            colSpan={colSpan}
                            hasRowActions={rowActions}
                            key={`1_${virtualRow.index}`}
                            columnAlignment={columnAlignment}
                            draggableTable={draggable}
                            height={virtualRow.size}
                            dropDisabled={dropDisabled}
                          />
                        ))
                    : rows.map((row, index) => (
                        <Row
                          rowIndex={index}
                          row={row}
                          columnCount={columnCount}
                          columnWidths={columnWidths}
                          colSpan={colSpan}
                          hasRowActions={rowActions}
                          key={`1_${index}`}
                          columnAlignment={columnAlignment}
                          draggableTable={draggable}
                          dropDisabled={dropDisabled}
                        />
                      ))}
                </tbody>
              </table>
            )}
          </TableScrollWrapper>
        )}
      </TableDragWrapper>

      {footer && (
        <Footer
          pagination={footer.pagination}
          actions={footer.actions}
          content={footer.content}
        />
      )}
    </div>
  );
};
