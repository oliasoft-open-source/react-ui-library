import React, { ReactNode } from 'react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './title.module.less';
import { Actions } from '../../actions/actions';
import { Heading } from '../../heading/heading';

export interface ITitleProps {
  name?: TStringOrNumber | ReactNode;
  actions?: any;
  actionsRight?: boolean;
  testId?: string;
}

export const Title = ({
  name = null,
  actions = null,
  actionsRight = false,
  testId,
}: ITitleProps) => {
  const isNameDefined = name !== null;
  const isActionsDefined = actions !== null;

  if (!isNameDefined && !isActionsDefined) {
    return null;
  }

  const actionsContainer = () => {
    if (!isActionsDefined) {
      return null;
    }
    const actionsComponent = <Actions actions={actions} />;
    if (actionsRight) {
      return actionsComponent;
    }
    return <div>{actionsComponent}</div>;
  };

  return (
    <div className={styles.title}>
      {isNameDefined ? (
        <Heading>
          <span data-testid={testId}>{name}</span>
        </Heading>
      ) : null}
      {actionsContainer()}
    </div>
  );
};
