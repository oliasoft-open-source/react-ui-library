import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Table } from '../table';
import * as storyData from '../table.stories-data';

export default {
  title: 'Basic/Table/Title & Header',
  component: Table,
  args: {
    table: {
      ...storyData.table,
    },
  },
} as Meta;

const Template: StoryFn<any> = (args) => {
  return (
    <Table
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

export const Units = Template.bind({});
Units.args = {
  table: storyData.tableUnits,
};

export const ActiveRow = Template.bind({});
ActiveRow.args = {
  table: storyData.tableActiveRow,
};

export const Title = Template.bind({});
Title.args = {
  table: storyData.tableTitle,
};

export const TitleJSX = Template.bind({});
TitleJSX.args = {
  table: storyData.tableTitleJsx,
};

export const ChildComponent = Template.bind({});
ChildComponent.args = {
  table: storyData.tableChildComponent,
};

export const Filter = Template.bind({});
Filter.args = {
  table: storyData.tableFilters,
};

export const Sort = Template.bind({});
Sort.args = {
  table: storyData.tableSort,
};

export const HeaderActionsNoLabel = Template.bind({});
HeaderActionsNoLabel.args = {
  table: storyData.tableWithActionHeaderAndNoLabel,
};

export const HelpAndLibraryIcons = Template.bind({});
HelpAndLibraryIcons.args = {
  table: storyData.tableHelpLibraryIcons,
};
