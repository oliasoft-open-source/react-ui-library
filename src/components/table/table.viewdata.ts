import { TEmpty } from 'typings/common-type-definitions';
import { TCellShape } from 'components/table/cell/cell-types/cell-types.interface';

export type TRowType = {
  cells: TCellShape[];
  actions?: any[];
  testId?: string;
  noDrag?: boolean;
  onRowMouseEnter?: TEmpty;
  onRowMouseLeave?: TEmpty;
};

export type TRowsType = TRowType[];

const getMaxCellCount = (rows: TRowsType): number => {
  const count = Math.max(
    ...rows.reduce((acc: number[], row: TRowType) => {
      const count = row.cells
        .map((c: any) => (c.colSpan ? c.colSpan : 1))
        .reduce((a: number, b: number) => a + b, 0);
      return acc.concat(count);
    }, []),
  );
  return !isNaN(count) ? count : 0;
};

export const getColumnCount = (rows: TRowsType, headers: TRowsType): number => {
  const cellCount = [getMaxCellCount(headers), getMaxCellCount(rows)];
  return Math.max(...cellCount);
};

const hasActions = (rows: TRowsType): number =>
  rows.reduce(
    (acc: number, row: TRowType) =>
      row.actions && row.actions.length > 0 ? 1 : acc,
    0,
  );

export const hasRowActions = (rows: TRowsType, headers: TRowsType): boolean => {
  return hasActions(headers) > 0 || hasActions(rows) > 0;
};
