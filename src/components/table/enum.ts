export enum CellType {
  INPUT = 'Input',
  NUMBER_INPUT = 'NumberInput',
  SELECT = 'Select',
  POPOVER = 'Popover',
  SLIDER = 'Slider',
  CHECKBOX = 'CheckBox',
  ACTIONS = 'Actions',
  AUTO_UNIT = 'AutoUnit',
}
