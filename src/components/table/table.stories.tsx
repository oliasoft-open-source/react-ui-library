import React, {
  ChangeEvent,
  ClipboardEventHandler,
  useEffect,
  useState,
} from 'react';
import { produce } from 'immer';
import { isNaN, isInteger, random } from 'lodash';
import { Meta, StoryFn } from '@storybook/react';
import { TChangeEvent, TEmpty } from 'typings/common-type-definitions';
import { HelpIcon } from 'components/help-icon/help-icon';
import { Grid } from 'components/layout/grid/grid';
import { Text } from 'components/text/text';
import { ISelectSelectedOption } from 'components/select/select.interface';
import { Table } from './table';
import { Card } from '../card/card';
import { Input } from '../input/input';
import { Field } from '../form/field';
import { Heading } from '../heading/heading';
import * as storyData from './table.stories-data';

export default {
  title: 'Basic/Table',
  component: Table,
  args: {
    table: {
      ...storyData.table,
    },
  },
} as Meta;

const Template: StoryFn<any> = (args) => {
  return (
    <Table
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};
export const Default = Template.bind({});

export const FixedWidth = Template.bind({});
FixedWidth.args = {
  table: { ...storyData.table, fixedWidth: '400px' },
};

export const AutoWidth = Template.bind({});
AutoWidth.args = {
  table: { ...storyData.table, fixedWidth: 'auto' },
};

export const ColumnWidths = Template.bind({});
ColumnWidths.args = {
  table: storyData.tableColumnWidths,
};
ColumnWidths.parameters = {
  docs: {
    description: {
      story:
        'Use sparingly when certain columns cannot be below a certain width. Unless the Table is auto width, set one column to `auto` to use remaining space.',
    },
  },
};

export const ColumnAlignment = Template.bind({});
ColumnAlignment.args = {
  table: storyData.tableAlignment,
};

export const ColumnHeaderAlignment = Template.bind({});
ColumnHeaderAlignment.args = {
  table: storyData.tableHeaderAlignment,
};
ColumnHeaderAlignment.parameters = {
  docs: {
    description: {
      story:
        'This is only required when you need to override `columnAlignment` in header cells (i.e. they have different alignment)',
    },
  },
};

export const InfiniteScrollFixedHeight = Template.bind({});
InfiniteScrollFixedHeight.args = {
  table: storyData.tableInfiniteScroll,
};

export const InfiniteScrollFullHeight = Template.bind({});
InfiniteScrollFullHeight.args = {
  table: storyData.tableInfiniteScrollFullHeight,
};
InfiniteScrollFullHeight.decorators = [
  (Story) => (
    <div style={{ position: 'absolute', inset: 0, padding: 'var(--padding)' }}>
      <Story />
    </div>
  ),
];
InfiniteScrollFullHeight.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const InCard: StoryFn<any> = () => (
  <Card heading={<Heading>Card heading</Heading>} padding={false}>
    <Table table={{ ...storyData.table, bordered: false } as any} />
  </Card>
);
InCard.parameters = {
  docs: { source: { type: 'dynamic' } },
};

export const Managed: StoryFn<any> = () => {
  /*
    Mock table data store (real apps should use Redux or similar)
  */
  const headings = ['Section', 'Width', 'Height'];
  let units = ['', 'm', 'm'];
  let data = [...Array(175).keys()].map((c, i) => [i, i * 2, i * 2]);
  /*
    Mock table data actions (real apps should use Redux or similar)
  */
  const convertUnit = (fromUnit: any, toUnit: any, value: any) => {
    return fromUnit === 'm' && toUnit === 'ft'
      ? value * 3.28084
      : fromUnit === 'ft' && toUnit === 'm'
      ? value / 3.28084
      : value;
  };
  const onChangeValue = (
    evt: any,
    rowIndex: any,
    cellIndex: any,
    render: any,
  ) => {
    data[rowIndex][cellIndex] = evt.target.value;
    render();
  };
  const onAddRow = (render: any) => {
    data = data.concat([['', '', '']] as any);
    render();
  };
  const onDeleteRow = (rowIndex: any, render: any) => {
    data = data.filter((r, i) => i !== rowIndex);
    render();
  };
  const onChangeUnit = (evt: any, columnIndex: any, render: any) => {
    const fromUnit = units[columnIndex];
    const toUnit = evt.target.value;
    units = units.map((u, i) => (i === columnIndex ? toUnit : u));
    data = data.map((r) =>
      r.map((c, i) =>
        i === columnIndex ? convertUnit(fromUnit, toUnit, c) : c,
      ),
    );
    render();
  };
  /*
    Container component manages state and configuration of table
  */
  const ManagedTable = () => {
    const rowsPerPageOptions = [
      { label: '10 / page', value: 10 },
      { label: '20 / page', value: 20 },
      { label: '50 / page', value: 50 },
      { label: 'Show all', value: 0 },
    ];
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [selectedPage, setSelectedPage] = useState(1);
    //hack to make Storybook re-render after event handlers alter data
    //only using this because example does not manage data with Redux / props
    const [toggle, forceRender] = useState(false);
    const render = () => {
      forceRender(!toggle);
    };
    const goToLastPage = () => {
      const lastPage = Math.ceil(data.length / rowsPerPage);
      setSelectedPage(lastPage);
    };
    const firstVisibleRow = (selectedPage - 1) * rowsPerPage;
    const lastVisibleRow =
      rowsPerPage === 0 ? data.length - 1 : firstVisibleRow + rowsPerPage;
    const table = {
      headers: [
        {
          cells: headings.map((h) => ({ value: h })),
          actions: [
            {
              primary: true,
              label: 'Add',
              icon: 'add',
              onClick: () => {
                onAddRow(render);
                goToLastPage();
              },
            },
          ],
        },
        {
          cells: [
            {},
            {
              options: [
                { label: 'm', value: 'm' },
                { label: 'ft', value: 'ft' },
              ],
              value: units[1],
              type: 'Select',
              native: true,
              onChange: (evt: any) => onChangeUnit(evt, 1, render),
            },
            {
              options: [
                { label: 'm', value: 'm' },
                { label: 'ft', value: 'ft' },
              ],
              value: units[2],
              type: 'Select',
              native: true,
              onChange: (evt: any) => onChangeUnit(evt, 2, render),
            },
          ],
        },
      ],
      rows: data.slice(firstVisibleRow, lastVisibleRow).map((row, i) => {
        const rowIndex = firstVisibleRow + i;
        return {
          cells: [...Array(3).keys()].map((c, i) => ({
            //https://stackoverflow.com/a/33352604
            value: row[i],
            type: 'Input',
            onChange: (evt: any) => onChangeValue(evt, rowIndex, i, render),
          })),
          actions: [
            {
              label: 'Delete',
              icon: 'minus',
              onClick: () => onDeleteRow(rowIndex, render),
            },
          ],
        };
      }),
      footer: {
        pagination: {
          rowCount: data.length,
          selectedPage,
          rowsPerPage: {
            onChange: (evt: any) => {
              const { value } = evt.target;
              setRowsPerPage(Number(value));
            },
            options: rowsPerPageOptions,
            value: rowsPerPage,
          },
          onSelectPage: (evt: any) => setSelectedPage(evt),
          small: true,
        },
      },
    };
    return <Table table={table as any} />;
  };
  return <ManagedTable />;
};

export const ManagedWithSort = () => {
  /*
    Mock table data store (real apps should use Redux or similar)
  */
  const headings = ['Section', 'Width', 'Height'];
  let keyedData = [...Array(20).keys()].map((_c, i) => ({
    Section: random(0, 100),
    Width: random(0, 100),
    Height: random(0, 100),
  }));
  /*
    Container component manages state and configuration of table
  */
  const TableWithSort: StoryFn<any> = () => {
    const [sort, setSort] = useState({ Section: 'up' });
    const sortDataRows = (dataRows: any, sort: any) =>
      dataRows.sort((a: any, b: any) =>
        Object.entries(sort)
          .map(([key, value]) => {
            switch (value) {
              case 'up': {
                return a[key] - b[key];
              }
              case 'down': {
                return b[key] - a[key];
              }
              default:
                return 0;
            }
          })
          .reduce((a, acc) => a || acc, 0),
      );
    const dataHeaders = (dataRowsKeys: any, sort: any, setSort: any) => {
      const dataSortCells = dataRowsKeys.map((key: any) => {
        const sortDirection = Object.keys(sort).includes(key) ? sort[key] : '';
        const prettifyHeaderValue = `${key[0].toUpperCase()}${key.slice(1)}`;
        return {
          key,
          value: prettifyHeaderValue,
          hasSort: true,
          sort: sortDirection,
          onSort: () => {
            const newSortDirection = sortDirection === 'up' ? 'down' : 'up';
            setSort({ [key]: newSortDirection });
          },
        };
      });
      return { dataSortCells };
    };
    const { dataSortCells } = dataHeaders(headings, sort, setSort);
    const sortedData = sortDataRows(keyedData, sort);
    const dataRows = [
      ...sortedData.map((dataRow: any) => {
        const rowsCells = Object.entries(dataRow).map(([key, value]) => ({
          key,
          value,
          type: 'Input',
          disabled: true,
        }));
        return {
          cells: rowsCells,
        };
      }),
    ];
    const table = {
      headers: [
        {
          cells: dataSortCells,
        },
      ],
      rows: dataRows,
    };
    return <Table table={table as any} />;
  };
  return <TableWithSort />;
};

export const ManagedWithFilterAndMultipleSort = () => {
  /*
    Mock table data store (real apps should use Redux or similar)
  */
  const headings = ['Section', 'Width', 'Height'];
  let keyedData = [...Array(175).keys()].map((_c, i) => ({
    Section: i,
    Width: i * 2,
    Height: i * 2,
  }));
  /*
        Container component manages state and configuration of table
      */
  const TableWithSortAndFilter: StoryFn<any> = () => {
    const rowsPerPageOptions = [
      { label: '10 / page', value: 10 },
      { label: '20 / page', value: 20 },
      { label: '50 / page', value: 50 },
      { label: 'Show all', value: 0 },
    ];
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [selectedPage, setSelectedPage] = useState(1);
    const [filters, setFilters] = useState({});
    const [sorts, setSorts] = useState({});
    useEffect(() => {
      setSelectedPage(1);
    }, [filters, sorts]);
    const firstVisibleRow = (selectedPage - 1) * rowsPerPage;
    const lastVisibleRow =
      rowsPerPage === 0 ? keyedData.length - 1 : firstVisibleRow + rowsPerPage;
    const filterAndSortDataRows = (dataRows: any, filters: any, sorts: any) =>
      dataRows
        .filter((row: any) =>
          Object.keys(filters).every((key) => {
            return filters[key] === ''
              ? true
              : row[key].toString().includes(filters[key]);
          }),
        )
        .sort((a: any, b: any) =>
          Object.entries(sorts)
            .map(([key, value]) => {
              switch (value) {
                case 'up': {
                  return a[key] - b[key];
                }
                case 'down': {
                  return b[key] - a[key];
                }
                default:
                  return 0;
              }
            })
            .reduce((a, acc) => a || acc, 0),
        );
    const dataHeaders = (
      dataRowsKeys: any,
      filters: any,
      setFilters: any,
      sorts: any,
      setSorts: any,
    ) => {
      const dataSortCells = dataRowsKeys.map((key: any) => {
        const sort = Object.keys(sorts).includes(key) ? sorts[key] : '';
        const prettifyHeaderValue = `${key[0].toUpperCase()}${key.slice(1)}`;
        return {
          key,
          value: prettifyHeaderValue,
          hasSort: true,
          sort,
          onSort: () => {
            const newSort = sort === '' ? 'up' : sort === 'up' ? 'down' : '';
            setSorts({ ...sorts, [key]: newSort });
          },
        };
      });
      const dataFilterCells = dataRowsKeys.map((key: any) => {
        const filterValue = Object.keys(filters).includes(key)
          ? filters[key]
          : '';
        return {
          key,
          value: filterValue,
          type: 'Input',
          placeholder: 'Filter',
          onChange: (ev: any) =>
            setFilters({ ...filters, [key]: ev.target.value }),
        };
      });
      return { dataSortCells, dataFilterCells };
    };
    const { dataSortCells, dataFilterCells } = dataHeaders(
      headings,
      filters,
      setFilters,
      sorts,
      setSorts,
    );
    const filteredAndSortedData = filterAndSortDataRows(
      keyedData,
      filters,
      sorts,
    );
    const dataRows = [
      ...filteredAndSortedData
        .slice(firstVisibleRow, lastVisibleRow)
        .map((dataRow: any) => {
          const rowsCells = Object.entries(dataRow).map(([key, value]) => ({
            key,
            value,
            type: 'Input',
            disabled: true,
          }));
          return {
            cells: rowsCells,
          };
        }),
    ];
    const table = {
      headers: [
        {
          cells: dataSortCells,
        },
        {
          cells: dataFilterCells,
        },
      ],
      rows: dataRows,
      footer: {
        pagination: {
          rowCount: filteredAndSortedData.length,
          selectedPage,
          rowsPerPage: {
            onChange: (evt: any) => {
              const { value } = evt.target;
              setRowsPerPage(Number(value));
            },
            options: rowsPerPageOptions,
            value: rowsPerPage,
          },
          onSelectPage: (evt: any) => setSelectedPage(evt),
          small: true,
        },
      },
    };
    return <Table table={table as any} />;
  };
  return <TableWithSortAndFilter />;
};

export const AutoAddNewRow = () => {
  const prices = {
    tea: 3.6,
    coffee: 3.2,
    orange: 4.1,
  };
  const drinks = [
    { label: 'Tea', value: 'tea' },
    { label: 'Coffee', value: 'coffee' },
    { label: 'Orange juice', value: 'orange' },
  ];
  const defaultEmptyRow = {
    cells: [
      {
        type: 'Select',
        options: drinks,
        value: { value: 'tea' },
      },
      {
        type: 'Input',
      },
      {
        type: 'Static',
      },
    ],
  };
  const initialRows = [...Array(15).keys()].map((row, rowIndex) => ({
    cells: [
      {
        type: 'Select',
        options: drinks,
        value: { value: 'tea' },
      },
      {
        type: 'Input',
        value: rowIndex + 1,
      },
      {
        type: 'Static',
      },
    ],
  }));
  const [rows, setRows] = useState(initialRows);
  const rowsPerPageOptions = [
    { label: '10 / page', value: 10 },
    { label: '20 / page', value: 20 },
    { label: '50 / page', value: 50 },
    { label: 'Show all', value: 0 },
  ];
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [selectedPage, setSelectedPage] = useState(1);
  const firstVisibleRow = (selectedPage - 1) * rowsPerPage;
  const lastVisibleRow =
    rowsPerPage === 0 ? rows.length - 1 : firstVisibleRow + rowsPerPage;

  const onAddRow = ({ newRow }: any = {}) => {
    const nextRows = produce(rows, (draft) => {
      draft.push(newRow ?? defaultEmptyRow);
    });
    setRows(nextRows);
  };

  const onDeleteRow = ({ rowIndex }: any) => {
    const nextRows = produce(rows, (draft) => {
      delete draft[rowIndex];
    }).filter((r) => r);
    setRows(nextRows);
  };

  const onChangeDrink = ({ rowIndex, value }: any) => {
    const nextRows = produce(rows, (draft) => {
      // @ts-ignore: write explicit reason
      draft[rowIndex].cells[0].value.value = value;
    });
    setRows(nextRows);
  };

  const onChangeQuantity = ({ rowIndex, value }: any) => {
    const nextRows = produce(rows, (draft) => {
      draft[rowIndex].cells[1].value = value;
    });
    setRows(nextRows);
  };

  const validQuantity = ({ value }: any) => {
    const numericValue = Number(value);
    return !isNaN(numericValue) && isInteger(numericValue) && numericValue > 0;
  };

  const hydratedRows = rows
    .slice(firstVisibleRow, lastVisibleRow)
    .map((row, i) => {
      const rowIndex = firstVisibleRow + i;
      const isLastRow = rowIndex === rows.length - 1;
      return {
        ...row,
        cells: row?.cells.map((cell, cellIndex) => {
          const drink = (row?.cells?.[0]?.value as any)?.value;
          const quantity = Number(row?.cells?.[1]?.value);
          const drinkPrice = (prices as any)?.[drink];
          const price =
            drinkPrice && validQuantity({ value: quantity })
              ? (drinkPrice * quantity).toFixed(2)
              : null;
          if (cellIndex === 0) {
            return {
              ...cell,
              onChange: (evt: any) =>
                onChangeDrink({ rowIndex, value: evt.target.value }),
            };
          } else if (cellIndex === 1) {
            return {
              ...cell,
              onChange: (evt: any) =>
                onChangeQuantity({ rowIndex, value: evt.target.value }),
              error: !validQuantity({ value: cell.value })
                ? 'Must be valid whole number'
                : null,
            };
          } else if (cellIndex === 2) {
            return {
              ...cell,
              value: price,
            };
          }
        }),
        actions: [
          {
            label: 'Delete',
            icon: 'minus',
            onClick: () => onDeleteRow({ rowIndex }),
          },
        ],
      };
    });
  return (
    <Table
      table={
        {
          defaultEmptyRow,
          onAddRow,
          columnAlignment: ['left', 'right', 'right'],
          headers: [
            {
              cells: [
                { value: 'Drink' },
                { value: 'Quantity' },
                { value: 'Total Price (£)' },
              ],
              actions: [
                {
                  primary: true,
                  icon: 'plus',
                  label: 'Add',
                  onClick: () => onAddRow(),
                },
              ],
            },
          ],
          rows: hydratedRows as any,
          footer: {
            pagination: {
              rowCount: rows.length,
              selectedPage,
              rowsPerPage: {
                onChange: (evt: any) => {
                  const { value } = evt.target;
                  setRowsPerPage(Number(value));
                },
                options: rowsPerPageOptions,
                value: rowsPerPage,
              },
              onSelectPage: (evt: any) => setSelectedPage(evt),
              small: true,
            },
          },
        } as any
      }
    />
  );
};

interface PastedData {
  pastedValues: string;
  rowIndex: number;
  cellIndex: number;
}

export const OnPasteCallback = () => {
  const headings = ['Section', 'Width', 'Height'];
  let data = Array.from(Array(10), (_, i) => [i, i * 2, i * 2]);
  const [pastedData, setpastedData] = useState<PastedData>({
    pastedValues: '',
    rowIndex: 0,
    cellIndex: 0,
  });
  /*
    Mock table data
  */

  const onChangeValue = (
    evt: ChangeEvent<HTMLInputElement>,
    rowIndex: number,
    cellIndex: number,
    render: TEmpty,
  ) => {
    data[rowIndex][cellIndex] = Number(evt.target.value);
    render();
  };
  const handleOnPaste = (
    evt: ClipboardEvent,
    rowIndex: number,
    cellIndex: number,
    render: TEmpty,
  ) => {
    let pastedValues = evt?.clipboardData?.getData('text') ?? '';
    setpastedData({ rowIndex, cellIndex, pastedValues });
    render();
    return { pastedData };
  };
  /*
    Container component manages state and configuration of table
  */
  const CallbackCellTable = () => {
    //hack to make Storybook re-render after event handlers alter data
    //only using this because example does not manage data with Redux / props
    const [toggle, forceRender] = useState(false);
    const render = () => {
      forceRender(!toggle);
    };

    const table = {
      headers: [
        {
          cells: headings.map((h) => ({ value: h })),
        },
      ],
      rows: data.map((row, i) => {
        const rowIndex = i;
        //row
        return {
          cells: [...Array(3).keys()].map((_, i) => ({
            //https://stackoverflow.com/a/33352604
            value: row[i],
            type: 'Input',
            onChange: (evt: ChangeEvent<HTMLInputElement>) =>
              onChangeValue(evt, rowIndex, i, render),
            onPaste: (evt: ClipboardEvent) =>
              handleOnPaste(evt, rowIndex, i, render),
          })),
        };
      }),
    };
    return (
      <>
        <Table table={table} />
        <Field label="Pasted Data">
          <Input value={pastedData.pastedValues} />
        </Field>
        <Field label="At Row">
          <Input value={pastedData.rowIndex} />
        </Field>
        <Field label="At Cell Index">
          <Input value={pastedData.cellIndex} />
        </Field>
      </>
    );
  };
  return <CallbackCellTable />;
};

export const ExampleTableWithInfoTooltips = () => {
  const table = {
    fixedWidth: 'auto',
    name: 'Table title',
    actions: [
      {
        icon: 'download',
        tooltip: 'Download',
      },
    ],
    columnAlignment: [...Array(10)].map((_, i) => 'right'),
    headers: [
      {
        cells: [...Array(10)].map((_, i) => ({
          value: 'Heading',
        })),
      },
      {
        cells: [...Array(10)].map((_, i) => ({
          options: [
            { label: 'm', value: 'm' },
            { label: 'ft', value: 'ft' },
          ],
          type: 'Select',
          searchable: false,
          value: { label: 'm', value: 'm' },
          autoLayerWidth: true,
        })),
      },
    ],
    rows: [...Array(10)].map((_, i) => ({
      cells: [...Array(10)].map((_, i) => ({
        value: 123.45,
      })),
      actions: [
        {
          icon: 'help',
          tooltip: (
            <Grid>
              <Text bold>Title goes here lorem ipsum</Text>
              <Text>Label: 123.45m</Text>
              <Text>Another label: 123.45m</Text>
              <Text>Label: 123.45m</Text>
            </Grid>
          ),
        },
      ],
    })),
  };
  return <Table table={table} />;
};

export const ExampleTableWithInlineUnits = () => {
  const table = {
    fixedWidth: 'auto',
    name: 'Table title',
    actions: [
      {
        icon: 'download',
        tooltip: 'Download',
      },
    ],
    headers: [
      {
        cells: [{ value: 'Label' }, { value: 'Value', colSpan: 2 }],
      },
    ],
    rows: [
      {
        cells: [
          { value: 'Country', style: { fontWeight: 'bold' }, width: '120px' },
          { value: 'Norway', colSpan: 2 },
        ],
        actions: [
          {
            icon: 'help',
            tooltip: (
              <Grid>
                <Text bold>Title goes here</Text>
                <Text>Details go here</Text>
              </Grid>
            ),
          },
        ],
      },
      {
        cells: [
          { value: 'Depth', style: { fontWeight: 'bold' } },
          { value: 123, width: '80px' },
          {
            options: [
              { label: 'm', value: 'm' },
              { label: 'ft', value: 'ft' },
            ],
            value: { label: 'm', value: 'm' },
            type: 'Select',
            width: '80px',
          },
        ],
        actions: [
          {
            icon: 'help',
            tooltip: (
              <Grid>
                <Text bold>Title goes here</Text>
                <Text>Details go here</Text>
              </Grid>
            ),
          },
        ],
      },
    ],
  };
  return <Table table={table} />;
};
