import { getCellAlignment, getHeaderAlignment } from './helper';

const styles = {
  leftAligned: 'cell-module-leftAligned-eBFS4',
  rightAligned: 'cell-module-rightAligned-ahWpT',
  centerAligned: 'cell-module-centerAligned-_ktBT',
};

describe('helper', () => {
  describe('getCellAlignment', () => {
    test('should return style alignment for header row', () => {
      const alignment = {
        bodyAlignment: undefined,
        headerAlignment: 'right',
        isHeader: true,
      };

      const result = getCellAlignment(alignment, styles);

      expect(result).toBe('cell-module-rightAligned-ahWpT');
    });

    test('should return text alignment for header row', () => {
      const alignment = {
        bodyAlignment: undefined,
        headerAlignment: 'right',
        isHeader: true,
      };

      const result = getCellAlignment(alignment, styles, true);

      expect(result).toBe('right');
    });

    test('should return default text alignment', () => {
      const alignment = {
        bodyAlignment: undefined,
        headerAlignment: undefined,
        isHeader: false,
      };

      const result = getCellAlignment(alignment, styles, true);

      expect(result).toBe('left');
    });

    test('should return default style alignment', () => {
      const alignment = {
        bodyAlignment: undefined,
        headerAlignment: undefined,
        isHeader: false,
      };

      const result = getCellAlignment(alignment, styles);

      expect(result).toBe('cell-module-leftAligned-eBFS4');
    });

    test('should return style alignment for body row', () => {
      const alignment = {
        bodyAlignment: 'center',
        headerAlignment: undefined,
        isHeader: false,
      };

      const result = getCellAlignment(alignment, styles);

      expect(result).toBe('cell-module-centerAligned-_ktBT');
    });

    test('should return default style alignment for header row', () => {
      const alignment = {
        bodyAlignment: undefined,
        headerAlignment: undefined,
        isHeader: true,
      };

      const result = getCellAlignment(alignment, styles);

      expect(result).toBe('cell-module-leftAligned-eBFS4');
    });

    test('should return null when set no arguments', () => {
      const result = getCellAlignment();

      expect(result).toBeNull();
    });
  });

  describe('getHeaderAlignment', () => {
    test('should return array of alignments when was set array of arrays', () => {
      const aligments = [
        ['right', 'center', 'right'],
        ['right', 'center', 'left', 'left', 'left', 'left', 'left', 'left'],
        ['right', 'center', 'left', 'left', 'right', 'left', 'left', 'left'],
      ];

      const result = getHeaderAlignment(aligments, true, 2);

      expect(result).toEqual([
        'right',
        'center',
        'left',
        'left',
        'right',
        'left',
        'left',
        'left',
      ]);
    });

    test('should return array of alignments when was set array of strings', () => {
      const aligments = [
        'center',
        'center',
        'center',
        'left',
        'left',
        'left',
        'right',
        'left',
      ];

      const result = getHeaderAlignment(aligments);

      expect(result).toEqual([
        'center',
        'center',
        'center',
        'left',
        'left',
        'left',
        'right',
        'left',
      ]);
    });

    test('should return empty array when was set empty array', () => {
      const result = getHeaderAlignment([]);

      expect(result).toEqual([]);
    });

    test('should return empty array when set falsy values or set no arguments', () => {
      const result1 = getHeaderAlignment(null as any);

      expect(result1).toEqual([]);

      const result2 = getHeaderAlignment(undefined as any);

      expect(result2).toEqual([]);

      const result3 = getHeaderAlignment(false as any);

      expect(result3).toEqual([]);

      const result4 = getHeaderAlignment('' as any);

      expect(result4).toEqual([]);

      const result5 = getHeaderAlignment();

      expect(result5).toEqual([]);
    });
  });
});
