import React, { useRef } from 'react';
import cx from 'classnames';
import { Align } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './cell.module.less';
import { getCellAlignment } from '../helper';
import {
  ISortCell,
  IStaticCell,
  TCellShape,
} from './cell-types/cell-types.interface';
import { CellWrapper } from './cell-wrapper';
import { CellType } from '../enum';

export interface ICellProps {
  cell?: TCellShape;
  isHeader: boolean;
  columnHeaderAlignment?: string;
  columnAlignment?: Align.LEFT | Align.RIGHT | null;
  width?: TStringOrNumber;
  testId?: string;
}

export const Cell = ({
  cell,
  isHeader,
  columnHeaderAlignment,
  columnAlignment = Align.LEFT,
  width: columnWidthProp,
  testId,
}: ICellProps) => {
  if (!cell) return null;
  const { style: styleProp, colSpan, rowSpan, width: cellWidthProp } = cell;
  const ref = useRef<HTMLTableCellElement | null>(null);
  const alignment = {
    bodyAlignment: columnAlignment,
    headerAlignment: columnHeaderAlignment,
    isHeader,
  };
  const cellAlignmentStyle = getCellAlignment(alignment, styles);
  const cellAlignmentText = getCellAlignment(alignment, styles, true);

  const cellTypeClassName =
    cell.type === CellType.INPUT ||
    cell.type === CellType.NUMBER_INPUT ||
    cell.type === CellType.SELECT ||
    cell.type === CellType.POPOVER
      ? styles.inputCell
      : cell.type === CellType.SLIDER
      ? styles.sliderCell
      : cell.type === CellType.CHECKBOX
      ? styles.checkBoxCell
      : cell.type === CellType.ACTIONS
      ? styles.actionsCell
      : styles.staticCell;

  const className = cx(
    styles.cell,
    cellTypeClassName,
    (cell as ISortCell).hasSort ? styles.sortingCell : null,
    cellAlignmentStyle,
    (cell as IStaticCell).breakWord ? styles.breakWord : '',
  );

  // Width defined in following precedence:
  // 1. Width defined in cell
  // 2. Width defined in columnWidths
  const width = cellWidthProp ?? columnWidthProp ?? undefined;
  // Cells cannot be smaller than width, but they may exceed it (if table width exceeds sum of cell widths)
  // Style prop takes precedence
  const style = { width, minWidth: width, ...styleProp };
  const Element = isHeader ? 'th' : 'td';

  return (
    <Element
      ref={ref}
      className={className}
      style={style}
      colSpan={colSpan}
      rowSpan={rowSpan}
    >
      <CellWrapper
        cell={cell}
        columnAlignment={cellAlignmentText ?? Align.RIGHT}
        isHeader={isHeader}
        testId={testId}
      />
    </Element>
  );
};
