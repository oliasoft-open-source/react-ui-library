import React, { useEffect, useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Table } from '../table';
import { Field } from '../../form/field';
import { Button } from '../../button/button';
import { Input } from '../../input/input';
import { Flex } from '../../layout/flex/flex';
import * as storyData from '../table.stories-data';

export default {
  title: 'Basic/Table/Cell',
  component: Table,
  args: {
    table: {
      ...storyData.table,
    },
  },
} as Meta;

const Template: StoryFn<any> = (args) => {
  return (
    <Table
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

export const Editable = Template.bind({});
Editable.args = {
  table: storyData.tableEditable,
};

export const Disabled = Template.bind({});
Disabled.args = {
  table: storyData.tableDisabled,
};

export const DateInput = Template.bind({});
DateInput.args = {
  table: storyData.tableDates,
};

export const Colspan = Template.bind({});
Colspan.args = {
  table: storyData.tableColspan,
};

export const ColspanWithWidths = Template.bind({});
ColspanWithWidths.args = { table: storyData.tableColspanWidths };

export const CellWidths = Template.bind({});
CellWidths.args = { table: storyData.tableCellWidths };

export const CellStyleProp = Template.bind({});
CellStyleProp.args = { table: storyData.tableCellStyleProp };

export const Rowspan = Template.bind({});
Rowspan.args = {
  table: storyData.tableRowspan,
};

export const Checkboxes = Template.bind({});
Checkboxes.args = {
  table: storyData.tableCheckboxes,
};

export const Icons = Template.bind({});
Icons.args = {
  table: storyData.tableIcons,
};

export const Links = Template.bind({});
Links.args = {
  table: storyData.tableLinks,
};

export const Actions = Template.bind({});
Actions.args = {
  table: storyData.tableActions,
};

export const Menus = Template.bind({});
Menus.args = {
  table: storyData.tableWithSubActions,
};

export const JSXContent = Template.bind({});
JSXContent.args = {
  table: storyData.tableJsxContent,
};

export const StaticCellTooltips = Template.bind({});
StaticCellTooltips.args = {
  table: storyData.tableStaticTooltips,
};

export const WordbreakLongContent = Template.bind({});
WordbreakLongContent.args = {
  table: storyData.tableBreakLongContent,
};
WordbreakLongContent.parameters = {
  docs: {
    description: {
      story:
        'Setting `breakWord: true` on a cell will allow very long text strings to break',
    },
  },
};

export const Popover = () => {
  const headings = ['Section', 'Width', 'Height'];
  let units = ['', 'm', 'm'];
  let data = [...Array(10).keys()].map((c, i) => [i, i * 2, i * 2]);
  /*
      Mock table data store (real apps should use Redux or similar)
    */
  const PopoverTable = () => {
    const CellInputForm = ({ close, value }: any) => (
      <>
        <Field label="Input">
          <Input value={value} width="100%" />
        </Field>
        <Flex gap>
          <Button colored label="Save" onClick={() => {}} />
          <Button label="Cancel" onClick={close} />
        </Flex>
      </>
    );
    const table = {
      fixedWidth: '100%',
      headers: [
        {
          cells: headings.map((h) => ({ value: h })),
        },
        {
          cells: [
            {},
            {
              menu: [
                { label: 'm', value: 'm' },
                { label: 'ft', value: 'ft' },
              ],
              value: units[1],
              type: 'Unit',
            },
            {
              menu: [
                { label: 'm', value: 'm' },
                { label: 'ft', value: 'ft' },
              ],
              value: units[2],
              type: 'Unit',
            },
          ],
        },
      ],
      rows: data.map((row, i) => {
        return {
          cells: [...Array(3).keys()].map((c, i) => ({
            //https://stackoverflow.com/a/33352604
            value: row[i],
            type: 'Popover',
            content: <CellInputForm value={row[i]} />,
          })),
        };
      }),
    };
    return <Table table={table as any} />;
  };
  return <PopoverTable />;
};

export const SinglePopoverAtOnce = () => {
  const headings = ['Section', 'Width', 'Height'];
  let units = ['', 'm', 'm'];
  let data = [...Array(10).keys()].map((c, i) => [i, i * 2, i * 2]);
  /*
    Mock table data store (real apps should use Redux or similar)
  */
  const SinglePopoverOpenedTable = () => {
    const [isPopOverCellOpened, setIsPopOverCellOpened] = useState(false);
    const CellInputForm = ({ close, value }: any) => {
      useEffect(() => {
        if (!isPopOverCellOpened) {
          setIsPopOverCellOpened(true);
        }
      }, []);
      const onClosePopOver = () => {
        setIsPopOverCellOpened(false);
        close();
      };
      return (
        <>
          <Field label="Input">
            <Input value={value} width="100%" />
          </Field>
          <Flex gap>
            <Button colored label="Save" onClick={onClosePopOver} />
            <Button label="Cancel" onClick={onClosePopOver} />
          </Flex>
        </>
      );
    };
    const table = {
      headers: [
        {
          cells: headings.map((h) => ({ value: h })),
        },
        {
          cells: [
            {},
            {
              menu: [
                { label: 'm', value: 'm' },
                { label: 'ft', value: 'ft' },
              ],
              value: units[1],
              type: 'Unit',
            },
            {
              menu: [
                { label: 'm', value: 'm' },
                { label: 'ft', value: 'ft' },
              ],
              value: units[2],
              type: 'Unit',
            },
          ],
        },
      ],
      rows: data.map((row, i) => {
        return {
          cells: [...Array(3).keys()].map((c, i) => ({
            //https://stackoverflow.com/a/33352604
            value: row[i],
            type: 'Popover',
            disabled: isPopOverCellOpened,
            closeOnOutsideClick: false,
            content: <CellInputForm value={row[i]} />,
          })),
        };
      }),
    };
    return <Table table={table as any} />;
  };
  return <SinglePopoverOpenedTable />;
};

export const OverflowingCells = Template.bind({});
OverflowingCells.args = { table: storyData.tableOverflowing };

export const TestScrolling = Template.bind({});
TestScrolling.args = {
  table: { ...storyData.tableActions, name: 'Food' },
};
TestScrolling.decorators = [
  (Story) => (
    <div
      style={{ width: 180, height: 180, overflow: 'hidden', resize: 'both' }}
    >
      <Story />
    </div>
  ),
];

export const Multiselect = () => {
  const [value, setValue] = useState([]);
  const table = {
    fixedWidth: 'auto',
    headers: [
      {
        cells: [{ value: 'Countries' }],
      },
    ],
    rows: [
      {
        cells: [
          {
            type: 'Select',
            width: '240px',
            multi: true,
            options: [
              { label: 'France', value: 'France' },
              { label: 'Poland', value: 'Poland' },
              { label: 'United Kingdom', value: 'United Kingdom' },
              { label: 'Vietnam', value: 'Vietnam' },
            ],
            value,
            onChange: (event: any) => setValue(event.target.value),
          },
        ],
      },
    ],
  };
  return <Table table={table} />;
};
