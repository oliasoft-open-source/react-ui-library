import React from 'react';
import cx from 'classnames';
import { Tooltip } from 'components/tooltip/tooltip';
import { isStringNumberOrNode } from 'helpers/types';
import styles from '../cell.module.less';
import { CellHelpIcon } from './cell-help-icon';
import { CellLibraryIcon } from './cell-library-icon';
import { Sort } from './sort-cell';
import { IStaticCell } from './cell-types.interface';

export interface IStaticCellProps {
  cell: IStaticCell;
  isHeader?: boolean;
  testId?: string;
}

export const StaticCell = ({ cell, isHeader, testId }: IStaticCellProps) => {
  const { error, warning, tooltip, maxTooltipWidth, hasSort, onSort, type } =
    cell;
  const field = (
    <div
      className={cx(
        styles.staticCellContent,
        type === 'Unit' ? styles.unit : '',
      )}
      onClick={hasSort ? (evt) => onSort && onSort(evt) : () => {}}
      data-error={error || null}
      data-warning={warning || null}
      data-testid={testId}
    >
      {cell.value}
      {isHeader ? (
        <>
          <CellHelpIcon cell={cell} />
          <CellLibraryIcon cell={cell} />
          <Sort cell={cell} />
        </>
      ) : null}
    </div>
  );
  return (
    <div className={styles.inputWrapper}>
      <Tooltip
        error={!!error}
        warning={!!warning}
        text={tooltip || error || warning}
        enabled={
          (tooltip && isStringNumberOrNode(tooltip)) ||
          (error && isStringNumberOrNode(error)) ||
          (warning && isStringNumberOrNode(warning)) ||
          false
        }
        maxWidth={maxTooltipWidth}
        display="block"
      >
        {field}
      </Tooltip>
    </div>
  );
};
