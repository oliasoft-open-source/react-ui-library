import React from 'react';
import { Actions } from 'components/actions/actions';
import { IActionCell } from './cell-types.interface';

export interface IActionsCellProps {
  cell: IActionCell;
}

export const ActionsCell = ({ cell }: IActionsCellProps) => {
  return <Actions actions={cell.actions} />;
};
