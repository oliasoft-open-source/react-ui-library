import React from 'react';
import { Slider } from 'components/slider/slider';
import styles from '../cell.module.less';
import { ISliderCell } from './cell-types.interface';

type ISliderCellProps = {
  cell: ISliderCell;
};

export const SliderCell = ({ cell }: ISliderCellProps) => {
  return (
    <div className={styles.inputWrapper}>
      <Slider
        label={cell.label}
        value={cell.value}
        min={cell.min}
        max={cell.max}
        step={cell.step}
        marks={cell.marks}
        onChange={(ev) => cell.onChange(ev)}
        showArrows={cell.showArrows}
        showTooltip={cell.showTooltip}
        tooltipFormatter={cell.tooltipFormatter}
        disabled={cell.disabled}
      />
    </div>
  );
};
