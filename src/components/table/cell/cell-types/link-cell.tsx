import React, { useContext } from 'react';
import cx from 'classnames';
import { DisabledContext } from 'helpers/disabled-context';
import { isStringNumberOrNode } from 'helpers/types';
import { Tooltip } from 'components/tooltip/tooltip';
import styles from '../cell.module.less';
import { ILinkCell } from './cell-types.interface';

interface ILinkCellProps {
  cell: ILinkCell;
  testId?: string;
}

export const LinkCell = ({ cell, testId }: ILinkCellProps) => {
  const disabledContext = useContext(DisabledContext);

  const { error, warning, tooltip, maxTooltipWidth } = cell;

  const isTooltipEnabled = (text: any) => {
    return isStringNumberOrNode(text);
  };

  return (
    <div className={styles.inputWrapper}>
      <Tooltip
        error={!!error}
        warning={!!warning}
        text={tooltip || error || warning}
        enabled={
          isTooltipEnabled(tooltip) ||
          isTooltipEnabled(error) ||
          isTooltipEnabled(warning) ||
          false
        }
        maxWidth={maxTooltipWidth}
      >
        <div
          className={styles.staticCellContent}
          data-error={error || null}
          data-warning={warning || null}
        >
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a
            className={cx(
              (cell.disabled || disabledContext) && styles.disabledLink,
            )}
            onClick={
              cell.disabled || disabledContext
                ? undefined
                : (evt) => {
                    evt.stopPropagation();
                    cell.onClick?.(evt);
                  }
            }
            data-testid={testId}
          >
            {cell.value}
          </a>
        </div>
      </Tooltip>
    </div>
  );
};
