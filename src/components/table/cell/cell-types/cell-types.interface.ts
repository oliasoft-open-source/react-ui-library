import React, { ReactNode } from 'react';
import {
  TEmpty,
  TStringOrNumber,
  TStringNumberNull,
  TChangeEventHandler,
} from 'typings/common-type-definitions';
import { TSliderMark } from 'components/slider/slider';
import { TCheckBoxClickHandler } from 'components/check-box/check-box';

export type StringNumberOrNode = TStringOrNumber | ReactNode;

export interface TCommonCell {
  testId?: string;
  style?: any;
  colSpan?: number;
  rowSpan?: number;
  width?: number | string;
}

export type TCellType =
  | 'Static'
  | 'Icon'
  | 'Select'
  | 'Input'
  | 'NumberInput'
  | 'Popover'
  | 'CheckBox'
  | 'Actions'
  | 'Link'
  | 'Slider'
  | 'Unit'
  | string;

export interface IPopoverCell extends TCommonCell {
  content: React.ReactNode;
  fullWidth: boolean;
  closeOnOutsideClick: boolean;
  disabled: boolean;
  value?: StringNumberOrNode;
  type: TCellType;
  error?: StringNumberOrNode;
  warning?: StringNumberOrNode;
  tooltip?: StringNumberOrNode;
  maxTooltipWidth?: TStringOrNumber;
  hasSort?: boolean;
}

export interface IStaticCell extends TCommonCell {
  value?: StringNumberOrNode;
  type: TCellType;
  error?: StringNumberOrNode;
  warning?: StringNumberOrNode;
  tooltip?: StringNumberOrNode;
  maxTooltipWidth?: TStringOrNumber;
  breakWord?: boolean;
  libraryIcon?: {
    onClick: TEmpty;
    tooltip: StringNumberOrNode;
  };
  helpIcon?: {
    onClick: TEmpty;
    tooltip: StringNumberOrNode;
  };
  hasSort?: boolean;
  onSort?: (evt: React.MouseEvent<HTMLDivElement>) => void;
}

export interface ISelectCell extends TCommonCell {
  type: TCellType;
  name?: string;
  value?: TStringOrNumber;
  onChange?: TChangeEventHandler;
  onCreate?: (ev: any) => void;
  error?: StringNumberOrNode;
  warning?: StringNumberOrNode;
  disabled?: boolean;
  placeholder?: string;
  menu?: any;
  options?: any;
  native?: boolean;
  clearable?: boolean;
  searchable?: boolean;
  deprecatedEventHandler?: boolean;
  maxTooltipWidth?: TStringOrNumber;
  autoLayerWidth?: boolean;
  multi?: boolean;
}

export interface IHelpIconCell extends TCommonCell {
  type: TCellType;
  helpIcon?: {
    onClick: TEmpty;
    tooltip: StringNumberOrNode;
  };
}

export interface ILibraryIconCell extends TCommonCell {
  type: TCellType;
  libraryIcon?: {
    onClick: TEmpty;
    tooltip: StringNumberOrNode;
  };
}

export interface ICheckBoxCell extends TCommonCell {
  label?: string | null;
  checked: boolean;
  type: TCellType;
  onChange: TCheckBoxClickHandler;
  disabled?: boolean;
}

export interface IIconCell extends TCommonCell {
  type: TCellType;
  icon: any;
  tooltip?: StringNumberOrNode;
}

export interface IInputCell extends TCommonCell {
  type: TCellType;
  name?: string;
  value?: TStringOrNumber;
  onChange?: TChangeEventHandler;
  onFocus?: (evt: React.FocusEvent<HTMLInputElement>) => void;
  onBlur?: (evt: React.FocusEvent<HTMLInputElement>) => void;
  onPaste?: React.ClipboardEventHandler<HTMLInputElement>;
  placeholder?: string;
  error?: string;
  warning?: string;
  disabled?: boolean;
  maxTooltipWidth?: TStringOrNumber;
  selectOnFocus?: boolean;
  tooltip?: StringNumberOrNode;
}

export interface ILinkCell extends TCommonCell {
  value?: TStringOrNumber;
  type: TCellType;
  disabled?: boolean;
  onClick?: (evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  error?: StringNumberOrNode;
  warning?: StringNumberOrNode;
  tooltip?: StringNumberOrNode;
  maxTooltipWidth?: TStringOrNumber;
}

export interface INumberInputCell extends TCommonCell {
  name?: string;
  value?: TStringOrNumber;
  type: TCellType;
  onChange?: TChangeEventHandler;
  onFocus?: (evt: React.FocusEvent<HTMLInputElement>) => void;
  onBlur?: (evt: React.FocusEvent<HTMLInputElement>) => void;
  placeholder?: string;
  error?: string | boolean;
  warning?: string | boolean;
  disabled?: boolean;
  tooltip?: StringNumberOrNode;
  left?: boolean;
  validationCallback?: TEmpty;
  allowEmpty?: boolean;
  enableCosmeticRounding: boolean;
  enableDisplayRounding: boolean;
  roundDisplayValue: (value: TStringOrNumber) => TStringOrNumber;
  selectOnFocus?: boolean;
}

export interface ISliderCell extends TCommonCell {
  type: TCellType;
  label?: string | null;
  value: number;
  min: number;
  max: number;
  step?: number;
  marks?: TSliderMark[];
  onChange: TChangeEventHandler;
  showArrows?: boolean;
  showTooltip?: boolean;
  tooltipFormatter?: (value: any) => any;
  disabled?: boolean;
}

export interface IActionCell extends TCommonCell {
  type: TCellType;
  actions: any;
}

export interface ISortCell extends TCommonCell {
  type: TCellType;
  hasSort?: boolean;
  sort?: 'up' | 'down';
  sortPriority?: number;
}

export interface ISimpleCell extends TCommonCell {
  value: TStringOrNumber | ReactNode;
  type?: TCellType;
}

export type TCellShape =
  | IPopoverCell
  | IStaticCell
  | ISelectCell
  | IHelpIconCell
  | ILibraryIconCell
  | ICheckBoxCell
  | IIconCell
  | IInputCell
  | ILinkCell
  | INumberInputCell
  | IActionCell
  | ISortCell
  | ISliderCell
  | ISimpleCell;
