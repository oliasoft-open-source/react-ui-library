import React from 'react';
import { IconType } from 'typings/common-types';
import { Icon } from 'components/icon/icon';
import { Text } from 'components/text/text';
import styles from '../cell.module.less';
import { ISortCell } from './cell-types.interface';

export interface ISortProps {
  cell: ISortCell;
}

export const Sort = ({ cell }: ISortProps) => {
  const { hasSort, sort, sortPriority } = cell;

  return hasSort ? (
    <span className={styles.sortingCellIcon}>
      {sort === 'down' ? (
        <Text link>
          <Icon icon={IconType.SORT_ASCENDING} />
        </Text>
      ) : sort === 'up' ? (
        <Text link>
          <Icon icon={IconType.SORT_DESCENDING} />
        </Text>
      ) : (
        <Text faint>
          <Icon icon={IconType.SORT_DESCENDING} />
        </Text>
      )}
      {sortPriority && sort ? <sup>{sortPriority}</sup> : null}
    </span>
  ) : null;
};
