import React from 'react';
import { Input } from 'components/input/input';
import { Align } from 'typings/common-types';
import { IInputCell } from './cell-types.interface';

export interface IInputCellProps {
  cell: IInputCell;
  columnAlignment?: Align.LEFT | Align.RIGHT | string;
  testId?: string;
  type?: 'text' | 'date';
}

export const InputCell = ({
  cell,
  columnAlignment,
  testId,
  type,
}: IInputCellProps) => {
  return (
    <Input
      name={cell.name}
      value={cell.value}
      onChange={(ev) => cell.onChange?.(ev)}
      onFocus={cell.onFocus}
      onBlur={cell.onBlur}
      onPaste={(ev) => cell.onPaste?.(ev)}
      placeholder={cell.placeholder}
      error={cell.error}
      warning={cell.warning}
      disabled={cell.disabled}
      isInTable
      maxTooltipWidth={cell.maxTooltipWidth}
      right={columnAlignment === Align.RIGHT}
      selectOnFocus={cell.selectOnFocus}
      testId={testId}
      tabIndex={cell.disabled ? -1 : 0}
      tooltip={cell.tooltip}
      type={type}
    />
  );
};
