import React from 'react';
import { Popover } from 'components/popover/popover';
import styles from '../cell.module.less';
import { StaticCell } from './static-cell';
import { IPopoverCell } from './cell-types.interface';

export interface IPopoverCellProps {
  cell: IPopoverCell;
  testId?: string;
}

export const PopoverCell = ({ cell, testId }: IPopoverCellProps) => {
  const { content, fullWidth, closeOnOutsideClick, disabled } = cell;

  return (
    <Popover
      content={content}
      fullWidth={fullWidth}
      closeOnOutsideClick={closeOnOutsideClick}
      disabled={disabled}
    >
      <div className={styles.popover}>
        <StaticCell cell={cell} testId={testId} />
      </div>
    </Popover>
  );
};
