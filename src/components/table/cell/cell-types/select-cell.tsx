import React from 'react';
import { Align } from 'typings/common-types';
import { Select } from 'components/select/select';
import { ISelectCell } from './cell-types.interface';

export interface ISelectCellProps {
  cell: ISelectCell;
  columnAlignment?: Align.LEFT | Align.RIGHT | string;
  testId?: string;
}

export const SelectCell = ({
  cell,
  columnAlignment,
  testId,
}: ISelectCellProps) => {
  return (
    <Select
      name={cell.name}
      borderRadius={0}
      options={cell.options}
      onChange={(ev) => cell.onChange?.(ev)}
      onCreate={
        cell.onCreate ? (ev) => cell.onCreate && cell.onCreate(ev) : undefined
      }
      error={cell.error}
      warning={cell.warning}
      disabled={cell.disabled}
      placeholder={cell.placeholder}
      isInTable
      value={cell.value}
      native={cell.native}
      clearable={cell.clearable}
      searchable={cell.searchable}
      deprecatedEventHandler={cell.deprecatedEventHandler}
      maxTooltipWidth={cell.maxTooltipWidth}
      width="100%"
      autoLayerWidth={cell.autoLayerWidth}
      multi={cell.multi}
      right={columnAlignment === Align.RIGHT}
      testId={testId}
      tabIndex={cell.disabled ? -1 : 0}
    />
  );
};
