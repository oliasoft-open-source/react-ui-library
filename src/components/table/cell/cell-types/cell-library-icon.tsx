import React from 'react';
import { IconType } from 'typings/common-types';
import { HelpIcon } from 'components/help-icon/help-icon';
import styles from '../cell.module.less';
import { ILibraryIconCell } from './cell-types.interface';

export interface ICellLibraryIconProps {
  cell: ILibraryIconCell;
}

export const CellLibraryIcon = ({ cell }: ICellLibraryIconProps) => {
  const { libraryIcon } = cell;
  return libraryIcon ? (
    <div className={styles.icon}>
      <HelpIcon
        onClick={libraryIcon.onClick}
        text={libraryIcon.tooltip}
        icon={IconType.LIBRARY}
      />
    </div>
  ) : null;
};
