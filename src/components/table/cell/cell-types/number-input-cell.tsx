import React from 'react';
import { Align } from 'typings/common-types';
import { NumberInput } from 'components/number-input/number-input';
import { INumberInputCell } from './cell-types.interface';

interface INumberInputCellProps {
  cell: INumberInputCell;
  columnAlignment: Align.LEFT | Align.RIGHT | string;
  testId?: string;
}

export const NumberInputCell = ({
  cell,
  columnAlignment,
  testId,
}: INumberInputCellProps) => {
  return (
    <NumberInput
      name={cell.name}
      value={cell.value}
      onChange={(ev) => cell.onChange?.(ev)}
      onFocus={cell.onFocus}
      onBlur={cell.onBlur}
      placeholder={cell.placeholder}
      error={cell.error}
      warning={cell.warning}
      disabled={cell.disabled}
      isInTable
      left={cell.left ?? columnAlignment === Align.LEFT}
      selectOnFocus={cell.selectOnFocus}
      tabIndex={cell.disabled ? -1 : 0}
      testId={testId}
      tooltip={cell.tooltip}
      validationCallback={cell.validationCallback}
      allowEmpty={cell.allowEmpty}
      enableCosmeticRounding={cell.enableCosmeticRounding}
      enableDisplayRounding={cell.enableDisplayRounding}
      roundDisplayValue={cell.roundDisplayValue}
    />
  );
};
