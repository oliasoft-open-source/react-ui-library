import React from 'react';
import { CheckBox } from 'components/check-box/check-box';
import { ICheckBoxCell } from './cell-types.interface';

interface ICheckBoxCellProps {
  cell: ICheckBoxCell;
  testId?: string;
}

export const CheckBoxCell = ({ cell, testId }: ICheckBoxCellProps) => {
  return (
    <CheckBox
      label={cell.label}
      checked={cell.checked}
      isInTable
      disabled={cell.disabled}
      onChange={(ev) => cell.onChange(ev)}
      testId={testId}
      tabIndex={cell.disabled ? -1 : 0}
    />
  );
};
