import React from 'react';
import { Icon } from 'components/icon/icon';
import { isStringNumberOrNode } from 'helpers/types';
import styles from '../cell.module.less';
import { IIconCell } from './cell-types.interface';
import { Tooltip } from '../../../tooltip/tooltip';

export interface IIconCellProps {
  cell: IIconCell;
}

export const IconCell = ({ cell }: IIconCellProps) => {
  const { tooltip, icon } = cell;
  return (
    <div className={styles.iconCellWrapper}>
      <Tooltip
        text={tooltip}
        enabled={!!tooltip && isStringNumberOrNode(tooltip)}
        display="flex"
        placement="top-center"
      >
        <Icon icon={icon} />
      </Tooltip>
    </div>
  );
};
