import React from 'react';
import { HelpIcon } from 'components/help-icon/help-icon';
import styles from '../cell.module.less';
import { IHelpIconCell } from './cell-types.interface';

export interface ICellHelpIconProps {
  cell: IHelpIconCell;
}

export const CellHelpIcon = ({ cell }: ICellHelpIconProps) => {
  const { helpIcon } = cell;
  return helpIcon ? (
    <div className={styles.icon}>
      <HelpIcon onClick={helpIcon.onClick} text={helpIcon.tooltip} />
    </div>
  ) : null;
};
