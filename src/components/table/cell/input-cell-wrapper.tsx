import React from 'react';
import { Align } from 'typings/common-types';
import cx from 'classnames';
import styles from './cell.module.less';
import { InputCell } from './cell-types/input-cell';
import { NumberInputCell } from './cell-types/number-input-cell';
import { SelectCell } from './cell-types/select-cell';

export interface IInputCellWrapperProps {
  cell: any;
  columnAlignment: Align.LEFT | Align.RIGHT | string;
  testId?: string;
}

export const InputCellWrapper = ({
  cell,
  columnAlignment,
  testId,
}: IInputCellWrapperProps) => {
  return (
    <div
      className={cx(
        styles.inputWrapper,
        cell.type === 'NumberInput' ? styles.numberInputWrapper : '',
      )}
    >
      {cell.type === 'Input' ? (
        <InputCell
          cell={cell}
          columnAlignment={columnAlignment}
          testId={testId}
        />
      ) : cell.type === 'NumberInput' ? (
        <NumberInputCell
          cell={cell}
          columnAlignment={columnAlignment}
          testId={testId}
        />
      ) : cell.type === 'DateInput' ? (
        <InputCell cell={cell} testId={testId} type="date" />
      ) : cell.type === 'Select' ? (
        <SelectCell
          cell={cell}
          columnAlignment={columnAlignment}
          testId={testId}
        />
      ) : null}
    </div>
  );
};
