import React from 'react';
import { Align } from 'typings/common-types';
import { InputCellWrapper } from './input-cell-wrapper';
import { LinkCell } from './cell-types/link-cell';
import { CheckBoxCell } from './cell-types/check-box-cell';
import { SliderCell } from './cell-types/slider-cell';
import { IconCell } from './cell-types/icon-cell';
import { ActionsCell } from './cell-types/action-cell';
import { PopoverCell } from './cell-types/popover-cell';
import { StaticCell } from './cell-types/static-cell';

export interface ICellWrapperProps {
  cell: any;
  isHeader: boolean;
  columnAlignment: Align.LEFT | Align.RIGHT | string;
  testId?: string;
}

export const CellWrapper = ({
  cell,
  isHeader,
  columnAlignment,
  testId,
}: ICellWrapperProps) => {
  if (!cell) return null;

  switch (cell.type) {
    case 'Input':
    case 'NumberInput':
    case 'DateInput':
    case 'Select':
      return (
        <InputCellWrapper
          cell={cell}
          columnAlignment={columnAlignment}
          testId={testId}
        />
      );
    case 'Link':
      return <LinkCell cell={cell} testId={testId} />;
    case 'CheckBox':
      return <CheckBoxCell cell={cell} testId={testId} />;
    case 'Slider':
      return <SliderCell cell={cell} />;
    case 'Icon':
      return <IconCell cell={cell} />;
    case 'Actions':
      return <ActionsCell cell={cell} />;
    case 'Popover':
      return <PopoverCell cell={cell} />;
    default:
      return <StaticCell cell={cell} isHeader={isHeader} testId={testId} />;
  }
};
