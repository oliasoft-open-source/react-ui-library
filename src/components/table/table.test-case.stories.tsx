import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Table } from './table';
import { table, tableInfiniteScrollDnD } from './table.stories-data';

export default {
  title: 'Basic/Table/Test Cases',
  component: Table,
  tags: ['!autodocs'],
} as Meta;

const Template: StoryFn<any> = (args) => {
  return (
    <Table
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

export const TestAlignments = Template.bind({});
TestAlignments.args = {
  table: {
    columnAlignment: [
      'left',
      'right',
      'left',
      'right',
      'left',
      'right',
      'left',
      'right',
    ],
    headers: [
      {
        cells: [
          { value: 'Input left' },
          { value: 'Input right' },
          { value: 'NumberInput left' },
          { value: 'NumberInput right' },
          { value: 'Select left' },
          { value: 'Select right' },
          { value: 'Static left' },
          { value: 'Static right' },
        ],
      },
    ],
    rows: [
      {
        cells: [
          {
            value: 'Value',
            type: 'Input',
          },
          {
            value: 'Value',
            type: 'Input',
          },
          {
            value: 123.45,
            type: 'NumberInput',
          },
          {
            value: 123.45,
            type: 'NumberInput',
          },
          {
            type: 'Select',
            options: [{ label: 'Value', value: 'Value' }],
            value: { label: 'Value', value: 'Value' },
          },
          {
            type: 'Select',
            options: [{ label: 'Value', value: 'Value' }],
            value: { label: 'Value', value: 'Value' },
          },
          { value: 'Value' },
          { value: 'Value' },
        ],
      },
    ],
  },
};

export const DragOverflow = Template.bind({});
DragOverflow.args = {
  table: {
    ...table,
    draggable: true,
  },
};
DragOverflow.decorators = [
  (story) => <div style={{ width: 200 }}>{story()}</div>,
];

export const TestInfiniteScrollDragAndDrop = Template.bind({});
TestInfiniteScrollDragAndDrop.args = {
  table: tableInfiniteScrollDnD,
};
