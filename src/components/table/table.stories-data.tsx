import React from 'react';
import { IconType, TriggerType } from 'typings/common-types';
import { Flex } from '../layout/flex/flex';
import { Menu } from '../menu/menu';
import { Text } from '../text/text';
import { Icon } from '../icon/icon';
import { MenuType } from '../menu/types';

export const table = {
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: 'Energy' },
        { value: 'Origin' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice', testId: 'table-tbody-cell-brown-rice' },
        { value: 100, testId: 'table-tbody-cell-100' },
        { value: 361, testId: 'table-tbody-cell-361' },
        { value: 'Vietnam', testId: 'table-tbody-cell-vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat', testId: 'table-tbody-cell-buckwheat' },
        { value: 50, testId: 'table-tbody-cell-50' },
        { value: 358, testId: 'table-tbody-cell-358' },
        { value: 'Poland', testId: 'table-tbody-cell-poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous', testId: 'table-tbody-cell-couscous' },
        { value: 10, testId: 'table-tbody-cell-10' },
        { value: 368, testId: 'table-tbody-cell-368' },
        { value: 'France', testId: 'table-tbody-cell-france' },
      ],
    },
  ],
};

export const tableColumnWidths = {
  ...table,
  columnWidths: ['200px', '20%', '100px', 'auto'],
};

export const tableStaticTooltips = {
  ...table,
  rows: table.rows.map((row) => ({
    ...row,
    cells: row.cells.map((cell) => ({ ...cell, tooltip: 'Tooltip' })),
  })),
};

export const tableCellWidths = {
  ...table,
  rows: table.rows.map((row) => ({
    ...row,
    cells: row.cells.map((cell) => ({ ...cell, width: '200px' })),
  })),
};

export const tableCellStyleProp = {
  ...table,
  rows: table.rows.map((row) => ({
    ...row,
    cells: row.cells.map((cell) => ({
      ...cell,
      style: {
        width: '200px',
        background: 'var(--color-background-table-header)',
        fontWeight: 'bold',
      },
    })),
  })),
};

export const tableBreakLongContent = {
  ...table,
  rows: [
    {
      cells: [
        {
          breakWord: true,
          value:
            'LoremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididuntutlaboreetdoloremagnaaliquaUtenimadminimveniamquisnostrudexercitationullamcolaborisnisiutaliquipexeacommodoconsequatDuisauteiruredolorinreprehenderitinvoluptatevelitessecillumdoloreeullamcolaborisnisiutaliquipexeacommodoconsequatDuisauteiruredolorinreprehenderitinvoluptatevelitessecillumdoloreeufugiatnullapariaturufugiatnullapariatur',
        },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    ...table.rows,
  ],
};

export const tableUnits = {
  ...table,
  headers: [
    ...table.headers,
    {
      cells: [
        {},
        {
          options: [
            { label: 'kg', value: 'kg' },
            { label: 'tonnes', value: 'tonnes' },
          ],
          type: 'Select',
          searchable: false,
          value: { label: 'kg', value: 'kg' },
          onChange: () => {},
        },
        { type: 'Unit', value: 'kcal / 100g' },
        {},
      ],
    },
  ],
};

export const tableActiveRow = {
  ...table,
  rows: table.rows.map((row, index) => ({
    ...row,
    active: index === 0,
    onRowClick: () => {},
  })),
};

export const tableErrorRow = {
  ...table,
  rows: table.rows.map((row, index) => ({
    ...row,
    error: index === 1,
  })),
};

export const tableWarningRow = {
  ...table,
  rows: table.rows.map((row, index) => ({
    ...row,
    warning: index === 1,
  })),
};

export const tableTitle = {
  ...table,
  name: 'Food',
};

export const tableTitleJsx = {
  ...table,
  name: (
    <Flex gap="var(--padding-xs)" alignItems="center">
      Food
      <Icon icon={IconType.STAR} />
    </Flex>
  ),
};

const actions = [
  {
    childComponent: (
      <Menu
        menu={{
          label: 'Import food',
          trigger: TriggerType.DROP_DOWN_BUTTON,
          sections: [
            { type: MenuType.HEADING, label: 'Import options' },
            { type: MenuType.OPTION, label: 'Import from csv' },
            { type: MenuType.OPTION, label: 'Import from library' },
          ],
          small: true,
        }}
      />
    ),
  },
];

export const tableChildComponent = {
  ...table,
  name: 'Food',
  actions,
  testId: 'table-child-component',
};

export const tableWithActionHeaderAndNoLabel = {
  ...table,
  actions,
  actionsRight: true,
};

export const tableInfiniteScroll = {
  ...table,
  headers: [
    ...table.headers,
    {
      cells: [
        {},
        {
          options: [
            { label: 'kg', value: 'kg' },
            { label: 'tonnes', value: 'tonnes' },
          ],
          type: 'Select',
          native: true,
          value: { label: 'kg', value: 'kg' },
          onChange: () => {},
        },
        { type: 'Unit', value: 'kcal / 100g' },
        {},
      ],
    },
  ],
  bordered: true,
  infiniteScroll: true,
  maxHeight: '500px',
  stickyHeaders: true,
  rows: Array.from({ length: 100 }, (_, index) => ({
    cells: [
      { type: 'Input', value: `Strawberry Letter ${index + 1}` },
      { type: 'Input', value: 100 },
      { type: 'Input', value: 361 },
      { type: 'Input', value: 'Vietnam' },
    ],
  })),
  footer: {
    content: (
      <>
        <strong>Footer content:</strong> goes here
      </>
    ),
  },
};

export const tableInfiniteScrollFullHeight = {
  ...tableInfiniteScroll,
  maxHeight: '100%',
};

export const tableInfiniteScrollDnD = {
  ...tableInfiniteScroll,
  draggable: true,
};

export const tableRowFocus = {
  ...table,
  rows: table.rows.map((row) => ({
    ...row,
    onRowFocus: () => {},
  })),
};

const subComponent = () => {
  return (
    <ul>
      <li>Carrots</li>
      <li>Chocolate</li>
      <li>Chorizo</li>
    </ul>
  );
};

export const tableJsxContent = {
  headers: [
    {
      cells: [{ value: 'Name' }, { value: 'Content' }],
    },
  ],
  rows: [
    {
      cells: [{ value: 'Sub-component' }, { value: subComponent() }],
    },
    {
      cells: [
        { value: 'JSX' },
        {
          value: (
            <div>
              Some <Text bold>bold content</Text>
            </div>
          ),
        },
      ],
    },
  ],
};

export const tableColspan = {
  ...table,
  headers: [
    {
      cells: [{ value: '' }, { value: 'Stats', colSpan: 2 }, { value: '' }],
    },
    ...table.headers,
  ],
  rows: [
    ...table.rows,
    {
      cells: [{ value: 'Total', colSpan: 4 }],
    },
  ],
};

export const tableRowspan = {
  ...table,
  headers: [
    {
      cells: [
        { value: 'Name', colSpan: 2 },
        { value: 'Origin', rowSpan: 2 },
      ],
    },
    {
      cells: [{ value: 'English' }, { value: 'Chinese' }],
    },
  ],
  rows: [
    {
      cells: [
        { type: 'Input', value: 'Brown rice' },
        { type: 'Input', value: '糙米' },
        {
          type: 'Select',
          options: [
            { label: 'China', value: 'China' },
            { label: 'France', value: 'France' },
            { label: 'Poland', value: 'Poland' },
          ],
          value: { label: 'China', value: 'China' },
          rowSpan: 2,
        },
      ],
    },
    {
      cells: [
        { type: 'Input', value: 'White rice' },
        { type: 'Input', value: '白米' },
      ],
    },
  ],
};

export const tableColspanWidths = {
  ...table,
  columnWidths: ['auto', '120px', '120px', 'auto'],
  headers: [
    {
      cells: [
        { value: 'Header' },
        { value: 'Header', colSpan: 2 },
        { value: 'Header' },
      ],
    },
  ],
};

export const tableAlignment = {
  ...table,
  columnAlignment: ['left', 'right', 'right', 'left'],
};

export const tableHeaderAlignment = {
  ...tableColspan,
  columnAlignment: ['left', 'right', 'right', 'left', 'right'],
  columnHeaderAlignments: [
    ['', 'center', ''],
    ['', 'right', 'right', ''],
  ],
};

export const tableEditable = {
  fixedWidth: 'auto',
  headers: [
    {
      cells: [
        {
          value: 'Input',
          testId: 'with-editable-cells-cell-name',
          width: '140px',
        },
        { value: 'Select (native)', width: '140px' },
        { value: 'Number Input', width: '140px' },
        { value: 'Select (custom)', width: '140px' },
        { value: 'Slider', width: '140px' },
        { value: 'Static' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        {
          value: 'Buckwheat',
          type: 'Input',
          onChange: () => {},
          testId: 'table-tbody-cell-buckwheat',
        },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        { value: 361, type: 'NumberInput', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'France', value: 'France' },
            { label: 'Poland', value: 'Poland' },
            { label: 'Vietnam', value: 'Vietnam' },
          ],
          value: { label: 'Poland', value: 'Poland' },
          onChange: () => {},
        },
        { type: 'Slider', value: 50, min: 0, max: 100, onChange: () => {} },
        { value: 361 },
      ],
    },
    {
      cells: [
        {
          value: '',
          type: 'Input',
          onChange: () => {},
          testId: 'table-tbody-cell-arborio',
          error: 'Error',
        },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
          error: 'Error',
        },
        { value: 361, type: 'NumberInput', onChange: () => {}, error: 'Error' },
        {
          type: 'Select',
          options: [
            { label: 'France', value: 'France' },
            { label: 'Italy', value: 'Italy' },
            { label: 'Vietnam', value: 'Vietnam' },
          ],
          value: { label: 'Italy', value: 'Italy' },
          onChange: () => {},
          error: 'Error',
        },
        {
          type: 'Slider',
          value: 50,
          min: 0,
          max: 100,
          onChange: () => {},
          error: 'Error',
        },
        { value: 361, error: 'Error' },
      ],
    },
    {
      cells: [
        {
          value: 'Couscous',
          type: 'Input',
          onChange: () => {},
          testId: 'table-tbody-cell-couscous',
          warning: 'Warning',
        },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
          warning: 'Warning',
        },
        {
          value: 361,
          type: 'NumberInput',
          onChange: () => {},
          warning: 'Warning',
        },
        {
          type: 'Select',
          options: [
            { label: 'France', value: 'France' },
            { label: 'Poland', value: 'Poland' },
            { label: 'Vietnam', value: 'Vietnam' },
          ],
          value: { label: 'France', value: 'France' },
          onChange: () => {},
          warning: 'Warning',
        },
        {
          type: 'Slider',
          value: 50,
          min: 0,
          max: 100,
          onChange: () => {},
          warning: 'Warning',
        },
        { value: 361, warning: 'Warning' },
      ],
    },
  ],
};

export const tableCheckboxes = {
  ...table,
  headers: table.headers.map((row) => ({
    ...row,
    cells: [
      {
        type: 'CheckBox',
        onChange: () => {},
      },
      ...row.cells,
    ],
  })),
  rows: table.rows.map((row) => ({
    ...row,
    cells: [
      {
        type: 'CheckBox',
        onChange: () => {},
      },
      ...row.cells,
    ],
  })),
};

export const tableDisabled = {
  ...tableEditable,
  rows: tableEditable.rows.map((row) => ({
    ...row,
    cells: row.cells.map((cell) => ({ ...cell, disabled: true })),
  })),
};

export const tableFilters = {
  ...table,
  headers: [
    ...table.headers,
    {
      cells: [
        {
          value: '',
          type: 'Input',
          placeholder: 'Search',
          onChange: () => {},
        },
        {
          value: '',
          type: 'Input',
          placeholder: 'Search',
          onChange: () => {},
        },
        {
          value: '',
          type: 'Input',
          placeholder: 'Search',
          onChange: () => {},
        },
        {
          value: '',
          type: 'Input',
          placeholder: 'Search',
          onChange: () => {},
        },
      ],
    },
  ],
};

export const tableSort = {
  headers: [
    {
      cells: [
        { value: 'Name' },
        {
          value: 'Weight (kg)',
          hasSort: true,
          sort: 'up',
          sortPriority: 1,
          onSort: () => {},
        },
        {
          value: 'Energy (kcal / 100g)',
          hasSort: true,
          sort: 'down',
          sortPriority: 2,
          onSort: () => {},
        },
        {
          value: 'Origin',
          hasSort: true,
          sort: '',
          sortPriority: 1,
          onSort: () => {},
        },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
  ],
};

export const tableIcons = {
  headers: [
    {
      cells: [
        {},
        { value: 'Name' },
        { value: 'Weight (kg)' },
        { value: 'Energy (kcal / 100g)' },
        { value: 'Origin' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        {
          type: 'Icon',
          icon: (
            <Icon icon={IconType.SUCCESS} color="var(--color-text-success)" />
          ),
          tooltip: 'Success tooltip here',
          width: 'var(--size)',
        },
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        {
          type: 'Icon',
          icon: (
            <Icon icon={IconType.WARNING} color="var(--color-text-warning)" />
          ),
          tooltip: 'Warning tooltip here',
          width: 'var(--size)',
        },
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        {
          type: 'Icon',
          icon: <Icon icon={IconType.ERROR} color="var(--color-text-error)" />,
          tooltip: 'Error tooltip here',
          width: 'var(--size)',
        },
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
  ],
};

export const tableLinks = {
  headers: [
    {
      cells: [{ value: 'Lorem' }, { value: 'Ipsum' }],
    },
  ],
  rows: [
    {
      cells: [
        {
          type: 'Link',
          value: 'Link text',
          onClick: () => console.log('Link clicked'),
        },
        {
          type: 'Link',
          value: 'Disabled link text',
          disabled: true,
          onClick: () => console.log('Link clicked'),
        },
      ],
    },
    {
      cells: [
        {
          type: 'Link',
          value: 'Static warning link',
          warning: 'Warning message',
          onClick: () => console.log('Link clicked'),
        },
        {
          type: 'Link',
          value: 'Static error link',
          error: 'Error message',
          onClick: () => console.log('Link clicked'),
        },
      ],
    },
  ],
};

export const tableActions = {
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight (kg)' },
        { value: 'Energy (kcal / 100g)' },
        { value: 'Origin' },
        { value: 'Lorem' },
        { value: 'Ipsum' },
        { value: 'Dolor' },
      ],
      actions: [
        {
          primary: true,
          label: 'Add',
          icon: 'add',
          onClick: () => {},
        },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
        { value: 'Lorem' },
        { value: 'Ipsum' },
        { value: 'Dolor' },
      ],
      actions: [{ label: 'Delete', icon: 'minus', onClick: () => {} }],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
        { value: 'Lorem' },
        { value: 'Ipsum' },
        { value: 'Dolor' },
      ],
      actions: [{ label: 'Delete', icon: 'minus', onClick: () => {} }],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
        { value: 'Lorem' },
        { value: 'Ipsum' },
        { value: 'Dolor' },
      ],
      actions: [
        {
          label: 'test',
          icon: 'minus',
          onClick: () => {},
          hidden: true,
        },
      ],
    },
  ],
};

export const tableWithSubActions = {
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight (kg)' },
        { value: 'Energy (kcal / 100g)' },
        { value: 'Origin' },
      ],
      actions: [
        {
          primary: true,
          label: 'Add',
          icon: 'add',
          onClick: () => {},
        },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: 'up',
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: 'down',
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: 'minus', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: 'up',
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: 'down',
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: 'minus', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: 'up',
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: 'down',
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: 'minus', onClick: () => {} },
      ],
    },
  ],
};

export const tableFooter = {
  ...table,
  footer: {
    content: (
      <>
        <strong>Footer content:</strong> goes here
      </>
    ),
    pagination: {
      rowCount: 500,
      selectedPage: 10,
      rowsPerPage: {
        onChange: () => {},
        options: [
          { label: '10 / page', value: 10 },
          { label: '20 / page', value: 20 },
          { label: '50 / page', value: 50 },
          { label: 'Show all', value: 0 },
        ],
        value: 20,
      },
      goTo: { onChange: () => {} },
      onSelectPage: () => {},
      small: true,
    },
    actions: [{ label: 'Star', icon: 'star', onClick: () => {} }],
  },
};

export const tableHelpLibraryIcons = {
  ...table,
  headers: table.headers.map((header) => ({
    ...header,
    cells: header.cells.map((cell) => ({
      ...cell,
      helpIcon: {
        onClick: () => console.log('Help clicked'),
        tooltip: 'Help text goes here',
      },
      libraryIcon: {
        onClick: () => console.log('Library clicked'),
        tooltip: 'Edit in library',
      },
    })),
  })),
};

export const tableOverflowing = {
  columnWidths: [undefined, '100px', '100px', '100px', undefined],
  columnAlignment: ['left', 'right', 'right', 'left', 'left', 'left', 'right'],
  headers: [
    {
      cells: [
        {
          value:
            'Lorem ipsum dolor sit amet consectetur adipiscing elit sed doeius mod tempor incididunt laboreet dolore magna aliqua',
        },
        { value: 'Weight (kg)' },
        { value: 'Energy (kcal / 100g)' },
        { value: 'Origin' },
        { value: 'Text' },
        { value: 'Static' },
        { value: 'Static Right' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        {
          breakWord: true,
          value:
            'LoremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididuntutlaboreetdoloremagnaaliquaUtenimadminimveniamquisnostrudexercitationullamcolaborisnisiutaliquipexeacommodoconsequatDuisauteiruredolorinreprehenderitinvoluptatevelitessecillumdoloreeullamcolaborisnisiutaliquipexeacommodoconsequatDuisauteiruredolorinreprehenderitinvoluptatevelitessecillumdoloreeufugiatnullapariaturufugiatnullapariatur',
        },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
          disabled: true,
        },
        {
          type: 'NumberInput',
          value: 361,
        },
        {
          type: 'Select',
          options: [
            { label: 'Vietnam', value: 'Vietnam' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Vietnam', value: 'Vietnam' },
          onChange: () => {},
          error: 'Error',
        },
        {
          value: 'Brown rice',
          type: 'Input',
          onChange: () => {},
          testId: 'table-tbody-cell-brown-rice',
          warning: 'Warning message',
        },
        {
          value: 'Static warning',
          warning: 'Warning message',
        },
        {
          value: '100',
        },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        {
          type: 'NumberInput',
          value: 361,
        },
        {
          type: 'Select',
          options: [
            { label: 'Vietnam', value: 'Vietnam' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Vietnam', value: 'Vietnam' },
          onChange: () => {},
        },
        {
          value: 'Brown rice',
          type: 'Input',
          onChange: () => {},
          testId: 'table-tbody-cell-brown-rice',
        },
        {
          type: 'Link',
          value: 'Static error link',
          error: 'Error message',
        },
        {
          value: '100',
        },
      ],
    },
  ],
};

export const tableDates = {
  fixedWidth: 'auto',
  headers: [
    {
      cells: [{ value: 'Name' }, { value: 'Date of birth' }],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'John Smith', width: '140px' },
        { value: '1980-05-05', type: 'DateInput', width: '110px' },
      ],
    },
    {
      cells: [
        { value: 'Jane Doe', width: '140px' },
        { value: '1984-12-12', type: 'DateInput', width: '110px' },
      ],
    },
  ],
};
