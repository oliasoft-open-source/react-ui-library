import { Align } from 'typings/common-types';

interface IAlignmentProps {
  bodyAlignment?: string | null;
  headerAlignment?: string | null;
  isHeader: boolean;
}

/**
 * getCellAlignment - returns string or class name
 * @param {IAlignmentProps} alignment - body cell alignment
 * @param {Record<string, string>} styles - styles object
 * @param {boolean} isGetText - get text for example ('left', 'right' or 'center') or class name. Default value false.
 * @returns {string|null}
 */
export const getCellAlignment = (
  alignment?: IAlignmentProps,
  styles: Record<string, string> = {
    leftAligned: '',
    rightAligned: '',
    centerAligned: '',
  },
  isGetText: boolean = false,
): string | null => {
  if (!alignment) return null;

  const { bodyAlignment, headerAlignment, isHeader } = alignment;
  const cellAlignment =
    isHeader && headerAlignment ? headerAlignment : bodyAlignment;

  switch (cellAlignment) {
    case Align.LEFT:
      return isGetText ? Align.LEFT : styles.leftAligned;
    case Align.RIGHT:
      return isGetText ? Align.RIGHT : styles.rightAligned;
    case Align.CENTER:
      return isGetText ? Align.CENTER : styles.centerAligned;
    default:
      return isGetText ? Align.LEFT : styles.leftAligned;
  }
};

/**
 * Returns alignments or empty array
 * @param {Array<string | string[]>} alignments can be array of arrays or array of strings
 * @param {boolean} isHeader
 * @param {number} rowIndex
 * @returns {Array<string>}
 */
export const getHeaderAlignment = (
  alignments?: Array<string | string[]>,
  isHeader: boolean = false,
  rowIndex: number = 0,
): string[] => {
  if (!alignments || alignments.length === 0) {
    return [];
  }

  const isArrays = alignments.every((alignment) => Array.isArray(alignment));
  const isStrings = alignments.every(
    (alignment) => typeof alignment === 'string',
  );

  if (isArrays && isHeader) {
    const alignmentRow = alignments[rowIndex] as string[];
    return alignmentRow || [];
  } else if (isStrings) {
    return alignments as string[];
  } else {
    console.warn(
      'getHeaderAlignment: alignments array contains a mix of strings and arrays, returning an empty array.',
    );
    return [];
  }
};
