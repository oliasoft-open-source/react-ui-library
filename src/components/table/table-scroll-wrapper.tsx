import React, { ReactNode } from 'react';
import { Virtualizer } from '@tanstack/react-virtual';
import styles from './table.module.less';
import { ITableTableProps } from './table';
import { TableVirtualScrollWrapper } from './table-virtual-scroll-wrapper';

export interface ITableScrollWrapperProps {
  table: ITableTableProps;
  theadRef: React.RefObject<HTMLTableSectionElement>;
  children: (props: {
    virtualizer?: Virtualizer<HTMLDivElement, Element>;
    tableStyle?: React.CSSProperties;
  }) => ReactNode;
}

export const TableScrollWrapper = ({
  table,
  theadRef,
  children,
}: ITableScrollWrapperProps) => {
  const { infiniteScroll = false } = table;

  return infiniteScroll ? (
    <TableVirtualScrollWrapper table={table} theadRef={theadRef}>
      {({ virtualizer, tableStyle }) => children({ virtualizer, tableStyle })}
    </TableVirtualScrollWrapper>
  ) : (
    <div id="scrollWrapper" className={styles.scroll}>
      {children({})}
    </div>
  );
};
