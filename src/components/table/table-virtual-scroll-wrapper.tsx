import React, { ReactNode, useRef } from 'react';
import { useVirtualizer, Virtualizer } from '@tanstack/react-virtual';
import styles from './table.module.less';
import { ITableTableProps } from './table';

export interface ITableVirtualScrollWrapperProps {
  table: ITableTableProps;
  theadRef: React.RefObject<HTMLTableSectionElement>;
  children: (props: {
    virtualizer?: Virtualizer<HTMLDivElement, Element>;
    tableStyle?: React.CSSProperties;
  }) => ReactNode;
}

export const TableVirtualScrollWrapper = ({
  table,
  theadRef,
  children,
}: ITableVirtualScrollWrapperProps) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  // TODO: Replace with design tokens once JS tokens available
  const MIN_ITEM_HEIGHT = 30;
  const BORDER_WIDTH = 1;
  const virtualizer = useVirtualizer({
    count: table.rows?.length ?? 0,
    getScrollElement: () => containerRef.current,
    estimateSize: () => MIN_ITEM_HEIGHT + BORDER_WIDTH,
    overscan: 10,
  });

  const getVirtualTableStyle = () => {
    const theadHeight = theadRef.current?.clientHeight ?? 0;
    const totalHeight = virtualizer.getTotalSize() + theadHeight;
    // Add a dynamic amount of top/bottom padding to virtual tables depending on visible rows
    // This makes sure the sticky header doesn't get scrolled out of view
    const items = virtualizer.getVirtualItems();
    const paddingTop = items.length > 0 ? items[0].start : 0;
    const paddingBottom =
      items.length > 0
        ? virtualizer.getTotalSize() - items[items.length - 1].end
        : 0;
    return {
      '--virtualPaddingTop': paddingTop + 'px',
      '--virtualPaddingBottom': paddingBottom + 'px',
      height: totalHeight,
    } as React.CSSProperties;
  };

  return (
    <div id="scrollWrapper" className={styles.scroll} ref={containerRef}>
      {children({ virtualizer, tableStyle: getVirtualTableStyle() })}
    </div>
  );
};
