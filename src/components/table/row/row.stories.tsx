import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Table } from '../table';
import { Text } from '../../text/text';
import * as storyData from '../table.stories-data';

export default {
  title: 'Basic/Table/Row',
  component: Table,
  args: {
    table: {
      ...storyData.table,
    },
  },
} as Meta;

const Template: StoryFn<any> = (args) => {
  return (
    <Table
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};
export const ActiveRow = Template.bind({});
ActiveRow.args = {
  table: storyData.tableActiveRow,
};

export const ErrorRow = Template.bind({});
ErrorRow.args = {
  table: storyData.tableErrorRow,
};

export const WarningRow = Template.bind({});
WarningRow.args = {
  table: storyData.tableWarningRow,
};

export const RowFocusEvent = Template.bind({});
RowFocusEvent.args = {
  table: storyData.tableRowFocus,
};

export const NotStriped = Template.bind({});
NotStriped.args = {
  table: {
    ...storyData.table,
    striped: false,
  },
};
NotStriped.parameters = {
  docs: {
    description: {
      story: '`striped: false` removes tinted background on alternative rows.',
    },
  },
};

export const Expandable: StoryFn<any> = () => {
  const [expandedRow, setExpandedRow] = useState<number | null>(null);
  const toggleExpandedRow = (rowIndex: number) =>
    expandedRow === rowIndex ? setExpandedRow(null) : setExpandedRow(rowIndex);
  return (
    <Table
      table={{
        ...storyData.table,
        striped: false,
        rows: storyData.table.rows.map((row, index) => ({
          ...row,
          onRowClick: () => toggleExpandedRow(index),
          expandedContent: {
            content:
              expandedRow === index ? (
                <div>Example expanded content</div>
              ) : null,
          },
        })),
      }}
    />
  );
};

export const DragAndDrop: StoryFn<any> = () => {
  const withDragAndDrop = {
    fixedWidth: 'auto',
    draggable: true,
    striped: false,
    headers: [
      {
        cells: [
          { value: 'Name' },
          { value: 'Description' },
          { value: 'Weight (kg)' },
          { value: 'Energy (kcal / 100g)' },
          { value: 'Origin' },
        ],
      },
    ],
    rows: [
      {
        noDrag: true,
        cells: [
          { value: 'Undraggable' },
          { value: 100 },
          { value: '' },
          { value: 361 },
          { value: 'Vietnam' },
        ],
      },
      ...[...Array(10)].map((_, index) => ({
        cells: [
          { value: 'Row ' + (index + 2) },
          { value: 'Description goes' },
          { value: 50 },
          { value: 358 },
          { value: 'Poland' },
        ],
      })),
    ],
  };
  const arraymove = (arr: any, from: any, to: any) => {
    const copy = [...arr];
    if (arr[to].noDrag) {
      return arr;
    }
    copy.splice(to, 0, copy.splice(from, 1)[0]);
    return copy;
  };
  const [rows, setRows] = useState(withDragAndDrop.rows);
  const onListReorder = (reorderData: any) => {
    const { to, from } = reorderData;
    arraymove(rows, from, to);
    setRows(arraymove(rows, from, to));
  };
  return (
    <Table table={{ ...withDragAndDrop, rows }} onListReorder={onListReorder} />
  );
};
DragAndDrop.decorators = [
  (Story) => (
    <div style={{ position: 'absolute', inset: 0, padding: 'var(--padding)' }}>
      <Story />
    </div>
  ),
];
DragAndDrop.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 200,
    },
  },
};

export const ConditionalDragAndDrop: StoryFn<any> = () => {
  const withDragAndDrop = {
    draggable: true,
    headers: [
      {
        cells: [{ value: 'Name' }, { value: 'Category' }],
      },
    ],
    rows: Array.from({ length: 10 }, (_, index) => ({
      cells: [
        { value: `Aardvark ${index + 1}` },
        { value: index < 5 ? 'Category 1' : 'Category 2' },
      ],
    })),
  };
  const arraymove = (arr: any, from: any, to: any) => {
    const copy = [...arr];
    if (arr[to].noDrag) {
      return arr;
    }
    copy.splice(to, 0, copy.splice(from, 1)[0]);
    return copy;
  };
  const [rows, setRows] = useState(withDragAndDrop.rows);
  const onListReorder = (reorderData: any) => {
    const { to, from } = reorderData;
    arraymove(rows, from, to);
    setRows(arraymove(rows, from, to));
  };
  return (
    <Table
      table={{ ...withDragAndDrop, rows }}
      onListReorder={onListReorder}
      canListReorder={({ to, from }) => {
        const toCategory = rows[to].cells[1].value;
        const fromCategory = rows[from].cells[1].value;
        return toCategory === fromCategory;
      }}
    />
  );
};

export const OnRowEnter: StoryFn<any> = () => {
  const [displayText, setDisplayText] = useState('Hover any row!');
  const rows = [1, 2, 3, 4, 5].map((val) => ({
    cells: [{ value: `Row ${val}` }, { value: `${val ** 2}` }],
    onRowMouseEnter: () => setDisplayText(`Hovering row ${val}`),
    onRowMouseLeave: () => setDisplayText('Not hovering any row...'),
  }));
  return (
    <>
      <Text>{displayText}</Text>
      <Table
        table={{
          fixedWidth: '100%',
          headers: [
            {
              cells: [{ value: 'Description' }, { value: 'Value' }],
              onRowMouseEnter: () => setDisplayText(`Hovering header row`),
              onRowMouseLeave: () => setDisplayText('Not hovering any row...'),
            },
          ],
          rows,
        }}
      />
    </>
  );
};
