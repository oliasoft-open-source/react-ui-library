import React, { ReactNode, useRef } from 'react';
import cx from 'classnames';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { IconType } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { TCellShape } from 'components/table/cell/cell-types/cell-types.interface';
import { ExpandedContentRow } from './expanded-content-row';
import { getHeaderAlignment } from '../helper';
import styles from './row.module.less';
import { Cell } from '../cell/cell';
import { Icon } from '../../icon/icon';
import { CellType } from '../enum';

interface IOnRowClickFunction {
  (): any;
  noStyle?: boolean;
}

export type TRowType = {
  active?: boolean;
  error?: boolean;
  warning?: boolean;
  cells: TCellShape[];
  noDrag?: boolean;
  onRowClick?: IOnRowClickFunction;
  onRowFocus?: IOnRowClickFunction;
  onRowMouseEnter?: IOnRowClickFunction;
  onRowMouseLeave?: IOnRowClickFunction;
  expandedContent?: {
    content: ReactNode;
    flush?: boolean;
  };
  actions?: any[];
};

export interface ITableRowProps {
  row: TRowType;
  rowIndex: number;
  isHeader?: boolean;
  columnCount: number;
  colSpan: number;
  columnHeaderAlignments?: string[] | string[][];
  columnAlignment?: string[];
  hasRowActions: boolean;
  draggableTable?: boolean;
  columnWidths?: string[];
  height?: number;
  testId?: string;
  dropDisabled?: boolean;
}

export const Row = ({
  row,
  rowIndex,
  isHeader = false,
  columnCount,
  colSpan,
  columnHeaderAlignments = [],
  columnAlignment = [],
  hasRowActions,
  draggableTable = false,
  columnWidths,
  dropDisabled = false,
  height,
  testId, //TODO: add testId
}: ITableRowProps) => {
  const columnHeaderAlignment = getHeaderAlignment(
    columnHeaderAlignments,
    isHeader,
    rowIndex,
  );

  const {
    onRowClick,
    onRowFocus,
    onRowMouseEnter,
    onRowMouseLeave,
    expandedContent,
    error,
    warning,
    active,
  } = row;

  const cells = row.cells.map((c, i) => {
    const key = `${isHeader ? 0 : 1}_${rowIndex}_${i}`;

    return (
      <Cell
        cell={c}
        key={key}
        isHeader={isHeader}
        columnAlignment={columnAlignment[i] as any}
        columnHeaderAlignment={columnHeaderAlignment[i]}
        width={columnWidths ? columnWidths[i] : undefined}
        testId={c.testId}
      />
    );
  });

  const rowActions = hasRowActions && (
    <Cell
      cell={{
        type: CellType.ACTIONS,
        actions: row.actions,
      }}
      key={columnCount}
      isHeader={isHeader}
    />
  );

  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({
    id: rowIndex.toString(),
    animateLayoutChanges: () => false,
  });

  const style = {
    transform: !dropDisabled ? CSS.Translate.toString(transform) : undefined,
    height,
    transition,
    opacity: isDragging ? 0 : undefined,
  };

  const getDragItem = () => {
    if (row.noDrag) {
      return <td aria-labelledby="unique-label-id" />;
    }
    return (
      <td className={styles.dragIconCell}>
        <div
          className={cx(
            styles.dragIcon,
            dropDisabled ? styles.dragIconDisabled : '',
          )}
          {...attributes}
          {...listeners}
        >
          <Icon icon={IconType.DRAG} />
        </div>
      </td>
    );
  };

  return isHeader ? (
    <>
      <tr
        key={rowIndex}
        onClick={onRowClick}
        onMouseEnter={onRowMouseEnter}
        onMouseLeave={onRowMouseLeave}
        onFocus={onRowFocus}
        className={cx(
          onRowClick ? styles.clickableRow : null,
          onRowMouseEnter ? styles.hoverableRow : null,
        )}
      >
        {draggableTable ? (
          // eslint-disable-next-line jsx-a11y/control-has-associated-label
          <th />
        ) : null}
        {cells}
        {rowActions}
      </tr>
      {expandedContent && expandedContent.content ? (
        <ExpandedContentRow
          key={`${rowIndex}_expanded_content`}
          colSpan={colSpan}
          flush={expandedContent.flush === true}
        >
          {expandedContent.content}
        </ExpandedContentRow>
      ) : null}
    </>
  ) : (
    <>
      <tr
        ref={!row.noDrag ? setNodeRef : null}
        key={rowIndex}
        onClick={onRowClick}
        onMouseEnter={onRowMouseEnter}
        onMouseLeave={onRowMouseLeave}
        onFocus={onRowFocus}
        className={cx(
          onRowClick && !onRowClick.noStyle ? styles.clickableRow : null,
          onRowMouseEnter && !onRowMouseEnter.noStyle
            ? styles.hoverableRow
            : null,
          active ? styles.rowActive : null,
        )}
        data-error={error || null}
        data-warning={warning || null}
        data-index={rowIndex}
        data-even={rowIndex % 2 === 0}
        style={style}
      >
        {draggableTable ? getDragItem() : null}
        {cells}
        {rowActions}
      </tr>
      {expandedContent && expandedContent.content ? (
        <ExpandedContentRow
          key={`${rowIndex}_expanded_content`}
          colSpan={colSpan}
          flush={expandedContent.flush === true}
        >
          {expandedContent.content}
        </ExpandedContentRow>
      ) : null}
    </>
  );
};
