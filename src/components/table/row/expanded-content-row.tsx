import React, { ReactNode } from 'react';
import cx from 'classnames';
import styles from './row.module.less';

export interface IExpandedContentRowProps {
  colSpan: number;
  children: ReactNode;
  flush?: boolean;
}

export const ExpandedContentRow = ({
  colSpan,
  children,
  flush,
}: IExpandedContentRowProps) => {
  return (
    <tr>
      <td
        colSpan={colSpan}
        className={cx(styles.expandableRow, flush ? styles.flush : '')}
      >
        {children}
      </td>
    </tr>
  );
};
