import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Table } from '../table';
import * as storyData from '../table.stories-data';

export default {
  title: 'Basic/Table/Footer',
  component: Table,
  args: {
    table: {
      ...storyData.table,
    },
  },
} as Meta;

const Template: StoryFn<any> = (args) => {
  return <Table {...args} />;
};

export const Footer = Template.bind({});
Footer.args = {
  table: storyData.tableFooter,
};
