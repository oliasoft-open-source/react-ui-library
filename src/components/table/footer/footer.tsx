import React, { ReactNode } from 'react';
import styles from './footer.module.less';
import { IPagination, Pagination } from '../../pagination/pagination';
import { Actions } from '../../actions/actions';
import { IAction } from '../../actions/actions.interface';

export interface IFooterProps {
  colSpan?: number;
  pagination: IPagination;
  actions?: IAction[];
  content?: ReactNode;
  testId?: string;
}

export const Footer = ({
  colSpan,
  pagination,
  actions,
  content,
  testId,
}: IFooterProps) => {
  const hasActions = actions && actions.length;
  const hasContent = content != null;

  const hasPagination = () => {
    if (pagination) {
      const { rowCount, rowsPerPage } = pagination;
      const minRows = Number(rowsPerPage?.options?.[0]?.value);
      return rowCount > minRows;
    }
    return false;
  };

  const showFooter = hasActions || hasContent || hasPagination();

  return showFooter ? (
    <div
      className={styles.footer}
      data-testid={`${testId ?? 'pagination'}-footer`}
    >
      {hasContent ? (
        <div className={styles.section}>
          <div>{content}</div>
        </div>
      ) : null}
      {hasActions || hasPagination() ? (
        <div className={styles.section}>
          {hasPagination() ? <Pagination pagination={pagination} /> : <div />}
          {hasActions && (
            <div>
              <Actions actions={actions} />
            </div>
          )}
        </div>
      ) : null}
    </div>
  ) : null;
};
