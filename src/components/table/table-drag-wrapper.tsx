import React, { ReactNode, useMemo, useState } from 'react';
import { createPortal } from 'react-dom';
import cx from 'classnames';
import {
  closestCenter,
  DndContext,
  DragEndEvent,
  DragOverEvent,
  DragOverlay,
  DragStartEvent,
  UniqueIdentifier,
} from '@dnd-kit/core';
import {
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { TAlign } from 'typings/common-types';
import styles from './table.module.less';
import { TRowsType } from './table.viewdata';
import { Row } from './row/row';

export interface ITableDragWrapperProps {
  children: (props: { dropDisabled?: boolean }) => ReactNode;
  colSpan: number;
  columnAlignment?: Array<TAlign | string>;
  columnCount: number;
  draggable?: boolean;
  onListReorder?: (obj: { from: number; to: number }) => void;
  canListReorder?: (obj: { from: number; to: number }) => boolean;
  rowActions: boolean;
  rows: TRowsType;
  tbodyRef?: React.RefObject<HTMLTableSectionElement>;
}

export const TableDragWrapper = (props: ITableDragWrapperProps) => {
  const {
    children,
    colSpan,
    columnAlignment,
    columnCount,
    draggable,
    onListReorder = () => {},
    canListReorder = () => true,
    rowActions,
    rows = [],
    tbodyRef,
  } = props;
  const [dragIndex, setDragIndex] = useState<UniqueIdentifier | null>(null);
  const [dropDisabled, setDropDisabled] = useState(false);

  const itemIds: UniqueIdentifier[] = useMemo(
    () => rows.map((_, index) => index.toString()),
    [rows],
  );

  const handleDragStart = (event: DragStartEvent) => {
    setDragIndex(event.active.id);
  };

  const handleDragEnd = (event: DragEndEvent) => {
    setDragIndex(null);
    setDropDisabled(false);
    const { active, over } = event;
    if (!active || !over || active.id === over.id) {
      return;
    }
    const from = Number(active.id);
    const to = Number(over?.id);
    if (!canListReorder({ from, to })) {
      return;
    }
    onListReorder({ from, to });
  };

  const handleDragOver = (event: DragOverEvent) => {
    const { active, over } = event;
    const from = Number(active.id);
    const to = Number(over?.id);
    const disabled = from !== to && !canListReorder({ from, to });
    setDropDisabled(disabled);
  };

  const getCalculatedWidths = () => {
    if (!tbodyRef?.current) {
      return [];
    }
    const rows = tbodyRef.current.querySelectorAll('tr');
    const activeRow = rows[Number(dragIndex)];
    const cells = activeRow?.querySelectorAll('td');
    if (!cells) {
      return [];
    }
    const widths = Array.from(cells).map(
      (cell) => `${cell.getBoundingClientRect().width}px`,
    );
    // Drop first item (drag icon)
    widths.shift();
    return widths;
  };

  if (!draggable) {
    return children({});
  }

  return (
    <DndContext
      collisionDetection={closestCenter}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragOver={handleDragOver}
    >
      <SortableContext items={itemIds} strategy={verticalListSortingStrategy}>
        {children({ dropDisabled })}
      </SortableContext>
      {!!dragIndex &&
        createPortal(
          <DragOverlay dropAnimation={null}>
            <table className={cx(styles.table, styles.dragOverlay)}>
              <tbody>
                <Row
                  rowIndex={Number(dragIndex)}
                  row={rows[Number(dragIndex)]}
                  columnCount={columnCount}
                  columnWidths={getCalculatedWidths()}
                  colSpan={colSpan}
                  hasRowActions={rowActions}
                  columnAlignment={columnAlignment}
                  draggableTable={draggable}
                  dropDisabled={dropDisabled}
                />
              </tbody>
            </table>
          </DragOverlay>,
          document.body,
        )}
    </DndContext>
  );
};
