import React, { ReactNode } from 'react';
import {
  TFunction,
  TStringNumberNull,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { IMenuSection } from 'components/menu/menu.interface';

export interface ISubAction {
  type?: string;
  label?: TStringNumberNull;
  icon?: ReactNode;
  onClick?: TFunction;
  testId?: string;
  disabled?: boolean;
  // TODO: Consolidate types for actions & menus
  menu?: {
    trigger: string;
    label: ReactNode | string;
    sections?: ReactNode | IMenuSection[];
    testId?: string;
  };
}

export interface IAction {
  label?: TStringNumberNull;
  childComponent?: ReactNode | (() => ReactNode);
  subActions?: ISubAction[];
  primary?: boolean;
  icon?: ReactNode;
  testId?: string;
  hidden?: boolean;
  disabled?: boolean;
  onClick?: (evt: React.MouseEvent, id?: TStringOrNumber) => void;
  tooltip?: ReactNode | string;
}

export interface IChildComponent {
  childComponent: ReactNode | (() => ReactNode);
  testId?: string;
}

export type IActionsType = (IAction | ISubAction | IChildComponent)[];
