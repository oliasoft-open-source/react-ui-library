import React, { ReactNode, useContext } from 'react';
import cx from 'classnames';
import { isFunction } from 'lodash';
import { TEmpty } from 'typings/common-type-definitions';
import { Color, IconType, TriggerType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import { Button } from '../button/button';
import styles from './actions.module.less';
import { Menu } from '../menu/menu'; // TODO - Dependency cycle
import { IAction, ISubAction } from './actions.interface';

export interface ISubMenuProps {
  subActions: ISubAction[];
  subMenuIcon?: ReactNode | string;
  primary?: boolean;
  testId?: string;
  tooltip?: ReactNode | string;
}

const SubmenuActions = (props: ISubMenuProps) => {
  const { subActions, subMenuIcon, primary, testId, tooltip } = props;
  const menuButton = (
    <Button
      colored={primary ? true : Color.MUTED}
      basic={!primary}
      round
      small
      icon={subMenuIcon || IconType.MENU}
      testId={testId}
      tooltip={tooltip}
    />
  );

  if (subActions.length) {
    const subMenu = {
      trigger: TriggerType.COMPONENT,
      component: menuButton,
      sections: subActions.map((a) => {
        return {
          type: a.type ?? 'Option',
          label: a.label,
          icon: a.icon,
          testId: a.testId,
          disabled: a.disabled,
          menu: a.menu,
          onClick: (evt: React.MouseEvent<HTMLButtonElement>) => {
            evt.stopPropagation();
            evt.preventDefault();
            if (isFunction(a.onClick)) {
              a.onClick(evt);
            }
          },
        };
      }),
    };
    return <Menu menu={subMenu} />;
  }
  return null;
};

export interface IActionProps {
  actions: IAction[];
  closeLayer?: TEmpty;
}

export const Actions = (props: IActionProps) => {
  const disabledContext = useContext(DisabledContext);
  const { actions = [], closeLayer } = props;
  return (
    <div className={cx(styles.actions)}>
      {actions.map((action, index) => {
        const hidden = action?.hidden || !action || !Object.keys(action).length;
        if (!hidden) {
          if (
            action.childComponent &&
            React.isValidElement(action.childComponent)
          ) {
            return (
              <div
                data-testid={action.testId}
                key={index}
                className={styles.childComponent}
              >
                {action.childComponent}
              </div>
            );
          } else if (action.subActions) {
            return (
              <SubmenuActions
                subActions={action.subActions}
                primary={action.primary}
                subMenuIcon={action.icon}
                testId={action.testId}
                tooltip={action.tooltip}
                key={index}
              />
            );
          } else {
            return (
              <Button
                key={index}
                colored={action.primary ? true : 'muted'}
                round
                basic={!action.primary}
                disabled={action.disabled}
                small
                name={String(action.label)}
                icon={action.icon ? action.icon : null}
                onClick={(evt) => {
                  if (closeLayer) {
                    closeLayer();
                  }
                  evt.stopPropagation();
                  evt.preventDefault();
                  if (
                    !(action.disabled || disabledContext) &&
                    /* Sentry WELLDESIGN-1EB */ isFunction(action.onClick)
                  ) {
                    action.onClick(evt);
                  }
                }}
                testId={action.testId}
                tooltip={action.tooltip}
              />
            );
          }
        }
      })}
    </div>
  );
};
