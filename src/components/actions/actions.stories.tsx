import React from 'react';
import { StoryFn } from '@storybook/react';
import { IActionsType } from './actions.interface';
import { Actions } from './actions';

const actions: IActionsType = [
  {
    label: 'Download',
    icon: 'download',
    onClick: () => {},
    testId: 'download',
  },
  {
    label: 'Help',
    icon: 'help',
    onClick: () => {},
    disabled: true,
    testId: 'help',
  },
  {
    label: 'More',
    testId: 'more',
    subActions: [
      {
        label: 'Move Up',
        icon: 'up',
        disabled: true,
        onClick: () => {},
        testId: 'move-up',
      },
      {
        label: 'Move Down',
        icon: 'down',
        onClick: () => {},
        testId: 'move-down',
      },
      {
        type: 'Menu',
        menu: {
          trigger: 'Text',
          label: 'Even more',
          sections: [
            {
              type: 'Option',
              label: 'Nested option 1',
              onClick: () => {},
            },
            {
              type: 'Option',
              label: 'Another nested option',
              onClick: () => {},
            },
          ],
        },
      },
    ],
  },
  {
    primary: true,
    label: 'Add',
    icon: 'add',
    onClick: () => {},
    testId: 'add',
  },
];

export default {
  title: 'Basic/Actions',
  component: Actions,
  args: {
    actions,
    right: false,
  },
  decorators: [
    (story: () => React.ReactNode) => (
      <div style={{ display: 'inline-flex' }}>{story()}</div>
    ),
  ],
  parameters: {
    docs: {
      source: {
        excludeDecorators: true,
      },
    },
  },
};

interface TemplateProps {
  actions: IActionsType;
  right?: boolean;
}

const Template: StoryFn<TemplateProps> = (args) => <Actions {...args} />;

export const Default = Template.bind({});

export const Tooltip = Template.bind({});
Tooltip.args = {
  actions: actions.map((action: any) => ({ ...action, tooltip: action.label })),
};
