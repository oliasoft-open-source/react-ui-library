import React from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import * as storyData from './button-group.stories-data';
import { ButtonGroup, IButtonGroupProps } from './button-group';

export default {
  title: 'Forms/ButtonGroup',
  component: ButtonGroup,
  args: {
    disabled: false,
    items: storyData.buttonsArrayOfObjects,
    value: '0',
    small: false,
    testId: undefined,
    onSelected: () => {},
  },
} as Meta<IButtonGroupProps>;

const Template: StoryFn<IButtonGroupProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: TStringOrNumber) => {
    updateArgs({ value: evt });
  };

  return (
    <ButtonGroup
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
      onSelected={handleChange}
    />
  );
};

export const Default = Template.bind({});

export const Icons = Template.bind({});
Icons.args = { items: storyData.buttonsWithIcons };

export const Tooltips = Template.bind({});
Tooltips.args = { items: storyData.buttonsWithTooltips };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const Small = Template.bind({});
Small.args = { small: true };

export const ArrayOfStrings = Template.bind({});
// @ts-ignore: data mismatch
ArrayOfStrings.args = { items: storyData.buttonsArrayOfStrings };
ArrayOfStrings.parameters = {
  docs: {
    description: {
      story: 'For simple cases when button key, label and value are the same.',
    },
  },
};

export const ArrayOfArrays = Template.bind({});
ArrayOfArrays.args = { items: storyData.buttonsArrayOfArrays };
ArrayOfArrays.parameters = {
  docs: {
    description: {
      story:
        'Avoid this deprecated legacy usage (still supported, but may be removed in future).',
    },
  },
};
