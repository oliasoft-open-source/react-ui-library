import React, { ReactNode, useContext } from 'react';
import { GroupOrder } from 'typings/common-types';
import { TFunction, TStringOrNumber } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { getType, isStringNumberOrNode } from 'helpers/types';
import { Button } from '../button/button';
import { Tooltip } from '../tooltip/tooltip';
import styles from './button-group.module.less';

export interface IBtnGroupItemProps {
  label?: string | null;
  value?: string;
  key?: TStringOrNumber;
  hidden?: boolean;
  icon?: any;
  error?: any;
  maxTooltipWidth?: any;
  testId?: string;
  tooltip?: any;
  warning?: any;
  disabled?: boolean;
}
export interface IButtonGroupProps {
  disabled?: boolean;
  basic?: boolean;
  items?: string[] | (string[] | IBtnGroupItemProps)[];
  header?: string;
  onSelected?: TFunction;
  small?: boolean;
  value?: TStringOrNumber;
  testId?: string;
}

export interface IButtonGroupItem {
  label: ReactNode;
  value: TStringOrNumber;
  key: TStringOrNumber;
  hidden: boolean;
  icon: any;
  error: any;
  maxTooltipWidth: any;
  testId: string;
  tooltip: any;
  warning: any;
  disabled: boolean;
}

export const ButtonGroup = (props: IButtonGroupProps) => {
  const {
    basic = false,
    items = [],
    header = '',
    onSelected = () => {},
    small = false,
    value: inputValue = '',
    testId = undefined,
    disabled,
  } = props;

  const disabledContextValue = useContext(DisabledContext);

  const onClick = (
    evt: React.MouseEvent,
    key: TStringOrNumber,
    value: string,
    label: string,
    disabledButton: boolean,
  ) => {
    evt.preventDefault();
    evt.stopPropagation();
    if (disabled || disabledButton || disabledContextValue) return;
    onSelected(key, value, label);
  };

  const getLabelFromButtonType = (button: any, type: string): ReactNode => {
    if (type === 'string') return button;
    if (type === 'object') return (button as IButtonGroupItem).label;
    if (type === 'array') return (button as [string, string])[1];
    return null;
  };

  const getValueFromButtonType = (button: any, type: string): string => {
    if (type === 'string') return button;
    if (type === 'object')
      return ((button as IButtonGroupItem).value as string) ?? '';
    if (type === 'array') return (button as [string, string])[0] ?? '';
    return '';
  };

  const getKeyFromButtonType = (
    button: any,
    type: string,
    fallbackValue: string,
  ): TStringOrNumber => {
    if (type === 'object' && 'key' in button) {
      return button.key;
    }
    return fallbackValue;
  };

  const getHiddenFromButtonType = (button: any, type: string): boolean => {
    return type === 'object' ? (button as IButtonGroupItem).hidden : false;
  };

  const getIconFromButtonType = (
    button: any,
    type: string,
  ): ReactNode | undefined => {
    return type === 'object' ? (button as IButtonGroupItem).icon : undefined;
  };

  const isActive = (
    type: string,
    key: TStringOrNumber,
    val: string,
  ): boolean => {
    if (type === 'object') return String(key) === String(inputValue);
    return val === inputValue;
  };

  const renderHeader = () => {
    if (header) {
      return <label className={styles.label}>{header}</label>;
    }
  };

  const renderButtons = () => {
    return items.map((button, i) => {
      const type = getType(button);
      const groupOrder =
        i === 0
          ? GroupOrder.FIRST
          : i === items.length - 1
          ? GroupOrder.LAST
          : GroupOrder.MIDDLE;
      const label = getLabelFromButtonType(button, type);
      const value = getValueFromButtonType(button, type);
      const key = getKeyFromButtonType(button, type, value);
      const hidden = getHiddenFromButtonType(button, type);
      const icon = getIconFromButtonType(button, type);
      const active = isActive(type, key, value);
      const { error, maxTooltipWidth, testId, tooltip, warning } =
        button as IButtonGroupItem;

      if (!hidden) {
        return (
          <Tooltip
            key={i}
            error={!!error}
            warning={!!warning}
            text={tooltip || error || warning}
            maxWidth={maxTooltipWidth}
            display="block"
            enabled={
              (tooltip && isStringNumberOrNode(tooltip)) ||
              (error && isStringNumberOrNode(error)) ||
              (warning && isStringNumberOrNode(warning)) ||
              false
            }
          >
            <Button
              active={active}
              basic={basic}
              disabled={disabled || (button as IButtonGroupItem).disabled}
              groupOrder={groupOrder}
              icon={icon}
              label={label}
              onClick={(evt) => {
                const stringLabel = typeof label === 'string' ? label : '';
                onClick(
                  evt,
                  key,
                  value,
                  stringLabel,
                  (button as IButtonGroupItem).disabled || false,
                );
              }}
              small={small}
              testId={testId}
            />
          </Tooltip>
        );
      }
      return null; // Added to handle cases where 'hidden' is true
    });
  };

  return (
    <div>
      <div>
        {renderHeader()}
        <div className={styles.buttonGroup} data-testid={testId}>
          {renderButtons()}
        </div>
      </div>
    </div>
  );
};
