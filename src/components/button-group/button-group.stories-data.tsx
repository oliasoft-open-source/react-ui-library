import React from 'react';
import { IBtnGroupItemProps } from './button-group';

export const buttonsArrayOfObjects: IBtnGroupItemProps[] = [
  { key: '0', label: 'Aardvarks', value: 'termites' },
  { key: '1', label: 'Kangaroos', value: 'grass' },
  { key: '2', label: 'Monkeys', value: 'bananas' },
  { key: '3', label: 'Possums', value: 'slugs', hidden: true },
  { key: '4', label: 'Pandas', value: 'bamboo', disabled: true },
];

export const buttonsWithIcons: IBtnGroupItemProps[] = [
  {
    key: '0',
    label: 'Upload',
    value: 'upload',
    icon: 'upload',
  },
  {
    key: '1',
    label: 'Delete',
    value: 'delete',
    icon: 'delete',
  },
  {
    key: '2',
    label: 'Help',
    value: 'help',
    icon: 'help',
  },
];

export const buttonsArrayOfStrings: string[] = ['One', 'Two', 'Three'];

export const buttonsArrayOfArrays: (string[] | IBtnGroupItemProps)[] = [
  ['first_item', 'One'],
  ['second_item', 'Two'],
  ['third_item', 'Three'],
];

export const buttonsWithTooltips: IBtnGroupItemProps[] = [
  {
    key: '0',
    label: 'Aardvarks',
    value: 'termites',
    tooltip: 'Eats 50k termites / night',
  },
  {
    key: '1',
    label: 'Kangaroos',
    value: 'grass',
    tooltip: <span>Babies are called joeys</span>,
  },
  {
    key: '2',
    label: 'Monkeys',
    value: 'bananas',
    warning: 'A bit cheeky',
  },
  {
    key: '3',
    label: 'Pandas',
    value: 'bamboo',
    disabled: true,
    tooltip: 'Extinct',
  },
];
