import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ITextProps, Text } from './text';

export default {
  title: 'Basic/Text',
  component: Text,
  args: {
    children: 'Text goes here',
    small: false,
    muted: false,
    error: false,
    warning: false,
    success: false,
    link: false,
    onClick: undefined,
  },
} as Meta;

const Template: StoryFn<ITextProps> = (args) => <Text {...args} />;
export const Default = Template.bind({});

export const Muted = Template.bind({});
Muted.args = { muted: true };

export const Faint = Template.bind({});
Faint.args = { faint: true };

export const Error = Template.bind({});
Error.args = { error: true };

export const Warning = Template.bind({});
Warning.args = { warning: true };

export const Success = Template.bind({});
Success.args = { success: true };

export const Small = Template.bind({});
Small.args = { small: true };

export const Link = Template.bind({});
Link.args = { link: true, onClick: () => {} };
