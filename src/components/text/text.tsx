import React from 'react';
import cx from 'classnames';
import styles from './text.module.less';

export interface ITextProps {
  bold?: boolean;
  center?: boolean;
  error?: boolean;
  faint?: boolean;
  link?: boolean;
  muted?: boolean;
  onClick?: (e: React.MouseEvent<HTMLSpanElement>) => void;
  small?: boolean;
  success?: boolean;
  warning?: boolean;
  children?: React.ReactNode;
}

export const Text = ({
  children,
  bold = false,
  center = false,
  error = false,
  faint = false,
  link = false,
  muted = false,
  onClick,
  small = false,
  success = false,
  warning = false,
}: ITextProps) => {
  return (
    <span
      className={cx(
        bold && styles.bold,
        center && styles.center,
        error && styles.error,
        faint && styles.faint,
        link && styles.link,
        muted && styles.muted,
        onClick && styles.clickable,
        small && styles.small,
        success && styles.success,
        warning && styles.warning,
      )}
      onClick={onClick}
    >
      {children}
    </span>
  );
};
