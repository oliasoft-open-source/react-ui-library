import React from 'react';
import cx from 'classnames';
import { NodeModel } from '@minoru/react-dnd-treeview';
import { Color, IconType } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import listStyles from '../list/list.module.less';
import styles from './tree.module.less';
import { Actions } from '../actions/actions';
import { MetaContent } from '../list/list-row/meta-content';
import { Button } from '../button/button';
import { Icon } from '../icon/icon';

export interface ITreeItemProps {
  node: NodeModel<any>;
  depth: number;
  isOpen: boolean;
  onToggle: (id: TStringOrNumber) => void;
  hasChild: boolean;
  draggable?: boolean;
  icons?: { expand: React.ReactNode; collapse: React.ReactNode };
  onDrop?: (event: React.DragEvent<HTMLDivElement>) => void;
}

// TODO: Replace this with reused list-item component
export const TreeItem = ({
  node,
  depth,
  isOpen,
  onToggle,
  hasChild,
  draggable,
  icons,
  onDrop,
}: ITreeItemProps) => {
  const { id, data } = node;
  const { active, testId, onClick, actions = [] } = data;

  const [isDragOver, setDragOver] = React.useState(false);
  const [dragOverStartTime, setDragOverStartTime] = React.useState(0);

  const handleToggle = (e: any) => {
    e.stopPropagation();
    onToggle(id);
  };

  // TODO: Decide whether to implement open on drag over
  // const dragOverProps = useDragOver(id, isOpen, onToggle);

  const handleDragEnter = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();

    if (!isDragOver) {
      setDragOver(true);
      setDragOverStartTime(new Date().getTime());
    }
  };

  const handleDragLeave = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();

    // Issue with onDragLeave being triggered at the same time as onDragEnter
    // Tiny delay before accepting leave, small enough to not be confused with human action
    if (new Date().getTime() - dragOverStartTime > 20) {
      setDragOver(false);
    }
  };

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();

    // Opening a closed parent when holding item over
    if (hasChild && !isOpen && new Date().getTime() - dragOverStartTime > 200) {
      handleToggle(event);
    }
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();

    setDragOver(false);
    setDragOverStartTime(new Date().getTime());

    onDrop?.(event);
  };

  return (
    <div
      className={cx(
        listStyles.item,
        listStyles.action,
        active ? listStyles.active : '',
        depth > 0 ? listStyles.indented : '',
        isDragOver ? listStyles.bordered : '',
      )}
      style={{
        marginInlineStart: depth * 24,
      }}
      data-testid={testId}
      onClick={(evt) => {
        if (onClick) {
          onClick(evt);
        } else if (hasChild) {
          handleToggle(evt);
        }
      }}
      onDrop={(evt) => (onDrop ? handleDrop(evt) : {})}
      onDragEnter={(evt) => (onDrop ? handleDragEnter(evt) : {})}
      onDragOver={(evt) => (onDrop ? handleDragOver(evt) : {})}
      onDragLeave={(evt) => (onDrop ? handleDragLeave(evt) : {})}
    >
      <div
        className={cx(
          listStyles.itemHeader,
          isDragOver ? listStyles.noPointerEvents : '',
        )}
        // TODO: Decide whether to implement open on drag over
        // eslint-disable-next-line react/jsx-props-no-spreading
        // {...dragOverProps}
      >
        {hasChild && (
          <div className={styles.toggle}>
            <Button
              basic
              colored={isOpen ? true : Color.MUTED}
              small
              round
              icon={
                isOpen
                  ? icons?.collapse || IconType.COLLAPSE
                  : icons?.expand || IconType.EXPAND
              }
              onClick={handleToggle}
            />
          </div>
        )}

        {draggable && (
          <div className={listStyles.drag}>
            <Icon icon={IconType.DRAG} />
          </div>
        )}

        <MetaContent item={data} />
        <div className={listStyles.right}>
          <div className={listStyles.actions}>
            <Actions actions={actions} />
          </div>
        </div>
      </div>
    </div>
  );
};
