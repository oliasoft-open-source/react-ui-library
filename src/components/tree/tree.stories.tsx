import React, {
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { TbFolder, TbFolderOpen } from 'react-icons/tb';
import { Meta, StoryFn } from '@storybook/react';
import { NodeModel } from '@minoru/react-dnd-treeview';
import { ITreeProps, Tree } from './tree';
import * as storyData from './tree.stories-data';
import { Text } from '../text/text';
import { Button } from '../button/button';

export default {
  title: 'Basic/Tree',
  component: Tree,
  args: {
    list: storyData.list,
  },
} as Meta;

const Template: StoryFn<ITreeProps> = (args) => (
  <Tree
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);
export const Default = Template.bind({});

export const Actions = Template.bind({});
Actions.args = { list: storyData.listActions };

export const Draggable = Template.bind({});
Draggable.args = { draggable: true };

export const Droppable = Template.bind({});
Droppable.args = { draggable: true, list: storyData.listControlParents };
Droppable.parameters = {
  docs: {
    description: {
      story:
        'Use the `droppable` prop to specify which items can have other items dropped into them.',
    },
  },
};

export const Flat = Template.bind({});
Flat.args = { draggable: true, list: storyData.listFlat };

export const ExpandedByDefault = Template.bind({});
ExpandedByDefault.args = { isInitialOpen: true };

export const OnOpenChangeCallback = Template.bind({});
OnOpenChangeCallback.args = {
  onChangeOpen: (id) => console.log('onChangeOpen here', id),
};

export const Managed = () => {
  const [items, setItems] = useState([
    {
      id: 1,
      parent: 0,
      droppable: true,
      name: 'Mammals',
    },
    {
      id: 2,
      parent: 1,
      droppable: true,
      name: 'Cats',
    },
    {
      id: 3,
      parent: 1,
      droppable: true,
      name: 'Dogs',
    },
    {
      id: 4,
      parent: 0,
      droppable: true,
      name: 'Amphibians',
    },
    {
      id: 5,
      parent: 4,
      droppable: true,
      name: 'Frogs',
    },
    {
      id: 6,
      parent: 0,
      droppable: true,
      name: 'Reptiles',
    },
  ]);
  const addItem = () => {
    const newItem = {
      id: Math.max(...items.map((item) => item.id)) + 1,
      parent: 0,
      droppable: true,
      name: 'New Item',
    };
    setItems([newItem].concat(items));
  };
  const list = {
    name: 'Animals',
    actions: [
      {
        label: 'Add',
        primary: true,
        icon: 'add',
        onClick: () => addItem(),
      },
    ],
    items,
  };
  return <Tree list={list} draggable onListReorder={setItems} />;
};

export const ManagedWithAllowParentReassignment = () => {
  const [items, setItems] = useState([
    {
      id: 1,
      parent: 0,
      droppable: true,
      name: 'Mammals',
    },
    {
      id: 2,
      parent: 1,
      droppable: true,
      name: 'Cats',
    },
    {
      id: 3,
      parent: 1,
      droppable: true,
      name: 'Dogs',
    },
    {
      id: 4,
      parent: 0,
      droppable: true,
      name: 'Amphibians',
    },
    {
      id: 5,
      parent: 4,
      droppable: true,
      name: 'Frogs',
    },
    {
      id: 6,
      parent: 0,
      droppable: true,
      name: 'Reptiles',
    },
  ]);
  const addItem = () => {
    const newItem = {
      id: Math.max(...items.map((item) => item.id)) + 1,
      parent: 0,
      droppable: true,
      name: 'New Item',
    };
    setItems([newItem].concat(items));
  };
  const list = {
    name: 'Animals',
    actions: [
      {
        label: 'Add',
        primary: true,
        icon: 'add',
        onClick: () => addItem(),
      },
    ],
    items,
  };
  return (
    <Tree
      list={list}
      draggable
      onListReorder={setItems}
      allowParentReassignment
    />
  );
};
const itemsAnimalClasses = [
  {
    id: 1,
    parent: 0,
    droppable: true,
    name: 'Mammals',
    onClick: () => console.log('Test'),
  },
  {
    id: 4,
    parent: 0,
    droppable: true,
    name: 'Amphibians',
  },
  {
    id: 6,
    parent: 0,
    droppable: true,
    name: 'Reptiles',
    active: true,
  },
];
const itemsAnimalGroups = [
  {
    id: 2,
    parent: 1,
    droppable: true,
    name: 'Cats',
  },
  {
    id: 3,
    parent: 1,
    droppable: true,
    name: 'Dogs',
  },
  {
    id: 5,
    parent: 4,
    droppable: true,
    name: 'Frogs',
  },
];

interface Item {
  id: number | string;
  parent: number;
  name: ReactNode;
}

export const LazyLoading = () => {
  const [items, setItems] = useState<Item[]>(itemsAnimalClasses);
  const [itemsState, setItemsState] = useState<
    Record<number, { isOpen: boolean; isLoading: boolean }>
  >({});

  const timerRef = useRef<number>(0);

  useEffect(() => {
    // Clear the timeout when the component unmounts
    return () => clearTimeout(timerRef.current);
  }, []);

  const itemHasChild = (data: NodeModel<any>): boolean => {
    return data.parent === 0;
  };

  const onItemToggle = useCallback((item: NodeModel<any>, isOpen: boolean) => {
    // simulate action that is loading items
    setItemsState((prevItemsState) => ({
      ...prevItemsState,
      [item.id]: {
        isOpen,
        isLoading: isOpen,
      },
    }));

    if (!isOpen) return;

    console.log('Loading');
    timerRef.current = window.setTimeout(() => {
      // Use window.setTimeout for correct type
      console.log('Loaded');
      setItems(
        (currentItems) =>
          currentItems
            .filter((existingItem) => existingItem.parent !== item.id) // Use item.id here
            .concat(
              itemsAnimalGroups.filter(
                (groupItem) => groupItem.parent === item.id,
              ),
            ), // And here
      );
      setItemsState((prevItemsState) => ({
        ...prevItemsState,
        [item.id]: {
          isOpen,
          isLoading: false,
        },
      }));
    }, 1000);
  }, []);

  const extraItems = useMemo(
    () =>
      Object.keys(itemsState)
        .filter((id) => itemsState[+id].isOpen)
        .map((id) => {
          const state = itemsState[+id];
          return {
            id: `${id}_extra`,
            parent: +id,
            name: state.isLoading ? (
              <Text muted>Loading...</Text>
            ) : (
              <Button
                label="Load more..."
                onClick={() => console.log(`Load more for parent ${id}`)}
              />
            ),
          };
        }),
    [itemsState],
  );

  const list = {
    name: 'Animals',
    items: items.concat(extraItems),
  };

  return (
    <Tree list={list} itemHasChild={itemHasChild} onItemToggle={onItemToggle} />
  );
};

export const CustomIcons = Template.bind({});
CustomIcons.args = {
  icons: {
    expand: <TbFolder />,
    collapse: <TbFolderOpen />,
  },
};

export const UsingFileOnDrop = Template.bind({});
UsingFileOnDrop.args = { list: storyData.listWithOnDrop, draggable: true };
