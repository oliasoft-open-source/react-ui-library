import React, { RefObject } from 'react';
import { DndProvider } from 'react-dnd';
import {
  Tree as TreeView,
  MultiBackend,
  getBackendOptions,
  NodeModel,
  DropOptions,
} from '@minoru/react-dnd-treeview';
import {
  TEmpty,
  TFunction,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { TreeItem } from './tree-item';
import listStyles from '../list/list.module.less';
import styles from './tree.module.less';
import { ListHeading } from '../list/list-row/list-heading';
import { TreePlaceholder } from './tree-placeholder';

export interface ITreeListProps {
  name?: string | React.ReactNode;
  noHeader?: boolean;
  items: {
    id: TStringOrNumber;
    name: string | React.ReactNode;
    parent: TStringOrNumber;
    droppable?: boolean;
    details?: string | React.ReactNode;
    active?: boolean;
    testId?: string;
    icon?: {
      icon: React.ReactNode;
      color: string;
      tooltip?: {
        text: TStringOrNumber | React.ReactNode;
        error?: boolean;
        warning?: boolean;
        placement?: string;
        enabled?: boolean;
        maxWidth?: TStringOrNumber;
        triggerOffset?: TStringOrNumber;
        possiblePlacements?: string[];
        fontSize?: TStringOrNumber;
        padding?: TStringOrNumber;
      };
    };
    onDrop?: (event: React.DragEvent<HTMLDivElement>) => void;
  }[];
  onClick?: TEmpty;
  actions?: any[];
}

interface DragSource extends NodeModel<any> {
  ref: { current: null | object };
  text: string;
  droppable: boolean;
  data: {
    name: string;
  };
}

export interface ITreeProps {
  list: ITreeListProps;
  draggable?: boolean;
  onListReorder?: (newList: any[]) => void;
  onChangeOpen?: (id: TStringOrNumber[]) => void;
  testId?: string;
  isInitialOpen?: boolean | Array<string> | Array<number>;
  treeRef?: RefObject<any>;
  itemHasChild?: (node: NodeModel<any>) => boolean;
  onItemToggle?: (node: NodeModel<any>, isOpen: boolean) => void;
  icons?: { expand: React.ReactNode; collapse: React.ReactNode };
  allowParentReassignment?: boolean;
  stickyHeader?: boolean;
}

export const Tree = ({
  list,
  draggable = false,
  onListReorder,
  onChangeOpen,
  testId,
  isInitialOpen = false,
  treeRef,
  itemHasChild,
  onItemToggle,
  icons,
  allowParentReassignment,
  stickyHeader,
}: ITreeProps) => {
  const handleOpen = (id: any) => {
    if (onChangeOpen) {
      onChangeOpen(id);
    }
  };

  const findNodeIndexById = (
    tree: NodeModel<any>[],
    id: number | undefined,
  ): number => {
    return tree?.findIndex((node) => node?.id === id) ?? -1;
  };

  const setDragSourceParentToGrandparent = (
    dragSource: DragSource,
    workingTree: NodeModel<any>[],
  ): void => {
    const dragSourceParentIndex = findNodeIndexById(
      workingTree,
      dragSource?.parent as number,
    );
    if (dragSourceParentIndex >= 0) {
      const grandparent = workingTree?.find(
        (node) => node?.id === workingTree[dragSourceParentIndex]?.parent,
      );
      dragSource.parent = grandparent ? (grandparent.id as number) : 0;
    }
  };

  const removeDragSourceFromWorkingTree = (
    dragSource: DragSource,
    workingTree: NodeModel<any>[],
  ): NodeModel<any>[] => {
    return workingTree?.filter((node) => node?.id !== dragSource?.id) ?? [];
  };

  const insertDragSourceAfterParentInWorkingTree = (
    dragSource: DragSource,
    dragSourceParentIndex: number,
    workingTree: NodeModel<any>[],
  ): NodeModel<any>[] => {
    workingTree?.splice(dragSourceParentIndex, 0, dragSource);
    return workingTree;
  };

  const transformTreeToList = (tree: NodeModel<any>[]) => {
    return tree.map(({ id, parent, droppable, data }) => {
      return {
        id,
        parent,
        droppable,
        ...data,
      };
    });
  };
  const handleDrop = (newTree: NodeModel<any>[], option: any) => {
    const { dragSource, dropTarget } = option;
    let workingTree = [...newTree];

    if (!onListReorder) return;

    const dragSourceIndex = findNodeIndexById(workingTree, dragSource?.id);
    const dragSourceParentIndex = findNodeIndexById(
      workingTree,
      dragSource?.parent,
    );

    let newList = [];

    if (
      dragSource?.parent === dropTarget?.id &&
      dragSourceIndex === 0 &&
      allowParentReassignment
    ) {
      setDragSourceParentToGrandparent(dragSource as DragSource, workingTree);
      workingTree = removeDragSourceFromWorkingTree(
        dragSource as DragSource,
        workingTree,
      );
      workingTree = insertDragSourceAfterParentInWorkingTree(
        dragSource,
        dragSourceParentIndex,
        workingTree,
      );
    }

    newList = transformTreeToList(workingTree);
    onListReorder(newList);
  };
  // Transform list to tree
  const tree = list?.items?.map(
    ({ id, droppable, parent, name, onDrop, ...rest }) => ({
      id,
      text: name,
      droppable,
      parent,
      data: {
        name,
        onDrop,
        ...rest,
      },
    }),
  ) as NodeModel<any>[];

  return (
    <div data-testid={testId}>
      {list.name && !list.noHeader && (
        <ListHeading
          name={list.name}
          actions={list.actions}
          stickyHeader={stickyHeader}
        />
      )}
      <div className={listStyles.list}>
        <DndProvider
          backend={MultiBackend}
          options={getBackendOptions()}
          context={window}
        >
          <TreeView
            ref={treeRef}
            tree={tree}
            sort={false}
            insertDroppableFirst={false}
            canDrag={() => draggable}
            canDrop={(_, { dragSource, dropTargetId }) => {
              if (dragSource?.parent === dropTargetId) {
                return true;
              }
            }}
            rootId={0}
            render={(node, { depth, isOpen, onToggle, hasChild }) => {
              return (
                <TreeItem
                  hasChild={itemHasChild ? itemHasChild(node) : hasChild}
                  node={node}
                  depth={depth}
                  isOpen={isOpen}
                  onToggle={() => {
                    onItemToggle?.(node, !isOpen);
                    onToggle();
                  }}
                  draggable={draggable}
                  icons={icons}
                  onDrop={node.data?.onDrop}
                />
              );
            }}
            onDrop={handleDrop}
            onChangeOpen={handleOpen}
            dropTargetOffset={5}
            placeholderRender={(_, { depth }) => (
              <TreePlaceholder depth={depth} />
            )}
            placeholderComponent="div"
            listComponent="div"
            listItemComponent="div"
            classes={{
              root: styles.tree,
              draggingSource: styles.draggingSource,
              dropTarget: styles.dropTarget,
              placeholder: styles.placeholderContainer,
            }}
            initialOpen={isInitialOpen}
          />
        </DndProvider>
      </div>
    </div>
  );
};
