export const list = {
  name: 'Animals',
  items: [
    {
      id: 1,
      parent: 0,
      droppable: true,
      name: 'Mammals',
      onClick: () => console.log('Test'),
    },
    {
      id: 2,
      parent: 1,
      droppable: true,
      name: 'Cats',
    },
    {
      id: 3,
      parent: 1,
      droppable: true,
      name: 'Dogs',
    },
    {
      id: 4,
      parent: 0,
      droppable: true,
      name: 'Amphibians',
    },
    {
      id: 5,
      parent: 4,
      droppable: true,
      name: 'Frogs',
    },
    {
      id: 6,
      parent: 0,
      droppable: true,
      name: 'Reptiles',
      active: true,
    },
  ],
};

export const listActions = {
  ...list,
  header: {
    // ...list.header, // TODO - @Jack - should be removed?
    actions: [
      {
        label: 'Add',
        primary: true,
        icon: 'add',
        onClick: () => {},
      },
    ],
  },
  items: list.items.map((item) => ({
    ...item,
    actions: [
      {
        label: 'Delete',
        icon: 'delete',
        onClick: () => {},
      },
    ],
  })),
};

export const listControlParents = {
  ...list,
  items: list.items.map((item) => ({
    ...item,
    droppable: item.parent === 0,
    details: item.parent === 0 ? 'Droppable' : 'Not droppable',
  })),
};

export const listFlat = {
  ...list,
  items: list.items.map((item) => ({
    ...item,
    parent: 0,
    droppable: false,
  })),
};

export const listWithOnDrop = {
  ...list,
  items: list.items.map((item) => ({
    ...item,
    onDrop: (event: React.DragEvent<HTMLDivElement>) =>
      console.log(
        'Dropped',
        event.dataTransfer?.files?.[0]?.name || '',
        'in',
        item.name,
      ),
  })),
};
