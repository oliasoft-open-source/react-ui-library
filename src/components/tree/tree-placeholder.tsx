import React from 'react';
import styles from './tree.module.less';

export interface ITreePlaceholderProps {
  depth: number;
}

export const TreePlaceholder = ({ depth }: ITreePlaceholderProps) => (
  <div className={styles.placeholder} style={{ left: depth * 24 }}></div>
);
