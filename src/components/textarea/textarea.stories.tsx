import React, { ChangeEvent } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { useArgs } from '@storybook/preview-api';
import { TextArea, ITextAreaProps } from './textarea';

export default {
  title: 'Forms/TextArea',
  component: TextArea,
  args: {
    disabled: false,
  },
} as Meta;

const Template: StoryFn<ITextAreaProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: ChangeEvent<HTMLTextAreaElement>) => {
    updateArgs({ value: evt.target.value });
  };
  return <TextArea {...args} onChange={handleChange} />;
};

export const Default = Template.bind({});
