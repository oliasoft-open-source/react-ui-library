import React, { FocusEvent, KeyboardEvent, useContext } from 'react';
import cx from 'classnames';
import {
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { isStringNumberOrNode } from 'helpers/types';
import { DisabledContext } from 'helpers/disabled-context';
import { Property } from 'csstype';
import { noop } from 'lodash';
import styles from './textarea.module.less';
import { Tooltip } from '../tooltip/tooltip';

export interface ITextAreaProps {
  name?: string;
  value?: TStringOrNumber;
  placeholder?: string;
  cols?: number;
  rows?: number;
  disabled?: boolean;
  onChange?: TChangeEventHandler<HTMLTextAreaElement>;
  onKeyPress?: (evt: KeyboardEvent<HTMLTextAreaElement>) => void;
  onFocus?: (evt: FocusEvent<HTMLTextAreaElement>) => void;
  onBlur?: (evt: FocusEvent<HTMLTextAreaElement>) => void;
  tabIndex?: number;
  error?: TStringOrNumber | React.ReactNode;
  warning?: TStringOrNumber | React.ReactNode;
  tooltip?: TStringOrNumber | React.ReactNode;
  maxTooltipWidth?: TStringOrNumber;
  resize?: Property.Resize;
  monospace?: boolean;
  testId?: string;
}

export const TextArea = ({
  name,
  value = '',
  placeholder = '',
  cols,
  rows,
  disabled = false,
  onChange = noop,
  onKeyPress = noop,
  onFocus = noop,
  onBlur = noop,
  tabIndex = 0,
  error = null,
  warning = null,
  tooltip = null,
  maxTooltipWidth,
  resize,
  monospace = false,
  testId,
}: ITextAreaProps) => {
  const disabledContext = useContext(DisabledContext);

  const textarea = (
    <textarea
      className={cx(
        styles.textarea,
        error ? styles.error : '',
        warning ? styles.warning : '',
        monospace ? styles.monospace : '',
      )}
      name={name}
      value={value}
      placeholder={placeholder}
      cols={cols}
      rows={rows}
      onChange={onChange}
      disabled={disabled || disabledContext}
      onKeyPress={onKeyPress}
      onFocus={onFocus}
      onBlur={onBlur}
      tabIndex={tabIndex}
      style={{ resize }}
      data-error={error || null}
      data-warning={warning || null}
      data-testid={testId}
    />
  );

  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      enabled={
        (tooltip && isStringNumberOrNode(tooltip)) ||
        (error && isStringNumberOrNode(error)) ||
        (warning && isStringNumberOrNode(warning)) ||
        false
      }
      maxWidth={maxTooltipWidth}
    >
      {textarea}
    </Tooltip>
  );
};
