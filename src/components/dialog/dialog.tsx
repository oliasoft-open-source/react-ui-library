import React, { ReactNode, Fragment } from 'react';
import cx from 'classnames';
import { isArray } from 'lodash';
import { TEmpty } from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import { Heading } from '../heading/heading';
import styles from './dialog.module.less';
import { Button } from '../button/button';

const convertStringToJsx = (children: ReactNode | ReactNode[]): ReactNode => {
  if (isArray(children)) {
    return children.map((text, index) => (
      <Fragment key={index}>
        {text} <br />
      </Fragment>
    ));
  }
  return children;
};

export interface IDialog {
  heading?: string | null;
  content?: ReactNode | string[];
  contentPadding?: number | string;
  footer?: ReactNode;
  width?: number | string;
  height?: number | string;
  bordered?: boolean;
  onClose?: TEmpty;
  scroll?: boolean;
  testId?: string;
}

export interface IDialogProps {
  dialog: IDialog;
}

export const Dialog = ({ dialog }: IDialogProps) => {
  const {
    heading,
    content,
    contentPadding = 'var(--padding)',
    footer,
    scroll,
    width,
    height,
    onClose,
    testId,
  } = dialog;
  return (
    <div
      className={cx(
        styles.dialog,
        width ? styles.inline : null,
        scroll ? styles.scroll : null,
      )}
      style={{ width, height }}
      data-testid={testId ?? null}
    >
      <div className={styles.header}>
        <Heading testId={testId && `${testId}-heading`} top marginBottom={0}>
          {heading}
        </Heading>
        {onClose ? (
          <div className={styles.dismiss}>
            <Button
              icon={IconType.CLOSE}
              round
              basic
              small
              colored="muted"
              onClick={onClose}
              testId={testId && `${testId}-dismiss`}
              ignoreDisabledContext
            />
          </div>
        ) : null}
      </div>
      <div
        data-testid={testId && `${testId}-content`}
        className={styles.content}
        style={{ padding: contentPadding }}
      >
        {convertStringToJsx(content)}
      </div>
      {footer && (
        <div
          data-testid={testId && `${testId}-footer`}
          className={styles.footer}
        >
          {footer}
        </div>
      )}
    </div>
  );
};
