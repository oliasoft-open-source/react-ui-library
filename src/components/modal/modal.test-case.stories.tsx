import React from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { expect, userEvent, waitFor, within } from '@storybook/test';
import { DisabledContext } from 'helpers/disabled-context';
import { IModalProps, Modal } from './modal';
import { Button } from '../button/button';
import { Dialog, IDialog, IDialogProps } from '../dialog/dialog';

export default {
  title: 'Modals/Modal/Test Cases',
  component: Modal,
  args: {
    visible: true,
    centered: true,
    width: '300px',
    heading: 'Are you sure?',
    content: <>This will complete the transaction</>,
    footer: (
      <>
        <Button label="Okay" colored onClick={() => {}} />
        <Button label="Cancel" onClick={() => {}} />
      </>
    ),
    onClose: () => {},
  },
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  tags: ['!autodocs'],
} as Meta;

const Template: StoryFn<IModalProps | IDialogProps> = () => {
  const [{ visible, centered, ...dialogProps }, updateArgs] = useArgs();
  const toggleModal = () => {
    updateArgs({ visible: !visible });
  };
  return (
    <>
      <Modal visible={visible} centered={centered}>
        <Dialog
          dialog={{
            ...dialogProps,
            onClose: toggleModal,
          }}
        />
      </Modal>
      <Button label="Open Modal" onClick={toggleModal} ignoreDisabledContext />
    </>
  );
};

export const Test = Template.bind({});
Test.args = { testId: 'testId' } as IDialog;
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(document.body);
  await step('Modal should appear when triggered', async () => {
    const trigger = canvas.getByText('Open Modal');
    await userEvent.click(trigger);
    await waitFor(() => {
      const modal = body.getByText('Are you sure?');
      expect(modal).toBeInTheDocument();
    });
  });
  await step('Modal should disappear when close button pressed', async () => {
    const dialog = await waitFor(() => body.getByTestId('testId'));
    const dismiss = await waitFor(() => body.getByTestId('testId-dismiss'));
    await userEvent.click(dismiss);
    await waitFor(() => expect(dialog).not.toBeInTheDocument());
  });
};

export const ModalCloseButtonDisabledContext = Template.bind({});
ModalCloseButtonDisabledContext.decorators = [
  (Story) => (
    <DisabledContext.Provider value>
      <Story />
    </DisabledContext.Provider>
  ),
];
