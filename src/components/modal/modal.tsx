import React, { ReactNode, useEffect, useRef } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { useKeyboardEvent } from 'hooks/use-keyboard-event';
import { Portal } from '../portal/portal';
import styles from './modal.module.less';

interface IModalWrapperProps {
  children: ReactNode;
}

const Wrapper = ({ children }: IModalWrapperProps) => {
  const wrapperRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (wrapperRef.current) {
      const firstFocusableElement = wrapperRef.current.querySelector(
        '[tabindex="0"]',
      ) as HTMLInputElement;
      if (firstFocusableElement) {
        firstFocusableElement.focus();
      } else {
        wrapperRef.current.focus();
      }
    }
  }, []);

  return (
    <div ref={wrapperRef} tabIndex={-1} className={cx(styles.wrapper)}>
      {children}
    </div>
  );
};

interface IModalContentProps {
  children: ReactNode;
  width: TStringOrNumber;
  centered: boolean;
}

const Content = ({ children, width, centered }: IModalContentProps) => (
  <div
    className={cx(styles.contentContainer, centered ? styles.centered : '')}
    style={{ maxWidth: width }}
  >
    {children}
  </div>
);

export interface IModalProps {
  children: ReactNode;
  visible?: boolean;
  centered?: boolean;
  width?: TStringOrNumber;
  onEnter?: () => void;
  onEscape?: () => void;
}

export const Modal = ({
  children,
  visible = false,
  centered = false,
  width = '100%',
  onEnter,
  onEscape,
}: IModalProps) => {
  const MODAL_CONTAINER_ID = 'modalContainer';

  // Create modal container at TOP of document if it doesn't already exist
  useEffect(() => {
    if (document.getElementById(MODAL_CONTAINER_ID)) {
      return;
    }
    const container = document.createElement('div');
    container.id = MODAL_CONTAINER_ID;
    document.body.insertBefore(container, document.body.firstChild);
    return () => {
      document.body.removeChild(container);
    };
  }, []);

  useKeyboardEvent(
    'Enter',
    () => {
      if (visible && onEnter) {
        onEnter();
      }
    },
    [visible, onEnter],
  );

  useKeyboardEvent(
    'Escape',
    () => {
      if (visible && onEscape) {
        onEscape();
      }
    },
    [visible, onEscape],
  );

  return (
    <>
      <Portal id={MODAL_CONTAINER_ID}>
        {visible ? (
          <Wrapper>
            <Content width={width} centered={centered}>
              {children}
            </Content>
          </Wrapper>
        ) : null}
      </Portal>
    </>
  );
};
