import React, { useState } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { TriggerType } from 'typings/common-types';
import { IModalProps, Modal } from './modal';
import { Button } from '../button/button';
import { Dialog, IDialog, IDialogProps } from '../dialog/dialog';
import { Popover } from '../popover/popover';
import { Input } from '../input/input';
import { InputGroup } from '../input-group/input-group';
import { Field } from '../form/field';
import { Menu } from '../menu/menu';
import { Select } from '../select/select';
import { Spacer } from '../layout/spacer/spacer';
import { Tooltip } from '../tooltip/tooltip';
import { MenuType } from '../menu/types';

export default {
  title: 'Modals/Modal',
  component: Modal,
  args: {
    visible: true,
    centered: true,
    width: '300px',
    heading: 'Are you sure?',
    content: <>This will complete the transaction</>,
    footer: (
      <>
        <Button label="Okay" colored onClick={() => {}} />
        <Button label="Cancel" onClick={() => {}} />
      </>
    ),
    onClose: () => {},
  },
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
} as Meta;

const Template: StoryFn<IModalProps | IDialogProps> = () => {
  const [{ visible, centered, ...dialogProps }, updateArgs] = useArgs();
  const toggleModal = () => {
    updateArgs({ visible: !visible });
  };
  return (
    <>
      <Modal visible={visible} centered={centered}>
        <Dialog
          dialog={{
            ...dialogProps,
            onClose: toggleModal,
          }}
        />
      </Modal>
      <Button label="Open Modal" onClick={toggleModal} />
    </>
  );
};

export const Default = Template.bind({});

export const Centered = Template.bind({});
Centered.args = { centered: true };

export const LineBreak = Template.bind({});
LineBreak.args = { content: ['This is a', 'new line!'] } as IDialog;

export const Scroll = Template.bind({});
Scroll.args = {
  scroll: true,
  content: (
    <div>{'Example modal content goes here lorem ipsum. '.repeat(100)}</div>
  ),
} as IDialog;

export const FullWidth = Template.bind({});
FullWidth.args = {
  width: '100%',
  content: (
    <div>{'Example modal content goes here lorem ipsum. '.repeat(100)}</div>
  ),
} as IDialog;

export const NoContentPadding = Template.bind({});
NoContentPadding.args = { contentPadding: 0 } as IDialog;

export const WithPopoverLayers = Template.bind({});
WithPopoverLayers.args = {
  content: (
    <>
      <Tooltip text="Tooltip text">Hover for tooltip</Tooltip>
      <Spacer />
      <Field label="Custom Select" helpText="Select an animal">
        <Select
          name="example"
          options={
            [
              'Aardvarks',
              'Kangaroos',
              'Monkeys',
              'Possums',
              'Gorilla',
              'Puma',
              'Patagonian Mara',
              'Tarsier',
              'Koala',
              'Panda',
              'Red Panda',
              'Impala',
              'Tiger',
              'Crocodile',
              'Bat',
            ] as any
          }
          onChange={() => {}}
          value="Monkeys"
          error="Bad value"
        />
      </Field>
      <Field label="Nested Menu">
        <Menu
          maxHeight="240px"
          menu={{
            label: 'Menu',
            trigger: TriggerType.DROP_DOWN_BUTTON,
            sections: [
              {
                type: MenuType.MENU,
                menu: {
                  trigger: 'Text',
                  label: 'Children',
                  sections: [...Array(100).keys()].map(() => ({
                    type: 'Option',
                    label: Math.random().toString(36).substring(7),
                    onClick: () => {},
                  })),
                },
              },
              ...[...Array(100).keys()].map(() => ({
                type: MenuType.OPTION,
                label: Math.random().toString(36).substring(7),
                onClick: () => {},
              })),
            ],
          }}
        />
      </Field>
      <Popover
        content={
          <InputGroup small>
            <Input value="Value" width="150px" error="Bad value" />
            <Button colored label="Save" onClick={() => {}} />
            <Button
              label="Cancel"
              onClick={() => {
                /* You'll need to define the 'close' action here */
              }}
            />
          </InputGroup>
        }
      >
        <Button colored label="Toggle me" onClick={() => {}} />
      </Popover>
    </>
  ),
} as IDialog;

export const KeyHandle = () => {
  const [visible, setVisible] = useState(false);

  const openModal = () => {
    setVisible(true);
  };

  const closeModal = () => {
    setVisible(false);
  };

  const handleSubmit = () => {
    console.log('Submit action');
    closeModal();
  };

  return (
    <>
      <Modal
        visible={visible}
        onEnter={() => {
          console.log('Enter');
          handleSubmit();
        }}
        onEscape={() => {
          console.log('Escape');
          closeModal();
        }}
      >
        <Dialog
          dialog={{
            heading: 'Test key handling',
            content: <Input />,
            footer: (
              <>
                <Button label="Done" colored="red" onClick={handleSubmit} />
                <Button label="Cancel" onClick={closeModal} />
              </>
            ),
            onClose: closeModal,
          }}
        />
      </Modal>
      <Button label="Open Modal" onClick={openModal} />
    </>
  );
};

export const MultipleModals = () => {
  const [visible1, setVisible1] = useState(false);
  const toggleVisible1 = () => setVisible1(!visible1);
  const [visible2, setVisible2] = useState(false);
  const toggleVisible2 = () => setVisible2(!visible2);

  return (
    <>
      <Modal visible={visible1}>
        <Dialog
          dialog={{
            heading: 'Modal 1',
            content: 'Lorem ipsum dolor sit amet',
            footer: (
              <>
                <Button label="Open modal 2" colored onClick={toggleVisible2} />
                <Modal visible={visible2}>
                  <Dialog
                    dialog={{
                      heading: 'Modal 2',
                      content: 'Lorem ipsum dolor sit amet',
                      footer: <Button label="Close" onClick={toggleVisible2} />,
                      onClose: toggleVisible2,
                    }}
                  />
                </Modal>
                <Button label="Close" onClick={toggleVisible1} />
              </>
            ),
            onClose: toggleVisible1,
          }}
        />
      </Modal>
      <Button label="Open modal 1" onClick={toggleVisible1} />
    </>
  );
};
