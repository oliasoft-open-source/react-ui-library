import React, { ChangeEvent } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { IInputProps, Input } from './input';

export default {
  title: 'Forms/Input',
  component: Input,
  args: {
    disabled: false,
    small: false,
    placeholder: 'Placeholder',
  },
} as Meta;

const Template: StoryFn<IInputProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: ChangeEvent<HTMLInputElement>) => {
    updateArgs({ value: evt.target.value });
  };

  return <Input {...args} onChange={handleChange} />;
};

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const Error = Template.bind({});
Error.args = { error: 'Error goes here' };

export const Warning = Template.bind({});
Warning.args = { warning: 'Warning goes here' };

export const Date = Template.bind({});
Date.args = { type: 'date', value: '2020-01-01' };

export const DisableSelectOnFocus = Template.bind({});
DisableSelectOnFocus.args = { value: 'Value', selectOnFocus: false };
