import React, {
  FocusEvent,
  ForwardedRef,
  forwardRef,
  KeyboardEventHandler,
  useContext,
} from 'react';
import cx from 'classnames';
import {
  TStringOrNumber,
  TStringNumberNull,
  TChangeEventHandler,
} from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { isStringNumberOrNode } from 'helpers/types';
import { noop } from 'lodash';
import { GroupOrder, TGroupOrder } from 'typings/common-types';
import styles from './input.module.less';
import { Tooltip } from '../tooltip/tooltip';

/*
  Gets the width when you want to set it based on the number of characters
  Replacement for input size property which is hard to style
*/
export const widthOfCharacters = (characterCount: number): string => {
  const padding = 'var(--padding-input-x)';
  const multiplier = '0.675em';
  return `calc((${characterCount} * ${multiplier}) + (2 * ${padding}))`;
};

export interface IInputProps {
  error?: TStringOrNumber | React.ReactNode;
  warning?: TStringOrNumber | React.ReactNode;
  tooltip?: TStringOrNumber | React.ReactNode;
  name?: string;
  type?: string;
  onChange?: TChangeEventHandler;
  onKeyPress?: KeyboardEventHandler<HTMLInputElement>;
  onFocus?: (evt: FocusEvent<HTMLInputElement>) => void;
  onBlur?: (evt: FocusEvent<HTMLInputElement>) => void;
  onPaste?: React.ClipboardEventHandler<HTMLInputElement>;
  onKeyDown?: KeyboardEventHandler<HTMLInputElement>;
  selectOnFocus?: boolean;
  small?: boolean;
  placeholder?: string | null;
  tabIndex?: number;
  value?: TStringNumberNull;
  disabled?: boolean;
  right?: boolean;
  groupOrder?: TGroupOrder;
  maxTooltipWidth?: TStringOrNumber;
  width?: TStringOrNumber;
  testId?: string;
  size?: number;
  isInTable?: boolean;
}

export const Input = forwardRef<HTMLInputElement, IInputProps>(
  (
    {
      error = null,
      tooltip = null,
      isInTable,
      width: propWidth = undefined,
      small = false,
      onChange = noop,
      placeholder = '',
      value = '',
      onKeyPress = () => {},
      onFocus = () => {},
      onBlur = () => {},
      onPaste = () => {},
      onKeyDown = () => {},
      selectOnFocus = true,
      name = undefined,
      type = 'text',
      tabIndex = 0,
      disabled = false,
      right = false,
      warning = null,
      groupOrder = null,
      maxTooltipWidth = undefined,
      testId = undefined,
      //deprecated props
      size = null, //use width instead
    },
    ref: ForwardedRef<HTMLInputElement>,
  ) => {
    const disabledContext = useContext(DisabledContext);

    const order = (() => {
      if (groupOrder) {
        switch (groupOrder) {
          case GroupOrder.FIRST:
            return styles.groupOrderFirst;
          case GroupOrder.LAST:
            return styles.groupOrderLast;
          default:
            return styles.groupOrderMiddle;
        }
      }
      return '';
    })();

    const width = propWidth || (size ? 'auto' : '100%');

    const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
      onFocus(e);
      if (selectOnFocus) {
        setTimeout(() => e.target.select(), 0);
      }
    };

    return (
      <div style={{ width }} className={order}>
        <Tooltip
          error={!!error}
          warning={!!warning}
          text={tooltip || error || warning}
          enabled={
            (tooltip && isStringNumberOrNode(tooltip)) ||
            (error && isStringNumberOrNode(error)) ||
            (warning && isStringNumberOrNode(warning)) ||
            false
          }
          maxWidth={maxTooltipWidth}
          display="block"
        >
          <input
            ref={ref}
            type={type || 'text'}
            size={size || 20}
            placeholder={placeholder ?? ''}
            value={value ?? ''}
            onChange={onChange}
            onKeyPress={onKeyPress}
            onKeyDown={onKeyDown}
            onFocus={handleFocus}
            onBlur={onBlur}
            onPaste={onPaste}
            name={name}
            tabIndex={tabIndex}
            autoComplete="off"
            disabled={disabled || disabledContext}
            className={cx(
              styles.input,
              right ? styles.right : '',
              small ? styles.small : '',
              isInTable ? styles.isInTable : '',
            )}
            style={{ width }}
            data-error={error || null}
            data-warning={warning || null}
            data-testid={testId}
          />
        </Tooltip>
      </div>
    );
  },
);
