import React from 'react';
import cx from 'classnames';
import { IconType } from 'typings/common-types';
import { TFunction } from 'typings/common-type-definitions';
import styles from './message.module.less';
import { Icon } from '../icon/icon';
import { MessageType, TMessageType } from './types';

export interface IDismissProps {
  type: TMessageType | string;
  onClose?: TFunction;
  isInToast?: boolean;
  testId?: string;
}

export const Dismiss = ({
  type,
  onClose,
  isInToast,
  testId,
}: IDismissProps) => {
  return (
    <div
      className={cx(
        styles.dismiss,
        isInToast ? styles.absolute : '',
        type === MessageType.INFO
          ? styles.info
          : type === MessageType.SUCCESS
          ? styles.success
          : type === MessageType.WARNING
          ? styles.warning
          : type === MessageType.ERROR
          ? styles.error
          : null,
      )}
    >
      <Icon icon={IconType.CLOSE} onClick={onClose} testId={testId} />
    </div>
  );
};
