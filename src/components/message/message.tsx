import React, { MouseEvent, ReactNode, useReducer } from 'react';
import cx from 'classnames';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import { Dismiss } from './dismiss';
import styles from './message.module.less';
import { Icon } from '../icon/icon';
import { MessageToggle, MessageType, TMessageType } from './types';

interface State {
  detailsVisible: boolean;
}

type Action = { type: MessageToggle.TOGGLE_DETAILS };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case MessageToggle.TOGGLE_DETAILS:
      return {
        ...state,
        detailsVisible: !state.detailsVisible,
      };
    default:
      return state;
  }
};

interface IDetailsProps {
  details: ReactNode;
  visible: boolean;
  dispatch: TEmpty;
}

const Details = ({ details, visible, dispatch }: IDetailsProps) => {
  return (
    <div>
      <div className={cx(styles.legendToggle)} onClick={dispatch}>
        {visible ? 'Hide details' : 'Show details'}
      </div>
      {visible ? <div className={styles.detailsText}>{details}</div> : null}
    </div>
  );
};

interface IDialogIconProps {
  type: TMessageType | string;
}

const DialogIcon = ({ type }: IDialogIconProps) => {
  const size = 16;
  switch (type) {
    case MessageType.SUCCESS:
      return <Icon icon={IconType.SUCCESS} size={size} />;
    case MessageType.WARNING:
      return <Icon icon={IconType.WARNING} size={size} />;
    case MessageType.ERROR:
      return <Icon icon={IconType.ERROR} size={size} />;
    default:
      return <Icon icon={IconType.INFO} size={size} />;
  }
};

export interface IMessage {
  visible?: boolean;
  type?: TMessageType | string;
  icon?: boolean;
  heading?: ReactNode;
  content?: ReactNode | string;
  details?: ReactNode;
  detailsVisible?: boolean;
  footer?: ReactNode;
  withDismiss?: boolean;
  onClose?: (evt: MouseEvent<SVGSVGElement>) => void;
  width?: TStringOrNumber;
  maxHeight?: TStringOrNumber;
}

export interface IMessageProps {
  message: IMessage;
}

export const Message = ({ message }: IMessageProps) => {
  const {
    visible,
    type = MessageType.INFO,
    icon,
    heading,
    content,
    details,
    detailsVisible = false,
    footer,
    withDismiss,
    onClose,
    width,
    maxHeight,
  } = message;

  const initialState: State = {
    detailsVisible,
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <>
      {visible ? (
        <div
          className={cx(
            styles.container,
            width ? styles.block : null,
            type === MessageType.SUCCESS
              ? styles.success
              : type === MessageType.WARNING
              ? styles.warning
              : type === MessageType.ERROR
              ? styles.error
              : styles.info,
          )}
          style={{ width, maxHeight }}
        >
          <div>
            {icon ? (
              <div className={styles.icon}>
                <DialogIcon type={type} />
              </div>
            ) : null}
          </div>
          <div className={styles.content}>
            {heading ? <div className={styles.heading}>{heading}</div> : null}
            <div>{content}</div>
            {details ? (
              <Details
                details={details}
                visible={state.detailsVisible}
                dispatch={() =>
                  dispatch({ type: MessageToggle.TOGGLE_DETAILS })
                }
              />
            ) : null}
            {!!footer && <div className={styles.footer}>{footer}</div>}
          </div>
          {withDismiss ? <Dismiss type={type} onClose={onClose} /> : null}
        </div>
      ) : null}
    </>
  );
};
