export enum MessageType {
  INFO = 'Info',
  SUCCESS = 'Success',
  WARNING = 'Warning',
  ERROR = 'Error',
}

export type TMessageType = 'Info' | 'Success' | 'Warning' | 'Error';

export enum MessageToggle {
  TOGGLE_DETAILS = 'TOGGLE_DETAILS',
}
