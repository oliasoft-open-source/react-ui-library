import { useArgs } from '@storybook/preview-api';
import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IMessage, Message } from './message';
import { Button } from '../button/button';
import { Grid } from '../layout/grid/grid';
import { MessageType } from './types';

const message: IMessage = {
  visible: true,
  heading: undefined,
  content: 'Here is some info',
  withDismiss: false,
  icon: false,
  details: undefined,
  maxHeight: undefined,
  type: MessageType.INFO,
};

export default {
  title: 'Basic/Message',
  component: Message,
  args: {
    ...message,
  },
  argTypes: {
    heading: {
      control: { type: 'text' },
    },
    details: {
      control: { type: 'text' },
    },
  },
} as Meta;

const Template: StoryFn<IMessage> = (args) => {
  const [_, updateArgs] = useArgs();

  const handleClose = () => {
    updateArgs({ visible: false });
    setTimeout(() => {
      updateArgs({ visible: true });
    }, 1000);
  };

  return (
    <Grid gap>
      <Message
        message={{
          ...args,
          onClose: handleClose,
          type: MessageType.INFO,
        }}
      />
      <Message
        message={{
          ...args,
          onClose: handleClose,
          type: MessageType.ERROR,
        }}
      />
      <Message
        message={{
          ...args,
          onClose: handleClose,
          type: MessageType.WARNING,
        }}
      />
      <Message
        message={{
          ...args,
          onClose: handleClose,
          type: MessageType.SUCCESS,
        }}
      />
    </Grid>
  );
};

export const Default = Template.bind({});

export const Heading = Template.bind({});
Heading.args = { heading: 'Message Heading' };

export const Icon = Template.bind({});
Icon.args = { icon: true };

export const Details = Template.bind({});
Details.args = {
  details:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at mi ac quam condimentum blandit at sit amet lectus. Nullam volutpat erat maximus, venenatis nisl sed, aliquam libero.',
};

export const DetailsShowDefault = Template.bind({});
DetailsShowDefault.args = {
  details:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at mi ac quam condimentum blandit at sit amet lectus. Nullam volutpat erat maximus, venenatis nisl sed, aliquam libero.',
  detailsVisible: true,
};

export const Footer = Template.bind({});
Footer.args = {
  type: MessageType.ERROR,
  icon: true,
  content: 'Unable to sync because connection settings are not set.',
  footer: (
    <>
      <Button
        key="edit-settings"
        small
        label="Edit connection settings"
        colored="primary"
      />
      <Button key="cancel-sync" small label="Cancel sync" />,
    </>
  ),
};

export const FullWidth = Template.bind({});
FullWidth.args = { width: '100%' };

export const FixedWidth = Template.bind({});
FixedWidth.args = { width: '300px' };

export const MaxHeight = Template.bind({});
MaxHeight.args = {
  maxHeight: '100px',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '.repeat(
    20,
  ),
};

export const Dismiss = Template.bind({});
Dismiss.args = {
  withDismiss: true,
  onClose: () => {},
};
