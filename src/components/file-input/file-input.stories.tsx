import React, { ChangeEvent, useState } from 'react';
import { useArgs } from '@storybook/preview-api';
import { Meta, StoryFn } from '@storybook/react';
import { FileInput, IFileInputProps, readFile } from './file-input';
import { Spacer } from '../layout/spacer/spacer';
import { Message } from '../message/message';
import { TextArea } from '../textarea/textarea';
import { MessageType } from '../message/types';

export default {
  title: 'Forms/FileInput',
  component: FileInput,
  args: {
    file: null,
  },
} as Meta;

const Template: StoryFn<IFileInputProps> = (args) => {
  const [_, updateArgs] = useArgs();
  const handleChange = (evt: ChangeEvent<HTMLInputElement>) => {
    updateArgs({ file: evt.target.value });
  };

  return <FileInput {...args} onChange={handleChange} />;
};
export const Default = Template.bind({});

export const RestrictFiletype = Template.bind({});
RestrictFiletype.args = { accept: '.json' };
RestrictFiletype.parameters = {
  docs: {
    description: {
      story:
        'See [accept attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/accept) to restrict file type',
    },
  },
};

export const ReadJSONFile = () => {
  const [file, setFile] = useState();
  const [json, setJson] = useState({});

  return (
    <>
      <FileInput
        file={file}
        onChange={async (evt) => {
          const file = evt.target.value;
          try {
            const text = await readFile(file);
            const json = JSON.parse(text as string);
            setJson(json);
          } catch (error) {
            console.log(error);
          }
          setFile(file);
        }}
        accept=".json"
      />
      <Spacer />
      {file ? (
        <TextArea monospace value={JSON.stringify(json, null, 2)} rows={20} />
      ) : (
        <Message
          message={{
            visible: true,
            content: 'Upload a JSON file to see preview',
            type: MessageType.INFO,
          }}
        />
      )}
    </>
  );
};

export const Image = () => {
  const [file, setFile] = useState();
  return (
    <>
      <FileInput
        file={file}
        onChange={(evt) => {
          setFile(evt.target.value);
        }}
        accept="image/*"
      />
      <Spacer />
      {file ? (
        <img
          src={file ? URL.createObjectURL(file) : ''}
          alt="Preview"
          style={{
            maxHeight: '400px',
            maxWidth: '400px',
          }}
        />
      ) : (
        <Message
          message={{
            visible: true,
            content: 'Upload an image file to see preview',
            type: MessageType.INFO,
          }}
        />
      )}
    </>
  );
};

export const Multi = () => {
  const [files, setFiles] = useState();
  const fileNames = files
    ? Object.values(files).map((value: any) => value.name)
    : [];

  return (
    <>
      <FileInput
        file={files}
        onChange={({ target }) => setFiles(target.value)}
        multi
      />
      <Spacer />
      {!!fileNames.length && (
        <TextArea monospace value={fileNames.join(', \n')} rows={10} />
      )}
    </>
  );
};
