export enum InputReaderMethods {
  READ_AS_TEXT = 'readAsText',
  READ_AS_ARRAY_BUFFER = 'readAsArrayBuffer',
  READ_AS_DATA_URL = 'readAsDataURL',
  READ_AS_BINARY_STRING = 'readAsBinaryString',
}

export type TInputReaderMethods =
  | 'readAsText'
  | 'readAsArrayBuffer'
  | 'readAsDataURL'
  | 'readAsBinaryString';
