import React, { ReactElement, useContext, useRef } from 'react';
import {
  TChangeEvent,
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { noop } from 'lodash';
import { InputGroup } from '../input-group/input-group';
import { Input } from '../input/input';
import { Button } from '../button/button';
import styles from './file-input.module.less';
import { InputReaderMethods, TInputReaderMethods } from './types';

// https://stackoverflow.com/a/65140891/942635
export type TFileReaderMethod = TInputReaderMethods | string;

export const readFile = (
  file: Blob = new Blob(),
  method: TFileReaderMethod = InputReaderMethods.READ_AS_TEXT,
): Promise<string | ArrayBuffer | null> => {
  const reader = new FileReader();

  return new Promise((resolve, reject) => {
    if (method in reader) {
      (reader[method as TInputReaderMethods] as (blob: Blob) => void)(file);
    } else {
      reject(new Error('Invalid FileReader method'));
      return;
    }

    reader.onload = () => {
      if (reader.result !== null) {
        resolve(reader.result);
      } else {
        reject(new Error('File reading resulted in null'));
      }
    };

    reader.onerror = (error) => reject(error);
  });
};

const getName = (file?: File, multi?: boolean) => {
  let name;
  if (!file) return null;

  if (multi) {
    name = `Selected - ${Object.keys(file)?.length} files`;
  } else {
    name = file?.name;
  }

  return name || null;
};

export interface IFileInputProps {
  label?: string | null;
  loading?: boolean;
  placeholder?: string;
  disabled?: boolean;
  file?: File;
  accept?: string;
  multi?: boolean;
  name?: string;
  width?: TStringOrNumber;
  onChange?: TChangeEventHandler;
  testId?: string;
}

export const FileInput = ({
  label = 'Select',
  loading = false,
  placeholder = 'No file selected',
  disabled = false,
  file,
  accept,
  multi,
  name,
  width,
  onChange = noop,
  testId,
}: IFileInputProps): ReactElement => {
  const disabledContext = useContext(DisabledContext);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const fileName = getName(file, multi);

  const handleClick = (): void => {
    if (!(disabled || disabledContext)) inputRef.current?.click();
  };

  const handleChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const { files } = evt.target;
    const value = multi ? (files ? [...files] : null) : files?.[0];

    const customEvent = {
      ...evt,
      target: {
        ...evt.target,
        value,
        name: evt.target.name,
      },
    };

    onChange(customEvent as TChangeEvent);

    // Reset the input value to allow selecting the same file again
    if (inputRef.current) {
      inputRef.current.value = '';
    }
  };

  return (
    <div onClick={handleClick} style={{ width }}>
      <InputGroup>
        <Input value={fileName || ''} placeholder={placeholder} disabled />
        <Button
          label={label}
          loading={loading}
          disabled={loading || disabled || disabledContext}
          groupOrder="last"
        />
      </InputGroup>
      <input
        name={name}
        ref={inputRef}
        className={styles.fileInput}
        type="file"
        accept={accept}
        multiple={multi}
        onChange={handleChange}
        data-testid={testId}
      />
    </div>
  );
};
