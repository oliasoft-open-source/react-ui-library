import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { expect, userEvent, waitFor, within } from '@storybook/test';
import { IMessageType, IToastProps, toast } from './toaster';
import { Button } from '../button/button';
import { MessageType } from '../message/types';

const messageInfo: IMessageType = {
  type: MessageType.INFO,
  icon: true,
  heading: 'Heading',
  content: 'Long content lorem ipsum dolor est compendum lorem ipsum.',
};

export default {
  title: 'Modals/Toaster/Test Cases',
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  source: { type: 'code' },
  args: {
    id: undefined as string | undefined,
    message: messageInfo,
    autoClose: 6000,
    onClose: () => {},
  },
  tags: ['!autodocs'],
} as Meta<IToastProps>;

const Template: StoryFn<IToastProps> = (args) => {
  return (
    <Button
      onClick={() =>
        toast({
          ...args,
          onClose: () => console.log('toast closed'),
        })
      }
      label="Show toast"
    />
  );
};

export const Test = Template.bind({});
Test.args = { id: 'testId', autoClose: 50 };
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Toast should appear when triggered', async () => {
    const trigger = canvas.getByText('Show toast');
    await userEvent.click(trigger);
    const toast = body.getByRole('alert');
    await expect(toast).toBeInTheDocument();
  });
  await step("Duplicate toasts shouldn't be created", async () => {
    const trigger = canvas.getByText('Show toast');
    await userEvent.click(trigger);
    const toast = body.queryAllByRole('alert');
    await expect(toast).toHaveLength(1);
  });
  await step('Toast should disappear when close button pressed', async () => {
    const toast = body.getByRole('alert');
    const dismiss = canvas.getByTestId('testId-dismiss');
    await userEvent.click(dismiss);
    await waitFor(() => expect(toast).not.toBeInTheDocument(), {
      timeout: 2500,
    });
  });
  await step('Toast should automatically close', async () => {
    const toast = body.queryByRole('alert');
    const trigger = canvas.getByText('Show toast');
    await userEvent.click(trigger);
    await waitFor(() => expect(toast).not.toBeInTheDocument(), {
      timeout: 2500,
    });
  });
};
