import React, { ReactElement, ReactNode } from 'react';
import {
  CloseButtonProps,
  Id as ToastId,
  Slide,
  toast as toastBase,
  ToastContent,
  ToastOptions,
} from 'react-toastify';
import { TEmpty } from 'typings/common-type-definitions';
import { Message } from '../message/message';
import { Dismiss } from '../message/dismiss';
import './toaster.less';
import { MessageType, TMessageType } from '../message/types';

export { ToastContainer as Toaster } from 'react-toastify';

export type IMessageType = {
  type?: TMessageType | string;
  icon?: boolean;
  heading?: string | number;
  content: ReactNode | string;
  details?: ReactNode | string | null;
};

export interface IToastProps {
  id?: ToastId;
  message: IMessageType;
  autoClose?: number | false;
  onClose?: TEmpty;
}

export const toast = ({
  id,
  message = { type: MessageType.INFO, content: '' },
  autoClose = 6000,
  onClose,
}: IToastProps) => {
  const { type = MessageType.INFO } = message ?? {};

  const CloseButton = ({ closeToast }: CloseButtonProps): ReactElement => (
    <Dismiss
      type={type}
      onClose={closeToast}
      isInToast
      testId={id !== undefined ? id.toString() + '-dismiss' : undefined}
    />
  );

  const content: ToastContent = (
    <Message
      message={{
        ...message,
        visible: true,
        width: '100%',
        maxHeight: '500px',
        withDismiss: false,
      }}
    />
  );

  const toastType: ToastOptions['type'] = (() => {
    switch (message?.type) {
      case MessageType.SUCCESS:
        return toastBase.TYPE.SUCCESS;
      case MessageType.WARNING:
        return toastBase.TYPE.WARNING;
      case MessageType.ERROR:
        return toastBase.TYPE.ERROR;
      default:
        return toastBase.TYPE.INFO;
    }
  })();

  if (id !== undefined && toastBase.isActive(id)) {
    return toastBase.update(id, {
      render: content,
      autoClose,
      onClose,
      type: toastType,
    });
  }

  return toastBase(content, {
    toastId: id,
    autoClose,
    onClose,
    hideProgressBar: autoClose === false,
    icon: false,
    closeOnClick: false,
    pauseOnHover: true,
    draggable: false,
    closeButton: CloseButton,
    transition: Slide,
    type: toastType,
  });
};

export const dismissToast = (id: ToastId): void => {
  toastBase.dismiss(id);
};
