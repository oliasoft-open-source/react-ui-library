import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Grid } from 'components/layout/grid/grid';
import { Text } from 'components/text/text';
import { Flex } from 'components/layout/flex/flex';
import { IMessageType, IToastProps, toast, dismissToast } from './toaster';
import { Button } from '../button/button';
import { Spacer } from '../layout/spacer/spacer';
import { MessageType } from '../message/types';

const messageSuccess: IMessageType = {
  type: MessageType.SUCCESS,
  icon: true,
  heading: 'Heading',
  content: 'Great results',
};

const messageError: IMessageType = {
  type: MessageType.ERROR,
  icon: true,
  heading: 'Heading',
  content: 'An error has happened',
  details: 'Bad input data lorem ipsum dolor est compendum lorem ipsum.',
};

const messageWarning: IMessageType = {
  type: MessageType.WARNING,
  icon: true,
  heading: 'Heading',
  content: 'Warning message',
};

const messageInfo: IMessageType = {
  type: MessageType.INFO,
  icon: true,
  heading: 'Heading',
  content: 'Long content lorem ipsum dolor est compendum lorem ipsum.',
};

export default {
  title: 'Modals/Toaster',
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  source: { type: 'code' },
  args: {
    id: undefined as string | undefined,
    message: messageInfo,
    autoClose: 6000,
    onClose: () => {},
  },
} as Meta<IToastProps>;

const Template: StoryFn<IToastProps> = (args) => {
  return (
    <Button
      onClick={() =>
        toast({
          ...args,
          onClose: () => console.log('toast closed'),
        })
      }
      label="Show toast"
    />
  );
};
export const Default = Template.bind({});

export const Success = Template.bind({});
Success.args = { message: messageSuccess };

export const Warning = Template.bind({});
Warning.args = { message: messageWarning };

export const Error = Template.bind({});
Error.args = { message: messageError };

export const PreventDuplicates = () => (
  <Button
    label="Show Toast"
    onClick={() => {
      const isSuccess = Math.random() >= 0.5;
      toast({
        message: {
          type: isSuccess ? 'Success' : 'Error',
          icon: true,
          content: isSuccess ? 'Success' : 'Error',
        } as IMessageType,
        onClose: () => console.log('toast closed'),
        id: 'uniqueId',
      });
    }}
  />
);
PreventDuplicates.parameters = {
  docs: {
    iframeHeight: 200,
    description: {
      story:
        'Pass a unique `id` in the `toast` object to prevent duplicates. Successive toasts with the same ID will update the existing toast.',
    },
  },
};

export const SessionExpiring = () => {
  const TOAST_ID = 'sessionExpiring';
  const message = {
    type: 'Info',
    content: (
      <Grid>
        <Text bold>You will be signed out soon</Text>
        <Text>
          For your security, you will be automatically signed out in 30 minutes
          unless you extend your session.
        </Text>
        <Spacer height="var(--padding-xxs)" />
        <Flex gap="var(--padding-xs)">
          <Button
            label="Extend session"
            small
            colored="success"
            onClick={() => dismissToast(TOAST_ID)}
          />
          <Button
            label="Dismiss"
            small
            onClick={() => dismissToast(TOAST_ID)}
          />
        </Flex>
      </Grid>
    ),
  } as IMessageType;

  return (
    <Button
      label="Show Toast"
      onClick={() => toast({ message, autoClose: false, id: TOAST_ID })}
    />
  );
};

export const SessionExpiringSoon = () => {
  const TOAST_ID = 'sessionExpiringSoon';
  const message = {
    type: 'Error',
    content: (
      <Grid>
        <Text bold>You will be signed out very soon</Text>
        <Text>
          For your security, you will be automatically signed out in 30 minutes
          unless you extend your session.
        </Text>
        <Spacer height="var(--padding-xxs)" />
        <Flex gap="var(--padding-xs)">
          <Button
            label="Extend session"
            small
            colored="success"
            onClick={() => dismissToast(TOAST_ID)}
          />
          <Button
            label="Dismiss"
            small
            onClick={() => dismissToast(TOAST_ID)}
          />
        </Flex>
      </Grid>
    ),
  } as IMessageType;

  return (
    <Button
      label="Show Toast"
      onClick={() => toast({ message, autoClose: false, id: TOAST_ID })}
    />
  );
};

export const SessionExtended = () => {
  const message = {
    type: 'Success',
    content: 'Your session has been extended for 1 hour',
  } as IMessageType;

  return <Button label="Show Toast" onClick={() => toast({ message })} />;
};
