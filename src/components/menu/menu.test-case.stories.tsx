import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { expect, userEvent, within } from '@storybook/test';
import { IMenuProps, Menu } from './menu';
import * as storyData from './menu.stories-data';

export default {
  title: 'Basic/Menu/Test Cases',
  component: Menu,
  args: {
    menu: storyData.menu,
  },
  tags: ['!autodocs'],
} as Meta;

const Template: StoryFn<IMenuProps> = (args) => (
  <Menu
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);

export const Test = Template.bind({});
Test.args = { testId: 'testId' };
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Clicking trigger should open menu', async () => {
    const component = canvas.getByTestId('testId');
    await userEvent.click(component);
    const layer = await body.findByTestId('testId-layer');
    await expect(layer).toBeInTheDocument();
  });
  await step('Clicking disabled item should do nothing', async () => {
    const item = body.getByText('Knight');
    await item.click();
    const layer = await body.findByTestId('testId-layer');
    await expect(layer).toBeInTheDocument();
  });
  await step('Clicking item should close menu', async () => {
    const item = body.getByText('King');
    await userEvent.click(item);
    const layer = body.queryByTestId('testId-layer');
    await expect(layer).not.toBeInTheDocument();
  });
};
