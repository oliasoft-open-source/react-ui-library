import React, { ReactNode } from 'react';
import cx from 'classnames';
import {
  TChangeEvent,
  TChangeEventHandler,
  TEmpty,
} from 'typings/common-type-definitions';
import { noop } from 'lodash';
import { IconType } from 'typings/common-types';
import { Icon } from '../../icon/icon';
import styles from '../menu.module.less';
import { Actions } from '../../actions/actions';

export interface IOptionProps {
  actions?: any[];
  label: ReactNode;
  url?: string;
  onClick?: (evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  description?: string;
  icon?: any;
  selected?: boolean;
  closeOnOptionClick?: boolean;
  close?: TEmpty;
  disabled?: boolean;
  inline?: boolean;
  title?: string;
  upload?: boolean;
  onChange?: TChangeEventHandler;
  testId?: string;
  component?: React.ElementType;
}

export const Option = ({
  actions,
  label,
  url,
  onClick,
  description,
  icon,
  selected,
  closeOnOptionClick,
  close,
  disabled,
  inline,
  title,
  upload,
  onChange = noop,
  testId,
  component: Component = 'a',
}: IOptionProps) => {
  const handleClick = (
    evt: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
  ) => {
    evt.stopPropagation();
    if (disabled) {
      evt.preventDefault();
      return;
    }
    if (upload) return;
    if (closeOnOptionClick) {
      close?.();
    }
    onClick?.(evt);
  };

  const handleFileChange: TChangeEventHandler = (evt) => {
    const { files } = evt?.target ?? {};
    if (files) {
      const customEvent = {
        ...evt,
        target: {
          ...evt.target,
          value: files,
          name: evt.target.name,
          files,
        },
      };
      onChange?.(customEvent as TChangeEvent<HTMLInputElement>);
      close?.();
    }
  };

  return (
    <Component
      href={url}
      to={url}
      className={cx(
        styles.option,
        disabled ? styles.disabled : '',
        inline ? styles.inline : '',
        selected ? styles?.selected : '',
      )}
      onClick={handleClick}
      data-testid={testId}
    >
      {icon ? (
        <span className={styles.icon}>
          <Icon icon={icon} />
        </span>
      ) : null}
      <div className={styles.optionContent}>
        {upload && (
          <input
            type="file"
            className={styles.fileInput}
            onChange={handleFileChange}
          />
        )}
        <span className={styles.label} title={`${title || label}`}>
          {label}
        </span>
        <span className={styles.description}>{description}</span>
      </div>
      <div className={styles.right}>
        {selected && (
          <span className={styles.check}>
            <Icon icon={IconType.CHECK} />
          </span>
        )}
        {!!actions?.length && (
          <div className={styles.actions}>
            <Actions actions={actions} />
          </div>
        )}
      </div>
    </Component>
  );
};
