import React from 'react';
import styles from '../menu.module.less';

export const Divider = () => <hr className={styles.divider} />;
