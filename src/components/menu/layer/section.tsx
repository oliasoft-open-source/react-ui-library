import React, { useContext } from 'react';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { Heading } from './heading';
import { Divider } from './divider';
import { Option } from './option';
import { DropDownMenu } from '../menu';
import { IMenuSection } from '../menu.interface';
import { MenuType } from '../types';

export interface ISectionProps {
  section: IMenuSection;
  closeOnOptionClick?: boolean;
  close: TEmpty;
  tree: any;
  path: string;
  maxHeight?: TStringOrNumber;
}

export const Section = ({
  section,
  closeOnOptionClick,
  close,
  tree,
  path,
  maxHeight,
}: ISectionProps) => {
  const disabledContext = useContext(DisabledContext);

  switch (section.type) {
    case MenuType.HEADING:
      return (
        <Heading
          label={section?.label ?? ''}
          onClick={section.onClick}
          icon={section.icon}
          testId={section.testId}
        />
      );
    case MenuType.DIVIDER:
      return <Divider />;
    case MenuType.OPTION:
      return (
        <Option
          actions={section?.actions}
          label={section?.label ?? ''}
          url={section?.url}
          onClick={section?.onClick}
          description={section?.description}
          icon={section?.icon}
          selected={section?.selected}
          closeOnOptionClick={closeOnOptionClick}
          close={close}
          disabled={section?.disabled || disabledContext}
          inline={section?.inline}
          title={section?.title}
          upload={section?.upload}
          onChange={section?.onChange}
          testId={section?.testId}
          component={section?.component}
        />
      );
    case MenuType.MENU:
      const selected = !!section?.menu?.sections?.find(
        (s: IMenuSection) => s?.selected,
      );
      return (
        <DropDownMenu
          menu={section.menu}
          selected={selected}
          title={section.title}
          closeOnOptionClick={closeOnOptionClick}
          isNested
          closeParent={close}
          tree={tree}
          path={path}
          maxHeight={maxHeight}
          testId={section.testId}
        />
      );
    default:
      return null;
  }
};
