type PlacementType = 'right-start' | 'bottom-start' | string;

export const placementOptions = (
  placement?: PlacementType,
  isNested: boolean = false,
): object => ({
  auto: true,
  preferX: 'right',
  preferY: 'bottom',
  placement: placement || (isNested ? 'right-start' : 'bottom-start'),
});
