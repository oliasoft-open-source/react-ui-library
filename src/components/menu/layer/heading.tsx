import React, { ReactNode } from 'react';
import styles from '../menu.module.less';

export interface IHeadingProps {
  label: ReactNode;
  onClick?: (evt: React.MouseEvent<HTMLSpanElement, MouseEvent>) => void;
  icon?: React.ReactNode;
  testId?: string;
}

export const Heading = ({ label, onClick, icon, testId }: IHeadingProps) => (
  <div
    onClick={(evt) => evt.stopPropagation()} //don't close menu
    className={styles.heading}
    data-testid={testId}
  >
    {label}
    <span className={styles.headingIcon} onClick={onClick}>
      {icon}
    </span>
  </div>
);
