import React, { isValidElement, ReactNode, useState } from 'react';
import cx from 'classnames';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import styles from '../menu.module.less';
import { Section } from './section';
import { childPath } from './path';
import { IMenuSection, IMenuShowAllButton } from '../menu.interface';
import { MenuType } from '../types';

export interface IMenuLayerProps {
  sections: IMenuSection[] | ReactNode;
  isNested?: boolean;
  width?: TStringOrNumber;
  closeOnOptionClick?: boolean;
  close?: TEmpty;
  tree?: any;
  path?: any;
  maxHeight?: TStringOrNumber;
  showAllButton?: IMenuShowAllButton;
  testId?: string;
}

export const Layer = ({
  sections,
  isNested,
  width,
  closeOnOptionClick,
  close = () => {},
  tree,
  path,
  maxHeight,
  showAllButton,
  testId,
}: IMenuLayerProps) => {
  if (isValidElement(sections)) {
    return <>{sections}</>;
  }

  const [showAll, setShowAll] = useState(!showAllButton?.visible); //show all by default
  const sectionsArray = sections as IMenuSection[];
  const additionalSections =
    showAllButton?.additionalSections?.map((s) => ({
      ...s,
      closeOnOptionClick,
      visible: true,
    })) ?? [];
  const showAllButtonOption =
    sectionsArray.length > 4
      ? [
          {
            type: MenuType.OPTION,
            label: showAll
              ? showAllButton?.showLessTitle || 'Show less'
              : showAllButton?.showAllTitle || 'Show all',
            onClick: () => setShowAll(!showAll),
            visible: true,
            closeOnOptionClick: false,
          },
        ]
      : [];

  const localSections = showAllButton?.visible
    ? sectionsArray
        .map((s, i: number) => {
          return showAll || i <= 3
            ? {
                ...s,
                visible: true,
                closeOnOptionClick,
              }
            : s;
        })
        .concat(
          showAllButtonOption as IMenuSection[],
          additionalSections as IMenuSection[],
        )
        .filter((s) => s?.visible)
    : sectionsArray.map((s) => ({ ...s, closeOnOptionClick }));

  return (
    <div
      className={cx(styles.layer, isNested ? styles.nested : null)}
      style={{ maxWidth: width, maxHeight }}
      data-testid={testId}
    >
      <ul>
        {localSections.map((section: IMenuSection, i: number) => (
          <li key={i}>
            <Section
              section={section}
              closeOnOptionClick={section.closeOnOptionClick}
              close={close}
              tree={tree}
              path={childPath(path, i)}
              maxHeight={maxHeight}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};
