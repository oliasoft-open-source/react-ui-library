import React, { ReactNode, useContext } from 'react';
import cx from 'classnames';
import { IconType, TGroupOrder, TriggerType } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import styles from '../menu.module.less';
import { Button as BasicButton } from '../../button/button';
import { Icon } from '../../icon/icon';

export interface IMenuButtonProps {
  trigger: 'Button' | 'DropDownButton' | string;
  label: ReactNode | string;
  colored?: boolean | string;
  small?: boolean;
  width?: TStringOrNumber;
  disabled?: boolean;
  groupOrder?: TGroupOrder;
  loading?: boolean;
  error?: string;
  warning?: string;
  active?: boolean;
  tabIndex?: number;
}

export const Button = ({
  trigger,
  label,
  colored,
  small,
  width,
  disabled,
  groupOrder,
  loading,
  error,
  warning,
  active,
  tabIndex,
}: IMenuButtonProps) => {
  const disabledContext = useContext(DisabledContext);

  const buttonLabel: ReactNode =
    trigger === TriggerType.BUTTON ? (
      label
    ) : trigger === TriggerType.DROP_DOWN_BUTTON ? (
      <span className={styles.middleAlignedInline}>
        <span className={styles.buttonLabel}>{label}</span>
        <span className={styles.buttonCaret}>
          <Icon icon={IconType.CHEVRON_DOWN} />
        </span>
      </span>
    ) : null;

  return (
    <div
      className={cx(
        styles.trigger,
        disabled || disabledContext ? styles.disabled : null,
      )}
      style={{ width }}
    >
      <BasicButton
        label={buttonLabel}
        colored={colored}
        small={small}
        width={width}
        groupOrder={groupOrder}
        onClick={() => {} /*handled in parent*/}
        loading={loading}
        disabled={disabled}
        error={error}
        warning={warning}
        active={active}
        tabIndex={tabIndex}
      />
    </div>
  );
};
