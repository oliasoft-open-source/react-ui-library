import React from 'react';
import cx from 'classnames';
import styles from '../menu.module.less';

interface IMenuComponentProps {
  component: React.ReactNode;
  disabled?: boolean;
  testId?: string;
}

export const Component = ({
  component,
  disabled,
  testId,
}: IMenuComponentProps) => {
  return (
    <div
      className={cx(styles.component, disabled ? styles.disabled : null)}
      data-testid={testId}
    >
      {component}
    </div>
  );
};
