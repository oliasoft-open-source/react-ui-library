import React, { ReactNode, Ref } from 'react';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { TGroupOrder, TriggerType, TTriggerType } from 'typings/common-types';
import { isStringNumberOrNode } from 'helpers/types';
import { Badge } from '../../badge/badge';
import { Button } from './button';
import { Text } from './text';
import { Tooltip } from '../../tooltip/tooltip';
import { Component } from './component';
import styles from '../menu.module.less';
import { MenuCarat } from '../types';

interface TriggerTooltipProps {
  tooltip?: TStringOrNumber | ReactNode;
  error?: TStringOrNumber | ReactNode;
  warning?: TStringOrNumber | ReactNode;
  children: ReactNode;
}

const TriggerTooltip = ({
  tooltip,
  error,
  warning,
  children,
}: TriggerTooltipProps) => {
  if (
    !isStringNumberOrNode(tooltip) &&
    !isStringNumberOrNode(error) &&
    !isStringNumberOrNode(warning)
  ) {
    return <>{children}</>;
  }
  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      display="inline-block"
    >
      {children}
    </Tooltip>
  );
};

export interface IMenuTriggerProps {
  active?: boolean;
  isDisabled?: boolean;
  badgeTitle?: string;
  badgeDot?: boolean;
  loading?: boolean;
  width?: TStringOrNumber;
  trigger: TTriggerType | string;
  label?: ReactNode | string | null;
  title?: string;
  onClickTrigger: TEmpty;
  colored?: boolean | string;
  small?: boolean;
  isNested?: boolean;
  contextMenu?: boolean;
  component?: ReactNode;
  groupOrder?: TGroupOrder;
  fullHeight?: boolean;
  isOpen?: boolean;
  error?: string;
  warning?: string;
  tooltip?: TStringOrNumber | ReactNode;
  testId?: string;
  children?: React.ReactNode;
  selected?: boolean;
  tabIndex?: number;
}

export const Trigger = React.forwardRef(
  (
    {
      active,
      isDisabled,
      badgeTitle,
      badgeDot,
      loading,
      width,
      trigger,
      label,
      title,
      onClickTrigger,
      colored,
      small,
      isNested,
      contextMenu,
      component,
      groupOrder,
      fullHeight,
      isOpen,
      error,
      warning,
      tooltip,
      testId,
      selected,
      tabIndex,
      ...restProps
    }: IMenuTriggerProps,
    ref: Ref<HTMLDivElement>,
  ) => {
    const triggerElement =
      trigger === TriggerType.BUTTON ||
      trigger === TriggerType.DROP_DOWN_BUTTON ? (
        <Button
          trigger={trigger}
          label={label ?? ''}
          colored={colored}
          small={small}
          width={width}
          disabled={isDisabled}
          groupOrder={groupOrder}
          loading={loading}
          error={error}
          warning={warning}
          active={isOpen || active}
          tabIndex={tabIndex}
        />
      ) : trigger === TriggerType.TEXT ? (
        <Text
          label={label ?? ''}
          title={title}
          disabled={isDisabled}
          carat={
            !contextMenu
              ? isNested
                ? MenuCarat.RIGHT
                : MenuCarat.DOWN
              : undefined
          }
          isOpen={isOpen}
          selected={selected}
        />
      ) : trigger === TriggerType.COMPONENT ? (
        <Component component={component} disabled={isDisabled} />
      ) : null;
    const wrappedTrigger =
      !isNested && (badgeTitle || badgeDot) ? (
        <Badge title={badgeTitle} dot={badgeDot} small={!badgeDot}>
          {triggerElement}
        </Badge>
      ) : (
        triggerElement
      );
    return (
      <div
        ref={ref}
        className={styles.wrapper}
        style={
          width
            ? { width }
            : {
                ...(fullHeight && {
                  height: '100%',
                  display: 'flex',
                }),
              }
        }
        onClick={(evt) => {
          evt.stopPropagation();
          evt.preventDefault();
          onClickTrigger();
        }}
        data-error={error || null}
        data-warning={warning || null}
        data-testid={testId}
      >
        <TriggerTooltip tooltip={tooltip} error={error} warning={warning}>
          {wrappedTrigger}
        </TriggerTooltip>
      </div>
    );
  },
);
