import React, { ReactNode } from 'react';
import cx from 'classnames';
import { IconType } from 'typings/common-types';
import styles from '../menu.module.less';
import { Icon } from '../../icon/icon';
import { MenuCarat, TMenuCarat } from '../types';

interface IMenuTextProps {
  label: ReactNode | string;
  title?: string;
  carat?: TMenuCarat | string;
  disabled?: boolean;
  isOpen?: boolean;
  selected?: boolean;
}

export const Text = ({
  label,
  title,
  carat,
  disabled,
  isOpen,
  selected,
}: IMenuTextProps) => {
  let titleText: string | undefined;
  if (title) {
    titleText = title;
  } else if (typeof label === 'string' || typeof label === 'number') {
    titleText = String(label);
  }
  return (
    <div
      className={cx(
        styles.trigger,
        disabled ? styles.disabled : null,
        isOpen ? styles.active : null,
        selected ? styles?.selected : null,
      )}
    >
      <span className={styles.label} title={titleText}>
        {label}
      </span>
      {carat && carat === MenuCarat.RIGHT ? (
        <span className={styles.arrow}>
          <Icon icon={IconType.CHEVRON_RIGHT} />
        </span>
      ) : carat && carat === MenuCarat.DOWN ? (
        <span className={styles.arrow}>
          <Icon icon={IconType.CHEVRON_DOWN} />
        </span>
      ) : null}
    </div>
  );
};
