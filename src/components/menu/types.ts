export enum MenuType {
  HEADING = 'Heading',
  DIVIDER = 'Divider',
  OPTION = 'Option',
  MENU = 'Menu',
}

export enum MenuCarat {
  FALSE = 'false',
  RIGHT = 'right',
  DOWN = 'down',
}

export type TMenuType = 'Heading' | 'Divider' | 'Option' | 'Menu';
export type TMenuCarat = 'false' | 'right' | 'down';
