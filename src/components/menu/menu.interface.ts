import { ElementType, ReactNode } from 'react';
import { Placement } from 'react-laag';
import {
  TChangeEventHandler,
  TEmpty,
  TFunction,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { TGroupOrder } from 'typings/common-types';
import { TMenuType } from './types';

export interface IMenuSection {
  type: TMenuType | string;
  label?: ReactNode;
  icon?: string | ReactNode;
  onClick?: TFunction;
  testId?: string;
  description?: string;
  selected?: boolean;
  disabled?: boolean;
  inline?: boolean;
  title?: string;
  download?: boolean;
  onChange?: TChangeEventHandler;
  menu?: any;
  visible?: boolean;
  closeOnOptionClick?: boolean;
  actions?: any[];
  url?: string;
  upload?: boolean;
  component?: ElementType;
}

export interface IMenuShowAllButton {
  visible: boolean;
  showAllTitle: string;
  showLessTitle: string;
  additionalSections: IMenuSection[];
}

export interface IUseContextMenuProps {
  sections: ReactNode | IMenuSection[];
  width?: TStringOrNumber;
  placement?: string;
  possiblePlacements: Placement[];
  closeOnOptionClick: boolean;
  closeParent?: TEmpty;
  tree: any;
  path: string;
  groupOrder?: TGroupOrder;
  overflowContainer: boolean;
  maxHeight?: TStringOrNumber;
  testId?: string;
}
