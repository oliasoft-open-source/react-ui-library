import React, { isValidElement, ReactNode, useContext, useState } from 'react';
import { isBoolean, isEmpty, isFunction } from 'lodash';
import { Placement, useLayer, useMousePositionAsTrigger } from 'react-laag';
import ResizeObserver from 'resize-observer-polyfill';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { TGroupOrder, TTriggerType } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import styles from './menu.module.less';
import {
  IMenuSection,
  IMenuShowAllButton,
  IUseContextMenuProps,
} from './menu.interface';
import { Layer, placementOptions, registerClose, siblings, Trigger } from '.';

export interface IMenuProps {
  menu: {
    trigger: TTriggerType | string;
    title?: string;
    colored?: boolean | string;
    small?: boolean;
    label?: ReactNode | string;
    component?: ReactNode;
    sections?: ReactNode | IMenuSection[];
    placement?: string;
    possiblePlacements?: Placement[];
    showAllButton?: IMenuShowAllButton;
    testId?: string;
    fullHeightTrigger?: boolean;
  };
  active?: boolean;
  contextMenu?: boolean;
  width?: TStringOrNumber;
  disabled?: boolean;
  badgeTitle?: string;
  badgeDot?: boolean;
  loading?: boolean;
  fullHeightTrigger?: boolean;
  closeOnOptionClick?: boolean;
  groupOrder?: TGroupOrder;
  overflowContainer?: boolean;
  maxHeight?: TStringOrNumber;
  testId?: string;
  open?: boolean;
  setOpen?: (state: boolean) => void;
  closeParent?: TEmpty;
  tree?: any;
  path?: any;
  isNested?: boolean;
  tooltip?: any;
  error?: any;
  warning?: any;
  title?: string;
  selected?: boolean;
  tabIndex?: number;
}

const isMenuEmpty = (menu: { sections?: any[] | ReactNode }): boolean => {
  const { sections } = menu;
  return (
    !isValidElement(sections) &&
    (!Array.isArray(sections) || sections.every((section) => isEmpty(section)))
  );
};

const useContextMenu = ({
  sections,
  width,
  placement,
  possiblePlacements,
  closeOnOptionClick,
  closeParent,
  tree,
  path,
  groupOrder,
  overflowContainer,
  maxHeight,
  testId,
}: IUseContextMenuProps): [
  ReactNode,
  (evt: React.MouseEvent<HTMLDivElement>) => void,
] => {
  const { hasMousePosition, resetMousePosition, handleMouseEvent, trigger } =
    useMousePositionAsTrigger();

  const close = resetMousePosition;
  const isOpen = hasMousePosition;

  const { layerProps, renderLayer } = useLayer({
    isOpen,
    trigger,
    ...placementOptions(placement),
    possiblePlacements,
    ResizeObserver,
    onOutsideClick: close,
    overflowContainer,
  });

  const layer =
    isOpen &&
    renderLayer(
      <div {...layerProps}>
        <Layer
          isNested={false}
          width={width}
          sections={sections}
          closeOnOptionClick={closeOnOptionClick}
          close={closeParent || close}
          tree={tree}
          path={path}
          maxHeight={maxHeight}
          testId={testId && `${testId}-layer`}
        />
      </div>,
    );

  return [layer, handleMouseEvent];
};

const ContextMenu = ({
  menu,
  width,
  disabled = false,
  closeOnOptionClick = true,
  closeParent,
  tree,
  path,
  groupOrder,
  testId,
}: IMenuProps) => {
  const disabledContext = useContext(DisabledContext);

  const {
    label,
    trigger,
    fullHeightTrigger,
    colored,
    small,
    component,
    placement,
    possiblePlacements = ['bottom-start', 'top-start'],
  } = menu;

  const [element, onContextMenu] = useContextMenu({
    sections: menu.sections,
    width,
    placement,
    possiblePlacements,
    closeOnOptionClick,
    closeParent,
    tree,
    path,
    groupOrder,
    overflowContainer: true,
    maxHeight: undefined,
    testId,
  });

  const isDisabled = disabled || isMenuEmpty(menu) || disabledContext;

  return (
    <div onContextMenu={onContextMenu} data-testid={testId}>
      <Trigger
        width={width}
        fullHeight={fullHeightTrigger}
        isDisabled={isDisabled}
        trigger={trigger}
        label={label}
        onClickTrigger={() => {}}
        colored={colored}
        small={small}
        isNested
        component={component}
        contextMenu
        groupOrder={groupOrder}
      />
      {element}
    </div>
  );
};

export const DropDownMenu = ({
  menu,
  width,
  disabled = false,
  badgeTitle,
  badgeDot = false,
  loading = false,
  isNested = false,
  closeOnOptionClick = true,
  closeParent,
  tree,
  path,
  groupOrder,
  overflowContainer = true,
  maxHeight,
  testId,
  open: openProp,
  setOpen: setOpenProp,
  tooltip,
  error,
  warning,
  selected,
  tabIndex,
  active,
}: IMenuProps) => {
  const disabledContext = useContext(DisabledContext);

  const {
    label,
    title,
    trigger,
    fullHeightTrigger,
    colored,
    small,
    sections,
    component,
    placement,
    showAllButton,
    possiblePlacements = ['bottom-start', 'top-start'],
  } = menu;

  const [isOpen, setOpen] =
    isBoolean(openProp) && isFunction(setOpenProp)
      ? [openProp, setOpenProp]
      : useState(false);

  const close = () => setOpen(false);
  const open = () => setOpen(true);

  const { triggerProps, layerProps, renderLayer } = useLayer({
    isOpen,
    ...placementOptions(placement, isNested),
    possiblePlacements,
    ResizeObserver,
    onOutsideClick: close,
    overflowContainer,
    onParentClose: close,
  });

  const isDisabled = disabled || isMenuEmpty(menu) || disabledContext;

  //keep a record of close handlers so we can close sibling layers
  registerClose(tree, path, close);

  const onClickTrigger = () => {
    if (isDisabled) {
      return;
    }

    if (isOpen) {
      close();
      return;
    }

    //first close any sibling menus
    siblings(tree, path).forEach((section) => {
      if (section.close) {
        section.close();
      }
    });
    open();
  };

  return (
    <>
      <Trigger
        ref={triggerProps.ref}
        isDisabled={isDisabled}
        badgeTitle={badgeTitle}
        badgeDot={badgeDot}
        loading={loading}
        width={width}
        fullHeight={fullHeightTrigger}
        trigger={trigger}
        label={label}
        title={title}
        onClickTrigger={onClickTrigger}
        colored={colored}
        small={small}
        isNested={isNested}
        component={component}
        groupOrder={groupOrder}
        isOpen={isOpen}
        active={active}
        tooltip={tooltip}
        error={error}
        warning={warning}
        testId={testId}
        selected={selected}
        tabIndex={tabIndex}
      />
      {isOpen &&
        renderLayer(
          <div
            {...layerProps}
            className={styles.layerContainer}
            data-testid={menu.testId}
            onClick={(evt) => evt.stopPropagation()}
          >
            <Layer
              sections={sections}
              isNested={isNested}
              width={width}
              closeOnOptionClick={closeOnOptionClick}
              close={closeParent || close}
              tree={tree}
              path={path}
              maxHeight={maxHeight}
              showAllButton={showAllButton}
              testId={testId && `${testId}-layer`}
            />
          </div>,
        )}
    </>
  );
};

export const Menu = (props: IMenuProps) => {
  return props.contextMenu ? (
    <ContextMenu {...props} tree={{}} path="" testId={props.testId} />
  ) : (
    <DropDownMenu {...props} tree={{}} path="" testId={props.testId} />
  );
};
