import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter, Link, Routes, Route } from 'react-router-dom';
import { IconType, TriggerType } from 'typings/common-types';
import { IMenuProps, Menu } from './menu';
import { Button } from '../button/button';
import { Flex } from '../layout/flex/flex';
import * as storyData from './menu.stories-data';
import { MenuType } from './types';

export default {
  title: 'Basic/Menu',
  component: Menu,
  args: {
    menu: storyData.menu,
  },
} as Meta;

const Template: StoryFn<IMenuProps> = (args) => (
  <Menu
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...args}
  />
);
export const Default = Template.bind({});

export const PrimaryTrigger = Template.bind({});
PrimaryTrigger.args = { menu: { ...storyData.menu, colored: true } };

export const DestructiveTrigger = Template.bind({});
DestructiveTrigger.args = {
  menu: { ...storyData.menu, label: 'Delete', colored: 'danger' },
};

export const TextTrigger = Template.bind({});
TextTrigger.args = { menu: { ...storyData.menu, trigger: TriggerType.TEXT } };

export const CustomTrigger = Template.bind({});
CustomTrigger.args = {
  menu: {
    ...storyData.menu,
    trigger: TriggerType.COMPONENT,
    component: <Button small colored round icon={IconType.SETTINGS} />,
  },
};

export const FileUpload = Template.bind({});
FileUpload.args = { menu: storyData.menuFileUpload };

export const Description = Template.bind({});
Description.args = { menu: storyData.menuDescription };

export const DescriptionOnRight = Template.bind({});
DescriptionOnRight.args = { menu: storyData.menuDescriptionOnRight };

export const Actions = Template.bind({});
Actions.args = { menu: storyData.menuActions };

export const Icons = Template.bind({});
Icons.args = { menu: storyData.menuIcons };

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const Tooltip = Template.bind({});
Tooltip.args = { tooltip: 'Tooltip goes here' };

export const Error = Template.bind({});
Error.args = { error: 'Error goes here' };

export const Warning = Template.bind({});
Warning.args = { warning: 'Warning goes here' };

export const Active = Template.bind({});
Active.args = { active: true };

export const Loading = Template.bind({});
Loading.args = {
  loading: true,
  disabled: true,
};

export const Badge = Template.bind({});
Badge.args = {
  badgeTitle: '3',
};

export const Dot = Template.bind({});
Dot.args = {
  badgeDot: true,
};

export const Empty = Template.bind({});
Empty.args = { menu: storyData.menuEmpty };

export const Submenus = Template.bind({});
Submenus.args = { menu: storyData.menuSubmenus };

export const WithoutCloseOnOptionClick = Template.bind({});
WithoutCloseOnOptionClick.args = { closeOnOptionClick: false };

export const RightClickContextMenu = Template.bind({});
RightClickContextMenu.args = {
  contextMenu: true,
  menu: {
    ...storyData.menu,
    trigger: TriggerType.TEXT,
    label: 'Right-click me',
  },
};

export const FullWidth = Template.bind({});
FullWidth.args = {
  width: '100%',
};

export const Small = Template.bind({});
Small.args = {
  menu: { ...storyData.menu, small: true },
};

export const JSXItems = Template.bind({});
JSXItems.args = { menu: storyData.menuJsxItems };

export const JSXContent = Template.bind({});
JSXContent.args = {
  menu: {
    ...storyData.menu,
    sections: <div>Testing</div>,
  },
};

export const ShowAll = Template.bind({});
ShowAll.args = { menu: storyData.menuShowAll };

export const Long = Template.bind({});
Long.args = {
  menu: storyData.menuLong,
};

export const ControlOpenStateFromParent: StoryFn<IMenuProps> = () => {
  const [open, setOpen] = useState(false);
  return (
    <Flex gap>
      <Menu menu={storyData.menu} open={open} setOpen={setOpen} />
      <Button label="Open" onClick={() => setOpen(true)} disabled={open} />
      <Button label="Close" onClick={() => setOpen(false)} disabled={!open} />
    </Flex>
  );
};

export const AsRouterLink = () => {
  return (
    <MemoryRouter>
      <Menu
        menu={{
          label: 'Go to',
          trigger: TriggerType.DROP_DOWN_BUTTON,
          sections: [
            {
              type: MenuType.OPTION,
              label: 'Dashboard',
              url: '/dashboard',
              component: Link,
            },
            {
              type: MenuType.OPTION,
              label: 'Settings',
              url: '/settings',
              component: Link,
            },
          ],
        }}
      />
      <Routes>
        <Route path="/dashboard" element={<div>Dashboard</div>} />
        <Route path="/settings" element={<div>Settings</div>} />
      </Routes>
    </MemoryRouter>
  );
};
