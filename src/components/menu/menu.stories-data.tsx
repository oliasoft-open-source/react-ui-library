import React from 'react';
import { TriggerType, TTriggerType } from 'typings/common-types';
import { TChangeEventHandler } from 'typings/common-type-definitions';
import { Flex } from '../layout/flex/flex';
import { Badge } from '../badge/badge';
import { IMenuSection } from './menu.interface';
import { MenuType } from './types';

const onFileChange: TChangeEventHandler = async (event): Promise<void> => {
  const { files } = event?.target ?? {};
  if (files) {
    console.log('onFileChange', files);
  }
};

export const menu: {
  label?: string;
  trigger: TTriggerType | string;
  sections: IMenuSection[];
} = {
  label: 'Select',
  trigger: TriggerType.DROP_DOWN_BUTTON,
  sections: [
    { type: MenuType.HEADING, label: 'Heading' },
    {
      type: MenuType.OPTION,
      label: 'King',
      onClick: () => {},
    },
    {
      type: MenuType.OPTION,
      label: 'Queen',
      selected: true,
      onClick: () => {},
    },
    { type: MenuType.DIVIDER },
    {
      type: MenuType.OPTION,
      label: 'Knight',
      disabled: true,
      onClick: () => {},
    },
    {
      type: MenuType.OPTION,
      label: 'Bishop',
      onClick: () => {},
    },
  ],
};

export const menuFileUpload = {
  ...menu,
  sections: menu.sections.map((section) =>
    section.type === MenuType.OPTION
      ? {
          ...section,
          onClick: undefined,
          onChange: onFileChange,
          upload: true,
        }
      : section,
  ),
};

export const menuJsxItems = {
  ...menu,
  label: (
    <Flex alignItems="center" gap="4px">
      Select
      <Badge small title="BETA" />
    </Flex>
  ),
  sections: Array(3).fill({
    type: 'Menu',
    trigger: 'Text',
    menu: {
      trigger: 'Text',
      placement: 'right-start',
      label: (
        <Flex alignItems="center" gap="4px">
          Parent
          <Badge small title="BETA" />
        </Flex>
      ),
      title: 'Parent',
      sections: Array(3).fill({
        type: 'Option',
        label: (
          <Flex alignItems="center" gap="4px">
            Child
            <Badge small title="BETA" />
          </Flex>
        ),
        title: 'Child',
      }),
    },
  }),
};

export const menuDescription = {
  label: '123 m',
  trigger: 'DropDownButton',
  sections: [
    {
      type: 'Option',
      label: 'Cat',
      description: 'Felis catus',
    },
    {
      type: 'Option',
      label: 'Dog',
      description: 'Canis familiaris',
      selected: true,
    },
    {
      type: 'Option',
      label: 'Hamster',
      description: 'Cricetinae',
    },
  ],
};

export const menuDescriptionOnRight = {
  label: '123 m',
  trigger: 'DropDownButton',
  sections: [
    {
      type: 'Option',
      label: '403.5433',
      description: 'ft',
      inline: true,
      onClick: () => {},
    },
    {
      type: 'Option',
      label: '123',
      description: 'm',
      inline: true,
      onClick: () => {},
      selected: true,
    },
    {
      type: 'Option',
      label: '0.123',
      description: 'km',
      inline: true,
      onClick: () => {},
    },
  ],
};

const actions = [
  {
    label: 'Delete',
    icon: 'delete',
    onClick: () => {},
  },
  {
    label: 'Rename',
    icon: 'rename',
    onClick: () => {},
  },
];

export const menuActions = {
  ...menu,
  sections: menu.sections.map((section) =>
    section.type === 'Option'
      ? {
          ...section,
          inline: true,
          actions,
        }
      : section,
  ),
};

export const menuIcons = {
  ...menu,
  sections: menu.sections.map((section) =>
    section.type === 'Option' || section.type === 'Heading'
      ? {
          ...section,
          icon: 'star',
          onClick: () => {},
        }
      : section,
  ),
};

export const menuSubmenus = {
  ...menu,
  sections: [...Array(3)].map((_, index) => ({
    type: 'Menu',
    trigger: 'Text',
    menu: {
      trigger: 'Text',
      placement: 'right-start',
      label: 'Parent',
      sections:
        index === 2
          ? []
          : [...Array(3)].map((_, subIndex) => ({
              type: 'Option',
              label: 'Child',
              onClick: () => {},
              selected: index === 1 && subIndex === 0,
              disabled: subIndex === 2,
            })),
    },
  })),
};

export const menuLong = {
  ...menu,
  sections: Array(100).fill({
    type: 'Option',
    label: 'Item',
    onClick: () => {},
  }),
};

export const menuEmpty = {
  label: 'No sections',
  trigger: 'DropDownButton',
  sections: [],
};

export const menuShowAll = {
  label: 'Select',
  trigger: 'DropDownButton',
  showAllButton: {
    visible: true,
    showAllTitle: 'Show all...',
    showLessTitle: 'Show less...',
    additionalSections: [
      {
        type: 'Divider',
      },
      {
        type: 'Option',
        label: 'This item is always shown',
        onClick: () => {},
      },
    ],
  },
  sections: Array(8).fill({
    type: 'Option',
    label: 'Item',
    onClick: () => {},
  }),
};
