import { Flex } from 'components/layout/flex/flex';
import { Select } from 'components/select/select';
import { Text } from 'components/text/text';
import React, { ReactNode } from 'react';
import styles from './setting-field.module.less';

export interface ISettingFieldProps {
  children: ReactNode;
  helpText?: string;
  disabled?: boolean;
  noPermission?: boolean;
  selectedOption?: boolean;
  handleSelectChange?: () => void;
}

export const SettingField = ({
  helpText,
  disabled = false,
  children,
  noPermission = false,
  selectedOption,
  handleSelectChange,
}: ISettingFieldProps) => {
  const renderHelpText = (text: string) => {
    const textWithNewlines = text.replace(/\\n/g, '\n');
    return textWithNewlines.split(/\n/).map((line, index) => (
      <React.Fragment key={index}>
        {line}
        <br />
      </React.Fragment>
    ));
  };

  const options = [
    { label: 'All users', value: false },
    { label: 'Admin only', value: true },
  ];

  return (
    <div className={styles.settingField}>
      <Flex justifyContent="space-between" wrap={false}>
        {children}
        {!noPermission && (
          <div style={{ flexShrink: 0 }}>
            <Select
              small
              searchable={false}
              name="userType"
              options={options}
              value={selectedOption}
              disabled={disabled}
              onChange={handleSelectChange || undefined}
              aria-label="Select user type"
              width="auto"
            />
          </div>
        )}
      </Flex>
      {helpText && <Text muted>{renderHelpText(helpText)}</Text>}
    </div>
  );
};
