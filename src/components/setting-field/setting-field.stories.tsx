import { Meta } from '@storybook/react';
import React, { useState } from 'react';
import { Accordion } from 'components/accordion/accordion';
import { Field } from 'components/form/field';
import { CheckBox } from 'components/check-box/check-box';
import { Heading } from 'components/heading/heading';
import { Input } from 'components/input/input';
import { SettingField } from './setting-field';

export default {
  title: 'Settings/SettingField',
  component: SettingField,
  parameters: {
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
} as Meta;

export const Settings = () => {
  const [isChecked, setIsChecked] = useState(false);
  const [isLocked, setIsLocked] = useState(false);

  const handleSelectChange = () => {
    setIsLocked((prev) => !prev);
  };

  return (
    <Accordion
      heading={<Heading>Test module</Heading>}
      managed
      bordered
      padding={false}
      expanded
    >
      <SettingField
        helpText="Help text"
        selectedOption={isLocked}
        handleSelectChange={handleSelectChange}
      >
        <Field label="Import offset Wells">
          <CheckBox
            label="Test checkbox"
            checked={isChecked}
            onChange={() => setIsChecked((prev) => !prev)}
            name="test-checkbox"
          />
        </Field>
      </SettingField>
      <SettingField
        helpText="The User Settings page allows you to manage and personalize your account settings. From here, you can access various modules that contain settings relevant to your profile, group, and application features. The settings are organized into modules for easy navigation, allowing you to find and adjust preferences according to your needs."
        selectedOption={isLocked}
        handleSelectChange={handleSelectChange}
      >
        <Field label="Import offset Wells">
          <Input value="" onChange={() => {}} name="test-input" />
        </Field>
      </SettingField>
    </Accordion>
  );
};

export const LockedSetting = () => {
  return (
    <Accordion
      heading={<Heading>Test module</Heading>}
      managed
      bordered
      padding={false}
      expanded
    >
      <SettingField
        disabled
        helpText="The User Settings page allows you to manage and personalize your account settings. From here, you can access various modules that contain settings relevant to your profile, group, and application features. The settings are organized into modules for easy navigation, allowing you to find and adjust preferences according to your needs."
        selectedOption={false}
        handleSelectChange={() => {}}
      >
        <Field label="Import offset Wells">
          <Input value="" onChange={() => {}} name="test-input" />
        </Field>
      </SettingField>
    </Accordion>
  );
};

export const WithoutPermission = () => {
  return (
    <Accordion
      heading={<Heading>Test module</Heading>}
      managed
      bordered
      padding={false}
      expanded
    >
      <SettingField
        disabled
        noPermission
        helpText="The User Settings page allows you to manage and personalize your account settings. From here, you can access various modules that contain settings relevant to your profile, group, and application features. The settings are organized into modules for easy navigation, allowing you to find and adjust preferences according to your needs."
      >
        <Field label="Import offset Wells">
          <Input value="" onChange={() => {}} name="test-input" />
        </Field>
      </SettingField>
    </Accordion>
  );
};

export const HelpTextLineBreak = () => {
  return (
    <Accordion
      heading={<Heading>Test module</Heading>}
      managed
      bordered
      padding={false}
      expanded
    >
      <SettingField
        disabled
        noPermission
        helpText="The User Settings page allows you to manage and personalize your account settings. \n From here, you can access various modules that contain settings relevant to your profile, group, and application features. \n The settings are organized into modules for easy navigation, allowing you to find and adjust preferences according to your needs."
      >
        <Field label="Import offset Wells">
          <Input value="" onChange={() => {}} name="test-input" />
        </Field>
      </SettingField>
    </Accordion>
  );
};
