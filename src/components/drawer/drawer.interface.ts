import React, { ReactNode } from 'react';
import { TFunction, TStringOrNumber } from 'typings/common-type-definitions';

export interface ITabBadgeProps {
  badge?: TStringOrNumber;
  children: ReactNode;
}

export interface IDrawerTab {
  icon?: ReactNode | React.JSX.Element;
  content?: ReactNode;
  badge?: TStringOrNumber;
  disabled?: boolean;
}

export interface IDrawerTabsProps {
  tabs: IDrawerTab[];
  activeTab: number;
  open: boolean;
  background: string;
  handleTabClick: (index: number) => void;
  width: number | string;
  testId?: string;
}

export interface IDrawerResizeHandleProps {
  handleAxis?: string;
}

export interface IDrawerResizeWrapperProps {
  children?: ReactNode;
  width: number | string;
  right?: boolean;
  onResize?: TFunction;
  setOpen?: ((isOpen: boolean) => void) | null;
}
