import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { IconType } from 'typings/common-types';
import { Drawer, IDrawerProps } from './drawer';
import { Button } from '../button/button';
import { List } from '../list/list';
import { Heading } from '../heading/heading';
import { Flex } from '../layout/flex/flex';
import { listSubheadingsBadges } from '../list/list.stories-data';
import { IDrawerTab } from './drawer.interface';
import { Icon } from '../icon/icon';

export default {
  title: 'Layout/Drawer',
  component: Drawer,
  args: {
    button: true,
    open: true,
    right: false,
    border: true,
    shadow: false,
    width: '300px',
    closedWidth: 'var(--size-sidebar)',
    top: undefined,
    children: <div style={{ padding: 'var(--padding)' }}>Drawer content</div>,
  },
  argTypes: {
    button: { table: { disable: true } },
  },
  decorators: [
    (story) => (
      <Flex height="400px" wrap={false}>
        <div
          style={{
            flexGrow: 1,
            padding: 'var(--padding)',
            background: 'var(--color-background)',
          }}
        >
          Page content
        </div>
        {story()}
      </Flex>
    ),
  ],
  parameters: {
    layout: 'fullscreen',
  },
} as Meta;

const Template: StoryFn<IDrawerProps> = (args) => {
  return (
    <Drawer
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};
export const Default = Template.bind({});

export const Right = Template.bind({});
Right.args = { right: true };

export const Fixed = Template.bind({});
Fixed.args = {
  fixed: true,
};
Fixed.parameters = {
  docs: {
    story: {
      inline: false,
      iframeHeight: 400,
    },
  },
};

export const CustomButton = Template.bind({});
CustomButton.args = {
  button: <Button colored round icon={IconType.LEFT} />,
  buttonAnimate: true,
};

export const Shadow = Template.bind({});
Shadow.args = {
  shadow: true,
};

export const WithList = () => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Drawer
        button={
          <Button
            onClick={() => setOpen(!open)}
            round
            icon={IconType.CHEVRON_LEFT}
          />
        }
        width="300px"
        closedWidth="var(--size-sidebar)"
        border
        open={open}
      >
        <List drawer list={listSubheadingsBadges} narrow={!open} />
      </Drawer>
      <Drawer
        button={
          <Button
            onClick={() => setOpen(!open)}
            round
            icon={IconType.CHEVRON_LEFT}
          />
        }
        width="300px"
        closedWidth="var(--size-sidebar)"
        border
        open={open}
      >
        <List drawer list={listSubheadingsBadges} narrow={!open} />
      </Drawer>
    </>
  );
};
WithList.parameters = {
  docs: {
    description: {
      story:
        'To hide `List` headings when the `Drawer` is closed, set the `narrow` prop on the `List`.',
    },
  },
};

export const Tabs = Template.bind({});
Tabs.args = {
  right: true,
  button: false,
  closedWidth: 0,
  width: ['200px', '300px', '400px', '400px'],
  defaultTabIndex: 1,
  tabs: [
    {
      icon: <Icon icon={IconType.HELP} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Help</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
    {
      icon: <Icon icon={IconType.SETTINGS} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Settings</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
    {
      icon: <Icon icon={IconType.LIST} />,
      badge: '2',
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>List</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
    {
      icon: <Icon icon={IconType.EDIT} />,
      disabled: true,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Edit</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
  ],
};
Tabs.parameters = {
  docs: {
    description: {
      story:
        'The top tab will be open by default, but you can set `defaultTabIndex` if you need a different tab open. If you need different widths per tab, pass an array of widths to `width`.',
    },
  },
};

export const ControlledOpenClose = () => {
  const [open, setOpen] = useState(false);
  const tabs: IDrawerTab[] = [
    { icon: <Icon icon={IconType.HELP} /> },
    { icon: <Icon icon={IconType.SETTINGS} /> },
  ];
  const handleClose = (tab?: IDrawerTab) => {
    if (tab) console.log('on close', tab);
  };

  const handleOpen = (tab?: IDrawerTab) => {
    if (tab) console.log('on open', tab);
  };

  return (
    <Drawer
      border
      right
      tabs={tabs}
      open={open}
      setOpen={setOpen}
      onClose={handleClose}
      onOpen={handleOpen}
    />
  );
};
ControlledOpenClose.parameters = {
  docs: {
    description: {
      story:
        'If a `setOpen` prop is set, the drawer will not manage its open state internally and can be controlled.',
    },
  },
};

export const ControlledTabs = () => {
  const [activeTab, setActiveTab] = useState(0);
  const tabs = [
    { icon: <Icon icon={IconType.HELP} /> },
    { icon: <Icon icon={IconType.SETTINGS} /> },
  ];
  return (
    <Drawer
      border
      right
      tabs={tabs}
      activeTab={activeTab}
      setActiveTab={setActiveTab}
    />
  );
};
ControlledTabs.parameters = {
  docs: {
    description: {
      story:
        'If `activeTab` and `setActiveTab` props are set, the drawer will not manage the tab state internally and can be controlled.',
    },
  },
};

export const MultipleTabbedDrawers = () => {
  const tabs1 = [
    {
      icon: <Icon icon={IconType.HELP} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Help</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
    {
      icon: <Icon icon={IconType.SETTINGS} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Settings</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
  ];
  const tabs2 = [
    {
      icon: <Icon icon={IconType.EDIT} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Edit</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
  ];
  return (
    <>
      <Drawer right tabs={tabs1} border open />
      <Drawer right tabs={tabs2} border open />
    </>
  );
};

export const Resizable = () => {
  const [open, setOpen] = useState(true);
  const [drawerWidth, setDrawerWidth] = useState(300);

  const tabs = [
    {
      icon: <Icon icon={IconType.HELP} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Help</Heading>
          {'Example content goes here lorem ipsum. '.repeat(500)}
        </div>
      ),
    },
    {
      icon: <Icon icon={IconType.SETTINGS} />,
      content: (
        <div style={{ padding: 'var(--padding)' }}>
          <Heading top>Settings</Heading>
          {'Example content goes here lorem ipsum dolor. '.repeat(500)}
        </div>
      ),
    },
  ];
  return (
    <>
      <Drawer
        right
        tabs={tabs}
        border
        open={open}
        setOpen={setOpen}
        width={drawerWidth}
        onResize={(width) => {
          setDrawerWidth(width as number);
        }}
      />
    </>
  );
};
