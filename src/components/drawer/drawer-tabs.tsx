import React from 'react';
import cx from 'classnames';
import styles from './drawer.module.less';
import { Badge } from '../badge/badge';
import { IDrawerTabsProps, ITabBadgeProps } from './drawer.interface';

const TabBadge = ({ badge, children }: ITabBadgeProps) => {
  if (!badge) return <>{children}</>;
  return (
    <Badge small margin={4} title={badge.toString()}>
      {children}
    </Badge>
  );
};

export const DrawerTabs = ({
  tabs,
  activeTab,
  open,
  background,
  handleTabClick,
  width,
  testId,
}: IDrawerTabsProps) => {
  return (
    <>
      <div className={styles.tabs}>
        {tabs.map((tab, index) => (
          <div
            key={index}
            className={cx(
              styles.tab,
              activeTab === index && open ? styles.active : '',
              tab.disabled ? styles.disabled : '',
            )}
            style={{ background }}
            onClick={!tab.disabled ? () => handleTabClick(index) : undefined}
            data-testid={testId && `${testId}-icon-${index}`}
          >
            <TabBadge badge={tab.badge}>{tab.icon}</TabBadge>
          </div>
        ))}
      </div>
      {tabs.map((tab, index) => {
        const active = activeTab === index;

        return (
          <div
            style={{ width: typeof width === 'number' ? `${width}px` : width }}
            className={styles.tabsContent}
            key={index}
            data-testid={testId && `${testId}-content-${index}`}
          >
            {active && open && tab.content}
          </div>
        );
      })}
    </>
  );
};
