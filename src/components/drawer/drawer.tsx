import React, { isValidElement, ReactNode, useState } from 'react';
import cx from 'classnames';
import {
  ButtonPosition,
  IconType,
  TButtonPosition,
} from 'typings/common-types';
import { TFunction, TStringOrNumber } from 'typings/common-type-definitions';
import { DrawerTabs } from './drawer-tabs';
import styles from './drawer.module.less';
import { DrawerResizeWrapper } from './drawer-resize-wrapper';
import { MIN_OPEN_WIDTH, TABS_WIDTH } from './drawer.constants';
import { Button } from '../button/button';
import { IDrawerTab } from './drawer.interface';

export interface IDrawerProps {
  background?: string;
  children?: ReactNode;
  fixed?: boolean;
  open?: boolean;
  setOpen?: (openState: boolean) => void;
  right?: boolean;
  shadow?: boolean;
  top?: TStringOrNumber;
  width?: TStringOrNumber | Array<TStringOrNumber>;
  closedWidth?: TStringOrNumber;
  button?: boolean | ReactNode;
  buttonAnimate?: boolean;
  buttonPosition?: TButtonPosition | string;
  border?: boolean | string;
  tabs?: IDrawerTab[];
  defaultTabIndex?: number;
  activeTab?: number;
  setActiveTab?: (index: number) => void;
  testId?: string;
  onResize?: TFunction;
  getActiveTab?: (tab: IDrawerTab) => void;
  onClose?: (tab?: IDrawerTab) => any;
  onOpen?: (tab?: IDrawerTab) => any;
}

export const Drawer = ({
  background = 'var(--color-background-raised)',
  fixed = false,
  open: openProp = false,
  setOpen: setOpenProp,
  right = false,
  width = 400,
  closedWidth = 0,
  shadow = false,
  top = 0,
  button = null,
  buttonAnimate = true,
  buttonPosition = ButtonPosition.BOTTOM,
  border = false,
  children = null,
  tabs,
  defaultTabIndex = 0,
  activeTab: activeTabProp,
  setActiveTab: setActiveTabProp,
  testId,
  onResize,
  getActiveTab,
  onClose,
  onOpen,
}: IDrawerProps) => {
  const isStandardButton = button === true;
  const isCustomButton = !isStandardButton && isValidElement(button);

  const [localOpen, setLocalOpen] = setOpenProp
    ? [openProp, setOpenProp]
    : isStandardButton || tabs
    ? useState(openProp)
    : [openProp, () => {}];

  const [localActiveTab, setLocalActiveTab] = useState(defaultTabIndex);

  const openState = setOpenProp ? openProp : localOpen;
  const setOpenState = setOpenProp || setLocalOpen;

  const activeTabState =
    setActiveTabProp !== undefined ? activeTabProp : localActiveTab;

  const setActiveTabState = setActiveTabProp || setLocalActiveTab;

  const openWidth = Array.isArray(width) ? width[activeTabState || 0] : width;
  const currentWidth = openState ? openWidth : tabs ? TABS_WIDTH : closedWidth;

  const handleTabClick = (index: number) => {
    const tabAction = (callback?: (tab: IDrawerTab) => void) => {
      if (tabs && callback) {
        callback(tabs[index]);
      }
    };

    if (activeTabState === index && openState && setOpenState) {
      tabAction(onClose);
      setOpenState(false);
    } else if (!openState && setOpenState) {
      tabAction(onOpen);
      setOpenState(true);

      if (onResize) {
        // TODO: Revisit this function for optimization or refactoring in the future
        const checkAndResize = (w: number | string | (number | string)[]) => {
          let targetWidth: number | string;

          // Check if 'w' is an array
          if (Array.isArray(w)) {
            // Use the width based on the active tab index or default to the first width value
            targetWidth = w[activeTabState || 0];
          } else {
            targetWidth = w;
          }

          if (typeof targetWidth === 'number' && targetWidth < MIN_OPEN_WIDTH) {
            onResize(MIN_OPEN_WIDTH);
          } else if (typeof targetWidth === 'string') {
            const numWidth = parseInt(targetWidth, 10);
            if (!isNaN(numWidth) && numWidth < MIN_OPEN_WIDTH) {
              onResize(MIN_OPEN_WIDTH);
            }
          }
        };

        checkAndResize(width);
      }
    }

    setActiveTabState(index);
    tabAction(getActiveTab);
  };

  return (
    <DrawerResizeWrapper
      width={currentWidth}
      onResize={onResize}
      setOpen={setOpenState}
      right={right}
    >
      <div
        className={cx(
          styles.drawer,
          shadow ? styles.shadow : '',
          fixed ? styles.fixed : styles.inline,
          right ? styles.right : styles.left,
        )}
        style={{ top }}
      >
        <div
          className={cx(styles.drawerContent, border && styles.border)}
          style={{
            background,
            borderColor: typeof border === 'string' ? border : undefined,
            width: currentWidth,
          }}
        >
          {tabs ? (
            <DrawerTabs
              {...{
                width: openWidth,
                testId,
                tabs,
                open: openState,
                activeTab: activeTabState || 0,
                background,
                handleTabClick,
              }}
            />
          ) : (
            <div style={{ width: openWidth }}>{children}</div>
          )}
        </div>

        {button && (
          <span
            className={cx(
              styles.toggleButton,
              localOpen &&
                (isStandardButton || (isCustomButton && buttonAnimate))
                ? styles.toggleButtonOpen
                : '',
              buttonPosition === 'top' ? styles.top : styles.bottom,
            )}
          >
            {isCustomButton ? (
              button
            ) : (
              <Button
                onClick={
                  setOpenState ? () => setOpenState(!openState) : undefined
                }
                round
                icon={right ? IconType.CHEVRON_RIGHT : IconType.CHEVRON_LEFT}
              />
            )}
          </span>
        )}
      </div>
    </DrawerResizeWrapper>
  );
};
