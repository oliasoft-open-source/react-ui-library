import React, { forwardRef, Ref, useState } from 'react';
import { toNumber } from 'lodash';
import { Resizable as ResizableBase, ResizableProps } from 'react-resizable';
import { IconType } from 'typings/common-types';
import styles from './drawer.module.less';
import { TABS_WIDTH } from './drawer.constants';
import {
  IDrawerResizeWrapperProps,
  IDrawerResizeHandleProps,
} from './drawer.interface';
import { Icon } from '../icon/icon';

const ResizeHandle = forwardRef(
  (props: IDrawerResizeHandleProps, ref: Ref<HTMLDivElement>) => {
    const { handleAxis, ...rest } = props;
    return (
      <div ref={ref} className={styles.resizeHandle} {...rest}>
        <Icon icon={IconType.DRAG} />
      </div>
    );
  },
);

const Resizable = ResizableBase as unknown as React.FC<ResizableProps>;

export const DrawerResizeWrapper = ({
  children = null,
  width = 400,
  right = false,
  onResize,
  setOpen,
}: IDrawerResizeWrapperProps) => {
  const [isResizing, setIsResizing] = useState(false);
  const MAXIMUM_OPEN_WIDTH = window.innerWidth / 2;

  const handleResize = (_: any, { size }: any) => {
    if (onResize) onResize(size.width);
  };

  return onResize ? (
    <Resizable
      width={toNumber(width)}
      height={100}
      minConstraints={[TABS_WIDTH, Infinity]}
      maxConstraints={[MAXIMUM_OPEN_WIDTH, Infinity]}
      onResize={handleResize}
      handle={<ResizeHandle />}
      resizeHandles={right ? ['w'] : ['e']}
      axis="x"
      className={isResizing ? styles.isResizing : ''}
      onResizeStart={() => {
        if (setOpen) setOpen(true);
        if (onResize) onResize(width);
        setIsResizing(true);
      }}
      onResizeStop={() => {
        setIsResizing(false);
        if (typeof width === 'number' && width < TABS_WIDTH + 50) {
          if (setOpen) setOpen(false);
        }
      }}
    >
      {children as any}
    </Resizable>
  ) : (
    <>{children}</>
  );
};
