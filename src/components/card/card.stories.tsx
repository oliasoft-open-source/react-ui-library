import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Card, ICardProps } from './card';
import { Heading } from '../heading/heading';

export default {
  title: 'Layout/Card',
  component: Card,
  args: {
    raised: false,
    children: 'Content goes here',
    padding: true,
    bordered: true,
  },
} as Meta;

const Template: StoryFn<ICardProps> = (args) => <Card {...args} />;

export const Default = Template.bind({});

export const WithHeading = Template.bind({});
WithHeading.args = {
  heading: <Heading>Heading</Heading>,
};

export const WithHelpHeading = Template.bind({});
WithHelpHeading.args = {
  heading: <Heading onClickHelp={() => console.log('help!')}>Heading</Heading>,
};

export const NoPadding = Template.bind({});
NoPadding.args = { padding: false };

export const NoBorder = Template.bind({});
NoBorder.args = { bordered: false };
