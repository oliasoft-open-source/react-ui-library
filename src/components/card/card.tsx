import React, { ReactNode } from 'react';
import cx from 'classnames';
import styles from './card.module.less';

export interface ICardProps {
  bordered?: boolean;
  heading?: ReactNode;
  margin?: string;
  padding?: boolean;
  raised?: boolean;
  children?: ReactNode;
}

export const Card = ({
  bordered = true,
  heading = null,
  children,
  margin = '0',
  padding = true,
  raised = false,
}: ICardProps) => {
  return (
    <div
      className={cx(
        styles.card,
        bordered ? styles.bordered : '',
        raised ? styles.raised : '',
      )}
      style={{ margin }}
    >
      {heading ? <div className={cx(styles.cardHeader)}>{heading}</div> : null}
      <div
        className={cx(
          styles.cardContent,
          heading ? styles.cardContentBorderTop : '',
          padding ? styles.padding : '',
        )}
      >
        {children}
      </div>
    </div>
  );
};
