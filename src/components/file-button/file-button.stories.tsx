import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Actions } from 'components/actions/actions';
import { FileButton, IFileButtonProps } from './file-button';

export default {
  title: 'Forms/FileButton',
  component: FileButton,
} as Meta;

const Template: StoryFn<IFileButtonProps> = (args) => <FileButton {...args} />;

export const Text = Template.bind({});
Text.args = { label: 'Import' };

export const Icon = Template.bind({});
Icon.args = {
  icon: 'upload',
  round: true,
  colored: 'muted',
  basic: true,
  small: true,
};

export const InActions = () => (
  <Actions
    actions={[
      {
        childComponent: (
          <FileButton icon="upload" round small basic colored="muted" />
        ),
      },
      {
        icon: 'download',
        onClick: () => {},
      },
      {
        icon: 'add',
        primary: true,
        onClick: () => {},
      },
    ]}
  />
);
