import React, { useContext, useRef } from 'react';
import { Button, IButtonProps } from 'components/button/button';
import { DisabledContext } from 'helpers/disabled-context';
import {
  TChangeEvent,
  TChangeEventHandler,
} from 'typings/common-type-definitions';
import { noop } from 'lodash';

export interface IFileButtonProps extends IButtonProps {
  file?: File;
  accept?: string;
  multi?: boolean;
  name?: string;
  onChange?: TChangeEventHandler;
}

export const FileButton = ({
  file,
  accept,
  multi,
  name,
  disabled,
  onChange = noop,
  ...buttonProps
}: IFileButtonProps) => {
  const disabledContext = useContext(DisabledContext);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const handleClick = (): void => {
    if (!(disabled || disabledContext)) inputRef.current?.click();
  };

  const handleChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const files = multi ? evt.target.files : evt.target.files?.[0];

    const customEvent = {
      ...evt,
      target: {
        ...evt.target,
        value: files,
        name: evt.target.name,
      },
    };

    onChange(customEvent as TChangeEvent);
    // Reset the input value to allow selecting the same file again
    if (inputRef.current) {
      inputRef.current.value = '';
    }
  };

  return (
    <>
      <Button {...buttonProps} onClick={handleClick} />
      <input
        name={name}
        ref={inputRef}
        style={{ display: 'none' }}
        type="file"
        accept={accept}
        multiple={multi}
        onChange={handleChange}
      />
    </>
  );
};
