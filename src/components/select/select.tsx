import React, { ReactNode } from 'react';
import {
  TChangeEvent,
  TChangeEventHandler,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { noop } from 'lodash';
import { NativeSelect } from './native-select/native-select';
import { CustomSelect } from './custom-select/custom-select';
import { standardizeInputs } from './select.input';
import { ISelectSelectedOption } from './select.interface';

export interface ISelectProps {
  name?: string;
  multi?: boolean;
  options?: ISelectSelectedOption[];
  value?: any;
  native?: boolean;
  onChange?: TChangeEventHandler;
  deprecatedEventHandler?: boolean;
  autoScroll?: boolean;
  warning?: TStringOrNumber | ReactNode;
  error?: TStringOrNumber | ReactNode;
  checkNonExistentValues?: boolean;
  closeOnOptionActionClick?: boolean;
  small?: boolean;
  width?: TStringOrNumber | null;
  borderRadius?: TStringOrNumber;
  onCreate?: (evt: any) => any;
  disabled?: boolean;
  placeholder?: string | null;
  isInTable?: boolean;
  clearable?: boolean;
  searchable?: boolean;
  maxTooltipWidth?: TStringOrNumber;
  autoLayerWidth?: boolean;
  right?: boolean;
  tabIndex?: number;
  testId?: string;
}

export const Select = (props: ISelectProps) => {
  const {
    name = '',
    options: rawOptions = [],
    value: rawSelectedOptions,
    native = false,
    onChange = noop,
    deprecatedEventHandler = false,
    autoScroll = true,
    warning,
    error,
    checkNonExistentValues = true,
    closeOnOptionActionClick = true,
    testId,
    disabled,
  } = props;
  const enableAutoScroll = !native && autoScroll === true;
  const {
    multi,
    simpleInputs,
    options,
    selectedOptions,
    firstSelectedOptionIndex,
    hasNonExistentValue,
  } = standardizeInputs(
    rawOptions,
    rawSelectedOptions,
    enableAutoScroll,
    checkNonExistentValues,
  );
  const canUseNative = !multi; //ignore searchable since it's on by default
  const useNative = native && canUseNative;
  if (native && !canUseNative) {
    console.warn('Native select does not support provided props (see stories)');
  }

  const nonExistentWarning = 'Value no longer available for re-selection';
  const fullWarning =
    hasNonExistentValue && !disabled ? (
      warning ? (
        <div>
          <div>{nonExistentWarning}</div>
          <div>{warning}</div>
        </div>
      ) : (
        nonExistentWarning
      )
    ) : (
      warning
    );

  const fullError =
    hasNonExistentValue && error && !disabled ? (
      <div>
        <div>{nonExistentWarning}</div>
        <div>{error}</div>
      </div>
    ) : (
      error
    );

  const onChangeSelectedValue = (
    evt: TChangeEvent,
    selectedOptions: ISelectSelectedOption | ISelectSelectedOption[],
  ) => {
    if (deprecatedEventHandler) {
      const newSelectedOptions: any = multi
        ? (selectedOptions as ISelectSelectedOption[]).map((o) =>
            simpleInputs ? o.value : o,
          )
        : simpleInputs
        ? (selectedOptions as ISelectSelectedOption).value
        : selectedOptions;
      if (newSelectedOptions) {
        onChange(newSelectedOptions);
      }
    } else {
      /*
       - For React synthetic events, mutate the values and forward the event
       - For native (keyboard) events, clone the enumerable properties and forward
       - Explanation:
         https://gitlab.com/oliasoft-open-source/react-ui-library/-/issues/77
     */
      const isSyntheticEvent = !(evt instanceof Event);
      const value = multi
        ? (selectedOptions as ISelectSelectedOption[])
        : (selectedOptions as ISelectSelectedOption).value;
      if (isSyntheticEvent) {
        evt.target.name = name;
        evt.target.value = value;
        if (!multi) {
          (evt.target as any).label = (
            selectedOptions as ISelectSelectedOption
          ).label;
        }
        onChange(evt);
      } else {
        const customEvent = {
          ...evt,
          target: {
            ...evt.target,
            name,
            value,
            label: !multi
              ? (selectedOptions as ISelectSelectedOption).label
              : undefined,
          },
        };
        onChange(customEvent as unknown as TChangeEvent);
      }
    }
  };

  return useNative ? (
    <NativeSelect
      {...props}
      options={options}
      selectedOption={selectedOptions as ISelectSelectedOption}
      onChange={onChangeSelectedValue as any}
      hasNonExistentValue={hasNonExistentValue}
      warning={fullWarning}
      error={fullError}
      testId={testId}
    />
  ) : (
    <CustomSelect
      {...props} // eslint-disable-line react/jsx-props-no-spreading
      options={options}
      selectedOptions={selectedOptions as ISelectSelectedOption}
      onChange={onChangeSelectedValue}
      multi={multi}
      firstSelectedOptionIndex={firstSelectedOptionIndex}
      warning={fullWarning}
      error={fullError}
      testId={testId}
      closeOnOptionActionClick={closeOnOptionActionClick}
    />
  );
};
