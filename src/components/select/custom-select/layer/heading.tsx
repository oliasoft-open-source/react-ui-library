import React from 'react';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from '../custom-select.module.less';

export const Heading = ({ label }: { label: TStringOrNumber }) => {
  return (
    <div
      onClick={(evt) => evt.stopPropagation()} //don't close menu
      className={styles.heading}
    >
      {label}
    </div>
  );
};
