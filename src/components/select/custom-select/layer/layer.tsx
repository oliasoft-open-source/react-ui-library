import React, { RefObject, useEffect } from 'react';
import cx from 'classnames';
import { FixedSizeList as ListBase, FixedSizeListProps } from 'react-window';
import { TEmpty } from 'typings/common-type-definitions';
import { MenuType } from 'components/menu/types';
import styles from '../custom-select.module.less';
import { ISelectSection, Section } from './section';
import { ISelectSelectedOption } from '../../select.interface';
import { MAX_OPTIONS_LENGTH } from '../helper';

export interface ISelectLayerProps {
  listRef: RefObject<ListBase>;
  isMulti?: boolean;
  sections: ISelectSection[];
  selectedOptions: ISelectSelectedOption | ISelectSelectedOption[];
  onSelectOption: (
    event?: React.MouseEvent<HTMLDivElement, MouseEvent>,
    section?: ISelectSection,
  ) => void;
  focusedOptionIndex?: number;
  width: number;
  small?: boolean;
  firstSelectedOptionIndex?: number;
  closeLayer: TEmpty;
  testId?: string;
}

const List = ListBase as unknown as React.FC<FixedSizeListProps<any>> as any;

export const Layer = ({
  listRef,
  isMulti,
  sections,
  selectedOptions,
  onSelectOption,
  focusedOptionIndex,
  width,
  small,
  firstSelectedOptionIndex,
  closeLayer,
  testId,
}: ISelectLayerProps) => {
  useEffect(() => {
    if (
      firstSelectedOptionIndex !== undefined &&
      firstSelectedOptionIndex !== null &&
      listRef.current !== null &&
      typeof listRef.current.scrollToItem === 'function'
    ) {
      listRef.current.scrollToItem(firstSelectedOptionIndex, 'start');
    }
  }, [firstSelectedOptionIndex, listRef]);

  const optionHeight = small ? 24 : 30;

  return (
    <div
      data-testid={testId}
      className={cx(styles.layer, small && styles.small)}
    >
      {!sections.length ? (
        <ul>
          <li>
            <div className={styles.message} style={{ width }}>
              No matches
            </div>
          </li>
        </ul>
      ) : (
        <List
          ref={listRef}
          height={
            sections.length < MAX_OPTIONS_LENGTH
              ? optionHeight * sections.length
              : optionHeight * MAX_OPTIONS_LENGTH
          }
          itemCount={sections.length}
          itemSize={optionHeight}
          width={width}
        >
          {({ index, style }: { index: number; style: any }) => (
            <div style={style}>
              <Section
                closeLayer={closeLayer}
                section={sections[index]}
                selected={
                  isMulti &&
                  sections[index].type !== MenuType.HEADING &&
                  Array.isArray(selectedOptions)
                    ? selectedOptions?.some(
                        (v) => v && v?.value === sections?.[index]?.value,
                      )
                    : !Array.isArray(selectedOptions) &&
                      sections?.[index]?.value === selectedOptions?.value
                }
                focused={focusedOptionIndex === index}
                onSelectOption={(evt) => onSelectOption(evt, sections[index])}
              />
            </div>
          )}
        </List>
      )}
    </div>
  );
};
