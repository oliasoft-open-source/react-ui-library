import React, { useContext } from 'react';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import { MenuType } from 'components/menu/types';
import { Heading } from './heading';
import { ISelectOptionProps, Option } from './option';

export interface ISelectSection extends ISelectOptionProps {
  type: MenuType.HEADING | MenuType.OPTION;
  value?: TStringOrNumber;
}

export type ISelectSectionProps = {
  section: ISelectSection;
  selected?: boolean;
  focused?: boolean;
  onSelectOption?: (
    event?: React.MouseEvent<HTMLDivElement, MouseEvent>,
    section?: ISelectSectionProps,
  ) => void;
  closeLayer?: TEmpty;
};

export const Section = ({
  section,
  selected,
  focused,
  onSelectOption,
  closeLayer,
}: ISelectSectionProps) => {
  const disabledContext = useContext(DisabledContext);

  switch (section.type) {
    case MenuType.HEADING:
      return <Heading label={section.label} />;
    case MenuType.OPTION:
    default:
      return (
        <Option
          closeLayer={closeLayer}
          actions={section.actions}
          label={section.label}
          details={section.details}
          icon={section.icon}
          disabled={section.disabled || disabledContext}
          selected={selected}
          focused={focused}
          onSelectOption={onSelectOption}
          testId={section.testId}
        />
      );
  }
};
