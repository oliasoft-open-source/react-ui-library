type PlacementType = 'bottom-start' | 'top-start';

export interface PlacementOptions {
  auto: boolean;
  possiblePlacements: PlacementType[];
  placement: PlacementType;
  preferX: 'right';
  preferY: 'bottom';
}

export const placementOptions: PlacementOptions = {
  auto: true,
  possiblePlacements: ['bottom-start', 'top-start'],
  placement: 'bottom-start',
  preferX: 'right',
  preferY: 'bottom',
};
