import React, { ReactNode } from 'react';
import cx from 'classnames';
import { TEmpty } from 'typings/common-type-definitions';
import { Text } from 'components/text/text';
import { Actions } from 'components/actions/actions';
import { Icon } from 'components/icon/icon';
import { IconType } from 'typings/common-types';
import styles from '../custom-select.module.less';
import { ISelectSectionProps } from './section';

export interface ISelectOptionProps {
  details?: string;
  label: string;
  icon?: ReactNode;
  disabled?: boolean;
  selected?: boolean;
  focused?: boolean;
  onSelectOption?: (
    event?: React.MouseEvent<HTMLDivElement, MouseEvent>,
    section?: ISelectSectionProps,
  ) => void;
  actions?: any[];
  closeLayer?: TEmpty;
  testId?: string;
}

export const Option = ({
  details,
  label,
  icon,
  disabled = false,
  selected = false,
  focused = false,
  onSelectOption,
  actions,
  closeLayer,
  testId,
}: ISelectOptionProps) => {
  return (
    <div
      className={cx(
        styles.option,
        disabled ? styles.disabled : '',
        selected ? styles?.selected : '',
        focused ? styles.focused : '',
      )}
      onClick={!disabled && onSelectOption ? onSelectOption : undefined}
      data-testid={testId}
    >
      <div className={styles.optionContent}>
        <span className={styles.label} title={label}>
          {label}
          {icon && <span className={styles.icon}>{icon}</span>}
        </span>
      </div>
      {details && (
        <div className={cx(styles.details)}>
          <Text faint>{details}</Text>
        </div>
      )}
      {!!actions?.length && (
        <div className={cx(styles.actions)}>
          <Actions actions={actions} closeLayer={closeLayer} />
        </div>
      )}

      <span className={styles.check}>
        {selected ? <Icon icon={IconType.CHECK} /> : null}
      </span>
    </div>
  );
};
