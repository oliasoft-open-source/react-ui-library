import { getTextWidth } from 'helpers/text';
import { ISelectSelectedOption } from '../select.interface';

// TODO: Draw these values from global design tokens shared with stylesheets (e.g. https://amzn.github.io/style-dictionary/)
const FONT_SIZE: string = '13px';
const FONT_SIZE_SMALL: string = '12px';
const FONT_FAMILY: string = 'Roobert';
const FONT_FAMILY_FALLBACK: string = 'sans-serif';
// Input padding on left
const PADDING: number = 12;
const PADDING_SMALL: number = 8;
// Width of toggle icon on right
const TOGGLE_SIZE: number = 30;
const TOGGLE_SIZE_SMALL: number = 24;
// Selected option tags
const TAG_PADDING: number = 5;
const TAG_PADDING_SMALL: number = 3;
const TAG_BORDER_WIDTH: number = 1;
const TAG_MARGIN: number = 2;
// Number of options before scrollbar appears in layer
export const MAX_OPTIONS_LENGTH = 10;

const getFont = (
  small: boolean = false,
  isFontLoaded: boolean = false,
): string => {
  const fontSize: string = small ? FONT_SIZE_SMALL : FONT_SIZE;
  const fontFamily: string = isFontLoaded ? FONT_FAMILY : FONT_FAMILY_FALLBACK;
  return `${fontSize} ${fontFamily}`;
};

const getExtraOptionWidth = (
  small: boolean = false,
  scrollbar: boolean = false,
): number => {
  // Scrollbar upper limit suggested on https://adminhacks.com/browser-scrollbar-widths.html
  const SCROLLBAR_WIDTH: number = 17;
  return (
    (small ? PADDING_SMALL : PADDING) +
    (small ? TOGGLE_SIZE_SMALL : TOGGLE_SIZE) +
    (scrollbar ? SCROLLBAR_WIDTH : 0)
  );
};

export const getWidestOptionWidth = (
  options: ISelectSelectedOption[],
  small: boolean,
  isFontLoaded: boolean,
  placeholder?: string,
): number => {
  const font: string = getFont(small, isFontLoaded);
  const placeholderWidth: number = placeholder
    ? getTextWidth(placeholder, font)
    : 0;
  const widestTextWidth: number = Math.ceil(
    options.reduce((acc: number, option: ISelectSelectedOption) => {
      const optionActionsWidth: number = (option?.actions?.length ?? 0) * 24; // actions width
      const optionIconWidth: number = option.icon
        ? (small ? parseInt(FONT_SIZE_SMALL) : parseInt(FONT_SIZE)) + 7
        : 0;
      const optionsText: string = `${option.label} ${option.details || ''}`;
      const optionWidth: number =
        getTextWidth(optionsText, font) + optionActionsWidth + optionIconWidth;
      acc = optionWidth > acc ? optionWidth : acc;
      return acc;
    }, placeholderWidth),
  );
  const extraWidth: number = getExtraOptionWidth(small, true);
  return widestTextWidth + extraWidth;
};

export const getMultiOptionWidth = (
  option: ISelectSelectedOption,
  small?: boolean,
  isFontLoaded?: boolean,
): number => {
  const font: string = getFont(true, isFontLoaded);
  const textWidth: number = getTextWidth(option.label as string, font);
  const extraWidth: number =
    (small ? TAG_PADDING_SMALL : TAG_PADDING) * 2 +
    TAG_BORDER_WIDTH * 2 +
    TAG_MARGIN;
  return Math.ceil(textWidth + extraWidth);
};
