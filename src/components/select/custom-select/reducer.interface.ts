import { TStringOrNumber } from 'typings/common-type-definitions';
import { ICustomSelectOptionType } from './custom-select.interface';
import { ReducerAction } from './enum';

export type ICustomSelectDirection = 'right' | 'left' | 'up' | 'down';

export interface ICustomSelectTriggerFocus {
  options: Array<'none' | 'clearAll' | number>;
  currentIndex: number;
  currentOption: boolean | 'none' | 'clearAll' | number;
  clearAll: boolean;
}

export interface ICustomSelectLayerFocus {
  options: (number | 'none')[];
  currentIndex: number;
  current: number | 'none';
}

export interface ICustomSelectState {
  visibleOptions?: ICustomSelectOptionType[];
  searchValue?: string;
  layerFocus: ICustomSelectLayerFocus;
  triggerFocus: ICustomSelectTriggerFocus;
  isLayerOpen?: boolean;
}

export interface ICustomSelectAction {
  type: ReducerAction;
  options?: ICustomSelectOptionType[];
  firstSelectedOptionIndex: number;
  value?: string;
  createAble?: boolean;
  direction?: ICustomSelectDirection;
  selectedOptions?: Array<
    | string
    | number
    | { label: TStringOrNumber; value: TStringOrNumber | boolean }
  >;
  nextLayerFocus?: ICustomSelectLayerFocus;
}
