import { ChangeEvent, ReactNode } from 'react';
import {
  TChangeEventHandler,
  TEmpty,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { TGroupOrder } from 'typings/common-types';
import { ISelectSelectedOption } from '../select.interface';

interface Heading {
  label: TStringOrNumber;
  type: string;
}

export type TCustomSelectSelectOption = ISelectSelectedOption | Heading;

export type TSelectedOptionsIndexType = keyof ISelectSelectedOption;

export type ICustomSelectOptionType = {
  createAble?: boolean;
  label?: string | null;
  value?: TStringOrNumber;
  disabled?: boolean;
  details?: string;
};

export type ICustomSelectScrollableComponent = {
  scrollToItem: (index: number) => void;
};

export interface ICustomSelectProps {
  multi?: boolean;
  disabled?: boolean;
  error?: TStringOrNumber | ReactNode;
  warning?: TStringOrNumber | ReactNode;
  tooltip?: TStringOrNumber | ReactNode;
  options: TCustomSelectSelectOption[];
  selectedOptions: ISelectSelectedOption | ISelectSelectedOption[];
  onChange?: TChangeEventHandler;
  onCreate?: (value: string) => void;
  placeholder?: string | null;
  small?: boolean;
  tabIndex?: number;
  width?: TStringOrNumber | null;
  autoLayerWidth?: boolean;
  onFocus?: TEmpty;
  onBlur?: TEmpty;
  searchable?: boolean;
  clearable?: boolean;
  isInTable?: boolean;
  groupOrder?: TGroupOrder;
  firstSelectedOptionIndex: number | null;
  right?: boolean;
  maxTooltipWidth?: TStringOrNumber;
  closeOnOptionActionClick?: boolean;
  testId?: string | undefined;
}
