import React, { MutableRefObject, ReactNode, useRef } from 'react';
import { noop } from 'lodash';
import styles from './trigger.module.less';
import { MultiSelectedOptions } from './multi-selected-options';
import { ISelectSelectedOption } from '../../select.interface';

interface ISelectInputProps {
  selectedOptions?: ISelectSelectedOption | ISelectSelectedOption[];
  searchable?: boolean;
  searchValue?: string;
  inputRef: MutableRefObject<HTMLInputElement | null>;
  onChange?: (value: string) => void;
  error?: string | ReactNode;
  placeholder?: string | null;
  small?: boolean;
  testId?: string;
}

export const Input = ({
  selectedOptions,
  searchable = false,
  searchValue,
  onChange = noop,
  inputRef,
  error,
  placeholder = 'Select...',
  small,
  testId,
}: ISelectInputProps) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  const isMulti = Array.isArray(selectedOptions);
  const hasSelectedValues = isMulti
    ? selectedOptions.length > 0
    : !!selectedOptions;
  const inputSize = searchValue ? searchValue.length + 1 : 1;

  const getMultiOptionsWidth = () => {
    const containerWidth = containerRef.current?.offsetWidth || 0;
    const inputWidth = inputRef.current?.offsetWidth || 0;
    return containerWidth - inputWidth;
  };

  return (
    <div className={styles.triggerInputContainer} ref={containerRef}>
      {!hasSelectedValues ? (
        <span className={styles.placeHolder}>
          {!searchValue?.length ? placeholder : ''}
        </span>
      ) : isMulti ? (
        <MultiSelectedOptions
          selectedOptions={selectedOptions as ISelectSelectedOption[]}
          small={small}
          width={getMultiOptionsWidth()}
          testId={testId}
        />
      ) : (
        <span
          className={styles.selectedSingleValue}
          data-testid={testId && `${testId}-value`}
        >
          {!searchValue?.length
            ? (selectedOptions as ISelectSelectedOption)?.label
            : ''}
        </span>
      )}
      {searchable ? (
        <input
          tabIndex={-1}
          ref={inputRef}
          size={inputSize}
          className={styles.input}
          value={searchValue}
          onChange={(evt) => onChange(evt.target.value)}
          data-error={error || null}
          data-testid={testId && `${testId}-input`}
        />
      ) : null}
    </div>
  );
};
