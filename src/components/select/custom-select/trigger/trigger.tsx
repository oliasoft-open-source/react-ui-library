import React, { ReactNode, RefObject, useEffect } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { GroupOrder, IconType, TGroupOrder } from 'typings/common-types';
import { useFocus } from 'hooks/use-focus';
import { Icon } from 'components/icon/icon';
import { Text } from 'components/text/text';
import { isStringNumberOrNode } from 'helpers/types';
import { Tooltip } from 'components/tooltip/tooltip';
import styles from './trigger.module.less';
import { Input } from './input';
import { ISelectSelectedOption } from '../../select.interface';

interface ISelectTriggerProps {
  selectedOptions?: ISelectSelectedOption | ISelectSelectedOption[];
  searchValue?: string;
  searchable?: boolean;
  clearable?: boolean;
  onClickTrigger: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  element?: ReactNode;
  error?: string | ReactNode;
  warning?: string | ReactNode;
  tooltip?: string | ReactNode;
  small?: boolean;
  isInTable?: boolean;
  disabled?: boolean;
  onChangeSearch?: (value: string) => void;
  onClickDeselect?: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  onClickClear?: (evt: React.MouseEvent<HTMLSpanElement, MouseEvent>) => void;
  tabIndex?: number;
  isOpen?: boolean;
  triggerRef?: RefObject<HTMLDivElement>;
  clearAllIsFocused?: boolean;
  focusedSelectedOptionIndex?: number;
  onFocus?: (evt: React.FocusEvent<HTMLDivElement>) => void;
  onBlur?: (evt: React.FocusEvent<HTMLDivElement>) => void;
  groupOrder?: TGroupOrder;
  maxTooltipWidth?: TStringOrNumber;
  placeholder?: string | null;
  right?: boolean;
  testId?: string;
}

export const Trigger = ({
  selectedOptions,
  searchValue,
  searchable,
  clearable,
  onClickTrigger,
  element,
  error,
  warning,
  tooltip,
  small,
  isInTable,
  disabled,
  onChangeSearch,
  onClickDeselect,
  onClickClear,
  tabIndex,
  isOpen,
  triggerRef,
  clearAllIsFocused,
  focusedSelectedOptionIndex,
  onFocus,
  onBlur,
  groupOrder,
  maxTooltipWidth,
  placeholder,
  right,
  testId,
}: ISelectTriggerProps) => {
  const [inputRef, setInputFocus] = useFocus();

  const hasSelection = Array.isArray(selectedOptions)
    ? selectedOptions.length > 0 && selectedOptions[0]?.value
    : selectedOptions?.value;
  const canClear = clearable && hasSelection;

  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case GroupOrder.FIRST:
          return styles.groupOrderFirst;
        case GroupOrder.LAST:
          return styles.groupOrderLast;
        default:
          return styles.groupOrderMiddle;
      }
    }
    return '';
  })();

  const selectedOptionsWithDetails =
    !Array.isArray(selectedOptions) && selectedOptions?.details
      ? {
          ...selectedOptions,
          label: (
            <div className={cx(styles.detailedLabel)}>
              <Text>{selectedOptions.label}</Text>
              <Text muted>{selectedOptions.details}</Text>
            </div>
          ),
        }
      : selectedOptions;

  const triggerInput = (
    <>
      {element}
      <div
        ref={triggerRef}
        tabIndex={tabIndex}
        className={cx(
          styles.trigger,
          isOpen ? styles.isOpen : '',
          disabled ? styles.disabled : '',
          small ? styles.small : '',
          isInTable ? styles.isInTable : '',
          right ? styles.right : '',
          order,
        )}
        onClick={(evt) => {
          evt.stopPropagation();
          requestAnimationFrame(() => {
            onClickTrigger(evt);
            if (typeof setInputFocus === 'function') {
              setInputFocus();
            }
          });
        }}
        onFocus={onFocus}
        onBlur={onBlur}
        data-error={error || null}
        data-warning={warning || null}
        data-testid={testId}
      >
        <Input
          selectedOptions={selectedOptionsWithDetails}
          placeholder={placeholder}
          searchable={searchable}
          searchValue={searchValue}
          inputRef={inputRef}
          onChange={onChangeSearch}
          error={error}
          small={small}
          testId={testId}
        />
        <span className={styles.icons}>
          {canClear ? (
            <span
              className={cx(
                styles.clearAll,
                clearAllIsFocused ? styles.focus : '',
              )}
              onClick={(evt) => {
                evt.stopPropagation();
                if (onClickClear) {
                  onClickClear(evt);
                }
              }}
              data-testid={testId && `${testId}-clear`}
            >
              <Icon icon={IconType.CLOSE} />
            </span>
          ) : (
            <span className={cx(styles.iconOpen)}>
              <Icon icon="chevron down" />
            </span>
          )}
        </span>
      </div>
    </>
  );

  return (
    <>
      <Tooltip
        error={!!error}
        warning={!!warning}
        text={tooltip || error || warning}
        enabled={
          (tooltip && isStringNumberOrNode(tooltip)) ||
          (error && isStringNumberOrNode(error)) ||
          (warning && isStringNumberOrNode(warning)) ||
          false
        }
        maxWidth={maxTooltipWidth}
      >
        {triggerInput}
      </Tooltip>
    </>
  );
};
