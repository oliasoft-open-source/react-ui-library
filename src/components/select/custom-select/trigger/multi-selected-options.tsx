import React, { useEffect, useState } from 'react';
import { Tooltip } from 'components/tooltip/tooltip';
import { useFontsReady } from 'hooks';
import styles from './trigger.module.less';
import { ISelectSelectedOption } from '../../select.interface';
import { getMultiOptionWidth } from '../helper';

interface ISelectMultiSelectedOptionsProps {
  selectedOptions: ISelectSelectedOption[];
  small?: boolean;
  width: number;
  testId?: string;
}

export const MultiSelectedOptions = ({
  selectedOptions,
  small,
  width,
  testId,
}: ISelectMultiSelectedOptionsProps) => {
  const isFontLoaded = useFontsReady();
  const [displayedOptions, setDisplayedOptions] = useState<
    ISelectSelectedOption[]
  >([]);
  const [overflowOptions, setOverflowOptions] = useState<
    ISelectSelectedOption[]
  >([]);

  useEffect(() => {
    let displayed: ISelectSelectedOption[] = [];
    let overflow: ISelectSelectedOption[] = [];
    let usedWidth = 0;
    // Calculate how many of the selected options we can display
    selectedOptions.forEach((option, index) => {
      usedWidth += getMultiOptionWidth(option, small, isFontLoaded);
      if (index === selectedOptions.length - 1 && usedWidth < width) {
        // This is the last option and there's enough space
        displayed.push(option);
      } else if (usedWidth < width - 24) {
        // This is not the last option and there's enough space (including the +X marker)
        displayed.push(option);
      } else {
        // There isn't enough space
        overflow.push(option);
      }
    });

    setDisplayedOptions(displayed);
    setOverflowOptions(overflow);
  }, [selectedOptions, isFontLoaded, width]);

  return (
    <div
      className={styles.multiOptions}
      data-testid={testId && `${testId}-value`}
    >
      {displayedOptions
        .filter((option) => option && option.label)
        .map((option, index) => (
          <span
            className={styles.multiOption}
            key={index}
            onClick={(evt) => {
              evt.stopPropagation();
            }}
          >
            {option.label}
          </span>
        ))}
      {overflowOptions.length > 0 && (
        <Tooltip
          text={overflowOptions.map((option, index) => (
            <div key={index}>{option.label}</div>
          ))}
          display="inline-flex"
        >
          <span className={styles.multiOption}>
            {displayedOptions.length
              ? `+${overflowOptions.length}`
              : `${overflowOptions.length} items`}
          </span>
        </Tooltip>
      )}
    </div>
  );
};
