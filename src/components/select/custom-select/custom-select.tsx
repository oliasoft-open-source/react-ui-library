import React, { KeyboardEvent, useContext, useReducer, useRef } from 'react';
import { mergeRefs } from 'react-laag';
import { useFocus, useFontsReady } from 'hooks';
import { DisabledContext } from 'helpers/disabled-context';
import { Layer } from './layer/layer';
import { Trigger } from './trigger/trigger';
import {
  initialLayerFocus,
  initialTriggerFocus,
  reducer,
} from './custom-select.reducer';
import styles from './custom-select.module.less';
import { ICustomSelectProps } from './custom-select.interface';
import {
  useCustomSelectLogic,
  UseCustomSelectLogicProps,
} from './hooks/use-custom-select-logic';
import { EventKey } from './hooks/enum';
import { FixedSizeList } from 'react-window';

export const CustomSelect = (props: ICustomSelectProps) => {
  const {
    multi: isMulti = false,
    disabled = false,
    error = null,
    warning = null,
    tooltip = null,
    options,
    selectedOptions,
    onCreate = null,
    placeholder = '',
    small = false,
    tabIndex = 0,
    onFocus = () => {},
    onBlur = () => {},
    searchable = true,
    clearable = false,
    maxTooltipWidth,
    closeOnOptionActionClick,
    //private props (don't use)
    isInTable = false,
    groupOrder,
    firstSelectedOptionIndex = 0,
    right = false,
    testId = undefined,
  } = props;
  const disabledContext = useContext(DisabledContext);
  const createAble = onCreate !== null;
  const listRef = useRef<FixedSizeList>(null);
  const [triggerRef, setTriggerFocus] = useFocus();
  const isFontLoaded = useFontsReady();

  const [state, dispatch] = useReducer(
    reducer,
    // @ts-ignore: write explicit reason: write explicit reason
    { options, firstSelectedOptionIndex },
    ({ options }) => ({
      searchValue: '',
      visibleOptions: options,
      layerFocus: initialLayerFocus(options, firstSelectedOptionIndex ?? 0), //also gets reset when layer opens (because options can change)
      triggerFocus: initialTriggerFocus,
      isLayerOpen: false,
    }),
  );

  const {
    getTriggerWidth,
    onKeyEvent,
    clearAllIsFocused,
    triggerProps,
    onClickTrigger,
    onChangeSearch,
    onClickDeselectOption,
    onClickClearAll,
    renderLayer,
    layerProps,
    closeLayer,
    onSelectOption,
    getLayerWidth,
    triggerBounds,
  } = useCustomSelectLogic({
    ...props,
    state,
    dispatch,
    setTriggerFocus,
    disabledContext,
    isFontLoaded,
    createAble,
    listRef,
  } as UseCustomSelectLogicProps);

  const onKeyDown = (evt: KeyboardEvent<HTMLElement>) => {
    if (Object.values(EventKey).includes(evt.key as EventKey)) {
      onKeyEvent(evt.key, evt);
    }
  };

  return (
    <div style={{ width: getTriggerWidth() }} onKeyDown={onKeyDown}>
      <Trigger
        selectedOptions={selectedOptions as any}
        searchValue={state.searchValue}
        isOpen={state.isLayerOpen}
        focusedSelectedOptionIndex={state.triggerFocus.currentOption}
        clearAllIsFocused={clearAllIsFocused}
        searchable={searchable}
        triggerRef={mergeRefs(triggerRef, triggerProps.ref) as any}
        error={error}
        warning={warning}
        tooltip={tooltip}
        small={small}
        isInTable={isInTable}
        disabled={disabled || disabledContext}
        clearable={clearable}
        tabIndex={tabIndex}
        onClickTrigger={onClickTrigger}
        onChangeSearch={onChangeSearch}
        onClickDeselect={onClickDeselectOption}
        onClickClear={onClickClearAll}
        onFocus={onFocus}
        onBlur={onBlur}
        groupOrder={groupOrder}
        maxTooltipWidth={maxTooltipWidth}
        placeholder={placeholder}
        right={right}
        testId={testId}
      />
      {state.isLayerOpen &&
        renderLayer(
          <div {...layerProps} className={styles.layerContainer}>
            <Layer
              listRef={listRef}
              isMulti={isMulti}
              sections={state.visibleOptions}
              selectedOptions={selectedOptions}
              onSelectOption={(evt, option) => {
                const close = !isMulti ? closeLayer : undefined;
                onSelectOption(evt, option, close);
              }}
              closeLayer={() => {
                if (closeOnOptionActionClick) {
                  closeLayer();
                }
              }}
              width={getLayerWidth(triggerBounds)}
              small={small}
              focusedOptionIndex={state.layerFocus.current}
              firstSelectedOptionIndex={firstSelectedOptionIndex ?? 0}
              testId={testId && `${testId}-layer`}
            />
          </div>,
        )}
    </div>
  );
};
