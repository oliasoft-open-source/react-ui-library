import { TStringOrNumber } from 'typings/common-type-definitions';
import {
  ICustomSelectAction,
  ICustomSelectDirection,
  ICustomSelectLayerFocus,
  ICustomSelectState,
  ICustomSelectTriggerFocus,
} from './reducer.interface';
import { TCustomSelectSelectOption } from './custom-select.interface';
import { MenuType } from '../../menu/types';
import { ReducerAction } from './enum';

export const initialTriggerFocus: ICustomSelectTriggerFocus = {
  options: ['none', 'clearAll'],
  currentIndex: 0,
  currentOption: false,
  clearAll: false,
};

const initialLayerFocusIndex = (firstSelectedOptionIndex: number): number =>
  firstSelectedOptionIndex !== null ? firstSelectedOptionIndex + 1 : 0;

export const initialLayerFocus = (
  options: TCustomSelectSelectOption[],
  firstSelectedOptionIndex: number,
): ICustomSelectLayerFocus => {
  const layerFocusOptions: ('none' | number)[] = [
    'none',
    ...options
      .map((o, i) => (o.type !== MenuType.HEADING ? i : null))
      .filter((o): o is number => o !== null),
  ];

  const layerFocusIndex = initialLayerFocusIndex(firstSelectedOptionIndex);

  return {
    options: layerFocusOptions,
    currentIndex: layerFocusIndex,
    current: layerFocusOptions[layerFocusIndex],
  };
};

export const nextLayerFocus = (
  direction: ICustomSelectDirection,
  layerFocus: ICustomSelectLayerFocus,
): ICustomSelectLayerFocus => {
  const sign = direction === 'up' ? -1 : 1;
  const increment = layerFocus.currentIndex + sign;
  const nextIndex =
    (increment < 0 ? layerFocus.options.length - 1 : increment) %
    layerFocus.options.length;
  const next = layerFocus.options[nextIndex];
  return {
    ...layerFocus,
    currentIndex: nextIndex,
    current: next,
  };
};

const nextTriggerFocus = (
  direction: ICustomSelectDirection,
  selectedOptions: Array<
    | string
    | number
    | { label: TStringOrNumber; value: TStringOrNumber | boolean }
  >,
  triggerFocus: ICustomSelectTriggerFocus,
): ICustomSelectTriggerFocus => {
  const sign = direction === 'left' ? -1 : 1;

  const nextOptions: ('none' | 'clearAll' | number)[] =
    selectedOptions && selectedOptions.length
      ? [
          'none',
          ...Array.from({ length: selectedOptions.length }, (_, i) => i),
          'clearAll',
        ]
      : ['none', 'clearAll'];

  const increment = triggerFocus.currentIndex + sign;
  const nextIndex =
    (increment < 0 ? nextOptions.length - 1 : increment) % nextOptions.length;
  const next = nextOptions[nextIndex];
  const clearAll = next === 'clearAll';

  return {
    options: nextOptions,
    currentIndex: nextIndex,
    currentOption: next,
    clearAll,
  };
};

const blurTriggerFocus = (): ICustomSelectTriggerFocus => initialTriggerFocus;

const blurLayerFocus = (
  layerFocus: ICustomSelectLayerFocus,
): ICustomSelectLayerFocus => ({
  ...layerFocus,
  currentIndex: 0,
  current: 'none',
});

export const reducer = (
  state: ICustomSelectState,
  action: ICustomSelectAction,
): ICustomSelectState => {
  switch (action.type) {
    case ReducerAction.SET_VISIBLE_OPTIONS: {
      return {
        ...state,
        visibleOptions: action.options || [],
      };
    }
    case ReducerAction.RESET_LAYER_FOCUS: {
      return {
        ...state,
        layerFocus: initialLayerFocus(
          action.options || [],
          action.firstSelectedOptionIndex || 0,
        ),
      };
    }
    case ReducerAction.CLEAR_SEARCH: {
      return {
        ...state,
        visibleOptions: action.options || [],
        searchValue: '',
      };
    }
    case ReducerAction.ON_CHANGE_SEARCH: {
      const visibleOptions = (action.options || []).filter((o) =>
        (String(o.label) + (o.details ? String(o.details) : ''))
          .toLowerCase()
          .includes(String(action.value || '').toLowerCase()),
      );

      let newVisibleOptions = [...visibleOptions];

      if (action.createAble && action.value && action.value !== '') {
        newVisibleOptions.push({
          label: `Create "${action.value}"`,
          value: action.value,
          createAble: true,
        });
      }

      return {
        ...state,
        searchValue: action.value,
        visibleOptions: newVisibleOptions,
        layerFocus: blurLayerFocus(state.layerFocus),
      };
    }
    case ReducerAction.FOCUS_TRIGGER_INPUTS: {
      return {
        ...state,
        triggerFocus: nextTriggerFocus(
          action.direction || 'right',
          action.selectedOptions || [],
          state.triggerFocus,
        ),
        layerFocus: blurLayerFocus(state.layerFocus),
      };
    }
    case ReducerAction.BLUR_TRIGGER_INPUTS: {
      return {
        ...state,
        triggerFocus: blurTriggerFocus(),
      };
    }
    case ReducerAction.FOCUS_LAYER_OPTIONS: {
      if (!action.nextLayerFocus) {
        return state;
      }
      return {
        ...state,
        layerFocus: action.nextLayerFocus,
        triggerFocus: blurTriggerFocus(),
      };
    }
    case ReducerAction.OPEN_LAYER: {
      return {
        ...state,
        isLayerOpen: true,
      };
    }
    case ReducerAction.CLOSE_LAYER: {
      return {
        ...state,
        isLayerOpen: false,
      };
    }
    default:
      return state;
  }
};
