export enum ReducerAction {
  SET_VISIBLE_OPTIONS = 'SET_VISIBLE_OPTIONS',
  RESET_LAYER_FOCUS = 'RESET_LAYER_FOCUS',
  CLEAR_SEARCH = 'CLEAR_SEARCH',
  ON_CHANGE_SEARCH = 'ON_CHANGE_SEARCH',
  FOCUS_TRIGGER_INPUTS = 'FOCUS_TRIGGER_INPUTS',
  BLUR_TRIGGER_INPUTS = 'BLUR_TRIGGER_INPUTS',
  FOCUS_LAYER_OPTIONS = 'FOCUS_LAYER_OPTIONS',
  OPEN_LAYER = 'OPEN_LAYER',
  CLOSE_LAYER = 'CLOSE_LAYER',
}
