import React, {
  ChangeEvent,
  KeyboardEvent,
  useCallback,
  useEffect,
  useRef,
} from 'react';
import { useLayer } from 'react-laag';
import {
  TChangeEventHandler,
  TEmpty,
  TStringOrNumber,
} from 'typings/common-type-definitions';
import { ISelectSelectedOption } from '../../select.interface';
import { getWidestOptionWidth } from '../helper';
import { placementOptions } from '../layer/placementOptions';
import {
  ICustomSelectDirection,
  ICustomSelectState,
} from '../reducer.interface';
import { nextLayerFocus } from '../custom-select.reducer';
import { TCustomSelectSelectOption } from '../custom-select.interface';
import { ActionTypes, EventKey } from './enum';

export interface UseCustomSelectLogicProps {
  state: ICustomSelectState;
  dispatch: (action: any) => void;
  options: TCustomSelectSelectOption[];
  setTriggerFocus: TEmpty;
  disabled: boolean;
  disabledContext: boolean;
  onCreate: ((value: string) => void) | null;
  onChange: TChangeEventHandler | null;
  selectedOptions: ISelectSelectedOption | ISelectSelectedOption[];
  width: TStringOrNumber | null;
  small: boolean;
  isFontLoaded: boolean;
  placeholder: string;
  firstSelectedOptionIndex: number;
  createAble: boolean;
  listRef: React.RefObject<any>;
  autoLayerWidth: boolean;
}

export const useCustomSelectLogic = ({
  state,
  dispatch,
  options,
  setTriggerFocus,
  disabled,
  disabledContext,
  onCreate,
  onChange,
  selectedOptions,
  width,
  small,
  isFontLoaded,
  placeholder,
  firstSelectedOptionIndex,
  createAble,
  listRef,
  autoLayerWidth,
}: UseCustomSelectLogicProps) => {
  const closeLayer = () => {
    if (state.isLayerOpen) {
      dispatch({ type: ActionTypes.CLOSE_LAYER });
    }
    setTriggerFocus();
  };

  const openLayer = () => dispatch({ type: ActionTypes.OPEN_LAYER });

  //Update visible options upon external changes to the options prop
  const isFirstRun = useRef(true);
  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
    } else {
      dispatch({ type: ActionTypes.SET_VISIBLE_OPTIONS, options });
    }
  }, [options]);

  const onSelectOption = (evt: any, option?: any, close?: TEmpty): void => {
    if (!(disabled || disabledContext)) {
      if (close) {
        close();
      }
      if (option) {
        if (option.createAble && onCreate) {
          onCreate(option.value);
        } else {
          if (!option.disabled) {
            if (Array.isArray(selectedOptions)) {
              const alreadySelected = selectedOptions.some(
                (selectedOption) => selectedOption.label === option.label,
              );

              if (!alreadySelected) {
                const newSelectedOptions = [...selectedOptions, option];
                if (onChange) {
                  onChange(evt, newSelectedOptions);
                }
              } else {
                const newSelectedOptions = selectedOptions.filter(
                  (opt) => opt.label !== option.label,
                );
                if (onChange) {
                  onChange(evt, newSelectedOptions);
                }
              }
            } else {
              if (onChange) {
                onChange(evt, option as ISelectSelectedOption);
              }
            }
          }
        }
      }

      dispatch({ type: ActionTypes.CLEAR_SEARCH, options });
      setTriggerFocus();
    }
  };

  const getTriggerWidth = useCallback(() => {
    if (!width) return '100%';
    if (width !== 'auto') return width;
    return getWidestOptionWidth(options, small, isFontLoaded, placeholder);
  }, [options, isFontLoaded]);

  const getLayerWidth = useCallback(
    (triggerBounds: any) => {
      if (!autoLayerWidth) return triggerBounds?.width;
      const layerWidth = Math.max(
        getWidestOptionWidth(options, small, isFontLoaded, placeholder),
        triggerBounds?.width,
      );
      return layerWidth;
    },
    [options, isFontLoaded],
  );

  const { renderLayer, layerProps, triggerProps, triggerBounds } = useLayer({
    isOpen: state?.isLayerOpen ?? false,
    overflowContainer: true,
    ...placementOptions,
    ResizeObserver,
    onOutsideClick: closeLayer,
    onParentClose: closeLayer,
  });

  const onClickTrigger = () => {
    if (disabled || disabledContext) {
      return;
    }

    if (state.isLayerOpen) {
      closeLayer();
      return;
    }

    setTriggerFocus();

    dispatch({
      type: ActionTypes.RESET_LAYER_FOCUS,
      options,
      firstSelectedOptionIndex,
    });
    openLayer();
  };

  // reset search filter when layer closes
  const isFirstRender = useRef(true);
  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
    } else {
      if (!state.isLayerOpen) {
        dispatch({ type: ActionTypes.CLEAR_SEARCH, options });
      }
    }
  }, [state.isLayerOpen]);

  const onClickDeselectOption = (evt: any, options?: any) => {
    const newSelectedOptions =
      selectedOptions instanceof Array
        ? selectedOptions.filter((option) => option.value !== options.value)
        : null;
    if (onChange) {
      onChange(evt, newSelectedOptions as ISelectSelectedOption);
    }
  };

  const onClickClearAll = (evt: ChangeEvent<any>) => {
    const newSelectedOptions =
      selectedOptions instanceof Array ? [] : { value: null };
    if (onChange) {
      onChange(evt, newSelectedOptions as ISelectSelectedOption);
    }
  };

  const onChangeSearch = (value: any) =>
    dispatch({
      type: ActionTypes.ON_CHANGE_SEARCH,
      options,
      value,
      createAble,
    });

  /*
     Keyboard actions
   */

  const clickTrigger = () => {
    openLayer();

    dispatch({ type: ActionTypes.BLUR_TRIGGER_INPUTS });
  };

  const scrollToItem = (index: number) => {
    if (listRef.current && Number.isInteger(index)) {
      listRef.current.scrollToItem(index);
    }
  };
  const focusNextLayerOption = (
    ICustomSelectDirection: ICustomSelectDirection,
  ) => {
    const next = nextLayerFocus(ICustomSelectDirection, state.layerFocus);

    dispatch({
      type: ActionTypes.FOCUS_LAYER_OPTIONS,
      nextLayerFocus: next,
    });
    scrollToItem(next.current as number);
  };
  const focusTriggerInputs = (
    ICustomSelectDirection: ICustomSelectDirection,
  ) => {
    dispatch({
      type: ActionTypes.FOCUS_TRIGGER_INPUTS,
      ICustomSelectDirection,
      selectedOptions,
    });
  };
  const clearAll = (evt: any) => onClickClearAll(evt);

  const deselectIsFocused =
    selectedOptions &&
    (selectedOptions as any)[state.triggerFocus.currentOption as any];

  const deselectOption = (evt: any) => {
    onClickDeselectOption(
      evt,
      (selectedOptions as any)[state.triggerFocus.currentOption as any],
    );
  };

  const selectOption = (evt: any) => {
    onSelectOption(
      evt,
      state?.visibleOptions?.[state.layerFocus.current as number],
      closeLayer,
    );
  };
  const clearAllIsFocused = state.triggerFocus.clearAll;

  const onKeyEvent = (
    key: string | ICustomSelectDirection,
    evt: KeyboardEvent<HTMLElement>,
  ) => {
    switch (key) {
      case EventKey.UP: {
        focusNextLayerOption('up');
        break;
      }
      case EventKey.DOWN: {
        if (state.isLayerOpen) {
          focusNextLayerOption('down');
        } else {
          clickTrigger();
        }
        break;
      }
      case EventKey.LEFT: {
        focusTriggerInputs('left');
        break;
      }
      case EventKey.RIGHT: {
        focusTriggerInputs('right');
        break;
      }
      case EventKey.ENTER: {
        if (clearAllIsFocused) {
          clearAll(evt);
        } else if (deselectIsFocused) {
          deselectOption(evt);
        } else {
          selectOption(evt);
        }
        break;
      }
      case EventKey.ESC: {
        closeLayer();
        break;
      }
      case EventKey.TAB: {
        dispatch({ type: ActionTypes.CLOSE_LAYER });
        break;
      }
      default:
    }
  };

  return {
    getTriggerWidth,
    onKeyEvent,
    clearAllIsFocused,
    triggerProps,
    onClickTrigger,
    onChangeSearch,
    onClickDeselectOption,
    onClickClearAll,
    renderLayer,
    layerProps,
    closeLayer,
    onSelectOption,
    getLayerWidth,
    triggerBounds,
  };
};
