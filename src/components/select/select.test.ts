import {
  isMulti,
  isSimpleOptions,
  isSimpleSelectedOptions,
  standardizeOptions,
  standardizeSelectedOptions,
  standardizeSelectedOption,
  getFirstSelectedOptionIndex,
  nonExistentValue,
} from './select.input';

describe('select (inputs)', () => {
  test('is multi select', () => {
    expect(isMulti([])).toBe(true);
    expect(isMulti([{ label: 'Monkeys', value: 'bananas' }])).toBe(true);
    expect(isMulti(null)).toBe(false);
    expect(isMulti('Monkeys')).toBe(false);
    expect(isMulti({ label: 'Monkeys', value: 'bananas' })).toBe(false);
  });

  test('is simple options', () => {
    expect(isSimpleOptions(['Cat', 'Mouse'] as any)).toBe(true);
    expect(isSimpleOptions([1, 2] as any)).toBe(true);
    expect(
      isSimpleOptions([
        { label: 'Cat', value: 'Cat' },
        { label: 'Mouse', value: 'Mouse' },
      ]),
    ).toBe(false);
    expect(
      isSimpleOptions([{ label: 'Cat', value: 'Cat' }, 'Mouse', 4] as any),
    ).toBe(true);
    expect(isSimpleOptions([])).toBe(false);
    expect(isSimpleOptions(undefined as any)).toBe(false);
  });

  test('is simple selected options', () => {
    expect(isSimpleSelectedOptions(['Cat', 'Mouse'])).toBe(true);
    expect(isSimpleSelectedOptions([11, 12])).toBe(true);
    expect(isSimpleSelectedOptions('Cat')).toBe(true);
    expect(isSimpleSelectedOptions(6)).toBe(true);
    expect(
      isSimpleOptions([
        { label: 'Cat', value: 'Cat' },
        { label: 'Mouse', value: 'Mouse' },
      ]),
    ).toBe(false);
    expect(
      isSimpleOptions([{ label: 'Cat', value: 'Cat' }, 'Mouse', 7] as any),
    ).toBe(true);
    expect(isSimpleOptions([])).toBe(false);
    expect(isSimpleOptions(undefined as any)).toBe(false);
  });

  test('standardize options', () => {
    expect(
      standardizeOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        false,
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(standardizeOptions(['Cat', 'Mouse'], true)).toStrictEqual([
      { label: 'Cat', value: 'Cat' },
      { label: 'Mouse', value: 'Mouse' },
    ]);
    expect(standardizeOptions([5, 6], true)).toStrictEqual([
      { label: 5, value: 5 },
      { label: 6, value: 6 },
    ]);
    expect(
      standardizeOptions([{ label: 'Cat', value: 'cat' }, 'Mouse', 9], true),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'Mouse' },
      { label: 9, value: 9 },
    ]);
    expect(standardizeOptions([], true)).toStrictEqual([]);
    expect(standardizeOptions(undefined as any, true)).toBeUndefined();
  });

  test('standardize selected options (single select)', () => {
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], {
        label: 'Cat',
        value: 'cat',
      }),
    ).toStrictEqual({ label: 'Cat', value: 'cat' });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], 'cat'),
    ).toStrictEqual({
      label: 'Cat',
      value: 'cat',
    });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], {
        value: 'cat',
        monkeys: 'bananas',
      } as any),
    ).toStrictEqual({
      label: 'Cat',
      value: 'cat',
    });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], 'Foobar'),
    ).toStrictEqual({ label: 'Foobar', value: 'Foobar' });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], {
        value: '',
      }),
    ).toBeUndefined();
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], null as any),
    ).toBeUndefined();
    expect(
      standardizeSelectedOptions([{ label: 8, value: 8 }], 8),
    ).toStrictEqual({
      label: 8,
      value: 8,
    });
    expect(
      standardizeSelectedOptions([{ label: 8, value: 8 }], '8'), //allow coercion
    ).toStrictEqual({
      label: 8,
      value: 8,
    });
    expect(standardizeSelectedOptions([], null as any)).toBeUndefined();
    expect(standardizeSelectedOptions([], undefined as any)).toBeUndefined();
  });

  test('standardize selected options (multi-select)', () => {
    expect(
      standardizeSelectedOptions(null as any, [
        { label: 'Cat', value: 'cat' },
        { label: 'Mouse', value: 'mouse' },
      ]),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        ['cat', 'mouse'],
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 1, value: 1 },
          { label: 2, value: 2 },
        ],
        [1, 2],
      ),
    ).toStrictEqual([
      { label: 1, value: 1 },
      { label: 2, value: 2 },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 1, value: 1 },
          { label: 2, value: 2 },
        ],
        ['1', '2'], //allow coercion
      ),
    ).toStrictEqual([
      { label: 1, value: 1 },
      { label: 2, value: 2 },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 5, value: 5 },
        ],
        ['cat', { label: 'Mouse', value: 'mouse' }, 5],
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
      { label: 5, value: 5 },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 5, value: 5 },
        ],
        [
          { value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          null,
          undefined,
          '',
        ] as any,
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(standardizeSelectedOptions(null as any, [])).toStrictEqual([]);
  });

  test('standardize selected option', () => {
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        { label: 'Mouse', value: 'mouse' },
      ),
    ).toStrictEqual({ label: 'Mouse', value: 'mouse' });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        { value: 'mouse' },
      ),
    ).toStrictEqual({ label: 'Mouse', value: 'mouse' });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        'mouse',
      ),
    ).toStrictEqual({ label: 'Mouse', value: 'mouse' });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        { value: 1 },
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        { value: '1' }, //allow coercion
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        1,
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        '1', //allow coercion
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        null as any,
      ),
    ).toBeUndefined();
    expect(standardizeSelectedOption([], 'mouse')).toStrictEqual({
      label: 'mouse',
      value: 'mouse',
    });
    //non-existent values
    expect(standardizeSelectedOption(undefined as any, 'mouse')).toStrictEqual({
      label: 'mouse',
      value: 'mouse',
    });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        { label: 'Vampire', value: 'vampire' },
      ),
    ).toStrictEqual({ label: 'Vampire', value: 'vampire' });
    expect(
      standardizeSelectedOption(['Cat', 'Mouse'] as any, 'Vampire'),
    ).toStrictEqual({ label: 'Vampire', value: 'Vampire' });
  });

  test('get first selected option index', () => {
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        { label: 'Mouse', value: 'mouse' },
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
          { label: 'Dog', value: 3 },
        ],
        { label: 'Mouse', value: 2 },
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
          { label: 'Dog', value: 3 },
        ],
        { label: 'Mouse', value: '2' }, //allow coercion
      ),
    ).toBe(1);
    expect(
      //with headings (should be omitted from index)
      getFirstSelectedOptionIndex(
        [
          { type: 'Heading', label: 'Enemies' },
          { label: 'Cat', value: 1 },
          { type: 'Heading', label: 'Friends' },
          { label: 'Mouse', value: 2 },
          { label: 'Dog', value: 3 },
        ],
        { label: 'Mouse', value: '2' }, //allow coercion
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'] as any, 'Mouse'),
    ).toBe(1);
    expect(getFirstSelectedOptionIndex([1, 2, 3] as any, 2)).toBe(1);
    expect(getFirstSelectedOptionIndex([1, 2, 3] as any, '2')).toBe(
      //allow coercion
      1,
    );
    expect(
      getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'] as any, [
        'Mouse',
        'Dog',
      ]),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        { label: 'Camel', value: 'camel' },
      ),
    ).toBeNull();
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [{ label: 'Camel', value: 'camel' }],
      ),
    ).toBeNull();
    expect(
      getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'] as any, 'Camel'),
    ).toBeNull();
    expect(
      getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'] as any, [
        'Camel',
        'Sheep',
      ]),
    ).toBeNull();
    expect(getFirstSelectedOptionIndex([], 'Camel')).toBeNull();
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        null as any,
      ),
    ).toBeNull();
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [],
      ),
    ).toBeNull();
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        undefined as any,
      ),
    ).toBeNull();
  });

  test('non-existent value', () => {
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        { label: 'Cat', value: 'cat' },
      ),
    ).toBe(false);
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        { label: 'Vampire', value: 'vampire' },
      ),
    ).toBe(true);
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        null as any,
      ),
    ).toBe(false);
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        undefined as any,
      ),
    ).toBe(false);
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        '',
      ),
    ).toBe(false);
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
      ),
    ).toBe(false);
    expect(
      nonExistentValue(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [
          { label: 'Mouse', value: 'mouse' },
          { label: 'Vampire', value: 'vampire' },
        ],
      ),
    ).toBe(true);
  });
});
