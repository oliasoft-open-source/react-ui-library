import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { ISelectProps, Select } from './select';
import { Spacer } from '../layout/spacer/spacer';
import * as storyData from './select.stories-data';
import { ICustomSelectProps } from './custom-select/custom-select.interface';
import { ISelectSelectedOption } from './select.interface';

export default {
  title: 'Forms/Select',
  component: Select,
  args: {
    options: storyData.options,
    native: false,
    multi: false,
    disabled: false,
    autoLayerWidth: false,
    clearable: false,
    searchable: true,
    right: false,
  },
} as Meta;

type TSelectType = ISelectProps | ICustomSelectProps;

const Template: StoryFn<TSelectType> = (args: any) => {
  const { multi, value } = args;
  const [selectedValue, setSelectedValue] = useState(
    value || (multi ? [] : undefined),
  );
  const handleChange = (evt: any) => {
    const { value } = evt.target;
    setSelectedValue(multi ? value : { value });
  };
  return (
    <Select
      value={selectedValue}
      onChange={handleChange}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const AutoWidth = Template.bind({});
AutoWidth.args = { width: 'auto' };

export const FixedWidth = Template.bind({});
FixedWidth.args = { width: '200px' };

export const Native = Template.bind({});
Native.args = { native: true };

export const Multiselect = Template.bind({});
Multiselect.args = { multi: true, clearable: true };

export const Empty = Template.bind({});
Empty.args = { options: [] };

export const Clearable = Template.bind({});
Clearable.args = { clearable: true };

export const NonExistentValue = Template.bind({});
NonExistentValue.args = { value: { label: 'Vampires', value: 'blood' } };

export const CustomPlaceholder = Template.bind({});
CustomPlaceholder.args = { placeholder: 'Select the best animal...' };

export const OptionsWithHeadings = Template.bind({});
OptionsWithHeadings.args = { options: storyData.optionsWithHeadings };

export const OptionsWithDetails = Template.bind({});
OptionsWithDetails.args = { options: storyData.optionsWithDetails };

export const OptionsWithIcons = Template.bind({});
OptionsWithIcons.args = { options: storyData.optionsWithIcons };

export const OptionsWithActions = Template.bind({});
OptionsWithActions.args = { options: storyData.optionsWithActions };

export const OptionsWithEverything = Template.bind({});
OptionsWithEverything.args = { options: storyData.optionsWithEverything };

export const NotSearchable = Template.bind({});
NotSearchable.args = { searchable: false };

export const RightAligned = Template.bind({});
RightAligned.args = { right: true };

export const AutoLayerWidth = Template.bind({});
AutoLayerWidth.parameters = {
  docs: {
    description: {
      story:
        'Avoid autoLayerWidth for very long lists in custom selects (slow)',
    },
  },
};
AutoLayerWidth.args = { width: '70px', autoLayerWidth: true };

export const Tooltip = Template.bind({});
Tooltip.args = { tooltip: 'Some info' };

export const Error = Template.bind({});
Error.args = { error: 'Some info' };

export const Warning = Template.bind({});
Warning.args = { warning: 'Some info' };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const NumericOptions = Template.bind({});
NumericOptions.args = {
  options: storyData.optionsNumeric,
  placeholder: undefined,
};

export const BooleanOptions = Template.bind({});
BooleanOptions.args = { options: storyData.optionsBoolean };

export const SimpleOptions = Template.bind({});
SimpleOptions.args = { options: storyData.optionsSimple as any };

export const VeryLongList = Template.bind({});
VeryLongList.parameters = {
  docs: {
    description: {
      story:
        'Avoid `width: auto` and autoLayerWidth for very long lists in custom selects (slow). Consider checkNonExistentValues={false}.',
    },
  },
};
VeryLongList.args = { options: storyData.optionsLong };

export const DeprecatedEventHandler = Template.bind({});
DeprecatedEventHandler.args = { deprecatedEventHandler: true };

export const OnCreate = () => {
  const [options, setOptions] = useState([
    { label: 'Aardvarks', value: 'termites' },
    { label: 'Kangaroos', value: 'grass' },
    { label: 'Monkeys', value: 'bananas' },
    { label: 'Possums', value: 'slugs' },
  ]);
  const [singleValue, setSingleValue] = useState<
    ISelectSelectedOption | undefined
  >(undefined);
  const [multiValue, setMultiValue] = useState<ISelectSelectedOption[]>([]);

  return (
    <>
      <Select
        name="example"
        options={options}
        onChange={(evt) => setSingleValue({ value: evt.target.value })}
        onCreate={(value) => {
          const newValue = { label: value, value };
          setOptions([...options, newValue]);
          setSingleValue(newValue);
        }}
        value={singleValue}
        clearable
      />
      <Spacer />
      <Select
        name="example"
        options={options}
        onChange={(evt) => setMultiValue(evt.target.value)}
        onCreate={(value) => {
          const newValue = { label: value, value };
          setOptions([...options, newValue]);
          console.log(value);
          setMultiValue([...multiValue, newValue]);
        }}
        value={multiValue}
        multi
        clearable
      />
    </>
  );
};
