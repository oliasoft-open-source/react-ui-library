import React, { useState } from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { expect, userEvent, waitFor, within } from '@storybook/test';
import { ISelectProps, Select } from './select';
import * as storyData from './select.stories-data';
import { ICustomSelectProps } from './custom-select/custom-select.interface';
import { ISelectSelectedOption } from './select.interface';

export default {
  title: 'Forms/Select/Test Cases',
  component: Select,
  args: {
    options: storyData.options,
  },
  tags: ['!autodocs'],
} as Meta;

type TSelectType = ISelectProps | ICustomSelectProps;

const Template: StoryFn<TSelectType> = (args: any) => {
  const { multi, value } = args;
  const [selectedValue, setSelectedValue] = useState(
    value || (multi ? [] : undefined),
  );
  const handleChange = (evt: any) => {
    const { value } = evt.target;
    setSelectedValue(multi ? value : { value });
  };
  return (
    <Select
      value={selectedValue}
      onChange={handleChange}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

export const TestDisabledNonExistentValue = Template.bind({});
TestDisabledNonExistentValue.args = {
  value: { label: 'Vampires', value: 'blood' },
  disabled: true,
};

export const Test = Template.bind({});
Test.args = { testId: 'testId', clearable: true };
Test.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Select item with click', async () => {
    const trigger = canvas.getByTestId('testId');
    await userEvent.click(trigger);
    const layer = await body.findByTestId('testId-layer');
    await waitFor(() => expect(layer).toBeInTheDocument());

    const option = within(layer).getByText('Wombats');
    await userEvent.click(option);
    const value = await canvas.findByTestId('testId-value');
    await waitFor(() =>
      expect(within(value).getByText('Wombats')).toBeInTheDocument(),
    );
  });
  await step('Clear selected value', async () => {
    const clear = canvas.getByTestId('testId-clear');
    await userEvent.click(clear);
    const value = canvas.queryByTestId('testId-value');
    await waitFor(() => expect(value).not.toBeInTheDocument());
  });
};

export const TestMulti = Template.bind({});
TestMulti.args = { testId: 'testId', clearable: true, multi: true };
TestMulti.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Select items with click', async () => {
    const trigger = canvas.getByTestId('testId');
    await userEvent.click(trigger);
    const layer = await body.findByTestId('testId-layer');
    await waitFor(() => expect(layer).toBeInTheDocument());

    const option1 = within(layer).getByText('Wombats');
    await userEvent.click(option1);
    const option2 = within(layer).getByText('Kangaroos');
    await userEvent.click(option2);
    await userEvent.click(canvasElement);
    await waitFor(() => expect(layer).not.toBeInTheDocument());

    const value = await canvas.findByTestId('testId-value');
    await waitFor(() =>
      expect(within(value).getByText('Wombats')).toBeInTheDocument(),
    );
    await waitFor(() =>
      expect(within(value).getByText('Kangaroos')).toBeInTheDocument(),
    );
  });
  await step('Deselect item with click', async () => {
    const trigger = canvas.getByTestId('testId');
    await userEvent.click(trigger);
    const layer = await body.findByTestId('testId-layer');
    await waitFor(() => expect(layer).toBeInTheDocument());

    const option1 = within(layer).getByText('Wombats');
    await userEvent.click(option1);
    await userEvent.click(canvasElement);
    await waitFor(() => expect(layer).not.toBeInTheDocument());

    const value = await canvas.findByTestId('testId-value');
    await waitFor(() =>
      expect(within(value).queryByText('Wombats')).not.toBeInTheDocument(),
    );
  });
  await step('Clear selected values', async () => {
    const clear = canvas.getByTestId('testId-clear');
    await userEvent.click(clear);
    const value = canvas.queryByTestId('testId-value');
    await waitFor(() => expect(value).not.toBeInTheDocument());
  });
};

export const TestDisabled = Template.bind({});
TestDisabled.args = { testId: 'testId', disabled: true };
TestDisabled.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Clicking disabled trigger should not show layer', async () => {
    const trigger = canvas.getByTestId('testId');
    await trigger.click();
    const layer = body.queryByTestId('testId-layer');
    await waitFor(() => expect(layer).not.toBeInTheDocument());
  });
};

export const TestCreate: StoryFn<TSelectType> = () => {
  const [options, setOptions] = useState(storyData.options);
  const [singleValue, setSingleValue] = useState<
    ISelectSelectedOption | undefined
  >(undefined);

  return (
    <Select
      testId="testId"
      name="example"
      options={options}
      onChange={(evt) => setSingleValue({ value: evt.target.value })}
      onCreate={(value) => {
        const newValue = { label: value, value };
        setOptions([...options, newValue]);
        setSingleValue(newValue);
      }}
      value={singleValue}
    />
  );
};

TestCreate.args = { testId: 'testId' };
TestCreate.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  const body = within(canvasElement.ownerDocument.body);
  await step('Create new item', async () => {
    const input = canvas.getByTestId('testId-input');
    await waitFor(() => expect(input).toBeInTheDocument());

    if (!input) return;
    await userEvent.type(input, 'New value');
    const option = body.getByText('Create "New value"');
    await userEvent.click(option);
    const value = await canvas.findByTestId('testId-value');
    await waitFor(() =>
      expect(within(value).getByText('New value')).toBeInTheDocument(),
    );
  });
};
