import { ReactNode } from 'react';
import { TStringNumberNull } from 'typings/common-type-definitions';

export interface ISelectSelectedOption {
  value?: TStringNumberNull | boolean;
  label?: string | ReactNode;
  details?: string;
  actions?: any[];
  icon?: boolean | ReactNode;
  type?: string;
  disabled?: boolean;
  testId?: string;
}
