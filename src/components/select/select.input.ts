import { TStringOrNumber } from 'typings/common-type-definitions';
import { isEmptyNullOrUndefined } from 'helpers/helpers';
import { ISelectSelectedOption } from './select.interface';
import { MenuType } from '../menu/types';

type Option = ISelectSelectedOption;
type SimpleOption = TStringOrNumber;
type SelectedOption = Option | SimpleOption;

export const isMulti = (selectedOptions: any): boolean =>
  selectedOptions instanceof Array;

export const standardizeOptions = (
  options: any[],
  simple: boolean,
): Option[] => {
  return simple && Array.isArray(options)
    ? options.map((o) =>
        typeof o === 'string' || typeof o === 'number'
          ? { label: o, value: o }
          : o,
      )
    : options;
};

export const standardizeSelectedOption = (
  standardizedOptions: Option[],
  selectedOption: SelectedOption,
): Option | undefined => {
  if (
    selectedOption !== null &&
    typeof selectedOption === 'object' &&
    selectedOption.hasOwnProperty('label') &&
    selectedOption.hasOwnProperty('value')
  ) {
    return selectedOption as Option;
  } else {
    const value =
      selectedOption !== null &&
      typeof selectedOption === 'object' &&
      selectedOption.hasOwnProperty('value')
        ? selectedOption.value
        : selectedOption;

    const foundFullValue: Option | undefined = Array.isArray(
      standardizedOptions,
    )
      ? standardizedOptions
          .filter((o) => o.type !== MenuType.HEADING)
          .find((o) => o.value == value /* allow coercion */)
      : undefined;

    return (
      foundFullValue ||
      (isEmptyNullOrUndefined(value)
        ? undefined
        : {
            value: value as TStringOrNumber | boolean,
            label: value as TStringOrNumber | boolean,
          })
    );
  }
};

export const standardizeSelectedOptions = (
  standardizedOptions: Option[],
  selectedOptions: SelectedOption[] | SelectedOption,
): Option[] | Option | undefined => {
  const multi = isMulti(selectedOptions);
  if (multi) {
    return (selectedOptions as SelectedOption[])
      .map((selectedOption) =>
        standardizeSelectedOption(standardizedOptions, selectedOption),
      )
      .filter(
        (selectedOption): selectedOption is Option =>
          selectedOption !== undefined,
      );
  } else {
    return selectedOptions === null
      ? undefined
      : standardizeSelectedOption(
          standardizedOptions,
          selectedOptions as SelectedOption,
        );
  }
};

export const isSimpleOptions = (options: Option[]): boolean =>
  Array.isArray(options) &&
  options.some((o) => typeof o === 'string' || typeof o === 'number');

export const isSimpleSelectedOptions = (
  selectedOptions: SelectedOption | SelectedOption[],
): boolean => {
  return (
    typeof selectedOptions === 'string' ||
    typeof selectedOptions === 'number' ||
    (Array.isArray(selectedOptions) &&
      isSimpleOptions(selectedOptions as any[]))
  );
};

export const getFirstSelectedOptionIndex = (
  options: Option[],
  selectedOptions: SelectedOption | SelectedOption[],
): number | null => {
  const firstSelected =
    selectedOptions instanceof Array
      ? selectedOptions.length
        ? selectedOptions[0]
        : null
      : selectedOptions;
  const firstSelectedValue =
    firstSelected === null || firstSelected === undefined
      ? null
      : typeof firstSelected === 'string' ||
        typeof firstSelected === 'number' ||
        typeof firstSelected === 'boolean'
      ? firstSelected
      : firstSelected.value;
  const index = options
    .filter((o) => o.type !== MenuType.HEADING)
    .findIndex((o) => {
      if (typeof o === 'string' || typeof o === 'number') {
        return o == firstSelectedValue; //allow coercion
      } else {
        return o.value == firstSelectedValue; //allow coercion
      }
    });
  return index === -1 ? null : index;
};

export const nonExistentValue = (
  options: Option[],
  selectedOptions: SelectedOption | SelectedOption[],
): boolean => {
  const multi = isMulti(selectedOptions);
  const selected = multi
    ? (selectedOptions as SelectedOption[])
    : isEmptyNullOrUndefined(selectedOptions)
    ? []
    : [selectedOptions];
  return !selected?.every((selectedValue: any) => {
    const found = options?.find((o) => o?.value === selectedValue?.value);
    return found !== undefined;
  });
};

export const standardizeInputs = (
  options: Option[],
  selectedOptions: SelectedOption | SelectedOption[],
  enableAutoScroll?: boolean,
  checkNonExistentValues?: boolean,
) => {
  const cleanedOptions = Array.isArray(options) ? options : []; // don't crash when options is null or wrong type
  const multi = isMulti(selectedOptions);
  const simpleOptions = isSimpleOptions(cleanedOptions);
  const standardizedOptions = standardizeOptions(cleanedOptions, simpleOptions);
  const firstSelectedOptionIndex = enableAutoScroll
    ? getFirstSelectedOptionIndex(cleanedOptions, selectedOptions)
    : null;
  const standardizedSelectedOptions = standardizeSelectedOptions(
    standardizedOptions,
    selectedOptions as any,
  );
  const hasNonExistentValue =
    checkNonExistentValues && standardizedSelectedOptions !== undefined
      ? nonExistentValue(
          standardizedOptions,
          standardizedSelectedOptions as SelectedOption | SelectedOption[],
        )
      : false;

  return {
    options: standardizedOptions,
    selectedOptions: standardizedSelectedOptions,
    multi,
    simpleInputs: simpleOptions,
    firstSelectedOptionIndex,
    hasNonExistentValue,
  };
};
