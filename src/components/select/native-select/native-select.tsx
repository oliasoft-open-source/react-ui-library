import React, { useContext, ChangeEvent } from 'react';
import cx from 'classnames';
import { TEmpty, TStringOrNumber } from 'typings/common-type-definitions';
import { GroupOrder, TGroupOrder } from 'typings/common-types';
import { DisabledContext } from 'helpers/disabled-context';
import { isStringNumberOrNode } from 'helpers/types';
import { noop } from 'lodash';
import styles from './native-select.module.less';
import { Tooltip } from '../../tooltip/tooltip';
import { ISelectSelectedOption } from '../select.interface';

export interface INativeSelectProps {
  borderRadius?: TStringOrNumber;
  disabled?: boolean;
  error?: TStringOrNumber | React.ReactNode | null;
  warning?: TStringOrNumber | React.ReactNode | null;
  tooltip?: TStringOrNumber | React.ReactNode | null;
  options: ISelectSelectedOption[];
  onChange?: (
    evt: ChangeEvent<HTMLSelectElement>,
    value: ISelectSelectedOption | { value: null },
  ) => void;
  onFocus?: TEmpty;
  onBlur?: TEmpty;
  right?: boolean;
  small?: boolean;
  tabIndex?: number;
  selectedOption?: ISelectSelectedOption;
  width?: TStringOrNumber | null;
  groupOrder?: TGroupOrder;
  testId?: string | undefined;
  isInTable?: boolean;
  clearable?: boolean;
  placeholder?: string | null;
  hasNonExistentValue?: boolean;
  maxTooltipWidth?: TStringOrNumber;
}

export const NativeSelect = ({
  disabled = false,
  error = null,
  warning = null,
  tooltip = null,
  options,
  onChange = noop,
  onFocus = noop,
  onBlur = noop,
  right = false,
  small = false,
  tabIndex = 0,
  selectedOption,
  width = null,
  groupOrder,
  testId,
  isInTable = false,
  clearable,
  placeholder,
  hasNonExistentValue,
  maxTooltipWidth,
  borderRadius,
}: INativeSelectProps) => {
  const disabledContext = useContext(DisabledContext);
  const internalValue =
    selectedOption === null
      ? 'unselected'
      : selectedOption?.value === null
      ? 'unselected'
      : selectedOption?.value; //null not allowed as a React prop to a select input
  const isUnselected = internalValue === 'unselected';

  const viewOptions = (
    internalValue === 'unselected' || clearable
      ? [
          <option
            key="unselected"
            value="unselected"
            disabled={!clearable}
            hidden={!clearable}
          >
            {placeholder || 'Select...'}
          </option>,
        ]
      : []
  )
    .concat(
      hasNonExistentValue
        ? [
            <option
              key="selectedNonExistentValue"
              value={selectedOption?.value as any}
              disabled={disabled || disabledContext}
            >
              {selectedOption?.label}
            </option>,
          ]
        : [],
    )
    .concat(
      options.map((option, i) => {
        return (
          <option
            key={i}
            value={option.value as any}
            data-testid={option.testId}
          >
            {option.label}
          </option>
        );
      }),
    );

  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case GroupOrder.FIRST:
          return styles.groupOrderFirst;
        case GroupOrder.LAST:
          return styles.groupOrderLast;
        default:
          return styles.groupOrderMiddle;
      }
    }
    return '';
  })();

  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      enabled={
        (tooltip && isStringNumberOrNode(tooltip)) ||
        (error && isStringNumberOrNode(error)) ||
        (warning && isStringNumberOrNode(warning)) ||
        false
      }
      maxWidth={maxTooltipWidth}
    >
      <select
        className={cx(
          styles.select,
          isUnselected ? styles.unSelected : '',
          right ? styles.right : '',
          small ? styles.small : '',
          isInTable ? styles.isInTable : '',
          order,
        )}
        style={{
          width: width || '',
          borderRadius: borderRadius || '',
        }}
        onChange={(evt) => {
          const { value } = evt.target;
          let nextValue;
          if (value === 'unselected') {
            nextValue = { value: null };
          } else {
            const foundOption = options.find(
              (o) => String(o.value) === String(value),
            );
            nextValue = foundOption || { value: null };
          }
          onChange(evt, nextValue);
        }}
        onFocus={onFocus}
        onBlur={onBlur}
        tabIndex={tabIndex}
        value={internalValue as any}
        disabled={disabled || disabledContext}
        data-error={error || null}
        data-warning={warning || null}
        data-testid={testId}
      >
        {viewOptions}
      </select>
    </Tooltip>
  );
};
