import React, { ReactElement } from 'react';
import { TEmpty } from 'typings/common-type-definitions';
import { IconType } from 'typings/common-types';
import { ISelectSelectedOption } from './select.interface';
import { Icon } from '../icon/icon';

type Action = {
  label: string;
  icon: string | ReactElement;
  onClick: TEmpty;
};

type Option = ISelectSelectedOption;

type Heading = {
  type: 'Heading';
  label: string;
};

type OptionOrHeading = Option | Heading;

export const options: Option[] = [
  { label: 'Aardvarks', value: 'termites' },
  { label: 'Kangaroos', value: 'grass' },
  { label: 'Koalas', value: 'leaves' },
  { label: 'Monkeys', value: 'bananas', disabled: true },
  { label: 'Wombats', value: 'bark' },
];

export const optionsWithDetails: Option[] = [
  { label: 'Kangaroos', value: 'grass', details: '(grass)' },
  { label: 'Monkeys', value: 'bananas', details: '(bananas)' },
  { label: 'Possums', value: 'slugs', details: '(slugs)' },
];

export const optionsWithHeadings: OptionOrHeading[] = [
  { type: 'Heading', label: 'Large' },
  { label: 'Aardvarks', value: 'termites' },
  { label: 'Kangaroos', value: 'grass' },
  { label: 'Monkeys', value: 'bananas' },
  { type: 'Heading', label: 'Small' },
  { label: 'Possums', value: 'slugs' },
];

export const optionsNumeric: Option[] = [
  { label: 1, value: 1 },
  { label: 2, value: 2 },
  { label: 3, value: 3 },
  { label: 4, value: 4 },
];

export const optionsBoolean: Option[] = [
  { label: 'Yes', value: true },
  { label: 'No', value: false },
];

export const optionsWithIcons: Option[] = [
  {
    label: 'Aardvarks',
    value: 'termites',
  },
  { label: 'Monkeys', value: 'bananas' },
  {
    label: 'Vampires',
    value: 'blood',
    icon: <Icon icon={IconType.WARNING} color="var(--color-text-warning)" />,
  },
  {
    label: 'Zombies',
    value: 'brains',
    icon: <Icon icon={IconType.ERROR} color="var(--color-text-error)" />,
  },
];

export const actions: Action[] = [
  {
    label: 'Delete',
    icon: 'delete',
    onClick: () => {},
  },
  {
    label: 'Rename',
    icon: 'rename',
    onClick: () => {},
  },
];

export const optionsWithActions: Option[] = [
  {
    label: 'Kangaroos',
    value: 'grass',
    actions,
  },
  {
    label: 'Monkeys',
    value: 'bananas',
    actions,
  },
  {
    label: 'Possums',
    value: 'slugs',
    actions,
    details: '(some details)',
  },
];

export const optionsWithEverything: Option[] = [
  {
    label: 'Kangaroos',
    value: 'grass',
    actions,
    icon: <Icon icon={IconType.WARNING} color="var(--color-text-warning)" />,
    details: '(grass)',
  },
  {
    label: 'Monkeys',
    value: 'bananas',
    actions,
    icon: <Icon icon={IconType.WARNING} color="var(--color-text-warning)" />,
    details: '(bananas)',
  },
  {
    label: 'Possums',
    value: 'slugs',
    actions,
    icon: <Icon icon={IconType.WARNING} color="var(--color-text-warning)" />,
    details: '(slugs)',
  },
];

export const optionsLong: Option[] = [...Array(10000).keys()].map((o, i) => ({
  label: Math.random().toString(36).substring(7),
  value: i,
}));

export const optionsSimple: string[] = [
  'Aardvarks',
  'Kangaroos',
  'Monkeys',
  'Possums',
];
