import React, { ReactNode } from 'react';
import cx from 'classnames';
import { TStringOrNumber } from 'typings/common-type-definitions';
import styles from './badge.module.less';

const isDark = (hexColor: string): boolean => {
  const c = hexColor.substring(1); // strip #
  const rgb = parseInt(c, 16); // convert RRGGBB to decimal
  /* eslint-disable no-bitwise */
  const r = (rgb >> 16) & 0xff; // extract red
  const g = (rgb >> 8) & 0xff; // extract green
  const b = (rgb >> 0) & 0xff; // extract blue
  /* eslint-enable no-bitwise */
  const luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
  return luma < 128;
};

export interface IBadgeProps {
  children?: ReactNode;
  color?: string;
  title?: TStringOrNumber | ReactNode | null;
  dot?: boolean;
  margin?: TStringOrNumber | null;
  small?: boolean;
}

export const Badge = ({
  children,
  color = '',
  title = null,
  dot = false,
  margin = null,
  small = false,
}: IBadgeProps) => {
  const visible = !(title === null && dot === false);
  return (
    <div className={styles.wrapper}>
      {visible && (
        <sup
          className={cx(
            styles.badge,
            isDark(color) ? styles.dark : styles.light,
            title !== null ? styles.label : styles.dot,
            small ? styles.small : '',
            children ? styles.hasChildren : '',
          )}
          style={{
            background: color || undefined,
            margin: margin || undefined,
          }}
        >
          {title}
        </sup>
      )}
      {children}
    </div>
  );
};
