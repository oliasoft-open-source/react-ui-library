import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { Badge, IBadgeProps } from './badge';
import { Button } from '../button/button';

export default {
  title: 'Basic/Badge',
  component: Badge,
  args: {
    title: '3',
    color: '',
    small: false,
    children: <Button name="example" label="Button" onClick={() => {}} />,
  },
} as Meta<IBadgeProps>;

const Template: StoryFn<IBadgeProps> = (args) => <Badge {...args} />;

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const Dot = Template.bind({});
Dot.args = { dot: true, title: undefined };

export const Standalone = Template.bind({});
Standalone.args = { children: undefined };

export const TestNoTitle = Template.bind({});
TestNoTitle.args = { title: null };

export const TestSmallNoTitle = Template.bind({});
TestSmallNoTitle.args = { small: true, title: null };
