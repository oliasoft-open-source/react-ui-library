import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter, Link, Route, Routes } from 'react-router-dom';
import { Flex } from '../layout/flex/flex';
import { Button, IButtonProps } from './button';
import { ButtonType } from './types';

export default {
  title: 'Forms/Button',
  component: Button,
  args: {
    label: 'Label',
  },
} as Meta;

const Template: StoryFn<IButtonProps> = (args) => (
  <Flex gap="var(--padding-sm)">
    <Button {...args} />
    <Button colored {...args} />
    <Button colored="success" {...args} />
    <Button colored="danger" {...args} />
  </Flex>
);

export const Default = Template.bind({});

export const Small = Template.bind({});
Small.args = { small: true };

export const Basic = Template.bind({});
Basic.args = { basic: true };

export const Active = Template.bind({});
Active.args = { active: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const WithIcon = Template.bind({});
WithIcon.args = { icon: 'delete' };

export const Round = Template.bind({});
Round.args = { round: true, icon: 'delete', label: undefined };

export const RoundMuted = Template.bind({});
RoundMuted.args = {
  round: true,
  icon: 'delete',
  label: undefined,
  colored: 'muted',
  basic: true,
};

export const FullWidth = Template.bind({});
FullWidth.args = { width: '100%' };

export const Submit = Template.bind({});
Submit.args = { type: ButtonType.SUBMIT };

export const TestSmallButtonWrapping = Template.bind({});
TestSmallButtonWrapping.args = {
  label: 'Lorem ipsum dolor est compendum',
  width: '100px',
  small: true,
};
export const Tooltip = Template.bind({});
Tooltip.args = { tooltip: 'Tooltip goes here' };

export const TestWrapping = Template.bind({});
TestWrapping.args = {
  label: 'Long label goes here lorem ipsum dolor est lorem ipsum',
  width: '200px',
};

export const TestWrappingSmall = Template.bind({});
TestWrappingSmall.args = {
  label: 'Long label goes here lorem ipsum dolor est lorem ipsum',
  width: '200px',
  small: true,
};

export const AsRouterLink = () => (
  <MemoryRouter>
    <Flex gap>
      <Button component={Link} url="/dashboard" label="Dashboard" />
      <Button component={Link} url="/settings" label="Settings" />
    </Flex>
    <Routes>
      <Route path="/dashboard" element={<div>Dashboard</div>} />
      <Route path="/settings" element={<div>Settings</div>} />
    </Routes>
  </MemoryRouter>
);
