export enum ButtonType {
  SUBMIT = 'submit',
  BUTTON = 'button',
  RESET = 'reset',
}

export type TButtonType = 'submit' | 'button' | 'reset';
