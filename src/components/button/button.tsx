import React, { MouseEvent, ReactNode, useContext } from 'react';
import cx from 'classnames';
import { Color, GroupOrder, TGroupOrder } from 'typings/common-types';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import buttonStyles from './button.module.less';
import { Spinner } from '../spinner/spinner';
import { Icon } from '../icon/icon';
import { Tooltip } from '../tooltip/tooltip';
import { ButtonType, TButtonType } from './types';

export interface IButtonTooltipWrapperProps {
  children: ReactNode;
  tooltip?: ReactNode | string;
}

const ButtonTooltipWrapper = ({
  children,
  tooltip = null,
}: IButtonTooltipWrapperProps) => {
  if (!tooltip) {
    return <>{children}</>;
  }

  return (
    <Tooltip text={tooltip} display="inline-flex">
      {children}
    </Tooltip>
  );
};

export interface IButtonProps {
  active?: boolean; // Show active state (non-colored buttons only)
  basic?: boolean; // Link style button
  colored?: boolean | string;
  disabled?: boolean;
  ignoreDisabledContext?: boolean;
  groupOrder?: TGroupOrder; // Position of button in `ButtonGroup` or `InputGroup`
  icon?: ReactNode | string; // deprecated
  label?: ReactNode | string | null;
  loading?: boolean; // Adds activity indicator
  name?: string | null;
  onClick?: (evt: MouseEvent<HTMLButtonElement>) => void;
  pill?: boolean; // *Deprecated*
  round?: boolean;
  small?: boolean;
  styles?: string;
  width?: TStringOrNumber;
  title?: string;
  type?: TButtonType | string;
  error?: string;
  warning?: string;
  testId?: string;
  tooltip?: ReactNode | string;
  inverted?: boolean; // *Deprecated*
  component?: React.ElementType;
  url?: string;
  tabIndex?: number;
}

export const Button = ({
  active = false,
  basic = false,
  colored = false,
  disabled = false,
  ignoreDisabledContext = false,
  groupOrder,
  icon = null,
  label = '',
  loading = false,
  name,
  pill = false, // *Deprecated*
  round = false,
  small = false,
  styles = '',
  width = '',
  title = '',
  type = ButtonType.BUTTON,
  onClick = () => {},
  error,
  warning,
  testId,
  tooltip,
  inverted = false, // *Deprecated*
  component: Component = 'button',
  url,
  tabIndex,
}: IButtonProps) => {
  const disabledContext = useContext(DisabledContext);

  const color = (() => {
    if (colored) {
      switch (colored) {
        case Color.DANGER:
        case Color.RED:
          return buttonStyles.red;
        case Color.SUCCESS:
        case Color.GREEN:
          return buttonStyles.green;
        case Color.MUTED:
          return buttonStyles.muted;
        default:
          return buttonStyles.orange;
      }
    }
    return '';
  })();

  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case GroupOrder.FIRST:
          return buttonStyles.groupOrderFirst;
        case GroupOrder.LAST:
          return buttonStyles.groupOrderLast;
        default:
          return buttonStyles.groupOrderMiddle;
      }
    }
    return '';
  })();

  const isDisabled = disabled || (disabledContext && !ignoreDisabledContext);

  return (
    <ButtonTooltipWrapper tooltip={tooltip}>
      <Component
        type={
          type === ButtonType.SUBMIT ? ButtonType.SUBMIT : ButtonType.BUTTON
        }
        className={cx(
          buttonStyles.button,
          active ? buttonStyles.active : '',
          basic ? buttonStyles.basic : '',
          color,
          isDisabled ? buttonStyles.disabled : '',
          (icon || loading) && !label ? buttonStyles.iconOnly : '',
          inverted ? buttonStyles.inverted : '',
          order,
          pill ? buttonStyles.pill : '',
          round ? buttonStyles.round : '',
          small ? buttonStyles.small : '',
          styles,
        )}
        disabled={isDisabled}
        name={name ?? ''}
        onClick={onClick}
        {...(title ? { title } : {})} // eslint-disable-line react/jsx-props-no-spreading
        style={{ width }}
        data-error={error || null}
        data-warning={warning || null}
        data-testid={testId}
        to={url}
        tabIndex={tabIndex}
      >
        {icon && (
          <span className={buttonStyles.icon}>
            <Icon icon={icon} />
          </span>
        )}
        {loading && (
          <span className={buttonStyles.icon}>
            <Spinner
              small={!small /*regular button use small spinner*/}
              tiny={small /*small button use tiny spinner*/}
              dark={!colored && !basic}
              colored={basic}
            />
          </span>
        )}
        {label}
      </Component>
    </ButtonTooltipWrapper>
  );
};
