import React from 'react';
import { FaAngleDoubleRight } from 'react-icons/fa';
import { StoryFn } from '@storybook/react';
import svgIcon from '../../images/icons/icon-custom.svg';
import { Icon, IconProps } from './icon';
import { Grid } from '../layout/grid/grid';
import { Flex } from '../layout/flex/flex';
import { Text } from '../text/text';
import { iconNames } from './named-icon';

export default {
  title: 'Basic/Icon',
  component: Icon,
  argTypes: {
    color: { control: 'text' },
    size: { control: 'text' },
  },
  args: {
    icon: 'settings',
  },
};

const Template: StoryFn<IconProps> = (args: IconProps) => <Icon {...args} />;

export const Default = Template.bind({});

export const NamedIcons = () => (
  <Grid gap columns="1fr 1fr 1fr">
    {iconNames.map((icon) => (
      <Flex gap="var(--padding-xs)" key={icon}>
        <Icon icon={icon} />
        <Text>{icon}</Text>
      </Flex>
    ))}
  </Grid>
);

export const Size = Template.bind({});
Size.args = { size: 'var(--size-lg)' };

export const ReactIcon = Template.bind({});
ReactIcon.args = { icon: <FaAngleDoubleRight /> };

export const Color = Template.bind({});
Color.args = { color: 'var(--color-text-primary)' };

export const CustomSVG = Template.bind({});
CustomSVG.args = {
  icon: svgIcon,
  size: 'var(--size-lg)',
  color: 'var(--color-text-primary)',
};

export const Base64SVG = Template.bind({});
Base64SVG.args = {
  icon: `data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMzAgMzAiIHdpZHRoPSIzMHB4IiBoZWlnaHQ9IjMwcHgiPgogICAgPHBhdGggZmlsbD0iY3VycmVudENvbG9yIiBkPSJNIDUgNCBDIDQuNDQ4IDQgNCA0LjQ0OCA0IDUgTCA0IDkgQyA0IDkuMjU2IDQuMDk3OTY4NyA5LjUxMjAzMTIgNC4yOTI5Njg4IDkuNzA3MDMxMiBDIDQuNjgyOTY4NyAxMC4wOTgwMzEgNS4zMTYwMzEyIDEwLjA5ODAzMSA1LjcwNzAzMTIgOS43MDcwMzEyIEwgNyA4LjQxNDA2MjUgTCAxMS40MTQwNjIgMTIuODI4MTI1IEMgMTEuNzg5OTI0IDEzLjIwMzk4NyAxMiAxMy43MTA4NDQgMTIgMTQuMjQyMTg4IEwgMTIgMjAgQSAxLjAwMDEgMS4wMDAxIDAgMSAwIDE0IDIwIEwgMTQgMTQuMjQyMTg4IEMgMTQgMTMuMTgxNTMxIDEzLjU3ODI2MyAxMi4xNjQyMDIgMTIuODI4MTI1IDExLjQxNDA2MiBMIDguNDE0MDYyNSA3IEwgOS43MDcwMzEyIDUuNzA3MDMxMiBDIDEwLjA5ODAzMSA1LjMxNzAzMTMgMTAuMDk4MDMxIDQuNjgzOTY4OCA5LjcwNzAzMTIgNC4yOTI5Njg4IEMgOS41MTIwMzEyIDQuMDk3OTY4NyA5LjI1NiA0IDkgNCBMIDUgNCB6IE0gMjEgNCBDIDIwLjc0NCA0IDIwLjQ4Nzk2OSA0LjA5Nzk2ODcgMjAuMjkyOTY5IDQuMjkyOTY4OCBDIDE5LjkwMTk2OSA0LjY4Mzk2ODggMTkuOTAxOTY5IDUuMzE3MDMxMyAyMC4yOTI5NjkgNS43MDcwMzEyIEwgMjEuNTg1OTM4IDcgTCAxNy4xNzE4NzUgMTEuNDE0MDYyIEMgMTYuNDIxNzM3IDEyLjE2NDIwMiAxNiAxMy4xODE1MyAxNiAxNC4yNDIxODggTCAxNiAyMCBBIDEuMDAwMSAxLjAwMDEgMCAxIDAgMTggMjAgTCAxOCAxNC4yNDIxODggQyAxOCAxMy43MTA4NDMgMTguMjEwMDc2IDEzLjIwMzk4NyAxOC41ODU5MzggMTIuODI4MTI1IEwgMjMgOC40MTQwNjI1IEwgMjQuMjkyOTY5IDkuNzA3MDMxMiBDIDI0LjY4Mzk2OSAxMC4wOTgwMzEgMjUuMzE3MDMxIDEwLjA5ODAzMSAyNS43MDcwMzEgOS43MDcwMzEyIEMgMjUuOTAyMDMxIDkuNTEyMDMxMiAyNiA5LjI1NiAyNiA5IEwgMjYgNSBDIDI2IDQuNDQ4IDI1LjU1MiA0IDI1IDQgTCAyMSA0IHogTSA0Ljk4NDM3NSAxMi45ODYzMjggQSAxLjAwMDEgMS4wMDAxIDAgMCAwIDQgMTQgTCA0IDI0IEMgNCAyNS4wOTMwNjMgNC45MDY5MzcyIDI2IDYgMjYgTCAyNCAyNiBDIDI1LjA5MzA2MyAyNiAyNiAyNS4wOTMwNjMgMjYgMjQgTCAyNiAxNCBBIDEuMDAwMSAxLjAwMDEgMCAxIDAgMjQgMTQgTCAyNCAyNCBMIDYgMjQgTCA2IDE0IEEgMS4wMDAxIDEuMDAwMSAwIDAgMCA0Ljk4NDM3NSAxMi45ODYzMjggeiIvPgo8L3N2Zz4K`,
  size: 'var(--size-lg)',
  color: 'var(--color-text-primary)',
};

export const Clickable = Template.bind({});
Clickable.args = { onClick: () => {} };
