import React, {
  isValidElement,
  ReactNode,
  CSSProperties,
  MouseEventHandler,
  useContext,
} from 'react';
import cx from 'classnames';
import { ReactSVG } from 'react-svg';
import { TStringOrNumber } from 'typings/common-type-definitions';
import { DisabledContext } from 'helpers/disabled-context';
import styles from './icon.module.less';
import { NamedIcon } from './named-icon';

export interface IconProps {
  icon: ReactNode | string;
  onClick?: MouseEventHandler<HTMLDivElement>;
  clickable?: boolean;
  color?: string;
  size?: TStringOrNumber;
  testId?: string;
}

/**
 * Icon wrapper component to support different icon formats:
 *     - string names (default recommendation)
 *     - react-icons
 *     - SVG (for special cases)
 */
export const Icon = ({
  icon,
  onClick,
  clickable = false,
  color,
  size,
  testId,
}: IconProps) => {
  const disabledContext = useContext(DisabledContext);

  const style: CSSProperties = {
    color,
    fill: color,
    fontSize: size || 'inherit',
    width: size || 'auto',
    height: size || 'auto',
  };

  return (
    <div
      className={cx(
        styles.wrapper,
        (clickable || onClick) && !disabledContext ? styles.clickable : '',
      )}
      style={style}
      onClick={(evt) =>
        onClick && !disabledContext ? onClick(evt) : undefined
      }
      data-testid={testId}
    >
      {isValidElement(icon) ? (
        icon
      ) : typeof icon === 'string' &&
        (icon.includes('.svg') || icon.includes('image/svg')) ? (
        <ReactSVG
          className={styles.customSvg}
          beforeInjection={(svg) => {
            if (size) {
              svg.setAttribute('width', String(size));
              svg.setAttribute('height', String(size));
            }
          }}
          src={icon}
        />
      ) : typeof icon === 'string' &&
        (icon.includes('.png') || icon.includes('image/png')) ? (
        <img alt="icon" className={styles.customPng} src={icon} />
      ) : typeof icon === 'string' ? (
        <NamedIcon icon={icon} />
      ) : null}
    </div>
  );
};
