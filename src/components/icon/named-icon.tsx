import React from 'react';
import {
  TbAlertTriangle,
  TbArrowDown,
  TbArrowLeft,
  TbArrowRight,
  TbArrowUp,
  TbArrowsDiagonal,
  TbArrowsMaximize,
  TbArrowsMinimize,
  TbArrowsMove,
  TbArrowsSplit,
  TbBell,
  TbBold,
  TbBolt,
  TbBook2,
  TbBuilding,
  TbCamera,
  TbChartBar,
  TbChartLine,
  TbChartPie2,
  TbCheck,
  TbChevronDown,
  TbChevronLeft,
  TbChevronRight,
  TbChevronUp,
  TbChevronsLeft,
  TbChevronsRight,
  TbCircle,
  TbCircleCheck,
  TbCircleX,
  TbClipboard,
  TbClock,
  TbCode,
  TbCopy,
  TbCornerDownRight,
  TbDeviceFloppy,
  TbDeviceLaptop,
  TbDotsVertical,
  TbDownload,
  TbDroplet,
  TbExclamationMark,
  TbExternalLink,
  TbEye,
  TbEyeOff,
  TbFile,
  TbFilter,
  TbFilterOff,
  TbFocusCentered,
  TbFolder,
  TbFolderSearch,
  TbGauge,
  TbGitFork,
  TbGripVertical,
  TbHelp,
  TbInfoCircle,
  TbItalic,
  TbKey,
  TbLayoutColumns,
  TbLayoutGrid,
  TbLayoutRows,
  TbLink,
  TbLinkOff,
  TbList,
  TbListNumbers,
  TbLoader2,
  TbLock,
  TbLockOpen,
  TbMapPin,
  TbMessageCircle,
  TbMinus,
  TbMoon,
  TbMovie,
  TbNote,
  TbPencil,
  TbPlayerPause,
  TbPlayerPlay,
  TbPlayerSkipBack,
  TbPlayerSkipForward,
  TbPlayerStop,
  TbPlayerTrackNext,
  TbPlayerTrackPrev,
  TbPlus,
  TbQuestionMark,
  TbRefresh,
  TbReload,
  TbRotate,
  TbRowInsertBottom,
  TbRowInsertTop,
  TbRuler3,
  TbScale,
  TbSearch,
  TbServer,
  TbSettings,
  TbSortAscending,
  TbSortDescending,
  TbSquare,
  TbStar,
  TbSun,
  TbTable,
  TbTarget,
  TbTrash,
  TbUpload,
  TbUser,
  TbUsers,
  TbX,
} from 'react-icons/tb';
import { VscCollapseAll, VscExpandAll } from 'react-icons/vsc';
import { AiOutlinePlusSquare, AiOutlineMinusSquare } from 'react-icons/ai';

export const iconNames = [
  'add',
  'plus',
  'add above',
  'add below',
  'arrow down',
  'down',
  'arrow left',
  'left',
  'arrow right',
  'right',
  'arrow up',
  'up',
  'bold',
  'book',
  'library',
  'camera',
  'clear',
  'close',
  'times',
  'chart',
  'chart line',
  'chart bar',
  'chart pie',
  'chat',
  'check',
  'checkmark',
  'chevron up',
  'chevron down',
  'chevron left',
  'chevron right',
  'chevron double left',
  'chevron double right',
  'clock',
  'clone',
  'duplicate',
  'code',
  'collapse',
  'collapse all',
  'company',
  'compare',
  'copy',
  'delete',
  'trash',
  'depth',
  'download',
  'export',
  'drag',
  'edit',
  'pencil',
  'rename',
  'electricity',
  'error',
  'exclamation',
  'expand',
  'expand all',
  'external',
  'fast forward',
  'fast backward',
  'file',
  'filter',
  'focus',
  'folder',
  'folder search',
  'fork',
  'gauge',
  'help',
  'hide',
  'indent',
  'info',
  'italic',
  'key',
  'laptop',
  'layout columns',
  'columns',
  'layout grid',
  'grid',
  'layout rows',
  'rows',
  'layout single',
  'square',
  'link',
  'list',
  'legend',
  'ul',
  'list numbers',
  'ol',
  'location',
  'lock',
  'maximize',
  'menu',
  'minimize',
  'minus',
  'moon',
  'move',
  'note',
  'notification',
  'bell',
  'paste',
  'pause',
  'play',
  'question',
  'redo',
  'rotate',
  'rotate right',
  'refresh',
  'sync',
  'save',
  'scale',
  'search',
  'server',
  'settings',
  'show',
  'eye',
  'sort',
  'sort descending',
  'sort down',
  'sort amount down',
  'sort ascending',
  'sort up',
  'sort amount up',
  'spinner',
  'split',
  'star',
  'step backward',
  'step forward',
  'stop',
  'success',
  'sun',
  'table',
  'target',
  'undo',
  'rotate left',
  'unfilter',
  'unlink',
  'unlock',
  'upload',
  'import',
  'user',
  'users',
  'group',
  'video',
  'warning',
  'alert',
  'water',
  'droplet',
];

interface NamedIconProps {
  icon: string;
}

export const NamedIcon = ({ icon }: NamedIconProps) => {
  switch (icon) {
    case 'add':
    case 'plus':
      return <TbPlus />;
    case 'add above':
      return <TbRowInsertTop />;
    case 'add below':
      return <TbRowInsertBottom />;
    case 'arrow down':
    case 'down':
      return <TbArrowDown />;
    case 'arrow left':
    case 'left':
      return <TbArrowLeft />;
    case 'arrow right':
    case 'right':
      return <TbArrowRight />;
    case 'arrow up':
    case 'up':
      return <TbArrowUp />;
    case 'bold':
      return <TbBold />;
    case 'book':
    case 'library':
      return <TbBook2 />;
    case 'camera':
      return <TbCamera />;
    case 'close':
    case 'clear':
    case 'times':
      return <TbX />;
    case 'chart':
    case 'chart line':
      return <TbChartLine />;
    case 'chart bar':
      return <TbChartBar />;
    case 'chart pie':
      return <TbChartPie2 />;
    case 'chat':
      return <TbMessageCircle />;
    case 'check':
    case 'checkmark':
      return <TbCheck />;
    case 'chevron up':
      return <TbChevronUp />;
    case 'chevron down':
      return <TbChevronDown />;
    case 'chevron left':
      return <TbChevronLeft />;
    case 'chevron right':
      return <TbChevronRight />;
    case 'chevron double left':
      return <TbChevronsLeft />;
    case 'chevron double right':
      return <TbChevronsRight />;
    case 'clock':
      return <TbClock />;
    case 'clone':
    case 'copy':
    case 'duplicate':
      return <TbCopy />;
    case 'code':
      return <TbCode />;
    case 'collapse':
      return <AiOutlineMinusSquare />; // Replace with TbSquareMinus when react-icons is updated
    case 'collapse all':
      return <VscCollapseAll />; // Replace with TbCopyMinus when react-icons is updated
    case 'company':
      return <TbBuilding />;
    case 'compare':
      return <TbScale />;
    case 'delete':
    case 'trash':
      return <TbTrash />;
    case 'depth':
      return <TbRuler3 style={{ transform: 'rotate(-90deg)' }} />;
    case 'download':
    case 'export':
      return <TbDownload />;
    case 'drag':
      return <TbGripVertical />;
    case 'edit':
    case 'pencil':
    case 'rename':
      return <TbPencil />;
    case 'electricity':
      return <TbBolt />;
    case 'error':
      return <TbCircleX />;
    case 'exclamation':
      return <TbExclamationMark />;
    case 'expand':
      return <AiOutlinePlusSquare />; // Replace with TbSquarePlus when react-icons is updated
    case 'expand all':
      return <VscExpandAll />; // Replace with TbCopyPlus when react-icons is updated
    case 'external':
      return <TbExternalLink />;
    case 'fast forward':
      return <TbPlayerTrackNext />;
    case 'fast backward':
      return <TbPlayerTrackPrev />;
    case 'file':
      return <TbFile />;
    case 'filter':
      return <TbFilter />;
    case 'focus':
      return <TbFocusCentered />;
    case 'folder':
      return <TbFolder />;
    case 'folder search':
      return <TbFolderSearch />;
    case 'fork':
      return <TbGitFork />;
    case 'gauge':
      return <TbGauge />;
    case 'help':
      return <TbHelp />;
    case 'hide':
      return <TbEyeOff />;
    case 'indent':
      return <TbCornerDownRight />;
    case 'info':
      return <TbInfoCircle />;
    case 'italic':
      return <TbItalic />;
    case 'key':
      return <TbKey />;
    case 'laptop':
      return <TbDeviceLaptop />;
    case 'layout columns':
    case 'columns':
      return <TbLayoutColumns />;
    case 'layout grid':
    case 'grid':
      return <TbLayoutGrid />;
    case 'layout rows':
    case 'rows':
      return <TbLayoutRows />;
    case 'layout single':
    case 'square':
      return <TbSquare />;
    case 'link':
      return <TbLink />;
    case 'list':
    case 'legend':
    case 'ul':
      return <TbList />;
    case 'list numbers':
    case 'ol':
      return <TbListNumbers />;
    case 'location':
      return <TbMapPin />;
    case 'lock':
      return <TbLock />;
    case 'maximize':
      return <TbArrowsMaximize />;
    case 'menu':
      return <TbDotsVertical />;
    case 'minimize':
      return <TbArrowsMinimize />;
    case 'minus':
      return <TbMinus />;
    case 'moon':
      return <TbMoon />;
    case 'move':
      return <TbArrowsMove />;
    case 'note':
      return <TbNote />;
    case 'notification':
    case 'bell':
      return <TbBell />;
    case 'paste':
      return <TbClipboard />;
    case 'pause':
      return <TbPlayerPause />;
    case 'play':
      return <TbPlayerPlay />;
    case 'question':
      return <TbQuestionMark />;
    case 'redo':
    case 'rotate':
    case 'rotate right':
      return <TbReload />;
    case 'refresh':
    case 'sync':
      return <TbRefresh />;
    case 'save':
      return <TbDeviceFloppy />;
    case 'scale':
      return <TbArrowsDiagonal />;
    case 'search':
      return <TbSearch />;
    case 'server':
      return <TbServer />;
    case 'settings':
      return <TbSettings />;
    case 'show':
    case 'eye':
      return <TbEye />;
    case 'sort':
    case 'sort descending':
    case 'sort down':
    case 'sort amount down':
      return <TbSortDescending />;
    case 'sort ascending':
    case 'sort up':
    case 'sort amount up':
      return <TbSortAscending />;
    case 'spinner':
      return <TbLoader2 />;
    case 'split':
      return <TbArrowsSplit />;
    case 'star':
      return <TbStar />;
    case 'step backward':
      return <TbPlayerSkipBack />;
    case 'step forward':
      return <TbPlayerSkipForward />;
    case 'stop':
      return <TbPlayerStop />;
    case 'success':
      return <TbCircleCheck />;
    case 'sun':
      return <TbSun />;
    case 'table':
      return <TbTable />;
    case 'target':
      return <TbTarget />;
    case 'undo':
    case 'rotate left':
      return <TbRotate />;
    case 'unfilter':
      return <TbFilterOff />;
    case 'unlink':
      return <TbLinkOff />;
    case 'unlock':
      return <TbLockOpen />;
    case 'upload':
    case 'import':
      return <TbUpload />;
    case 'user':
      return <TbUser />;
    case 'users':
    case 'group':
      return <TbUsers />;
    case 'video':
      return <TbMovie />;
    case 'warning':
    case 'alert':
      return <TbAlertTriangle />;
    case 'water':
    case 'droplet':
      return <TbDroplet />;
    case '':
      return null;
    default:
      return <TbCircle />; //just a default icon so it's obvious when missing
  }
};
