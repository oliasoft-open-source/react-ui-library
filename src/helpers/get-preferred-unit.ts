import { unitFromQuantity } from '@oliasoft-open-source/units';

/**
 * Retrieves the preferred unit for a given quantity from a specified unit template or defaults to a standard unit if not found.
 * If the unitTemplate is undefined or does not contain a mapping for the unitkey, a console warning is issued, and a default unit is applied.
 */
export const getPreferredUnit = (
  unitkey: string,
  unitTemplate: Record<string, string> | undefined,
): string => {
  const preferredUnit = unitTemplate?.[unitkey];
  if (!preferredUnit) {
    console.log(
      `Warning: 'unitTemplate' missing for '${unitkey}'. Using initial unit, preferred unit or value unit.`,
    );
    return unitFromQuantity(unitkey) ?? '';
  }

  return preferredUnit;
};
