import { checkConversion } from 'helpers/check-conversion/check-conversion';

describe('checkConversion', () => {
  it('should return false if ALT_UNITS[unitKey] returns an empty array or undefined', () => {
    expect(
      checkConversion({ value: 10, unitkey: 'nonexistentKey', toUnit: 'm' }),
    ).toBe(false);
  });

  it('should return false if the value is not a valid number', () => {
    expect(
      checkConversion({ value: '1asdasd', unitkey: 'length', toUnit: 'm' }),
    ).toBe(false);
    expect(
      checkConversion({ value: NaN, unitkey: 'length', toUnit: 'm' }),
    ).toBe(false);
    expect(
      checkConversion({ value: Infinity, unitkey: 'length', toUnit: 'm' }),
    ).toBe(false);
  });

  it('should return false if toUnit is not in the availableUnits for the provided unitKey', () => {
    expect(
      checkConversion({ value: 10, unitkey: 'length', toUnit: 'bar' }),
    ).toBe(false);
  });

  it('should return true if all checks pass', () => {
    expect(checkConversion({ value: 10, unitkey: 'length', toUnit: 'm' })).toBe(
      true,
    );
    expect(
      checkConversion({ value: '20', unitkey: 'weight', toUnit: 'kg' }),
    ).toBe(true);
  });

  it('should return false if availableUnits are defined but the toUnit is not present', () => {
    expect(
      checkConversion({ value: 10, unitkey: 'weight', toUnit: 'stone' }),
    ).toBe(false);
  });
});
