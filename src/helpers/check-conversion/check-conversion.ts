import {
  ALT_UNITS,
  getUnit,
  validateNumber,
} from '@oliasoft-open-source/units';

interface ICheckConversionParams {
  value: number | string;
  unitkey: string;
  toUnit: string;
}

export const checkConversion = ({
  value,
  unitkey,
  toUnit,
}: ICheckConversionParams): boolean => {
  // Check if ALT_UNITS[unitKey] returns a non-empty array
  const availableUnits = ALT_UNITS[unitkey as keyof typeof ALT_UNITS];
  if (!availableUnits || availableUnits.length === 0) {
    return false;
  }

  // Check if value is a valid number (not infinite, NaN, etc.)
  if (!validateNumber(value)?.valid) {
    return false;
  }

  // Fail if `toUnit` is not in `availableUnits`
  if (!availableUnits.includes(toUnit)) {
    return false;
  }

  // Check if toUnit is included in the ALT_UNITS[unitKey]
  const valueUnit = getUnit(String(value));

  // Fail if `value` has a unit and that unit is not in `availableUnits`
  if (valueUnit && !availableUnits.includes(valueUnit)) {
    return false;
  }

  // All checks passed
  return true;
};
