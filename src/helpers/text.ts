export const getTextWidth = (text: string, font: string): number => {
  const element = document.createElement('canvas');
  const context = element.getContext('2d');
  if (!context) {
    return 0; // Fallback to 0 width if context isn't available
  }
  context.font = font;
  return context.measureText(text).width;
};
