import React from 'react';

export const DisabledContext = React.createContext<boolean>(false);
