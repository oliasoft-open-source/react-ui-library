import {
  toNum,
  toString,
  isScientificStringNum,
  roundToPrecision,
} from '@oliasoft-open-source/units';

const COSMETIC_ROUNDING_DEFAULT_PRECISION = 14;

export const safeToString = (value: string | number): string =>
  String(toString(value));

/*
  Optional Excel-style "cosmetic rounding" of display values (for numerical user inputs)

  This means rounding to slightly less precision than the underlying floating-point number type, to
  resolve floating-point "rounding noise" associated with storing values in one unit but converting
  and displaying them in another unit.

  This solution was evaluated by Tools Team in relation to tickets OW-10614 and OW-15745.

  - OW-15745 UnitTable (to solve "noisy audit log diffs in tables with different display and storage units")
  - OW-10614 NumberInput and UnitInput (to solve libraries rounding noise errors)
*/
export const roundNumberCosmetic = (value: string): string => {
  const isScientific = isScientificStringNum(value);
  return !isScientific
    ? safeToString(
        roundToPrecision(toNum(value), COSMETIC_ROUNDING_DEFAULT_PRECISION),
      )
    : value;
};
