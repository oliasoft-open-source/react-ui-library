import { roundNumberCosmetic } from './cosmetic-rounding';

describe('roundNumberCosmetic', () => {
  test('cosmetically rounds values with rounding noise', () => {
    expect(roundNumberCosmetic('2')).toBe('2');
    expect(roundNumberCosmetic('9750.000000000004')).toBe('9750');
  });
  test('handles small numbers OW-17645', () => {
    expect(roundNumberCosmetic('0.0002')).toBe('0.0002');
    expect(roundNumberCosmetic('0.0000002')).toBe('0.0000002');
  });
});
