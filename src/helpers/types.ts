import { isValidElement, ReactNode } from 'react';
import { isNumber, isString } from 'lodash';
import { TStringOrNumber } from 'typings/common-type-definitions';

export const isStringNumberOrNode = (
  value: TStringOrNumber | ReactNode,
): boolean => {
  return isString(value) || isNumber(value) || isValidElement(value);
};

export const getType = (item: any): string => {
  return Array.isArray(item) ? 'array' : typeof item;
};
