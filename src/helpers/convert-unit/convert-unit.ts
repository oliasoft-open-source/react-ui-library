import {
  convertAndGetValue,
  convertSamePrecision,
  getUnit,
  getValue,
  isValueWithUnit,
  unitFromQuantity,
  validateNumber,
  withUnit,
} from '@oliasoft-open-source/units';
import { isWrongValue } from 'components/unit-input/utils/is-wrong-value';
import { checkConversion } from 'helpers/check-conversion/check-conversion';
import { safeToString } from 'helpers/cosmetic-rounding/cosmetic-rounding';

export interface IConvertUnitProps {
  value: string;
  unitkey: string;
  toUnit: string;
  fromUnit?: string;
  doNotConvertValue?: boolean;
  exactPrecision?: boolean;
}

export interface IConvertUnitResult {
  value: string | undefined;
  error?: string | null;
}

/**
 * convertUnit is responsible for converting a given value from one unit to another.
 * - Validates the input value and units.
 * - Converts the value using the specified target unit.
 * - Returns the converted value or an error message if the conversion fails.
 */
export const convertUnit = ({
  value: inputValue,
  unitkey,
  toUnit,
  fromUnit,
  doNotConvertValue = false,
  exactPrecision = false,
}: IConvertUnitProps): IConvertUnitResult => {
  const value = isValueWithUnit(inputValue) ? getValue(inputValue) : inputValue;
  const isInfinity = [Infinity, -Infinity].includes(Number(value));

  if (doNotConvertValue) {
    return { value, error: null };
  }

  if (isInfinity) {
    return { value, error: null };
  }

  const defaultFromUnit =
    fromUnit ||
    (isValueWithUnit(inputValue)
      ? getUnit(inputValue)
      : unitFromQuantity(unitkey));

  if (!validateNumber(value)?.valid) {
    return { value: undefined, error: 'Invalid number format' };
  }

  if (!toUnit) {
    return { value: undefined, error: 'Target unit is missing' };
  }

  if (!defaultFromUnit) {
    return { value: undefined, error: 'Source unit is missing' };
  }

  if (isWrongValue(value)) {
    return { value: undefined, error: 'Value is invalid for conversion' };
  }

  try {
    const valueWithUnit = withUnit(value, defaultFromUnit);

    if (checkConversion({ value: valueWithUnit, toUnit, unitkey })) {
      const result = exactPrecision
        ? convertSamePrecision(valueWithUnit, toUnit)
        : convertAndGetValue(valueWithUnit, toUnit);
      return { value: getValue(safeToString(result)), error: null };
    }

    return { value: getValue(valueWithUnit), error: null };
  } catch (e) {
    return { value: undefined, error: 'Conversion not possible' };
  }
};
