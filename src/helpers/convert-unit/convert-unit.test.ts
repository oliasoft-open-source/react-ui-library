import {
  convertUnit,
  IConvertUnitProps,
} from 'helpers/convert-unit/convert-unit';

describe('convertUnit', () => {
  it('should return the input value if doNotConvertValue is true', () => {
    const mockProps: IConvertUnitProps = {
      value: '10',
      unitkey: 'length',
      toUnit: 'ft',
      fromUnit: 'm',
      doNotConvertValue: true,
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({ value: mockProps.value, error: null });
  });

  it('should return an error if the value is not a valid number', () => {
    const mockProps: IConvertUnitProps = {
      value: '123asd',
      unitkey: 'length',
      toUnit: 'ft',
      fromUnit: 'm',
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({
      value: undefined,
      error: 'Invalid number format',
    });
  });

  it('should return an error if the target unit is missing', () => {
    const mockProps: IConvertUnitProps = {
      value: '10',
      unitkey: 'length',
      toUnit: '',
      fromUnit: 'm',
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({
      value: undefined,
      error: 'Target unit is missing',
    });
  });

  it('should return converted value to fallback unit', () => {
    const mockProps: IConvertUnitProps = {
      value: '10',
      unitkey: 'length',
      toUnit: 'ft',
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({ value: '32.808398950131235', error: null });
  });

  it('should convert the value and return the result if conversion is successful', () => {
    const mockProps: IConvertUnitProps = {
      value: '10',
      unitkey: 'length',
      toUnit: 'ft',
      fromUnit: 'm',
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({ value: '32.808398950131235', error: null });
  });

  it('should return the original value with unit if no conversion is needed', () => {
    const mockProps: IConvertUnitProps = {
      value: '10',
      unitkey: 'length',
      toUnit: 'm', // Same as fromUnit, so no conversion needed
      fromUnit: 'm',
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({ value: '10', error: null });
  });

  it('should convert the value with the same precision if preservePrecision is true', () => {
    const mockProps: IConvertUnitProps = {
      value: '10',
      unitkey: 'length',
      toUnit: 'ft',
      fromUnit: 'm',
      exactPrecision: true,
    };

    const result = convertUnit(mockProps);
    expect(result).toEqual({ value: '32.8', error: null });
  });
});
