import { Context, createContext, useContext } from 'react';

type UnitContextType = any;

let UnitContext: Context<UnitContextType | null> =
  createContext<UnitContextType | null>(null);

export const useUnitContext = () => {
  return useContext(UnitContext);
};

let isInitialized = false;

export const initializeContext = (context: Context<UnitContextType | null>) => {
  if (!isInitialized) {
    isInitialized = true;
    UnitContext = context;
  }
};
