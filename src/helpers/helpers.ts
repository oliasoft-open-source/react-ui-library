export const isEmptyNullOrUndefined = (value: any): boolean =>
  value === null || value === undefined || value === '';
