import React from 'react';
import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';

import { Main } from '../../views/main/main';

export const AppRoutes = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Main />} />
      </Routes>
    </Router>
  );
};
