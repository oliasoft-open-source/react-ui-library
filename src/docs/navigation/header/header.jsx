import React from 'react';
import { Link } from 'react-router-dom';
import { ImMobile } from 'react-icons/im';
import { SiStorybook } from 'react-icons/si';
import { TopBar } from '../../../index';
import { baseUrl } from '../../config/config';
import styles from './header.module.less';

export const Header = () => {
  const storybookUrl = `${baseUrl}storybook/`;
  return (
    <TopBar
      content={[
        {
          type: 'Component',
          component: (
            <span className={styles.link}>
              <Link to="/">
                <ImMobile color="var(--color-text-primary)" size={40} />
                <span>React UI Library</span>
              </Link>
            </span>
          ),
        },
      ]}
      contentRight={[
        {
          type: 'Button',
          label: 'Storybook',
          icon: <SiStorybook />,
          onClick: () => {
            const win = window.open(storybookUrl, '_blank');
            win.focus();
          },
        },
      ]}
    />
  );
};
