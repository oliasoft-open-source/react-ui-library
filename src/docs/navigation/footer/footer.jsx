import React from 'react';
import { RiGitlabFill } from 'react-icons/ri';
import { ImMobile } from 'react-icons/im';
import { Spacer, TextLink, Text, Flex, Heading, Divider } from '../../../index';

export const Footer = () => {
  return (
    <>
      <Divider />
      <Flex direction="column" alignItems="center">
        <Heading top marginBottom={0}>
          <Flex alignItems="center">
            <ImMobile size={20} />
            <Spacer width="var(--padding-xxs)" />
            React UI Library
          </Flex>
        </Heading>
        <Spacer />
        <Text muted>
          &copy; {new Date().getFullYear()} Oliasoft AS and contributors
        </Text>
        <Spacer />
        <Flex>
          <TextLink href="https://gitlab.com/oliasoft-open-source/react-ui-library/-/blob/master/LICENSE">
            Code licensed MIT
          </TextLink>
          <Spacer width="var(--padding)" />
          <TextLink href="https://gitlab.com/oliasoft-open-source/react-ui-library">
            <Flex alignItems="center">
              <RiGitlabFill />
              <Spacer width="var(--padding-xxs)" />
              Gitlab
            </Flex>
          </TextLink>
          <Spacer width="var(--padding)" />
          <TextLink href="https://www.oliasoft.com/">Oliasoft</TextLink>
        </Flex>
      </Flex>
    </>
  );
};
