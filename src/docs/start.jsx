import React from 'react';
import ReactDOM from 'react-dom/client';

import { AppRoutes } from './navigation/routes/routes';

import '../style/global.less';

const Root = () => {
  return <AppRoutes />;
};

const root = ReactDOM.createRoot(document.getElementById('content'));
root.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
);
