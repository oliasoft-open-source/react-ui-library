import React from 'react';
import {
  FaCode,
  FaCogs,
  FaWpforms,
  FaExternalLinkAlt,
  FaRegWindowMaximize,
  FaListUl,
  FaPaintBrush,
} from 'react-icons/fa';
import { BsFillGridFill } from 'react-icons/bs';
import { MdTabletMac } from 'react-icons/md';
import { RiPagesFill, RiArchiveDrawerFill, RiChat2Line } from 'react-icons/ri';
import { SiStorybook } from 'react-icons/si';
import { TiInputChecked } from 'react-icons/ti';
import { AiOutlineUngroup } from 'react-icons/ai';
import { ImTable, ImSpinner8, ImBell } from 'react-icons/im';
import {
  Card,
  Spacer,
  Heading,
  TextLink,
  Text,
  Flex,
  Grid,
  Button,
} from '../../../index';
import { Page } from '../../components/page/page';
import { baseUrl } from '../../config/config';
import styles from './main.module.less';

const description = [
  {
    title: 'Comprehensive',
    icon: <BsFillGridFill size={36} />,
    text: (
      <>
        A <strong>comprehensive</strong> set of layout, navigation and control
        components for building modern web apps.
      </>
    ),
  },
  {
    title: 'Configurable',
    icon: <FaCogs size={36} />,
    text: (
      <>
        <strong>Flexible</strong>, configurable components with detailed usage
        examples via Storybook.
      </>
    ),
  },
  {
    title: 'Responsive',
    icon: <MdTabletMac size={36} />,
    text: (
      <>
        Easily build <strong>responsive</strong> layouts that work well on a
        variety of devices.
      </>
    ),
  },
  {
    title: 'Open-source',
    icon: <FaCode size={36} />,
    text: (
      <>
        Contribute and fork freely under a permissive,{' '}
        <strong>open-source</strong>{' '}
        <TextLink href="https://gitlab.com/oliasoft-open-source/react-ui-library/-/blob/master/LICENSE">
          MIT License.
        </TextLink>
      </>
    ),
  },
];

const features = [
  {
    title: 'Page Layouts',
    icon: <RiPagesFill size={36} />,
    text: 'Support for responsive layouts with Page, Row, Column, Card, and Accordion components.',
  },
  {
    title: 'Drawers',
    icon: <RiArchiveDrawerFill size={36} />,
    text: 'Easily support expandable drawers.',
  },
  {
    title: 'Navigation',
    icon: <FaExternalLinkAlt size={36} />,
    text: 'Flexible TopBar, SideBar, Tabs, Breadcrumbs, Pagination and other navigation components.',
  },
  {
    title: 'Inputs',
    icon: <TiInputChecked size={36} />,
    text: 'Comprehensive inputs including Selects, Checkboxes, Toggles, RadioButtons, Buttons, and Sliders.',
  },
  {
    title: 'Input Groups',
    icon: <AiOutlineUngroup size={36} />,
    text: 'Combine groups of inputs flexible ways, including text-label addons.',
  },
  {
    title: 'Form Layouts',
    icon: <FaWpforms size={36} />,
    text: 'Convenient stacked and row form layouts, with all inputs supported.',
  },
  {
    title: 'Notifications',
    icon: <ImBell size={36} />,
    text: 'Consistently-styled Messages and toast Messages.',
  },
  {
    title: 'Tables',
    icon: <ImTable size={36} />,
    text: 'A configurable table component with support for many input and custom cells.',
  },
  {
    title: 'Lists',
    icon: <FaListUl size={36} />,
    text: 'A configurable list component.',
  },
  {
    title: 'Progress',
    icon: <ImSpinner8 size={36} />,
    text: 'Flexible spinners and loaders.',
  },
  {
    title: 'Modals',
    icon: <FaRegWindowMaximize size={36} />,
    text: 'Simple, flexible Modals and Dialogs.',
  },
  {
    title: 'Modern Popover Engine',
    icon: <RiChat2Line size={36} />,
    text: (
      <>
        Uses a React-friendly positioning engine (
        <a href="https://www.react-laag.com/">React-Laag</a>) for popovers,
        tooltips menus, and custom selects.
      </>
    ),
  },
  {
    title: 'Flexible styling',
    icon: <FaPaintBrush size={36} />,
    text: (
      <>
        Supports the two most popular React styling approaches:{' '}
        <a href="https://github.com/css-modules/css-modules/">CSS Modules</a>{' '}
        and <a href="https://styled-components.com/">styled-components</a>.
      </>
    ),
  },
];

export const Main = () => {
  const storybookUrl = `${baseUrl}storybook/`;
  return (
    <Page>
      <div className={styles.container}>
        <Spacer height="100px" />
        <Flex direction="column">
          <h1 className={styles.headline}>
            User Interface Components for React Applications
          </h1>
          {description.map((item, index) => (
            <p className={styles.lead} key={index}>
              <Text muted>{item.text}</Text>
            </p>
          ))}
        </Flex>

        <Spacer height="30px" />

        <Button
          label="Storybook"
          colored
          icon={<SiStorybook />}
          onClick={() => {
            const win = window.open(storybookUrl, '_blank');
            win.focus();
          }}
        />

        <Spacer height="100px" />

        <Grid
          columns="1fr 1fr 1fr 1fr"
          columnsTablet="1fr 1fr"
          columnsMobile="1fr"
          gap
        >
          {features.map((item, index) => (
            <Card key={index}>
              <Text faint>{item.icon}</Text>
              <Spacer height="5px" />
              <Heading top marginBottom="5px">
                {item.title}
              </Heading>
              <Text muted>{item.text}</Text>
            </Card>
          ))}
        </Grid>
      </div>
    </Page>
  );
};
