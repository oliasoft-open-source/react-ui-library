import React from 'react';
import { Page as PageBase } from '../../../index';
import { Footer } from '../../navigation/footer/footer';
import { Header } from '../../navigation/header/header';

export const Page = ({ children }) => {
  return (
    <>
      <Header />
      <PageBase padding="var(--padding)" left={0} top={60}>
        {children}
        <Footer />
      </PageBase>
    </>
  );
};
