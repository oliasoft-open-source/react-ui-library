import { ChangeEvent, MouseEvent } from 'react';

// Complex Types
export type TObject = { [key: string]: any };
export type TArray = any[];

// Function Types
export type TFunction = (...args: any[]) => any;

// Generic Types
export type TObjectOf<T = any> = { [key: string]: T };
export type TArrayOf<T = any> = T[];
export type TFunctionWithParams<P extends any[] = any[], R = any> = (
  ...args: P
) => R;

// Combined Types
export type TStringOrNumber = string | number;
export type TStringNumberNull = string | number | null;
export type TCommonValue = string | number | TArrayOf<string | number>;
export type TCommonFunction = TFunction;

// Combined Generic Types
export type TGenericCommonValue<T = string | number> = T | TArrayOf<T>;
export type TGenericCommonFunction<P extends any[] = any[], R = any> = (
  ...args: P
) => R;

// React events
// Mouse Events
export type TMouseEventHandler = (evt: MouseEvent) => void;

// Keyboard Events
export type TKeyboardEventHandler = (evt: KeyboardEvent) => void;

// Change Events
export type TChangeEvent<T extends HTMLElement = HTMLInputElement> =
  ChangeEvent<T> & {
    target:
      | T
      | { name: string; value: any; label?: string; files?: FileList | null };
  };

// Custom Change Event Handler Type
export type TChangeEventHandler<T extends HTMLElement = HTMLInputElement> = (
  event: TChangeEvent<T>,
  ...additionalProps: { [key: string]: any }[]
) => void;

// Universal Event Handler with any event type
export type TAnyEventHandler = (evt: any) => void;

// Type for a function that takes no arguments and returns nothing.
export type TEmpty = () => void;
