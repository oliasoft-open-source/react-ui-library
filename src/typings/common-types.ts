export enum TriggerType {
  TEXT = 'Text',
  BUTTON = 'Button',
  DROP_DOWN_BUTTON = 'DropDownButton',
  COMPONENT = 'Component',
}

export type TTriggerType = 'Text' | 'Button' | 'DropDownButton' | 'Component';

export enum GroupOrder {
  FIRST = 'first',
  MIDDLE = 'middle',
  LAST = 'last',
}

export type TGroupOrder = 'first' | 'middle' | 'last';

export enum Color {
  DANGER = 'danger',
  RED = 'red',
  WHITE = 'white',
  SUCCESS = 'success',
  GREEN = 'green',
  MUTED = 'muted',
}

export type TColor = 'danger' | 'red' | 'white' | 'success' | 'green' | 'muted';

export enum Align {
  LEFT = 'left',
  RIGHT = 'right',
  CENTER = 'center',
}

export type TAlign = 'left' | 'right' | 'center';

export enum ButtonPosition {
  TOP = 'top',
  BOTTOM = 'bottom',
}

export type TButtonPosition = 'top' | 'bottom';

export enum IconType {
  LIBRARY = 'library',
  ADD = 'add',
  DRAG = 'drag',
  HELP = 'help',
  NOTIFICATION = 'notification',
  MENU = 'menu',
  EDIT = 'edit',
  FAST_FORWARD = 'fast forward',
  INDENT = 'indent',
  STAR = 'star',
  LIST = 'list',
  SETTINGS = 'settings',
  CHECK = 'check',
  LOCK = 'lock',
  UNLOCK = 'unlock',
  CLOSE = 'close',
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error',
  INFO = 'info',
  LEFT = 'left',
  COLLAPSE = 'collapse',
  EXPAND = 'expand',
  SORT_ASCENDING = 'sort ascending',
  SORT_DESCENDING = 'sort descending',
  FAST_BACKWARD = 'fast backward',
  CHEVRON_DOWN = 'chevron down',
  CHEVRON_RIGHT = 'chevron right',
  CHEVRON_LEFT = 'chevron left',
  CHEVRON_DOUBLE_LEFT = 'chevron double left',
  CHEVRON_DOUBLE_RIGHT = 'chevron double right',
}

export type TIconType =
  | 'library'
  | 'add'
  | 'drag'
  | 'help'
  | 'notification'
  | 'menu'
  | 'edit'
  | 'fast forward'
  | 'indent'
  | 'star'
  | 'list'
  | 'settings'
  | 'check'
  | 'lock'
  | 'unlock'
  | 'close'
  | 'success'
  | 'warning'
  | 'error'
  | 'info'
  | 'left'
  | 'collapse'
  | 'expand'
  | 'sort ascending'
  | 'sort descending'
  | 'fast backward'
  | 'chevron down'
  | 'chevron right'
  | 'chevron left'
  | 'chevron double left'
  | 'chevron double right';

export enum Theme {
  DARK = 'dark',
  WHITE = 'white',
  LIGHT = 'light',
  INHERIT = 'inherit',
}

export type TTheme = 'dark' | 'white' | 'light' | 'inherit';

export enum InputType {
  TEXT = 'text',
  RADIO = 'radio',
  CHECKBOX = 'checkbox',
}

export type TInputType = 'text' | 'radio' | 'checkbox';

export enum ElementType {
  LINK = 'Link',
  BUTTON = 'Button',
  MENU = 'Menu',
  COMPONENT = 'Component',
}
