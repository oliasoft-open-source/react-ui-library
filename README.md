# React UI Library

## Description

The React UI Library by Oliasoft is a versatile set of React UI components designed for building modern web applications. It offers a comprehensive suite of layout, navigation, and control components that are flexible and configurable. With detailed examples available in Storybook, developers can easily understand how to implement and customize components for their needs. This library is open-source and can be freely used and modified under the MIT License.

## Installation

To install the React UI Library, you can use either npm or yarn as follows:

Using npm:

```bash
npm install @oliasoft-open-source/react-ui-library
```

Using yarn:
```bash
yarn add @oliasoft-open-source/react-ui-library
```

## Quick Start: React and Vite

To quickly start a new project using the React UI Library with Vite, you can use the `create-vite` tool, which sets up everything for you in a few simple steps:

1. **Create a New Vite Project:** Use the `create-vite` command to scaffold a new React project. Run:
   ```bash
   npm create vite@latest my-new-project -- --template react
   ```
    Or with yarn

   ```bash
   yarn create vite my-new-project --template react
   ```

2. **Navigate into the Project Directory:**

    ```bash
   cd my-new-project
    ```

3. **Install React UI Library: Add the React UI Library to your project:**

   ```bash
   npm install @oliasoft-open-source/react-ui-library
   ```
   Or with yarn

   ```bash
    yarn add @oliasoft-open-source/react-ui-library
   ```

4. **Modify the Sample Page:** Open the src/App.jsx file and import a component from the React UI Library to use it:

    ```bash
    import React from 'react';
    import { Text } from '@oliasoft-open-source/react-ui-library';

    function App() {
      return (
        <div className="App">
          <Text>Hello World!</Text>
        </div>
      );
    }

    export default App;
    ```

5. **Start the Development Server: Begin development by starting the Vite server:**

   ```bash
   npm run dev
   ```
   Or with yarn

   ```bash
    yarn dev
   ```

# Components

- [Accordion](#accordion)
- [AccordionWithDefaultToggle](#accordionwithdefaulttoggle)
- [Actions](#actions)
- [Badge](#badge)
- [Breadcrumb](#breadcrumb)
- [Button](#button)
- [ButtonGroup](#buttongroup)
- [Card](#card)
- [CheckBox](#checkbox)
- [Column](#column)
- [Divider](#divider)
- [Dialog](#dialog)
- [Drawer](#drawer)
- [Empty](#empty)
- [FileInput & readFile](#fileinput--readfile)
- [Field](#field)
- [Flex](#flex)
- [FormRow](#formrow)
- [Grid](#grid)
- [Icon](#icon)
- [InputGroup](#inputgroup)
- [InputGroupAddon](#inputgroupaddon)
- [HelpIcon](#helpicon)
- [Heading](#heading)
- [Input](#input)
- [Label](#label)
- [List](#list)
- [ListHeading](#listheading)
- [ListSubheading](#listsubheading)
- [Loader](#loader)
- [Menu](#menu)
- [Message](#message)
- [Modal](#modal)
- [OptionDropdown](#optiondropdown)
- [Page](#page)
- [Pagination](#pagination)
- [Popover](#popover)
- [Portal](#portal)
- [PrintHeader](#printheader)
- [ProgressBar](#progressbar)
- [RadioButton](#radiobutton)
- [RichTextInput](#richtextinput)
- [NativeSelect](#nativeselect)
- [Row](#row)
- [Select](#select)
- [SideBar](#sidebar)
- [Slider](#slider)
- [Spacer](#spacer)
- [Spinner](#spinner)
- [Table](#table)
- [Tabs](#tabs)
- [Text](#text)
- [TextLink](#textlink)
- [TextArea](#textarea)
- [toast](#toast)
- [Toggle](#toggle)
- [Tooltip](#tooltip)
- [TopBar](#topbar)
- [PopConfirm](#popconfirm)
- [Tree](#tree)
- [NumberInput](#numberinput)
- [UnitInput](#unitinput)
- [UnitTable](#unittable)

## Storybook

To see live examples and demos of the components, please visit our [Storybook](https://oliasoft-open-source.gitlab.io/react-ui-library/storybook/?path=/docs/buttons-links--docs).

## License

This project is open-source and available under the [MIT License](LICENSE).

## Questions or Issues?

If you have any questions or encounter any issues, please open an issue on the [issues page](https://gitlab.com/oliasoft-open-source/react-ui-library/issues) of the repository.

# Components Details

## Accordion Component <a id="accordion"></a>

The `Accordion` component provides a versatile way to toggle visibility of content. It can be expanded to show content or collapsed to hide content, with additional options for styling and behavior customization.

## Properties

| Prop Name      | Description                                                                 | Default Value |
|----------------|-----------------------------------------------------------------------------|---------------|
| `heading`      | The content displayed in the accordion header.                              | None (required) |
| `expanded`     | If true, the accordion will be initially expanded.                          | `false`       |
| `managed`      | Determines if the accordion's expansion state is controlled externally.     | `false`       |
| `bordered`     | If true, adds a border to the accordion.                                   | `false`       |
| `padding`      | If true, adds padding inside the accordion content area.                    | `true`        |
| `children`     | The content to be displayed within the accordion when it is expanded.       | None (optional) |
| `onClick`      | Function to handle click events on the accordion header.                    | None (optional) |
| `squareBottom` | If true, the bottom of the accordion will be square instead of rounded.    | `false`       |
| `testId`       | An optional identifier used for testing.                                    | None (optional) |

## Usage Example

```jsx
<Accordion
  heading="Section 1"
  expanded
  bordered
  onClick={() => console.log("Accordion clicked")}
>
  Content goes here
</Accordion>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## AccordionWithDefaultToggle <a id="accordionwithdefaulttoggle"></a>

An enhanced accordion component that includes a default toggle feature, allowing for more interactive and user-driven content display. It extends the basic functionality of an accordion by offering a built-in toggle option with customizable label and behavior.

### Props

| Prop Name            | Description                                                                                          | Default Value  |
|----------------------|------------------------------------------------------------------------------------------------------|----------------|
| `heading`            | The content to be displayed in the accordion header.                                                | None (required)|
| `toggleLabel`        | Label for the default toggle switch.                                                                 | None (required)|
| `expanded`           | Controls if the accordion is expanded or collapsed by default.                                       | `false`        |
| `defaultEnabled`     | Determines if the default toggle is enabled.                                                         | `true`         |
| `onClickDefaultToggle`| Function called when the default toggle is clicked. Receives the change event as an argument.       | None (optional) |
| `padding`            | If `true`, adds padding to the accordion content for better spacing.                                 | `true`         |
| `children`           | The content to be displayed inside the accordion when it is expanded.                                | None (optional) |

### Usage Example

```jsx
<AccordionWithDefaultToggle
  heading="Section 1"
  toggleLabel="Enable Section"
  onClickDefaultToggle={(evt) => console.log(evt.target.checked)}
  expanded
  defaultEnabled
  padding
>
  {/* Content here */}
</AccordionWithDefaultToggle>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Actions <a id="actions"></a>

The `Actions` component is designed to display a set of actions or options. It supports primary actions, subactions, icons, tooltips, and can conditionally hide or disable actions. Ideal for implementing interactive lists, toolbars, or context menus within your application.

### Properties

| Prop Name      | Description                                               | Type                                                       | Default Value (optional) |
|----------------|-----------------------------------------------------------|------------------------------------------------------------|---------------------------|
| `actions`      | Array of action items to display.                         | `IAction[]`                                                |                           |
| `closeLayer`   | Function to close the action layer. Optional.             | `TEmpty` (Function with no arguments and no return value) | None                      |

### `IAction` Properties

| Prop Name          | Description                                         | Type                                                          | Default Value (optional) |
|--------------------|-----------------------------------------------------|---------------------------------------------------------------|---------------------------|
| `label`            | Text label for the action.                         | `TStringNumberNull` (String, number, or null)                 | None                      |
| `childComponent`   | Component to render as a child.                    | `ReactNode` \| `(() => ReactNode)`                            | None                      |
| `subActions`       | Array of subaction items.                          | `ISubAction[]`                                                | None                      |
| `primary`          | Marks the action as primary.                       | `boolean`                                                     | False                     |
| `icon`             | Icon to display alongside the label.               | `ReactNode`                                                   | None                      |
| `testId`           | Identifier for testing purposes.                   | `string`                                                      | None                      |
| `hidden`           | Whether the action is hidden.                      | `boolean`                                                     | False                     |
| `disabled`         | Whether the action is disabled.                    | `boolean`                                                     | False                     |
| `onClick`          | Function to call when the action is clicked.       | `(evt: React.MouseEvent, id?: TStringOrNumber) => void`      | None                      |
| `tooltip`          | Tooltip to display on hover.                       | `ReactNode` \| `string`                                       | None                      |

### Usage Example

```jsx
<Actions
  actions={[
    {
      label: 'Action 1',
      onClick: () => console.log('Action 1 clicked'),
      primary: true,
    },
    {
      label: 'Action 2',
      onClick: () => console.log('Action 2 clicked'),
      disabled: true,
    },
    {
      label: 'Action 3',
      onClick: () => console.log('Action 3 clicked'),
      tooltip: 'This is a tooltip for Action 3',
    },
  ]}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Badge <a id="badge"></a>

The `Badge` component is used to display a small badge, often used to show a count or to label an item with a short piece of text. This component can be customized with color, size, and optional dot mode for notifications or status indicators.

### Properties

| Prop Name | Description                                           | Default Value |
|-----------|-------------------------------------------------------|---------------|
| children  | The content inside the badge.                         | None (optional) |
| color     | The color of the badge.                               | '' (empty string) |
| title     | The title or text displayed inside the badge.         | None (optional) |
| dot       | If true, displays a dot instead of the children.     | false         |
| margin    | Margin around the badge.                              | None (optional) |
| small     | If true, displays a smaller version of the badge.    | false         |

### Usage Example

```jsx
<Badge
  color="red"
  title="New"
  dot
  margin="0 8px"
  small
>
  {/* Children here, if any */}
</Badge>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Breadcrumb Component  <a id="breadcrumb"></a>

The `Breadcrumb` component displays a navigation path, typically used to show the user's location within a hierarchical structure.

**Props:**

| Prop Name | Description | Default Value |
|---|---|---|
| links | An array of objects representing the breadcrumb links. (See `ILinkProps` for details) | Required |
| small | If `true`, displays a smaller version of the breadcrumb. | `false` |

**ILinkProps Interface:**

This interface defines the properties for each breadcrumb link.

| Prop Name | Description |
|---|---|
| type | The type of the link (implementation-specific). | Optional |
| label | The text displayed for the link. | Optional |
| url | The URL the link points to. | Optional |
| onClick | A function to be called when the link is clicked. | Optional |
| active | If `true`, indicates the link is the current location. | Optional |
| disabled | If `true`, the link is disabled and cannot be clicked. | Optional |
| element | A React node to render instead of the default link behavior. | Optional |

**Usage Example:**

```jsx
<Breadcrumb
  links={[
    { label: 'Home', url: '/' },
    { label: 'Products', url: '/products' },
    { label: 'Current Product', active: true },
  ]}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Button <a id="button"></a>

The `Button` component is a versatile interface element that supports various styles and states, including active, basic, colored, disabled, and loading states. It's suitable for a wide range of actions within an application, from form submissions to interactive commands.

### Props

| Prop          | Description                                                | Default Value  |
|---------------|------------------------------------------------------------|----------------|
| `active`      | Shows the button in an active state.                       | `false`        |
| `basic`       | Applies a link-style appearance to the button.             | `false`        |
| `colored`     | Applies color styling. Can be boolean or string (color code or name). | `false`    |
| `disabled`    | If true, disables the button.                              | `false`        |
| `groupOrder`  | Specifies the button's position in a group (e.g., start, middle, end). | None (optional) |
| `icon`        | An icon to be displayed on the button. Deprecated in favor of using `label`. | None (optional) |
| `ignoreDisabledContext`        | Allows the button to be enabled even when inside `DisabledContext`. | `false` |
| `label`       | The content of the button, which can be text or a ReactNode. | None (optional) |
| `loading`     | Shows an activity indicator, suggesting that an action is in progress. | `false`    |
| `name`        | The name of the button, which can be used in forms.        | None (optional) |
| `onClick`     | Function to call when the button is clicked.               | None (optional) |
| `pill`        | Deprecated. Use `round` for rounded corners instead.       | `false`        |
| `round`       | If true, applies rounded corners to the button.            | `false`        |
| `small`       | If true, displays the button in a smaller size.            | `false`        |
| `styles`      | Custom styles to be applied to the button.                 | None (optional) |
| `width`       | The width of the button, can be a number or string.        | None (optional) |
| `title`       | Tooltip text for the button.                               | None (optional) |
| `type`        | The button type (e.g., "button", "submit").                | `button`       |
| `error`       | Displays an error state or message.                        | None (optional) |
| `warning`     | Displays a warning state or message.                       | None (optional) |
| `testId`      | A test identifier for the button.                          | None (optional) |
| `tooltip`     | Tooltip content. Can be text or a ReactNode.               | None (optional) |
| `inverted`    | Deprecated.                                                | `false`        |
| `tabIndex`      | The tab index of the button.                             | None (optional) |

### Usage Example

```jsx
<Button
  label="Click Me"
  onClick={() => console.log('Button clicked')}
  colored="blue"
  small
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## ButtonGroup <a id="buttongroup"></a>

The `ButtonGroup` component allows for the grouping of button-like elements, making it useful for presenting a selection of options or actions together. This component supports customization in terms of appearance, selection handling, and more, making it versatile for various UI scenarios.

### IButtonGroupProps

| Prop          | Description                                                    | Default Value  |
|---------------|----------------------------------------------------------------|----------------|
| `disabled`    | If true, disables all buttons in the group.                    | None (optional) |
| `basic`       | If true, applies a basic style to the button group.            | `false`        |
| `items`       | An array of items (strings or `IBtnGroupItemProps`) in the group. | `[]`           |
| `header`      | An optional header for the group.                              | `''`           |
| `onSelected`  | Callback function triggered when an item is selected.          | `() => {}`     |
| `small`       | If true, applies a smaller size to the buttons in the group.   | `false`        |
| `value`       | The currently selected value.                                  | None (optional) |
| `testId`      | A unique identifier for testing purposes.                      | None (optional) |

### IBtnGroupItemProps

| Prop               | Description                                      | Default Value  |
|--------------------|--------------------------------------------------|----------------|
| `label`            | The label for the item.                          | None (optional) |
| `value`            | The value associated with the item.              | None (required) |
| `key`              | A unique key for the item.                       | None (required) |
| `hidden`           | If true, the item is hidden.                     | `false`        |
| `icon`             | An icon displayed next to the item label.        | None (optional) |
| `error`            | Displays an error state for the item.            | None (optional) |
| `maxTooltipWidth`  | The maximum width of the item's tooltip.         | None (optional) |
| `testId`           | A unique identifier for testing purposes.        | None (optional) |
| `tooltip`          | Tooltip text or component for the item.          | None (optional) |
| `warning`          | Displays a warning state for the item.           | None (optional) |
| `disabled`         | If true, the item is disabled.                   | `false`        |

### Usage Example

```jsx
<ButtonGroup
  items={[
    { label: 'Button 1', value: '1' },
    { label: 'Button 2', value: '2', disabled: true },
    { label: 'Button 3', value: '3' }
  ]}
  onSelected={(selectedValue) => console.log(selectedValue)}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Card <a id="card"></a>

### Props

| Prop       | Description                                                      | Default Value  |
|------------|------------------------------------------------------------------|----------------|
| `bordered` | If true, displays a border around the card.                      | `true`         |
| `heading`  | Optional heading content for the card.                           | None (optional) |
| `margin`   | The margin around the card, specified as a CSS value.            | `'0'`          |
| `padding`  | If true, applies padding inside the card.                        | `true`         |
| `raised`   | If true, applies a box-shadow to give the card a "raised" effect. | `false`        |
| `children` | The content within the card.                                     | None (optional) |

### Usage Example

```jsx
<Card heading="Card Title" raised>
  <p>This is some content within a card.</p>
</Card>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## CheckBox <a id="checkbox"></a>

The `CheckBox` component is used to create a checkbox input field, often utilized for options that can be toggled between two states, such as true/false or yes/no. This component supports various customizations including label display, size adjustments, and integrated help text.

### Props

| Prop          | Description                                                      | Default Value   |
|---------------|------------------------------------------------------------------|-----------------|
| `checked`     | Determines whether the checkbox is checked.                      | `false`         |
| `isInTable`   | If true, optimizes the checkbox for use within a table.         | `false`         |
| `label`       | The text label associated with the checkbox.                     | None (optional) |
| `name`        | The name of the checkbox input, useful for form submission.      | None (required) |
| `noMargin`    | If true, removes the margin around the checkbox for tighter UI integration. | `false`      |
| `onChange`    | A callback function that is called when the checkbox state changes. | None (required) |
| `tabIndex`    | The tab index of the checkbox.                                   | `0`             |
| `disabled`    | If true, disables the checkbox preventing user interaction.      | `false`         |
| `small`       | If true, renders a smaller version of the checkbox.              | `false`         |
| `testId`      | A unique identifier for testing purposes.                        | None (optional) |
| `key`         | A unique key for rendering the component in lists.               | `''`            |
| `dataix`      | Custom data index attribute for the component.                   | `0`             |
| `value`       | The value of the checkbox.                                       | None (optional) |
| `helpText`    | Optional help text displayed near the checkbox.                  | None (optional) |
| `onClickHelp` | A callback function triggered when the help text is clicked.     | None (optional) |

### Usage Example

```jsx
<CheckBox
  label="Accept Terms"
  name="termsAccepted"
  checked={this.state.accepted}
  onChange={e => this.setState({ accepted: e.target.checked })}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Column <a id="column"></a>

The `Column` component is a layout tool designed for creating structured, flexible column-based layouts within your application. It supports a wide range of customization options, including background color, border control, padding, and responsive width adjustments, making it an essential element for responsive design.

### Props

| Prop            | Description                                                      | Default Value         |
|-----------------|------------------------------------------------------------------|-----------------------|
| `background`    | The background color of the column.                              | `'transparent'`       |
| `borderLeft`    | Controls the presence and style of the left border.              | None (optional)       |
| `borderRight`   | Controls the presence and style of the right border.             | None (optional)       |
| `children`      | The content to be displayed inside the column.                   | None (optional)       |
| `flex`          | If `true`, enables flex layout for the column contents.          | `true`                |
| `flexbox`       | If `true`, the column will use flexbox layout.                   | `false`               |
| `padding`       | Controls padding inside the column. Can be boolean or string.    | `false`               |
| `scroll`        | If `true`, enables scrolling within the column.                  | `false`               |
| `showScrollbar` | Controls the visibility of the scrollbar.                        | `true`                |
| `spacing`       | Controls the spacing around items inside the column.             | `'var(--padding)'`    |
| `width`         | The width of the column, responsive on all devices.              | None (optional)       |
| `widthMobile`   | The column's width specifically for mobile devices.              | None (optional)       |
| `widthTablet`   | The column's width specifically for tablet devices.              | None (optional)       |
| `testId`        | A unique identifier for testing purposes.                        | None (optional)       |

### Usage Example

```jsx
<Column
  background="lightgrey"
  borderLeft="1px solid black"
  padding="20px"
  width={100}
  widthMobile={50}
>
  {/* Column Content Here */}
</Column>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Divider <a id="divider"></a>

The `Divider` component visually separates content within your layout. It's customizable with respect to margin, color, and alignment, making it versatile for various design needs. It can be particularly useful in forms, lists, or between sections of content to provide a clear visual distinction.

### Props

| Prop       | Description                                       | Default Value        |
|------------|---------------------------------------------------|----------------------|
| `margin`   | Margin around the divider, specified as a string or number. | `'var(--padding)'` |
| `color`    | The color of the divider line.                    | `'var(--color-border)'` |
| `align`    | The alignment of the divider within its container. Options are `left`, `center`, or `right`. | `Align.CENTER` |
| `children` | The content to display within the divider. This can be used to place text or icons in the divider line. | None (optional) |

### Usage Example

```jsx
<Divider align="center" color="grey" margin="20px">
  <Text>OR</Text>
</Divider>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Dialog <a id="dialog"></a>

The `Dialog` component encapsulates a dialog box or modal's structure and behavior, providing a flexible way to display content in a floating layer above the app's main UI. This component supports a variety of content and layouts, including headers, footers, and customizable padding, making it ideal for confirmations, information dialogs, and more complex interactions.

### Props

| Prop               | Description                                           | Default Value     |
|--------------------|-------------------------------------------------------|-------------------|
| `heading`          | The title or heading of the dialog.                   | None (optional)   |
| `content`          | The main content of the dialog, can be a node or an array of strings. | None (optional) |
| `contentPadding`   | Padding inside the dialog, around the content.       | `'var(--padding)'`|
| `footer`           | Footer content, typically used for actions.          | None (optional)   |
| `width`            | Width of the dialog.                                  | None (optional)   |
| `height`           | Height of the dialog.                                 | None (optional)   |
| `bordered`         | If true, the dialog will have a border.              | None (optional)   |
| `onClose`          | Function to call when the dialog is requested to close. | None (optional) |
| `scroll`           | Enables scrolling within the dialog.                 | None (optional)   |
| `testId`           | A test identifier for the component.                 | None (optional)   |

### Usage Example

```jsx
<Dialog
  dialog={{
    heading: "Dialog Title",
    content: "This is the content of the dialog.",
    footer: <button onClick={() => console.log('Closing dialog')}>Close</button>,
    width: 400,
    scroll: true,
    onClose: () => console.log('Dialog closed'),
  }}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Drawer <a id="drawer"></a>

The `Drawer` component is a flexible and customizable sidebar or navigation drawer that slides in from the side of your application. It supports both left and right placement, fixed positioning, and the inclusion of tabs for additional organization. The drawer can be triggered to open or close programmatically, making it adaptable for various user interactions.

### Props

| Prop               | Description                                                         | Default Value                   |
|--------------------|---------------------------------------------------------------------|---------------------------------|
| `background`       | The background color of the drawer.                                 | `'var(--color-background-raised)'` |
| `children`         | The content displayed inside the drawer.                            | None (optional)                |
| `fixed`            | If true, the drawer is fixed in position.                           | `false`                        |
| `open`             | Controls the open state of the drawer.                              | `false`                        |
| `setOpen`          | Function to set the open state of the drawer.                       | None (optional)                |
| `right`            | If true, the drawer slides in from the right.                       | `false`                        |
| `shadow`           | If true, the drawer casts a shadow.                                 | `false`                        |
| `top`              | The distance of the drawer from the top of the viewport.            | `0`                            |
| `width`            | The width of the drawer when opened. Can be a single value or an array for responsive design. | `400`                        |
| `closedWidth`      | The width of the drawer when closed.                                | `0`                            |
| `button`           | Optional button to toggle the drawer. Can be boolean or a ReactNode for custom button. | None (optional)                |
| `buttonAnimate`    | If true, the toggle button animates on state change.                | `true`                         |
| `buttonPosition`   | Position of the toggle button.                                      | `ButtonPosition.BOTTOM`        |
| `border`           | If true, adds a border to the drawer. Can be a boolean or a string for custom border styles. | `false`                       |
| `tabs`             | Array of tabs for organizing content within the drawer.             | None (optional)                |
| `defaultTabIndex`  | The index of the tab to be selected by default.                     | `0`                            |
| `activeTab`        | Controls the active tab state.                                      | None (optional)                |
| `setActiveTab`     | Function to set the active tab.                                     | None (optional)                |
| `testId`           | A unique identifier for testing purposes.                           | None (optional)                |
| `onResize`         | Callback function that is called when the drawer is resized.        | None (optional)                |
| `getActiveTab`     | Function that receives the active tab information.                  | None (optional)                |
| `onClose`          | Callback function that is called when the drawer is closed.         | None (optional)                |
| `onOpen`           | Callback function that is called when the drawer is opened.         | None (optional)                |

### Usage Example

```jsx
<Drawer
  open={true}
  right
  width="250px"
  onClose={() => console.log("Drawer closed")}
>
  {/* Content here */}
</Drawer>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Empty <a id="empty"></a>

The `Empty` component is a useful UI element for displaying a state where there is no data or content available. It can be customized with specific dimensions and text, making it adaptable to various scenarios where you might need to inform users that a section or area is intentionally empty.

### Props

| Prop       | Description                                      | Default Value |
|------------|--------------------------------------------------|---------------|
| `width`    | The width of the empty state container.          | `'auto'`      |
| `height`   | The height of the empty state container.         | `'auto'`      |
| `text`     | The text or ReactNode displayed in the empty state. | `'No data'` |
| `children` | Children elements to be rendered inside the empty state container. | None (optional) |
| `testId`   | A unique identifier for testing purposes.        | None (optional) |

### Usage Example

```jsx
<Empty
  width={300}
  height={200}
  text="No items found."
>
  <Button>Refresh</Button>
</Empty>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## FileInput <a id="fileinput"></a>

The `FileInput` component allows users to select files from their device storage. It supports single and multiple file selections, customizable via props. This component is ideal for forms requiring file uploads, offering options for file type acceptance, loading states, and accessibility features.

### Props

| Prop           | Description                                               | Default Value       |
|----------------|-----------------------------------------------------------|---------------------|
| `label`        | The label displayed on the file input.                    | `'Select'`          |
| `loading`      | Displays a loading state if set to true.                  | `false`             |
| `placeholder`  | Text displayed when no file is selected.                  | `'No file selected'`|
| `disabled`     | If true, disables the file input.                         | `false`             |
| `file`         | The currently selected file.                              | None (optional)     |
| `accept`       | Defines the file types the file input should accept.      | None (optional)     |
| `multi`        | Allows multiple files to be selected if set to true.      | None (optional)     |
| `name`         | The name attribute of the input element.                  | None (optional)     |
| `width`        | The width of the file input.                              | None (optional)     |
| `onChange`     | Callback function called when the selected file changes.  | None (optional)     |
| `testId`       | A test identifier for the component.                      | None (optional)     |

### Usage Example

```jsx
<FileInput
  label="Upload Document"
  accept=".pdf"
  multi
  onChange={(evt) => console.log(evt.target.files)}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Field <a id="field"></a>

The `Field` component serves as a container for form elements, providing a structured layout for labels, input fields, help text, and additional features like lock icons or information tooltips.

### Props

| Prop             | Description                                                  | Default Value                |
|------------------|--------------------------------------------------------------|------------------------------|
| `label`          | The label for the field.                                     | None                         |
| `labelLeft`      | If true, aligns the label to the left.                       | `false`                      |
| `labelWidth`     | The width of the label.                                      | `'auto'`                     |
| `children`       | The content of the field, typically an input component.      | None (required)              |
| `helpText`       | Additional text to assist users with the field.              | None                         |
| `helpTextMaxWidth` | The maximum width of the help text.                         | `'300px'`                    |
| `onClickHelp`    | A callback function triggered when the help text is clicked. | `() => {}`                   |
| `lock`           | Configuration for a lock icon.                               | `{ visible: false, active: false, onClick: () => {}, tooltip: '', testId: undefined }` |
| `info`           | Additional information displayed alongside the field.        | `''`                         |
| `libraryIcon`    | Configuration for an icon from a library.                    | None                         |
| `testId`         | A unique identifier for testing purposes.                    | -                  |

### Usage Example

```jsx
<Field
  label="Username"
  helpText="Choose a unique username"
  info="Your username must be at least 6 characters long."
>
  <Input placeholder="Enter your username" />
</Field>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Flex <a id="flex"></a>

The `Flex` component enables flexible layout arrangements by providing properties to control the alignment, direction, and spacing of its children elements.

### Props

| Prop              | Description                                               | Default Value |
|-------------------|-----------------------------------------------------------|---------------|
| `alignItems`     | The alignment of flex items along the cross axis.         | `'initial'`   |
| `justifyContent` | The alignment of flex items along the main axis.          | `'initial'`   |
| `direction`      | The direction of the flex container's main axis.          | `'initial'`   |
| `height`         | The height of the flex container.                         | `'initial'`   |
| `children`       | The child elements of the flex container.                 | `null`        |
| `wrap`           | If true, allows flex items to wrap onto multiple lines.   | `true`        |
| `gap`            | The gap between flex items.                              | `false`       |

### Usage Example

```jsx
<Flex alignItems="center" justifyContent="space-between" direction="row" gap={10}>
  <div>Item 1</div>
  <div>Item 2</div>
  <div>Item 3</div>
</Flex>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## FormRow <a id="formrow"></a>

The `FormRow` component is used to group form elements together in a row layout.

### Props

| Prop       | Description                            | Default Value |
|------------|----------------------------------------|---------------|
| `children` | The child elements of the form row.   | `null`        |

### Usage Example

```jsx
<FormRow>
  <Input label="Username" />
  <Button label="Submit" onClick={handleSubmit} />
</FormRow>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Grid <a id="grid"></a>

The `Grid` component allows you to create a grid layout with specified rows and columns.

### Props

| Prop            | Description                                         | Default Value |
|-----------------|-----------------------------------------------------|---------------|
| `rows`          | The CSS value for defining the grid rows            | `'initial'`   |
| `columns`       | The CSS value for defining the grid columns         | `'initial'`   |
| `columnsTablet` | The CSS value for defining grid columns on tablets  | `null`        |
| `columnsMobile` | The CSS value for defining grid columns on mobiles  | `null`        |
| `gap`           | Whether to include gap between grid items           | `false`       |
| `height`        | The CSS value for defining the grid height          | `'initial'`   |
| `children`      | The child elements of the grid                      | `null`        |

### Usage Example

```jsx
<Grid rows="auto" columns="repeat(3, 1fr)" gap={20}>
  <GridItem>Item 1</GridItem>
  <GridItem>Item 2</GridItem>
  <GridItem>Item 3</GridItem>
</Grid>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Icon <a id="icon"></a>

The `Icon` component is a wrapper used to render various types of icons within your application.

### Props

| Prop        | Description                                            | Default Value |
|-------------|--------------------------------------------------------|---------------|
| `icon`      | The icon to be rendered. This can be a ReactNode, string name, or SVG. |               |
| `onClick`   | The callback function triggered when the icon is clicked. | -   |
| `clickable` | Determines whether the icon is clickable or not.       | `false`       |
| `color`     | The color of the icon.                                 | -   |
| `size`      | The size of the icon.                                  | -   |
| `testId`    | A test id for identification in automated tests.       | -   |

### Usage Example

```jsx
<Icon icon="search" color="blue" size={24} />
```

In this example, the `Icon` component renders an icon with the name "search", colored blue, and sized at 24 pixels.

- The `icon` prop accepts various formats such as string names, React components, or SVG elements.
- If using a string name, it's recommended to use icon libraries like Font Awesome or Material Icons.
- When providing a custom SVG, ensure it's properly formatted and compatible with React's rendering.
- Setting the `clickable` prop to `true` adds cursor pointer and hover effects to the icon.

```jsx
<Icon icon={<CustomIcon />} clickable onClick={handleIconClick} />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## InputGroup <a id="inputgroup"></a>

The `InputGroup` component is used to group together multiple input elements or components.

### Props

| Prop       | Description                                           | Default Value |
|------------|-------------------------------------------------------|---------------|
| `children` | The input elements or components to be grouped.       |               |
| `small`    | Determines whether the input group is small or not.  | `false`       |
| `width`    | The width of the input group.                         | `'100%'`      |

### Usage Example

```jsx
<InputGroup>
  <Input placeholder="Username" />
  <Button>Search</Button>
</InputGroup>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## InputGroupAddon <a id="inputgroupaddon"></a>

The `InputGroupAddon` component is used to add additional content or components to an `InputGroup`.

### Props

| Prop          | Description                                                | Default Value |
|---------------|------------------------------------------------------------|---------------|
| `children`    | The content or components to be added to the input group. |               |
| `groupOrder`  | The order of the addon within the input group.             | `null`        |
| `small`       | Determines whether the addon is small or not.              | `false`       |

### Usage Example

```jsx
<InputGroup>
  <InputGroupAddon>$</InputGroupAddon>
  <Input placeholder="Amount" />
</InputGroup>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## HelpIcon <a id="helpicon"></a>

The `HelpIcon` component provides an icon with optional tooltip text to offer assistance or additional information.

### Props

| Prop        | Description                                                                                         | Default Value |
|-------------|-----------------------------------------------------------------------------------------------------|---------------|
| `text`      | The text or content to display in the tooltip.                                                     |               |
| `onClick`   | A function to be called when the icon is clicked.                                                   |               |
| `icon`      | The icon to display. It can be a ReactNode or a string representing an icon (e.g., "help", "info"). | `'help'`      |
| `active`    | Determines whether the icon is in an active state.                                                 | `false`       |
| `maxWidth`  | The maximum width of the tooltip.                                                                   | `'300px'`     |
| `testId`    | The test ID for identifying the component in tests.                                                 |               |

### Usage Example

```jsx
<HelpIcon text="Click here for more information" />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Heading <a id="heading"></a>

The `Heading` component renders a heading element with optional features such as help text and an icon.

### Props

| Prop           | Description                                                                                                          | Default Value |
|----------------|----------------------------------------------------------------------------------------------------------------------|---------------|
| `children`     | The content to be rendered within the heading element.                                                                |               |
| `helpText`     | Additional text or content to provide assistance or information about the heading.                                   |               |
| `onClick`      | A function to be called when the heading is clicked.                                                                  |               |
| `onClickHelp`  | A function to be called when the help text is clicked.                                                                |               |
| `onIconClick`  | A function to be called when the icon is clicked.                                                                     |               |
| `icon`         | The icon element to be displayed alongside the heading.                                                               |               |
| `libraryIcon`  | Configuration for the library icon, containing an `onClick` function and an optional `tooltip` text.                |               |
| `marginBottom` | The margin at the bottom of the heading.                                                                              |               |
| `top`          | Determines whether the heading is displayed at the top of the container.                                              | `false`       |
| `testId`       | The test ID for identifying the component in tests.                                                                   |               |

### Usage Example

```jsx
<Heading>Page Title</Heading>
```

```jsx
<Heading
  top={true}
  onClick={() => console.log('Heading clicked')}
  helpText="This is the main title of the page"
  onClickHelp={() => console.log('Help text clicked')}
  icon={<QuestionCircleOutlined />}
  libraryIcon={{ onClick: () => console.log('Library icon clicked'), tooltip: 'More info' }}
  marginBottom="10px"
>
  Page Title
</Heading>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Input <a id="input"></a>

The `Input` component provides an input field for users to enter data. It supports various features such as error and warning messages, tooltips, and custom event handlers.

### Props

| Prop            | Description                                                                                                         | Default Value |
|-----------------|---------------------------------------------------------------------------------------------------------------------|---------------|
| `error`         | Error message or content to indicate validation errors.                                                             | `null`        |
| `warning`       | Warning message or content to indicate potential issues.                                                            | `null`        |
| `tooltip`       | Additional information or context provided as a tooltip.                                                            | `null`        |
| `name`          | The name attribute of the input field.                                                                             | -   |
| `type`          | The type of input field (e.g., text, number, email).                                                               | `'text'`      |
| `onChange`      | Event handler called when the input value changes.                                                                  | `() => {}`    |
| `onKeyPress`    | Event handler called when a key is pressed while the input is focused.                                              | `() => {}`    |
| `onFocus`       | Event handler called when the input is focused.                                                                    | `() => {}`    |
| `onBlur`        | Event handler called when the input loses focus.                                                                    | `() => {}`    |
| `onPaste`       | Event handler called when content is pasted into the input field.                                                   | `() => {}`    |
| `small`         | Determines whether to render a smaller-sized input field.                                                           | `false`       |
| `placeholder`   | Placeholder text displayed when the input is empty.                                                                 | `''`          |
| `tabIndex`      | The tab index of the input field.                                                                                   | `0`           |
| `value`         | The value of the input field.                                                                                       | `''`          |
| `disabled`      | Determines whether the input field is disabled.                                                                     | `false`       |
| `right`         | Determines whether to align content to the right of the input field.                                                | `false`       |
| `groupOrder`    | Position of the input field within a group or sequence.                                                             | `null`        |
| `maxTooltipWidth` | The maximum width of the tooltip.                                                                                  | -   |
| `testId`        | Test ID for identifying the component in tests.                                                                     | -   |
| `size`          | *Deprecated.* Use `width` instead to define the size of the input field.                                             | `null`        |
| `isInTable`     | Specifies whether the input is used within a table.                                                                 | `false`       |
| `width`         | Width of the input field.                                                                                           | -   |

### Usage Example

```jsx
<Input
  type="text"
  placeholder="Enter your name"
  value={name}
  onChange={(e) => setName(e.target.value)}
  error={nameError}
  tooltip="Please enter your full name"
  disabled={isDisabled}
/>
```

```jsx
<Input
  type="number"
  placeholder="Enter your age"
  value={age}
  onChange={(e) => setAge(e.target.value)}
  warning={ageWarning}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Label <a id="label"></a>

The `Label` component represents a label or caption associated with an input field or other form elements. It can include optional elements such as help text, lock icons, and library icons.

### Props

| Prop               | Description                                                                                                        | Default Value |
|--------------------|--------------------------------------------------------------------------------------------------------------------|---------------|
| `label`            | The text or content of the label.                                                                                 | `null`        |
| `width`            | The width of the label.                                                                                           | `'auto'`      |
| `helpText`         | Help text or additional information associated with the label.                                                    | `''`          |
| `helpTextMaxWidth` | The maximum width of the help text.                                                                               | `'300px'`     |
| `onClickHelp`      | Event handler called when the help icon or text is clicked.                                                        | -   |
| `lock`             | Configuration for the lock icon displayed with the label.                                                          | See below     |
| `libraryIcon`      | Configuration for the library icon displayed with the label.                                                       | -   |
| `labelLeft`        | Determines whether to align the label to the left side.                                                             | `false`       |
| `info`             | Additional information or tooltip associated with the label.                                                       | -   |

**Lock Props:**

| Prop       | Description                                           | Default Value |
|------------|-------------------------------------------------------|---------------|
| `visible`  | Determines whether the lock icon is visible.         | `false`       |
| `active`   | Determines whether the lock icon is active.          | `false`       |
| `onClick`  | Event handler called when the lock icon is clicked.  | `() => {}`    |
| `tooltip`  | Tooltip text displayed when hovering over the icon.  | `''`          |
| `testId`   | Test ID for identifying the lock icon in tests.      | -   |

### Usage Example

```jsx
<Label
  label="Username"
  helpText="Enter your username"
  onClickHelp={() => console.log('Help clicked')}
  lock={{
    visible: true,
    active: false,
    onClick: () => console.log('Lock clicked'),
    tooltip: 'Locked',
  }}
  libraryIcon={{
    onClick: () => console.log('Library icon clicked'),
    tooltip: 'Open library',
  }}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## List <a id="list"></a>

The `List` component renders a list of data items with various customization options such as borders, expanding items, and draggable rows.

### Props

| Prop                   | Description                                                                                                     | Default Value            |
|------------------------|-----------------------------------------------------------------------------------------------------------------|--------------------------|
| `drawer`               | Specifies whether the list is rendered as a drawer.                                                             | `false`                  |
| `list`                 | The data to be displayed in the list.                                                                           | **Required**             |
| `bordered`             | Determines whether the list items have borders.                                                                 | `false`                  |
| `expanding`            | Specifies whether the list items can be expanded to show additional content.                                    | `false`                  |
| `narrow`               | Determines whether the list items have a narrower layout.                                                       | `false`                  |
| `toggleNarrow`         | Specifies whether to enable toggling the narrow mode.                                                           | `false`                  |
| `onToggleNarrow`       | Callback function called when toggling the narrow mode.                                                          | `() => {}`               |
| `invokeEditOnRowClick` | Determines whether clicking on a row invokes editing mode.                                                       | `true`                   |
| `noHeader`             | Specifies whether to hide the header of the list.                                                               | `false`                  |
| `stickyHeader`         | Determines whether the list header sticks to the top when scrolling.                                             | -              |
| `draggable`            | Specifies whether the list items can be dragged to reorder.                                                      | `false`                  |
| `onListReorder`        | Callback function called when reordering the list items.                                                         | `() => {}`               |
| `marginBottom`         | The margin bottom applied to the list.                                                                          | `0`                      |
| `height`               | The height of the list.                                                                                         | -              |
| `testId`               | Test ID for identifying the list component.                                                                      | -              |
| `scrollDetails`        | Configuration for scrolling behavior and infinite scroll functionality. See details below.                      | See details below        |

**Scroll Details:**

| Prop                        | Description                                                                                                     | Default Value            |
|-----------------------------|-----------------------------------------------------------------------------------------------------------------|--------------------------|
| `scrollable`                | Determines whether the list is scrollable.                                                                      | `false`                  |
| `hideScrollbar`             | Determines whether to hide the scrollbar when scrolling.                                                        | `false`                  |
| `triggerScrollToActiveItem` | Specifies whether scrolling should be triggered to display the active item.                                      | `false`                  |
| `infiniteScroll`            | Specifies whether to enable infinite scroll functionality.                                                       | `false`                  |

### Usage Example

```jsx
<List
  list={myData}
  bordered
  expanding
  draggable
  stickyHeader
  marginBottom={20}
  height={300}
  testId="my-list"
  scrollDetails={{
    hideScrollbar: true,
    triggerScrollToActiveItem: true,
  }}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## ListHeading <a id="listheading"></a>

The `ListHeading` component represents the header of a list. It typically includes the name or title of the list and optional actions.

### Props

| Prop             | Description                                                                                                     | Default Value            |
|------------------|-----------------------------------------------------------------------------------------------------------------|--------------------------|
| `name`           | The name or title of the list.                                                                                  | **Required**             |
| `actions`        | An array of action items to be displayed in the header.                                                          | `[]`                     |
| `toggleNarrow`   | Specifies whether to enable toggling the narrow mode for the list.                                               | `false`                  |
| `onToggleNarrow` | Callback function called when toggling the narrow mode.                                                          | `() => {}`               |
| `stickyHeader`   | Determines whether the list header sticks to the top when scrolling.                                              | -              |

### Usage Example

```jsx
<ListHeading
  name="My List"
  actions={[{ label: 'Add', onClick: handleAdd }, { label: 'Edit', onClick: handleEdit }]}
  toggleNarrow
  onToggleNarrow={handleToggleNarrow}
  stickyHeader
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## ListSubheading <a id="listsubheading"></a>

The `ListSubheading` component represents a subheading within a list. It is typically used to provide additional information or context for a group of items within the list.

### Props

| Prop    | Description                                                        | Default Value |
|---------|--------------------------------------------------------------------|---------------|
| `item`  | An object containing data for the subheading item.                 | **Required**  |
| `index` | The index of the subheading item within the list.                  | -   |

### Usage Example

```jsx
<ListSubheading item={{ label: 'Group A', count: 5 }} index={0} />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Loader <a id="loader"></a>

The `Loader` component renders a loading indicator, typically used to indicate that content is being fetched or processed.

### Props

| Prop               | Description                                                                                          | Default Value  |
|--------------------|------------------------------------------------------------------------------------------------------|----------------|
| `width`            | The width of the loader.                                                                            |     |
| `height`           | The height of the loader.                                                                           |     |
| `text`             | The text to display alongside the loader.                                                           | `''` (empty)   |
| `details`          | Additional details or information to display below the loader.                                       | `''` (empty)   |
| `fullViewPortSize` | A boolean indicating whether the loader should cover the entire viewport.                            | `false`        |
| `cover`            | A boolean indicating whether the loader should cover its parent element.                             | `false`        |
| `children`         | Additional React elements to render alongside the loader.                                            | `null`         |
| `theme`            | The theme of the loader. This can be either `Theme.DARK` or `Theme.LIGHT`.                            | `Theme.DARK`   |
| `testId`           | A string value representing the test ID for testing purposes.                                         | `null`         |

### Usage Example

```jsx
<Loader text="Loading..." cover />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Menu <a id="menu"></a>

The `Menu` component is used to render a menu with various options or sections.

### Props

| Prop                        | Description                                                                                               | Default Value |
|-----------------------------|-----------------------------------------------------------------------------------------------------------|---------------|
| `menu`                      | An object containing properties for configuring the menu.                                                  | `{}`          |
| `contextMenu`               | A boolean indicating whether the menu is a context menu.                                                    | `false`       |
| `width`                     | The width of the menu.                                                                                     | -   |
| `disabled`                  | A boolean indicating whether the menu is disabled.                                                          | `false`       |
| `badgeTitle`                | The title text for a badge, if applicable.                                                                 | `''` (empty)  |
| `badgeDot`                  | A boolean indicating whether to display a dot badge.                                                        | `false`       |
| `loading`                   | A boolean indicating whether the menu is in a loading state.                                                | `false`       |
| `fullHeightTrigger`         | A boolean indicating whether the trigger should have full height.                                           | `false`       |
| `closeOnOptionClick`        | A boolean indicating whether the menu should close when an option is clicked.                               | `false`       |
| `groupOrder`                | The order of the menu group.                                                                              | `null`        |
| `overflowContainer`         | A boolean indicating whether the menu should have an overflow container.                                     | `false`       |
| `maxHeight`                 | The maximum height of the menu.                                                                            | -   |
| `testId`                    | A string representing the test ID for testing purposes.                                                     | -   |
| `open`                      | A boolean indicating whether the menu is open.                                                              | `false`       |
| `setOpen`                   | A function to set the open state of the menu.                                                               | -   |
| `closeParent`               | A function to close the parent menu.                                                                       | -   |
| `tree`                      | The tree structure of the menu.                                                                            | `null`        |
| `path`                      | The path of the menu.                                                                                      | `null`        |
| `isNested`                  | A boolean indicating whether the menu is nested.                                                            | `false`       |
| `tooltip`                   | The tooltip for the menu.                                                                                  | `null`        |
| `error`                     | The error message for the menu.                                                                            | `null`        |
| `warning`                   | The warning message for the menu.                                                                          | `null`        |
| `title`                     | The title text for the menu.                                                                               | `''` (empty)  |

### Menu Properties

The `menu` prop accepts an object with the following properties:

- `trigger`: The trigger type or string.
- `title`: The title of the menu.
- `colored`: A boolean indicating whether the menu is colored or a string representing the color.
- `small`: A boolean indicating whether the menu is small.
- `label`: The label for the menu.
- `component`: The component to render inside the menu.
- `sections`: The sections of the menu.
- `placement`: The placement of the menu.
- `possiblePlacements`: The possible placements of the menu.
- `showAllButton`: The configuration for the "show all" button.
- `testId`: The test ID for the menu.
- `fullHeightTrigger`: A boolean indicating whether the trigger should have full height.

### Usage Example

```jsx
<Menu
  menu={{
    trigger: 'click',
    title: 'Options',
    colored: true,
    small: true,
    label: 'Menu Label',
    sections: [<MenuItem key="1" label="Item 1" />, <MenuItem key="2" label="Item 2" />],
    placement: 'bottom-end',
    testId: 'menu-test-id',
    fullHeightTrigger: true,
  }}
  open={true}
  setOpen={(state) => setOpen(state)}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Message <a id="message"></a>

The `Message` component is used to display informative messages to users.

### Props

| Prop           | Description                                           | Default Value  |
|----------------|-------------------------------------------------------|----------------|
| `message`      | An object containing properties for the message.      | `{}`           |

### Message Properties

The `message` prop accepts an object with the following properties:

- `visible`: A boolean indicating whether the message is visible.
- `type`: The type of the message, such as info, success, warning, or error.
- `icon`: A boolean indicating whether to display an icon with the message.
- `heading`: The heading or title of the message.
- `content`: The main content of the message.
- `details`: Additional details or information related to the message.
- `detailsVisible`: Whether the details are visible by default (default: `false`).
- `footer`: The footer content of the message.
- `withDismiss`: A boolean indicating whether the message can be dismissed.
- `onClose`: A function to handle the close event of the message.
- `width`: The width of the message.
- `maxHeight`: The maximum height of the message.

### Usage Example

```jsx
<Message
  message={{
    visible: true,
    type: MessageType.INFO,
    icon: true,
    heading: 'Information',
    content: 'This is an informative message.',
    details: 'Additional details can be provided here.',
    detailsVisible: true,
    footer: <Button label="Close" onClick={handleClose} />,
    withDismiss: true,
    onClose: handleClose,
    width: '400px',
    maxHeight: '200px',
  }}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Modal <a id="modal"></a>

The `Modal` component is used to display content in a modal dialog box.

### Props

| Prop        | Description                                      | Default Value |
|-------------|--------------------------------------------------|---------------|
| `children`  | The content to be displayed within the modal.    | -             |
| `visible`   | A boolean indicating whether the modal is visible.| `false`       |
| `centered`  | A boolean indicating whether the modal is centered vertically and horizontally. | `false` |
| `width`     | The width of the modal.                          | `'100%'`      |
| `onEnter`   | A function called when the modal is opened.      | -             |
| `onEscape`  | A function called when the escape key is pressed to close the modal. | -    |

### Usage Example

```jsx
<Modal
  visible={isModalVisible}
  centered
  width="500px"
  onEnter={handleModalEnter}
  onEscape={handleModalEscape}
>
  {/* Content of the modal */}
</Modal>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Option Dropdown <a id="optiondropdown"></a>

The `OptionDropdown` component is used to display a dropdown menu with selectable options.

### Props

| Prop          | Description                                                                                   | Default Value |
|---------------|-----------------------------------------------------------------------------------------------|---------------|
| `name`        | The name attribute for the dropdown input element.                                             | -             |
| `label`       | The label for the dropdown.                                                                   | -             |
| `options`     | An array of options to be displayed in the dropdown. Each option should have a `label` and `value` property. | -   |
| `onChange`    | A function to be called when the selected options change.                                      | -             |
| `showHeader`  | A boolean indicating whether to display a header above the dropdown options.                  | `true`        |
| `maxHeight`   | The maximum height of the dropdown menu.                                                      | `'40vh'`      |
| `testId`      | Test ID used for testing purposes.                                                            | -              |

### Usage Example

```jsx
<OptionDropdown
  name="myDropdown"
  label="Select an option"
  options={[
    { label: 'Option 1', value: 'option1' },
    { label: 'Option 2', value: 'option2' },
    { label: 'Option 3', value: 'option3' },
  ]}
  onChange={handleDropdownChange}
  showHeader={true}
  maxHeight="300px"
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Page <a id="page"></a>

The `Page` component is used to create a page layout with optional padding and scrolling.

### Props

| Prop        | Description                                                                                           | Default Value            |
|-------------|-------------------------------------------------------------------------------------------------------|--------------------------|
| `children`  | The content to be rendered within the page layout.                                                    | -                        |
| `left`      | The width of the left margin of the page.                                                             | `var(--size-sidebar)`    |
| `padding`   | Determines whether padding should be applied to the page content.                                      | `true`                   |
| `scroll`    | A boolean indicating whether the page should be scrollable if its content overflows.                   | `true`                   |
| `top`       | The height of the top margin of the page.                                                             | `var(--size-topbar)`     |

### Usage Example

```jsx
<Page
  left="20%"
  padding={true}
  scroll={true}
  top="50px"
>
  {/* Page content goes here */}
</Page>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Pagination <a id="pagination"></a>

The `Pagination` component provides navigation controls for navigating through a large set of data, allowing users to move between pages.

### Props

| Prop                   | Description                                                                                                          | Default Value |
|------------------------|----------------------------------------------------------------------------------------------------------------------|---------------|
| `pagination`           | An object containing pagination configuration options.                                                               | -             |
| `rowCount`             | The total number of rows/items to be paginated.                                                                      | -             |
| `selectedPage`         | The currently selected page number.                                                                                  | -             |
| `onSelectPage`         | A function to handle page selection. It takes the selected page number as an argument.                                | -             |
| `rowsPerPage`          | Configuration options for selecting the number of rows/items per page.                                               | -             |
| `small`                | A boolean indicating whether to render a smaller version of the pagination controls.                                 | `false`       |
| `errorMessageTemplate`| A template string used to display an error message if pagination fails.                                                | -             |
| `testIds`              | An object containing test IDs for specific elements within the pagination component.                                  | -             |

### Usage Example

```jsx
<Pagination
  pagination={{
    rowCount: 1000,
    selectedPage: 1,
    onSelectPage: handlePageSelection,
    rowsPerPage: {
      options: [10, 20, 50],
      selected: 10,
      onChange: handleRowsPerPageChange,
    },
    small: false,
    errorMessageTemplate: "Pagination failed: {error}",
    testIds: {
      doubleRightBtn: "pagination-double-right-btn",
    },
  }}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Popover <a id="popover"></a>

The `Popover` component displays additional content or options related to a specific element when triggered.

### Props

| Prop                  | Description                                                                                                  | Default Value  |
|-----------------------|--------------------------------------------------------------------------------------------------------------|----------------|
| `children`            | The element that triggers the display of the popover content.                                                | -              |
| `content`             | The content to be displayed inside the popover.                                                              | -              |
| `placement`           | The placement of the popover relative to its trigger element.                                                | `'top-center'` |
| `closeOnOutsideClick` | A boolean indicating whether the popover should close when clicking outside of it.                           | `true`         |
| `fullWidth`           | A boolean indicating whether the popover should take up the full width of its container.                     | `false`        |
| `showCloseButton`     | A boolean indicating whether to display a close button within the popover.                                   | `false`        |
| `testId`              | A test ID for the popover component, used for testing purposes.                                              | -              |
| `disabled`            | A boolean indicating whether the popover is disabled and should not be triggered.                            | `false`        |
| `isOpen`              | Controls the visibility of the popover. If defined, the popover is in controlled mode.                       | -              |
| `onToggle`            | Callback function that is called when the popover is opened or closed. Receives the new state as an argument.| -              |

### Usage Example

```jsx
<Popover
  content={<div>Popover Content</div>}
  placement="bottom-right"
  closeOnOutsideClick={false}
  fullWidth
  showCloseButton
  disabled={false}
>
  <Button>Open Popover</Button>
</Popover>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Portal <a id="portal"></a>

The `Portal` component facilitates rendering React elements at a different location in the DOM tree.

### Props

| Prop       | Description                                       | Default Value |
|------------|---------------------------------------------------|---------------|
| `id`       | The id of the target DOM node where children will be rendered. | -             |
| `children` | The React elements to be rendered inside the portal.         | -             |

### Usage Example

```jsx
<Portal id="portal-container">
  <div>This content will be rendered in a different location in the DOM tree.</div>
</Portal>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## PrintHeader <a id="printheader"></a>

The `PrintHeader` component is used to display a header with a logo when printing.

### Props

| Prop        | Description                                           | Default Value |
|-------------|-------------------------------------------------------|---------------|
| `logo`      | The URL or path to the logo image.                    | -             |
| `alt`       | The alternative text for the logo image.              | -             |
| `logoWidth` | The width of the logo.                                | `'100px'`     |

### Usage Example

```jsx
<PrintHeader
  logo="/path/to/logo.png"
  alt="Company Logo"
  logoWidth="150px"
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## ProgressBar <a id="progressbar"></a>

The `ProgressBar` component is used to display a visual progress bar indicating the completion percentage of a task.

### Props

| Prop                 | Description                                                                                                      | Default Value |
|----------------------|------------------------------------------------------------------------------------------------------------------|---------------|
| `width`              | The width of the progress bar.                                                                                   | `'auto'`      |
| `inverted`           | Determines whether the progress bar is inverted (i.e., progress from right to left).                              | `false`       |
| `colored`            | Determines whether the progress bar is colored.                                                                  | `true`        |
| `showProgressColors` | Determines whether the progress bar shows different colors based on progress (if `colored` is `true`).           | `false`       |
| `percentage`         | The completion percentage of the task (a value between 0 and 100).                                                | `0`           |
| `noLabel`            | Determines whether to hide the label displaying the percentage value.                                             | `false`       |

### Usage Example

```jsx
<ProgressBar
  width="300px"
  inverted={false}
  colored={true}
  showProgressColors={false}
  percentage={75}
  noLabel={false}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## RadioButton <a id="radiobutton"></a>

The `RadioButton` component provides a set of radio buttons for selecting options within a group.

### Props

| Prop                | Description                                                                                   | Default Value       |
|---------------------|-----------------------------------------------------------------------------------------------|---------------------|
| `name`              | The name attribute of the radio button group.                                                 | -                   |
| `label`             | The label for the radio button group.                                                          | `null`              |
| `options`           | An array of objects representing each radio button option.                                     | -                   |
| `value`             | The currently selected value or option object.                                                 | -                   |
| `inline`            | (Deprecated) Determines whether the radio buttons should be displayed inline.                 | `false`             |
| `disabled`          | Determines whether the radio buttons are disabled.                                             | `false`             |
| `small`             | Determines whether to use a smaller size for the radio buttons.                                | `false`             |
| `onChange`          | A function called when the selected value changes.                                             | -                   |
| `noMargin`          | Determines whether to remove margins around the radio buttons.                                 | `false`             |
| `onClick`           | (Deprecated) A function called when a radio button is clicked.                                  | `() => {}`          |
| `mainLabel`         | (Deprecated) The main label for the radio button group.                                         | `''`                |
| `radioButtonsData`  | (Deprecated) An array of objects representing each radio button option.                         | -                   |
| `classForContainer` | (Deprecated) A class name for the container element.                                             | `'grouped fields'` |
| `testId`            | A string used to define a test ID for testing purposes.                                         | -                   |

### Usage Example

```jsx
<RadioButton
  name="gender"
  label="Gender"
  options={[
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' },
    { label: 'Other', value: 'other' },
  ]}
  value="male"
  onChange={(e) => console.log('Selected value:', e.target.value)}
  disabled={false}
  small={true}
  noMargin={false}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## RichTextInput <a id="richtextinput"></a>

The `RichTextInput` component provides a text input field with rich text editing capabilities.

### Props

| Prop              | Description                                                              | Default Value |
|-------------------|--------------------------------------------------------------------------|---------------|
| `disabled`        | Determines whether the text input is disabled.                           | `false`       |
| `onChange`        | A function called when the content of the text input changes.             | -             |
| `placeholder`     | The placeholder text displayed when the text input is empty.              | -             |
| `value`           | The current value of the text input.                                      | -             |
| `toolbarComponent`| A custom toolbar component for additional editing options.                | -             |

### Usage Example

```jsx
<RichTextInput
  placeholder="Enter your text here..."
  onChange={(markdown) => console.log('Markdown:', markdown)}
  value=""
  disabled={false}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## NativeSelect <a id="nativeselect"></a>

The `NativeSelect` component renders a native HTML select element with various customization options.

### Props

| Prop                   | Description                                                                                     | Default Value |
|------------------------|-------------------------------------------------------------------------------------------------|---------------|
| `borderRadius`         | The border radius of the select element.                                                        | -             |
| `disabled`             | Determines whether the select element is disabled.                                               | `false`       |
| `error`                | Error message to display.                                                                       | -             |
| `warning`              | Warning message to display.                                                                     | -             |
| `tooltip`              | Tooltip message to display.                                                                     | -             |
| `options`              | An array of options to populate the select element.                                              | -             |
| `onChange`             | A function called when the selected option changes.                                              | -             |
| `onFocus`              | A function called when the select element receives focus.                                         | -             |
| `onBlur`               | A function called when the select element loses focus.                                            | -             |
| `right`                | Determines whether the select arrow is positioned on the right side.                              | `false`       |
| `small`                | Determines whether to render a smaller version of the select element.                              | `false`       |
| `tabIndex`             | Specifies the tab order of the select element.                                                   | `0`           |
| `selectedOption`       | The currently selected option.                                                                   | -             |
| `width`                | The width of the select element.                                                                 | `null`        |
| `groupOrder`           | The order of the option groups.                                                                  | `null`        |
| `testId`               | A string used to identify the select element for testing purposes.                                 | -   |
| `isInTable`            | Determines whether the select element is rendered within a table.                                  | `false`       |
| `clearable`            | Determines whether the select element allows clearing the selected option.                         | `false`       |
| `placeholder`          | Placeholder text displayed when no option is selected.                                            | `null`        |
| `hasNonExistentValue` | Determines whether the select element includes a non-existent value option.                        | `false`       |
| `maxTooltipWidth`      | The maximum width of the tooltip.                                                                | -             |

### Usage Example

```jsx
<NativeSelect
  disabled={false}
  options={[
    { label: 'Option 1', value: 'option1' },
    { label: 'Option 2', value: 'option2' },
    { label: 'Option 3', value: 'option3' },
  ]}
  onChange={(evt, option) => console.log('Selected option:', option)}
  onFocus={() => console.log('Select element focused')}
  onBlur={() => console.log('Select element blurred')}
  right={false}
  small={false}
  tabIndex={0}
  selectedOption={{ label: 'Option 1', value: 'option1' }}
  width="200px"
  groupOrder={null}
  testId="select-element"
  isInTable={false}
  clearable={true}
  placeholder="Select an option"
  hasNonExistentValue={false}
  maxTooltipWidth="200px"
  borderRadius="5px"
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Row Component <a id="row"></a>

The `Row` component is used to create a horizontal layout with flexible alignment and spacing options.

### Props

| Prop             | Description                                                                                                         | Default Value |
|------------------|---------------------------------------------------------------------------------------------------------------------|---------------|
| `alignItems`     | Specifies the alignment of the children along the cross-axis (vertical alignment).                                  | `initial`     |
| `justifyContent` | Defines the alignment of the children along the main axis (horizontal alignment).                                    | `initial`     |
| `children`       | The content to be rendered within the row.                                                                          | `null`        |
| `flex`           | Determines whether the row should be flexible (able to grow and shrink based on available space).                  | `false`       |
| `height`         | The height of the row.                                                                                              | `'auto'`      |
| `marginBottom`   | The margin bottom of the row.                                                                                       | `'0'`         |
| `marginTop`      | The margin top of the row.                                                                                          | `'0'`         |
| `spacing`        | The spacing between child elements in the row.                                                                      | `'var(--padding)'` |
| `wrap`           | Determines whether the children should wrap onto multiple lines if they exceed the row's width.                    | `false`       |
| `testId`         | A string used to identify the row for testing purposes.                                                             | `null`        |

### Usage Example

```jsx
<Row
  alignItems="center"
  justifyContent="space-between"
  flex={true}
  height="100px"
  marginBottom="20px"
  marginTop="10px"
  spacing="10px"
  wrap={false}
  testId="row-component"
>
  <div>Child 1</div>
  <div>Child 2</div>
  <div>Child 3</div>
</Row>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Select Component <a id="select"></a>

The `Select` component is used to render a dropdown menu for selecting options from a list.

### Props

| Prop                        | Description                                                                                         | Default Value |
|-----------------------------|-----------------------------------------------------------------------------------------------------|---------------|
| `name`                      | The name attribute of the select element.                                                           | `''`          |
| `multi`                     | Determines whether multiple options can be selected.                                                | `false`       |
| `options`                   | An array of options to be rendered in the dropdown menu.                                            | `[]`          |
| `value`                     | The currently selected option(s).                                                                   | -   |
| `native`                    | Determines whether to use the native HTML `<select>` element.                                        | `false`       |
| `onChange`                  | A callback function invoked when the selection changes.                                              | `() => {}`    |
| `onCreate`                  | A callback function invoked when a new option is created.                                            | `() => {}`    |
| `deprecatedEventHandler`    | Determines whether to use deprecated event handlers.                                                 | `false`       |
| `autoScroll`                | Determines whether the dropdown menu should auto-scroll.                                              | `true`        |
| `warning`                   | Warning message to be displayed.                                                                    | -   |
| `error`                     | Error message to be displayed.                                                                      | -   |
| `checkNonExistentValues`    | Determines whether to check for non-existent values.                                                 | `true`        |
| `closeOnOptionActionClick`  | Determines whether to close the select dropdown on option action click.                               | `true`        |
| `testId`                    | A string used to identify the select component for testing purposes.                                 | -   |
| `disabled`                  | Determines whether the select component is disabled.                                                 | `false`       |

The `ISelectSelectedOption` interface represents the structure of an option in the dropdown menu.

| Property   | Description                                                                               |
|------------|-------------------------------------------------------------------------------------------|
| `value`    | The value of the option.                                                                  |
| `label`    | The display label of the option.                                                          |
| `details`  | Additional details or description of the option.                                          |
| `actions`  | Actions associated with the option.                                                        |
| `icon`     | Icon to be displayed next to the option.                                                   |
| `type`     | Type of the option.                                                                       |
| `disabled` | Determines whether the option is disabled.                                                 |
| `testId`   | A string used to identify the option for testing purposes.                                 |

### Usage Example

```jsx
<Select
  name="exampleSelect"
  options={[
    { value: 'option1', label: 'Option 1' },
    { value: 'option2', label: 'Option 2' },
    { value: 'option3', label: 'Option 3' }
  ]}
  value="option2"
  onChange={(value) => console.log('Selected value:', value)}
  placeholder="Select an option"
  disabled={false}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## SideBar Component <a id="sidebar"></a>

The `SideBar` component represents a sidebar navigation menu with collapsible sections and items.

### Props

| Prop                        | Description                                                                                                     | Default Value |
|-----------------------------|-----------------------------------------------------------------------------------------------------------------|---------------|
| `options`                   | An object containing the title of the sidebar and its sections.                                                 | `{}`          |
| `startOpen`                 | Determines whether the sidebar sections should start open or collapsed.                                         | `false`       |
| `onShiftClickToggleOpen`    | A callback function invoked when the user shift-clicks to toggle the sidebar open or closed.                      | `() => {}`    |
| `top`                       | The top position of the sidebar.                                                                                | -   |

The `IOptionItem` interface represents an individual item within a sidebar section.

| Property         | Description                                                                                       |
|------------------|---------------------------------------------------------------------------------------------------|
| `label`          | The label or display text of the item.                                                           |
| `value`          | The value associated with the item.                                                              |
| `icon`           | The icon displayed alongside the item label.                                                     |
| `onClick`        | A callback function invoked when the item is clicked.                                             |
| `isActive`       | Determines whether the item is currently active.                                                  |
| `isExperimental` | Indicates whether the item is experimental or not.                                                |
| `invalid`        | Indicates whether the item is invalid.                                                            |
| `testId`         | A string used to identify the item for testing purposes.                                          |

The `IOptionSection` interface represents a section within the sidebar, containing a heading and a list of items.

| Property   | Description                                     |
|------------|-------------------------------------------------|
| `heading`  | The heading or title of the section.            |
| `items`    | An array of items within the section.           |

### Usage Example

```jsx
<SideBar
  options={{
    title: 'Navigation',
    sections: [
      {
        heading: 'Section 1',
        items: [
          { label: 'Item 1', value: 'item1', icon: <Icon />, onClick: handleItemClick },
          { label: 'Item 2', value: 'item2', icon: <Icon />, onClick: handleItemClick },
        ]
      },
      {
        heading: 'Section 2',
        items: [
          { label: 'Item 3', value: 'item3', icon: <Icon />, onClick: handleItemClick },
          { label: 'Item 4', value: 'item4', icon: <Icon />, onClick: handleItemClick },
        ]
      }
    ]
  }}
  startOpen={true}
  onShiftClickToggleOpen={handleShiftClickToggle}
  top="50px"
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Slider Component <a id="slider"></a>

The `Slider` component allows users to select a value or a range of values by dragging a handle along a track.

### Props

| Prop              | Description                                                                                         | Default Value      |
|-------------------|-----------------------------------------------------------------------------------------------------|--------------------|
| `name`            | The name attribute of the slider input.                                                             |                    |
| `label`           | The label displayed above the slider.                                                               |                    |
| `width`           | The width of the slider.                                                                            | `'100%'`           |
| `labelWidth`      | The width of the label.                                                                             | `'auto'`           |
| `value`           | The current value or range of values selected by the slider.                                         |                    |
| `min`             | The minimum value of the slider.                                                                    |                    |
| `max`             | The maximum value of the slider.                                                                    |                    |
| `step`            | The step value for the slider.                                                                      | `1`                |
| `marks`           | An array of marks to display on the slider track.                                                    | `[]`               |
| `showArrows`      | Determines whether arrow buttons for incremental adjustments should be displayed.                    | `false`            |
| `showTooltip`     | Determines whether tooltips should be displayed when hovering over the slider handles.               | `false`            |
| `tooltipFormatter`| A function to format the tooltip content.                                                            | `v => v`           |
| `disabled`        | Determines whether the slider is disabled.                                                          | `false`            |
| `range`           | Determines whether the slider allows selecting a range of values.                                    | `false`            |
| `small`           | Determines whether to use a small-sized slider.                                                      | `false`            |
| `vertical`        | An object containing configuration options for a vertical slider variant.                           | `{ enabled: false, width: '100px', height: '400px' }` |
| `onChange`        | A callback function invoked when the slider value changes.                                           |                    |

The `TSliderMark` type represents a mark on the slider track.

| Property    | Description                                     |
|-------------|-------------------------------------------------|
| `value`     | The value of the mark.                         |
| `label`     | The label displayed for the mark.              |
| `tooltip`   | The tooltip content displayed for the mark.    |

The `THandleProps` type represents the props for the slider handle component.

| Property    | Description                             |
|-------------|-----------------------------------------|
| `index`     | The index of the handle.                |
| `value`     | The value of the handle.                |
| `dragging`  | Indicates whether the handle is being dragged. |

### Usage Example

```jsx
<Slider
  label="Slider Label"
  value={50}
  min={0}
  max={100}
  step={1}
  marks={[
    { value: 0, label: '0' },
    { value: 50, label: '50' },
    { value: 100, label: '100' }
  ]}
  onChange={({ target: { value } }) => console.log(value)}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Spacer Component <a id="spacer"></a>

The `Spacer` component is used to add space between elements within a layout.

### Props

| Prop      | Description                                       | Default Value   |
|-----------|---------------------------------------------------|-----------------|
| `height`  | The height of the spacer.                         | `'var(--padding)'` |
| `width`   | The width of the spacer.                          | `null`          |
| `flex`    | Determines whether the spacer should use flexbox. | `false`         |

### Usage Example

```jsx
<Spacer height="20px" />
```

This will create a vertical space of 20 pixels.

```jsx
<Spacer width="20px" />
```

This will create a horizontal space of 20 pixels.

```jsx
<Spacer flex />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Spinner Component <a id="spinner"></a>

The `Spinner` component displays a spinning animation, indicating that content is loading or processing.

### Props

| Prop      | Description                                                | Default Value |
|-----------|------------------------------------------------------------|---------------|
| `small`   | Determines if the spinner should be small.                 | `false`       |
| `colored` | Determines if the spinner should be colored.               | `false`       |
| `dark`    | Determines if the spinner should have a dark background.   | `false`       |
| `tiny`    | Determines if the spinner should be tiny.                  | `false`       |

### Usage Example

```jsx
<Spinner />
```

This will render a default-sized spinner with a light background.

```jsx
<Spinner small colored dark />
```

This will render a small-sized spinner with colored animation on a dark background.

```jsx
<Spinner tiny />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Table Component <a id="table"></a>

The `Table` component renders tabular data with various customization options.

### Props

| Prop                  | Description                                                                                      | Default Value |
|-----------------------|--------------------------------------------------------------------------------------------------|---------------|
| `onListReorder`       | Function called when rows are reordered.                                                        | `() => {}`    |
| `canListReorder`      | Function to compare `from` and `to` to determine if reordering is allowed.                      | `() => true`  |
| `table`               | An object containing table configuration.                                                        |               |
| `table.name`          | Name or title of the table.                                                                     |               |
| `table.actionsRight`  | Determines if actions should be displayed on the right side of the table.                        | `false`       |
| `table.fixedWidth`    | Fixed width of the table.                                                                       |               |
| `table.maxHeight`     | Maximum height of the table.                                                                    |               |
| `table.columnWidths`  | Widths of individual columns.                                                                   |               |
| `table.className`     | Custom class name for the table.                                                                 |               |
| `table.columnHeaderAlignments` | Alignment of column headers.                                                                |               |
| `table.columnAlignment` | Alignment of table columns.                                                                   |               |
| `table.infiniteScroll`| Determines if infinite scrolling is enabled.                                                     | `false`       |
| `table.headers`       | Array containing table header data.                                                              |               |
| `table.rows`          | Data for table rows.                                                                            |               |
| `table.footer`        | Footer configuration for the table.                                                              |               |
| `table.draggable`     | Determines if rows are draggable.                                                                |               |
| `table.bordered`      | Determines if the table should have borders.                                                     | `true`        |
| `table.striped`       | Determines if the table rows should be striped.                                                  | `true`        |
| `table.testId`        | Test ID for the table.                                                                           |               |
| `table.onAddRow`      | Function called when adding a new row to the table.                                              | `null`        |
| `table.defaultEmptyRow` | Default empty row data for the table.                                                          | `null`        |
| `table.stickyHeaders` | Determines if table headers should be sticky.                                                    | `false`       |
| `table.actions`       | Additional actions for the table.                                                                 |               |

### Row Type

| Property             | Description                                          |
|----------------------|------------------------------------------------------|
| `cells`              | Array of cell data for the row.                      |
| `actions`            | Additional actions for the row.                      |
| `testId`             | Test ID for the row.                                 |
| `noDrag`             | Determines if row dragging is disabled.              |
| `onRowMouseEnter`    | Function called when the mouse enters the row.       |
| `onRowMouseLeave`    | Function called when the mouse leaves the row.       |

### Footer Props

| Property          | Description                                  |
|-------------------|----------------------------------------------|
| `colSpan`         | Number of columns spanned by the footer.     |
| `pagination`      | Pagination configuration for the footer.      |
| `actions`         | Additional actions for the footer.           |
| `content`         | Custom content for the footer.               |

### Usage Example

```jsx
<Table
  onListReorder={(obj) => console.log('Reordered: ', obj)}
  table={{
    name: 'Sample Table',
    headers: ['Name', 'Age', 'Location'],
    rows: [
      { cells: ['John', 25, 'New York'], testId: 'row1' },
      { cells: ['Alice', 30, 'Los Angeles'], testId: 'row2' },
      { cells: ['Bob', 28, 'Chicago'], testId: 'row3' },
    ],
    footer: {
      pagination: {
        rowCount: 50,
        selectedPage: 1,
        onSelectPage: (value) => console.log('Selected page: ', value),
      },
    },
    actions: [<Button key="addBtn" label="Add Row" onClick={() => console.log('Row added')} />],
  }}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## UnitTable Component <a id="unittable"></a>

The `UnitTable` component is a wrapper around `Table`, with local state and additional logic to handle displaying tables in preferred units, and converting return values when cells are edited.

### Props

| Prop                     | Description                                                                                                            | Default Value |
|--------------------------|------------------------------------------------------------------------------------------------------------------------|---------------|
| `table`                  | An object containing table configuration, similar to `ITableTableProps`.                                               |          |
| `unitConfig`             | Array of unit configuration objects, each with `unitKey`, `storageUnit`, optional `preferredUnit`, and `onChange` callback. See [unitConfig item structure](#unitconfig-item-structure). |          |
| `convertBackToStorageUnit` | Whether values should be converted back to the storage unit when edited.                                                | `true`        |
| `enableCosmeticRounding` | Enables cosmetic rounding of displayed values (Excel-style) to reduce visual precision noise.                          | `true`        |
| `enableDisplayRounding`  | Enables rounding of displayed values in number inputs until the cell is focused.                                        | `true`        |
| `onListReorder`          | Function called when rows are reordered via drag-and-drop. Receives `{ from: number; to: number }` as an argument.     | `() => {}`    |
| `canListReorder`         | Function to determine whether a row can be reordered. Receives `{ from: number; to: number }` as an argument and returns `boolean`. | `() => true`  |

---

### `unitConfig` Item Structure

Each object in the `unitConfig` array should have the following shape:

| Key              | Type                                  | Description                                                                                                           |
|------------------|---------------------------------------|------------------------------------------------------------------------------------------------------------------------|
| `unitKey`        | `string`                              | Key representing the type of unit (e.g., `length`, `temperature`, `density`).                                         |
| `storageUnit`    | `string`                              | The unit in which data is stored internally (e.g., `m`, `ft`, `C`, `sg`).                                             |
| `preferredUnit`  | `string` _(optional)_                  | The user's preferred display unit (e.g., `ft`, `F`, `ppg`).                                                           |
| `onChange`       | `(params: { oldUnit: string; newUnit: string; unitKey: string }) => void` _(optional)_ | Callback when the display unit is changed by the user.                                                               |

---

### Usage Example

```jsx
<UnitTable
  table={{
    draggable: true,
    headers: [...],
    rows: [...],
  }}
  unitConfig={[
    {
      unitKey: 'length',
      storageUnit: 'm',
      preferredUnit: 'ft',
      onChange: ({ oldUnit, newUnit, unitKey }) =>
        console.log(`Unit changed for ${unitKey} from ${oldUnit} to ${newUnit}`),
    },
  ]}
  onListReorder={({ from, to }) => console.log(`Moved row from ${from} to ${to}`)}
  canListReorder={({ from, to }) => from !== to}
/>
```

## Tabs Component <a id="tabs"></a>

The `Tabs` component creates a tabbed interface for organizing content.

### Props

| Prop              | Description                                                          | Default Value |
|-------------------|----------------------------------------------------------------------|---------------|
| `name`            | Name of the tabs (optional).                                         | -             |
| `options`         | Array of tab options.                                                | `null`        |
| `children`        | Content of the tabs.                                                 | `null`        |
| `value`           | Currently selected tab option.                                       | -             |
| `onChange`        | Function called when a tab is selected.                               | `() => {}`    |
| `padding`         | Determines if padding should be applied to the tabs container.        | `false`       |
| `margin`          | Determines if margin should be applied to the tabs container.         | `true`        |
| `contentPadding`  | Determines if padding should be applied to the tab content.           | `true`        |
| `testId`          | Test ID for the tabs component.                                       | -             |
| `activeTabIndex`  | **(Deprecated)** Index of the active tab.                             | -             |
| `tabs`            | **(Deprecated)** Array of tab options (use `options` prop instead).   | -             |
| `onChangeTab`     | **(Deprecated)** Function called when a tab is changed.               | `() => {}`    |

### Tab Option

| Property       | Description                                      |
|----------------|--------------------------------------------------|
| `key`          | Unique identifier for the tab.                   |
| `label`        | Label or title of the tab.                       |
| `value`        | Value associated with the tab.                   |
| `url`          | URL associated with the tab.                     |
| `hidden`       | Determines if the tab is hidden.                 |
| `disabled`     | Determines if the tab is disabled.               |
| `invalid`      | Determines if the tab is invalid.                |
| `badge`        | Badge content displayed on the tab.              |
| `right`        | Determines if the badge should be on the right.  |
| `testId`       | Test ID for the tab.                             |

### Usage Example

```jsx
<Tabs
  name="example-tabs"
  options={[
    { label: 'Tab 1', value: 'tab1' },
    { label: 'Tab 2', value: 'tab2' },
    { label: 'Tab 3', value: 'tab3', disabled: true },
  ]}
  value={{ label: 'Tab 1', value: 'tab1' }}
  onChange={(evt) => console.log('Selected tab: ', evt.target.value)}
  padding={true}
  margin={false}
  contentPadding={false}
  testId="example-tabs"
>
  <div>Content for Tab 1</div>
  <div>Content for Tab 2</div>
  <div>Content for Tab 3</div>
</Tabs>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Text Component <a id="text"></a>

The `Text` component renders text with customizable styles.

### Props

| Prop        | Description                                             | Default Value |
|-------------|---------------------------------------------------------|---------------|
| `bold`      | Determines if the text should be displayed in bold.     | `false`       |
| `center`    | Determines if the text should be centered.              | `false`       |
| `error`     | Determines if the text should represent an error state. | `false`       |
| `faint`     | Determines if the text should have a faint appearance.  | `false`       |
| `link`      | Determines if the text should appear as a link.         | `false`       |
| `muted`     | Determines if the text should have a muted appearance. | `false`       |
| `onClick`   | Function called when the text is clicked.               | -   |
| `small`     | Determines if the text should be displayed in a small size. | `false` |
| `success`   | Determines if the text should represent a success state. | `false`     |
| `warning`   | Determines if the text should represent a warning state. | `false`     |
| `children`  | The content to be rendered as text.                     | -   |

### Usage Example

```jsx
<Text bold center error onClick={() => console.log('Text clicked')}>
  Error Message
</Text>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## TextLink Component <a id="textlink"></a>

The `TextLink` component renders text as a clickable link.

### Props

| Prop       | Description                                             | Default Value |
|------------|---------------------------------------------------------|---------------|
| `href`     | The URL that the link should navigate to.               | -   |
| `target`   | Specifies where to open the linked document.            | -   |
| `testId`   | A unique identifier used for testing purposes.          | -   |
| `onClick`  | Function called when the link is clicked.               | -   |
| `children` | The content to be rendered as a clickable link.         | -   |

### Usage Example

```jsx
<TextLink href="https://example.com" target="_blank" onClick={() => console.log('Link clicked')}>
  Visit Example
</TextLink>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## TextArea Component <a id="textarea"></a>

The `TextArea` component renders a multi-line text input field.

### Props

| Prop            | Description                                                                 | Default Value |
|-----------------|-----------------------------------------------------------------------------|---------------|
| `name`          | The name of the text area field.                                             | -             |
| `value`         | The current value of the text area.                                          | `''`          |
| `placeholder`   | The placeholder text displayed when the text area is empty.                 | `''`          |
| `cols`          | The visible width of the text area (in average character widths).            | -             |
| `rows`          | The number of visible text lines in the text area.                           | -             |
| `disabled`      | If `true`, the text area is disabled and cannot be edited.                   | `false`       |
| `onChange`      | Function called when the text area value changes.                             | `() => {}`    |
| `onKeyPress`    | Function called when a key is pressed while the text area is focused.         | `() => {}`    |
| `onFocus`       | Function called when the text area receives focus.                            | `() => {}`    |
| `onBlur`        | Function called when the text area loses focus.                               | `() => {}`    |
| `tabIndex`      | Specifies the tab order of the text area.                                    | `0`           |
| `error`         | Error message to display below the text area if there is an error.            | `null`        |
| `warning`       | Warning message to display below the text area if there is a warning.        | `null`        |
| `tooltip`       | Tooltip message to display when hovering over the text area.                 | `null`        |
| `maxTooltipWidth` | The maximum width of the tooltip.                                           | -             |
| `resize`        | Specifies whether the text area is resizable or not.                         | -             |
| `monospace`     | If `true`, the text area uses a monospaced font.                              | `false`       |
| `testId`        | A unique identifier used for testing purposes.                                | -             |

### Usage Example

```jsx
<TextArea
  name="example"
  value={textareaValue}
  placeholder="Enter your text here"
  rows={4}
  cols={50}
  disabled={false}
  onChange={(e) => setTextareaValue(e.target.value)}
  error={textareaError}
  warning={textareaWarning}
  tooltip="This is a tooltip message"
  resize="vertical"
  monospace={false}
  testId="textarea-test-id"
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Toggle Component <a id="toggle"></a>

The `Toggle` component represents a toggle switch that allows users to change between two states, typically "on" and "off".

### Props

| Prop          | Description                                                                                   | Default Value |
|---------------|-----------------------------------------------------------------------------------------------|---------------|
| `name`        | The name of the toggle input field.                                                           | -   |
| `label`       | The label displayed next to the toggle switch.                                                | `null`        |
| `checked`     | If `true`, the toggle switch is turned on; if `false`, it's turned off.                       | `false`       |
| `disabled`    | If `true`, the toggle switch is disabled and cannot be interacted with.                       | `false`       |
| `display`     | The display style of the toggle switch.                                                       | -   |
| `small`       | If `true`, a smaller version of the toggle switch is rendered.                                 | `false`       |
| `onChange`    | Function called when the state of the toggle switch changes.                                   | `() => {}`    |
| `noMargin`    | If `true`, no margin is applied to the toggle switch.                                          | `false`       |
| `testId`      | A unique identifier used for testing purposes.                                                 | -   |
| `helpText`    | Help text associated with the toggle switch.                                                   | -   |
| `onClickHelp` | Function called when the help icon associated with the toggle switch is clicked.               | -   |

### Usage Example

```jsx
<Toggle
  name="toggle-switch"
  label="Enable Notifications"
  checked={toggleState}
  disabled={false}
  onChange={(e) => setToggleState(e.target.checked)}
  small={false}
  noMargin={false}
  testId="toggle-test-id"
  helpText="Enable this toggle to receive notifications."
  onClickHelp={(e) => console.log('Help icon clicked')}
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Toast Component <a id="toast"></a>

The `toast` function generates a toast notification with the provided message content.

### Props

| Prop        | Description                                                                                   | Default Value |
|-------------|-----------------------------------------------------------------------------------------------|---------------|
| `id`        | The unique identifier for the toast notification.                                             | -   |
| `message`   | The content and configuration of the toast message.                                            | `{ type: MessageType.INFO, content: '' }` |
| `autoClose` | The duration in milliseconds after which the toast should automatically close. Set to `false` to disable auto-closing. | `6000` (6 seconds) |
| `onClose`   | Function called when the toast is closed.                                                     | `() => {}`    |

### Message Type

The `IMessageType` type defines the structure of the message content for the toast notification.

| Property   | Description                                                                           | Default Value |
|------------|---------------------------------------------------------------------------------------|---------------|
| `type`     | The type of the message, such as `info`, `success`, `warning`, or `error`.           | `MessageType.INFO` |
| `icon`     | If `true`, an icon corresponding to the message type will be displayed.               | `false`       |
| `heading`  | The heading or title of the message.                                                   | -   |
| `content`  | The main content of the message.                                                       | `''`          |
| `details`  | Additional details or supplementary information related to the message content.       | `null`        |

### Usage Example

```jsx
<>
  <Button label="Info" onClick={() => toast({
    message: messageInfo,
    onClose: () => console.log('toast closed')
  })} />
  <Spacer />
  <Button label="Error" onClick={() => toast({
    message: messageError,
    onClose: () => console.log('toast closed')
  })} />
  <Spacer />
  <Button label="Warning" onClick={() => toast({
    message: messageWarning,
    onClose: () => console.log('toast closed')
  })} />
  <Spacer />
  <Button label="Success" onClick={() => toast({
    message: messageSuccess,
    onClose: () => console.log('toast closed')
  })} />
</>;
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Tooltip Component <a id="tooltip"></a>

The `Tooltip` component creates a tooltip that appears when hovering over its children.

### Props

| Prop               | Description                                                                                                                                                                          | Default Value                          |
|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------|
| `children`         | The content over which the tooltip will be triggered.                                                                                                                               | -                            |
| `text`             | The text or content to display within the tooltip.                                                                                                                                   | `''`                                   |
| `error`            | If `true`, the tooltip will be styled as an error tooltip.                                                                                                                          | `false`                                |
| `warning`          | If `true`, the tooltip will be styled as a warning tooltip.                                                                                                                         | `false`                                |
| `placement`        | The initial placement of the tooltip relative to its children.                                                                                                                      | `'top-center'`                         |
| `possiblePlacements` | An array of possible placements for the tooltip.                                                                                                                                    | `['top-center', 'left-center', 'bottom-center', 'right-center']` |
| `enabled`          | If `true`, the tooltip will be enabled and shown on hover.                                                                                                                          | `true`                                 |
| `maxWidth`         | The maximum width of the tooltip.                                                                                                                                                   | `'none'`                               |
| `triggerOffset`    | The offset in pixels between the tooltip and its trigger element.                                                                                                                   | `12`                                   |
| `fontSize`         | The font size of the tooltip text.                                                                                                                                                  | `'inherit'`                            |
| `padding`          | The padding of the tooltip content.                                                                                                                                                 | `'var(--padding-xxs) var(--padding-xs)'` |
| `display`          | The display property of the tooltip container.                                                                                                                                       | `'inline'`                             |
| `flex`             | The flex property of the tooltip container.                                                                                                                                          | `'none'`                               |
| `forceOpen`        | If `true`, the tooltip will always be open and displayed, regardless of hover or focus events.                                                                                      | `false`                                |

### Usage Example

```jsx
// Basic Tooltip
<Tooltip text="Hello, World!">
  <button>Hover Me</button>
</Tooltip>

// Tooltip with Error Styling
<Tooltip text="Invalid input" error>
  <input type="text" placeholder="Enter text" />
</Tooltip>

// Tooltip with Custom Placement and Styling
<Tooltip text="Click here to submit" placement="bottom-center" warning maxWidth="200px" fontSize="14px">
  <button>Submit</button>
</Tooltip>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## TopBar Component <a id="topbar"></a>

The `TopBar` component represents the top bar of the application, typically used for displaying titles, navigation options, and other important information.

### Props

| Prop              | Description                                                                                                                                              | Default Value          |
|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|
| `title`           | The configuration for the title displayed in the top bar.                                                                                               | `{}`                   |
| `appSwitcher`     | The configuration for the app switcher displayed in the top bar.                                                                                        | `{}`                   |
| `content`         | An array of content items to be displayed in the top bar, typically on the left side.                                                                   | `[]`                   |
| `contentRight`    | An array of content items to be displayed in the top bar, typically on the right side.                                                                  | `[]`                   |
| `warning`         | A warning message to be displayed in the top bar.                                                                                                        | `''`                   |
| `height`          | The height of the top bar.                                                                                                                               | `'var(--size-topbar)'` |
| `fixedPosition`   | If `true`, the top bar will have a fixed position at the top of the viewport.                                                                            | `true`                 |

### Usage Example

```jsx
// Basic TopBar with Title and App Switcher
<TopBar
  title={{ text: 'My App', onClick: handleTitleClick }}
  appSwitcher={{ onClick: handleAppSwitcherClick }}
/>

// TopBar with Content and Warning Message
<TopBar
  content={[
    { text: 'Home', onClick: handleHomeClick },
    { text: 'About', onClick: handleAboutClick },
  ]}
  contentRight={[
    { text: 'Profile', onClick: handleProfileClick },
    { text: 'Settings', onClick: handleSettingsClick },
  ]}
  warning="You have unsaved changes."
/>

// TopBar with Fixed Position and Custom Height
<TopBar
  title={{ text: 'Dashboard' }}
  height="60px"
  fixedPosition
/>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## PopConfirm Component <a id="popconfirm"></a>

The `PopConfirm` component creates a popover with confirmation buttons, typically used to confirm or cancel an action.

### Props

| Prop                 | Description                                                       | Default Value      |
|----------------------|-------------------------------------------------------------------|--------------------|
| `children`           | The content that triggers the popover when clicked.               | -                  |
| `placement`          | The placement of the popover relative to the trigger element.     | `'top-center'`     |
| `closeOnOutsideClick`| If `true`, the popover closes when clicked outside of it.         | `true`             |
| `fullWidth`          | If `true`, the popover will take the full width of its container. | `false`            |
| `title`              | The title displayed in the popover.                               | `''`               |
| `cancelText`         | The text for the cancel button.                                   | `'Cancel'`         |
| `onClickCancel`      | The function called when the cancel button is clicked.            | Closes the popover |
| `okText`             | The text for the OK button.                                       | `'Ok'`             |
| `onClickOk`          | The function called when the OK button is clicked.                | `() => {}`         |
| `disableConfirmButton`| If `true`, disable confirm button.                               | `false`            |
| `overflowContainer`  | If `true`, the popover will overflow its container if necessary.  | `false`            |
| `testId`             | Optional test id for testing purposes.                            | -                  |

### Usage Example

```jsx
<PopConfirm
  title="Are you sure you want to delete this item?"
  onClickOk={() => handleDelete()}
>
  <Button>Delete</Button>
</PopConfirm>

<PopConfirm
  placement="bottom-right"
  title="Save changes before leaving?"
  okText="Save"
  cancelText="Don't Save"
  onClickOk={() => handleSave()}
  onClickCancel={() => handleCancel()}
>
  <Button>Leave</Button>
</PopConfirm>
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## Tree Component <a id="tree"></a>

The `Tree` component represents a hierarchical structure of items in a tree format. It allows users to navigate through the hierarchy and interact with the items.

### Props

#### ITreeProps

| Prop                 | Description                                                                                   | Default Value          |
|----------------------|-----------------------------------------------------------------------------------------------|------------------------|
| `list`               | The list of items to be displayed in the tree.                                                | -                      |
| `draggable`          | If `true`, allows dragging and dropping of tree nodes to reorder them.                         | `false`                |
| `onListReorder`      | Callback function invoked when the order of items in the tree is changed through dragging.    | -                      |
| `onChangeOpen`       | Callback function invoked when the open/close state of tree nodes changes.                     | -                      |
| `testId`             | Optional test id for testing purposes.                                                         | -            |
| `isInitialOpen`      | Determines which nodes should be initially open.                                               | `false`                |
| `treeRef`            | Reference to the tree component.                                                               | -                      |
| `itemHasChild`       | Function to determine if a tree node has child nodes.                                          | -                      |
| `onItemToggle`       | Callback function invoked when a tree node is toggled (opened/closed).                          | -                      |
| `icons`              | Custom icons for expand and collapse actions.                                                  | -                      |
| `stickyHeader`         | Determines whether the list header sticks to the top when scrolling.                                             | -              |

#### ITreeListProps

| Prop                 | Description                                                                                   | Default Value          |
|----------------------|-----------------------------------------------------------------------------------------------|------------------------|
| `name`               | The name of the tree list.                                                                    | -            |
| `noHeader`           | If `true`, hides the header of the tree list.                                                  | `false`                |
| `items`              | The array of tree items to be displayed.                                                       | -                      |
| `onClick`            | Callback function invoked when a tree node is clicked.                                          | -                      |
| `actions`            | Additional actions associated with the tree list.                                               | -            |

### Usage Example

```jsx
  const treeItems = [
    {
      id: '1',
      name: 'Item 1',
      parent: '',
      droppable: true,
      details: 'Details of Item 1',
      active: true,
      testId: 'item-1',
      icon: {
        icon: <IconComponent />,
        color: 'blue',
        tooltip: {
          text: 'Tooltip for Item 1',
          error: false,
          warning: true,
          placement: 'right',
          enabled: true,
          maxWidth: '200px',
          triggerOffset: '20px',
          possiblePlacements: ['top', 'bottom', 'left', 'right'],
          fontSize: '14px',
          padding: '8px',
        },
      },
    },
  ];

    <Tree
      list={{ items: treeItems }}
      draggable={true}
      onListReorder={handleListReorder}
      onChangeOpen={(ids) => console.log(ids)}
      testId="my-tree"
      isInitialOpen={['1']}
      treeRef={treeRef}
      itemHasChild={(node) => node.children.length > 0}
      onItemToggle={handleNodeToggle}
    />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## NumberInput Component <a id="numberinput"></a>

The `NumberInput` component provides an input field for entering numeric values. It allows users to input numerical data and provides options for customization and error handling.

### Props

#### INumberInputProps

| Prop               | Description                                                                                   | Default Value          |
|--------------------|-----------------------------------------------------------------------------------------------|------------------------|
| `name`             | The name of the input field.                                                                 | -            |
| `placeholder`      | The placeholder text displayed when the input is empty.                                       | `''`                   |
| `disabled`         | If `true`, disables the input field.                                                          | `false`                |
| `error`            | Error message to display, or `true` if there is an error.                                      | `false`                |
| `left`             | If `true`, aligns the content to the left.                                                    | `false`                |
| `small`            | If `true`, reduces the size of the input field.                                                | `false`                |
| `width`            | The width of the input field.                                                                 | `'100%'`               |
| `value`            | The value of the input field.                                                                 | `''`                   |
| `onChange`         | Callback function invoked when the value of the input changes.                                 | `() => {}`             |
| `onFocus`          | Callback function invoked when the input field is focused.                                     | `() => {}`             |
| `tabIndex`         | Specifies the tab order of the input field.                                                   | `0`                    |
| `testId`           | Optional test id for testing purposes.                                                         | -            |
| `tooltip`          | Tooltip text or component to display.                                                          | `null`                 |
| `warning`          | Warning message to display, or `true` if there is a warning.                                   | `false`                |
| `validationCallback` | Callback function invoked for validation.                                                     | `() => {}`             |
| `allowEmpty`       | If `true`, allows the input field to be empty.                                                 | `false`                |
| `isInTable`        | If `true`, indicates that the input field is within a table.                                    | `false`                |
| `groupOrder`       | Specifies the order of input fields within a group.                                            | -            |

### Usage Example

```jsx
    <NumberInput
      name="quantity"
      placeholder="Enter quantity"
      value={10}
      onChange={handleInputChange}
      onFocus={handleInputFocus}
      tabIndex={1}
      testId="quantity-input"
      error="Invalid quantity"
      warning="Low quantity"
      validationCallback={handleValidation}
      isInTable={false}
      groupOrder={null}
    />
```

<hr style="border: none; border-right: 2px solid black; width: 100%; height: 1px;">

## UnitInput Component <a id="unitinput"></a>

The `UnitInput` component allows users to input values with associated units. It provides options for customization, including predefined options, error handling, and validation callbacks.

### Props

#### IUnitInputProps

| Prop                      | Description                                                                                              | Default Value |
|---------------------------|----------------------------------------------------------------------------------------------------------|---------------|
| `name`                    | The name of the input field.                                                                             | `''`          |
| `placeholder`             | The placeholder text displayed when the input is empty.                                                  | `''`          |
| `disabled`                | If `true`, disables the input field.                                                                     | `false`       |
| `disabledUnit`            | If `true`, disables the unit selection.                                                                  | `false`       |
| `error`                   | Error message to display, or `true` if there is an error.                                                 | `null`        |
| `left`                    | If `true`, aligns the content to the left.                                                               | `false`       |
| `small`                   | If `true`, reduces the size of the input field.                                                           | `false`       |
| `width`                   | The width of the input field.                                                                            | `'100%'`      |
| `value`                   | The value of the input field.                                                                            | `''`          |
| `unitkey`                 | The key associated with the unit value.                                                                  | `''`          |
| `initUnit`                | The initial unit value.                                                                                  | `''`          |
| `noConversion`            | If `true`, disables automatic unit conversion.                                                           | `false`       |
| `onChange`                | Callback function invoked when the value of the input changes.                                           | `() => {}`    |
| `onClick`                 | Callback function invoked when the input field is clicked.                                                | `() => {}`    |
| `onFocus`                 | Callback function invoked when the input field is focused.                                                | `() => {}`    |
| `onSwitchUnit`            | Callback function invoked when the unit is switched.                                                      | `() => {}`    |
| `unitTemplate`            | Template for rendering units.                                                                            | `{}`          |
| `doNotConvertValue`       | If `true`, disables value conversion.                                                                    | `false`       |
| `testId`                  | Test id for testing purposes.                                                                            | -   |
| `warning`                 | Warning message to display, or `true` if there is a warning.                                              | `null`        |
| `predefinedOptions`       | An array of predefined options.                                                                          | -   |
| `initialPredefinedOption` | If `true`, selects the initial predefined option.                                                        | `false`       |
| `shouldLinkAutomaticly`   | If `true`, automatically links the predefined option and the value.                                       | `true`        |
| `selectedPredefinedOptionKey` | The key of the selected predefined option.                                                           | -   |
| `validationCallback`      | Callback function for validation.                                                                        | `() => {}`    |
| `disabledValidation`      | If `true`, disables validation.                                                                         | `false`       |
| `allowEmpty`              | If `true`, allows the input field to be empty.                                                           | `false`       |
| `autoValue`               | Automatic value to set.                                                                                  | -   |

### Usage Example

```jsx
    <UnitInput
      name="weight"
      placeholder="Enter weight"
      value="10"
      unitkey="kg"
      initUnit="kg"
      onChange={handleInputChange}
      onFocus={handleInputFocus}
      onSwitchUnit={handleSwitchUnit}
      testId="weight-input"
      error="Invalid weight"
      warning="Low weight"
      predefinedOptions={[
        { value: 'kg', label: 'Kilogram' },
        { value: 'g', label: 'Gram' },
        { value: 'lb', label: 'Pound' },
      ]}
      initialPredefinedOption={true}
      shouldLinkAutomaticly={true}
      selectedPredefinedOptionKey="kg"
      validationCallback={handleValidation}
      disabledValidation={false}
      allowEmpty={false}
      autoValue="20"
    />
```
