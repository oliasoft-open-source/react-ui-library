import { defineConfig } from 'vite';
import tsconfigPaths from 'vite-tsconfig-paths';
import dts from 'vite-plugin-dts';
import react from '@vitejs/plugin-react';
import { resolve } from 'path';
import peerDeepExternal from 'rollup-plugin-peer-deps-external';

// https://vitejs.dev/config/

export default defineConfig({
  base: '/',
  root: process.cwd(),
  define: {
    globalThis: 'globalThis',
  },
  assetsInclude: ['/sb-preview/runtime.js'],
  plugins: [
    tsconfigPaths(),
    react(),
    dts({
      rollupTypes: true,
      insertTypesEntry: true,
    }),
  ],
  build: {
    outDir: 'dist',
    minify: false,
    sourcemap: true,
    emptyOutDir: true,
    copyPublicDir: false,
    lib: {
      entry: resolve(__dirname, 'src/index.ts'),
      formats: ['es'],
      fileName: 'index',
    },
    rollupOptions: {
      plugins: peerDeepExternal(),
      output: {
        assetFileNames: 'global.css',
      },
    },
  },
  resolve: {
    alias: {
      components: resolve(__dirname, './src/components'),
      helpers: resolve(__dirname, './src/helpers'),
      hooks: resolve(__dirname, './src/hooks'),
      style: resolve(__dirname, './src/style'),
      typings: resolve(__dirname, './src/typings'),
      docs: resolve(__dirname, './src/docs'),
    },
    conditions: ['development', 'production'],
  },
  css: {
    preprocessorOptions: {
      sass: {
        api: 'modern',
      },
    },
  },
});
